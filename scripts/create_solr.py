from pymongo import MongoClient
from solrcloudpy import *

# Create Solr data from the group data in mongodb.
# Warning: deletes and recreates entire solr index data

client = MongoClient('mongodb://tring:0n10n@192.168.10.6:30000/tring?authSource=tring')
db=client.tring
groups=db.groups
handles=db.handles
gidMbrCountMap={}
for h in handles.find():
    print h
for h in handles.find():
    if h['status'] == 0:
        if h['parentGroupMember']['gid'] not in gidMbrCountMap:
            gidMbrCountMap[h['parentGroupMember']['gid']] =  1
        else:
            gidMbrCountMap[h['parentGroupMember']['gid']] = gidMbrCountMap[h['parentGroupMember']['gid']] + 1
conn = SolrConnection(["192.168.10.5:8983", "192.168.10.4:8983","192.168.10.7:8983"], version="5.5.1")
#conn = SolrConnection(["localhost:8983"], version="4.7.2")

lst = []
for g in groups.find():
    if 'pgid' in g:
        continue
    if 'closed' in g and g['closed']:
        continue 
    grp = {'groupname': g['name'], 'id': g['_id'], 'membercount':gidMbrCountMap[g['_id']], 
    'categories': g['categories'], 'o2m': g['o2m']}
    if 'description' in g:
        grp['description'] = g['description']
    if 'country' in g:
        grp['country'] = g['country']
    if 'rules' in g:
        grp['rules'] = g['rules']
    if 'state' in g:
        grp['state'] = g['state']
    if 'city' in g:
        grp['city'] = g['city']
    lst.append(grp)
    print grp
conn.collection1.add(lst)
