import psutil
import sys
from smtplib import SMTP
from socket import *
from email.mime.text import MIMEText
import datetime

class NoSolr:
    """"NoSolr"""
    pass
class NoHivemq:
    """NoHivemq"""
    pass
class NoZk:
    """NoZk"""
    pass
class NoVertx:
    """NoVertx"""
    pass
class NoMongod:
    """NoMongod"""
    pass

hostname = gethostname()

def checkvertx(jpids):
    for pid in jpids:
        try:
            proc=psutil.Process(pid)
            cmdline=proc.as_dict(['cmdline'])['cmdline']
            str=" ".join(cmdline)
            if 'vertx' in str:
                return pid
        except:
            pass
    raise NoVertx()
    
def checksolr(jpids):
    for pid in jpids:
        try:
            proc=psutil.Process(pid)
            cmdline=proc.as_dict(['cmdline'])['cmdline']
            str=" ".join(cmdline)
            if 'solr' in str:
                return pid
        except:
            pass
    raise NoSolr()
    
def checkzk(jpids):
    for pid in jpids:
        try:
            proc=psutil.Process(pid)
            cmdline=proc.as_dict(['cmdline'])['cmdline']
            str=" ".join(cmdline)
            if 'zookeeper' in str:
                return pid
        except:
            pass
    raise NoZk()
            
def checkhivemq(jpids):
    for pid in jpids:
        try:
            proc=psutil.Process(pid)
            cmdline=proc.as_dict(['cmdline'])['cmdline']
            str=" ".join(cmdline)
            if 'hivemq' in str:
                return pid
        except:
            pass
    raise NoHivemq()

def checkmongod():
    for proc in psutil.process_iter():
        procname = proc.as_dict(['name'])['name']
        if procname != 'mongod':
            continue
        cmdline=proc.as_dict(['cmdline'])['cmdline']
        str=" ".join(cmdline)
        if 'mongod' in str:
            return proc.as_dict(['pid'])['pid']
    raise NoMongod()

def sendmail(emailtext, errorflag):
    global hostname
    me = hostname +'@ctrls'
    msg= MIMEText(emailtext)

    tolist = ['sachinautomatic@gmail.com', 'jchaganti@gmail.com', 'vipin.pr@gmail.com', 'murus.rns@gmail.com', 'arun@manifolde.com', 'mr.arun.uday@gmail.com']
    errortxt=''
    if errorflag:
        errortxt = 'ERROR!'
    msg['Subject'] = errortxt + ' Status of servers on %s (%s)' % (hostname, gethostbyname(hostname))
    msg['From'] = me
    msg['To'] = ';'.join(tolist)
    s = SMTP('localhost')
    s.sendmail(me, tolist, msg.as_string())
    s.quit()
    
def get_pinfo(name,pid):
    try:
        proc=psutil.Process(pid)
        procdict=proc.as_dict(['pid','cmdline','create_time','status','cpu_percent','memory_percent','num_fds'])
        return """
        %s
        pid: %d
        cmdline: %s
        created: %s
        status: %s
        cpu_percent: %f
        memory_percent: %f
        num_fds: %d
        -------------------------------------------------
        """ % (name,procdict['pid'],' '.join(procdict['cmdline']),datetime.datetime.fromtimestamp(procdict['create_time']).strftime("%d-%m-%Y %H:%M:%S"),procdict['status'],procdict['cpu_percent'],procdict['memory_percent'],procdict['num_fds'] or 0)      
    except:
        pass
    
def sysinfo():
    global hostname
    boottime=datetime.datetime.fromtimestamp(psutil.boot_time()).strftime("%d-%m-%Y %H:%M:%S")
    virtmem = psutil.virtual_memory()
    diskio= psutil.disk_io_counters()
    netio = psutil.net_io_counters()
    cputimes = psutil.cpu_times()
    currenttime = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    du=psutil.disk_usage('/')
    return """
    Host: %s %s report at time %s
    -------------------------------------------------
    Last boot time: %s
    CPU times: %s
    Memory: Available: %ld, Used: %ld
    Disk Usage ('/'): %s
    Disk IO Counters: %s
    Net IO counters: %s
    ===================================================================================
    """ %(hostname, gethostbyname(hostname), currenttime,boottime, str(cputimes), virtmem.available,virtmem.used,str(du), str(diskio),str(netio))
    
    
if __name__=="__main__":
 args=sys.argv
 argc = len(sys.argv)
 if argc < 2:
  print "Error: usage - python processhealth.py [vertx] [hivemq] [solr] [mongod]"
  sys.exit()
 vertxerror=False
 solrerror=False
 mongoerror=False
 hivemqerror=False
 zkerror=False

 vertxpinfo=""
 solrpinfo=""
 mongopinfo=""
 hivemqpinfo=""
 zkpinfo=""

 errorflag = False
 infoflag = False

 javapids = [proc.as_dict(['pid'])['pid'] for proc in psutil.process_iter() if proc.as_dict(['name'])['name']=='java']
 
 for arg in args[1:]:
    if arg=='vertx':
        try:
            pid=checkvertx(javapids)
            vertxpinfo=get_pinfo('vertx',pid)
        except NoVertx:
            vertxerror=True
            pass
    elif arg=='solr':
        try:
            zkpid=checkzk(javapids)
            zkpinfo=get_pinfo('zookeeper',zkpid)
        except NoZk:
            zkerror=True
            pass
        try:
            solrpid=checksolr(javapids)
            solrpinfo=get_pinfo('solr',solrpid)
        except NoSolr:
            solrerror=True
            pass
    elif arg=='mongod':
        try:
            pid=checkmongod()
            mongopinfo=get_pinfo('mongod',pid)
        except:
            mongoerror=True
            pass
    elif arg=='hivemq':
        try:
            pid=checkhivemq(javapids)
            hivemqpinfo=get_pinfo('hivemq',pid)
        except NoHivemq:
            hivemqerror=True
            pass
    elif arg=='info':
        infoflag=True

if vertxerror:
    vertxpinfo = "      Vertx not running on host %s\n      -------------------------------------------------" % hostname
    errorflag=True
if zkerror:
    zkpinfo = "     Zookeeper not running on host %s\n      -------------------------------------------------" % hostname
    errorflag=True
if solrerror:
    solrpinfo = "       Solr not running on host %s\n       -------------------------------------------------" % hostname
    errorflag=True
if mongoerror:
    mongopinfo = "      Mongod not running on host %s\n     -------------------------------------------------" % hostname
    errorflag=True
if hivemqerror:
    hivemqpinfo = "     Hivemq not running on host %s\n     -------------------------------------------------" % hostname
    errorflag=True

allText = sysinfo()+'\n'+ vertxpinfo +'\n' + zkpinfo +'\n'+ solrpinfo +'\n' +mongopinfo +'\n' + hivemqpinfo +'\n'
print 'allText is :-'
print allText
if errorflag or infoflag:
    sendmail(allText, errorflag)
    