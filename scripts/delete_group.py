from pymongo import MongoClient
from solrcloudpy import *
import sys

client = MongoClient('mongodb://tring:0n10n@192.168.10.6:30000/tring?authSource=tring')
db=client.tring
groups=db.groups
handles=db.handles
if __name__ == "__main__":
  argc = len(sys.argv)
  if argc < 2:
    print 'Error: At least one argument for group name is required'
    print 'Usage: python delete_group.py <groupname>'
    print 'deletes group info from mongodb and solr'
    exit()
  argv=sys.argv
  gname = argv[1]
  grp= groups.find_one({"name":gname})
  hdls = handles.find({'parentGroupMember.gid': grp['_id']}) 
  print grp
  for h in hdls:
    print h
    handles.remove(h['_id'])
  groups.remove(grp['_id'])

  
  conn = SolrConnection(["192.168.10.5:8983", "192.168.10.4:8983","192.168.10.7:8983"], version="5.5.1")
  #conn = SolrConnection(["localhost:8983"], version="4.7.2")

  conn.collection1.delete(query={'q':'groupname:'+ gname})
