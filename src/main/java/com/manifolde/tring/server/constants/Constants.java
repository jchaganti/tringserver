package com.manifolde.tring.server.constants;

/**
 *
 * @author amitvalse
 */
public class Constants {

    public static String SERVER_ADDRESS = "http://localhost:8983/solr";
    
    public static String NOTIFICATION_ICON = "http://mulkindia.in/resources/images/tring.png";
    
    public static final String REGEX_WHITE_SPACE = "\\s";
}
