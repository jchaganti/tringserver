package com.manifolde.tring.server.sms;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.net.HttpHeaders;
import com.manifolde.tring.server.sms.MessageBuilder.MESSAGE.ADDRESS;
import com.manifolde.tring.server.sms.MessageBuilder.MESSAGE.SMS;
import com.manifolde.tring.server.sms.MessageBuilder.MESSAGE.USER;
import com.manifolde.tring.server.utils.Utils;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;


public class MessageBuilder {

  private static final Logger log = LoggerFactory.getLogger(MessageBuilder.class);
  private static XmlMapper xmlMapper;
  private static final String USERNAME = "manifoldexml1";
  private static final String PASSWORD = "mani1234";
  private static final String SEQ = "1"; 
  private static final String VER = "1.2"; 
  private static final String FROM = "tringy";
  private static final String SMS_TEXT = "Your Tring PIN is %d";
  
  
  static {
    JacksonXmlModule module = new JacksonXmlModule();
    module.setDefaultUseWrapper(false);
    xmlMapper = new XmlMapper(module);
  }  
  
  @JacksonXmlRootElement
  public static class MESSAGE {

    @JacksonXmlProperty(isAttribute = true)
    private String VER;
    
    @JacksonXmlProperty
    private USER USER;
    
    @JacksonXmlProperty
    private SMS SMS;

    public MESSAGE() {
      super();
    }

    public MESSAGE(String vER, USER uSER, SMS sMS) {
      super();
      VER = vER;
      USER = uSER;
      SMS = sMS;
    }

    public static class USER {

      @JacksonXmlProperty(isAttribute = true)
      private String USERNAME;

      @JacksonXmlProperty(isAttribute = true)
      private String PASSWORD;

      public USER() {
        super();
      }

      public USER(String uSERNAME, String pASSWORD) {
        super();
        USERNAME = uSERNAME;
        PASSWORD = pASSWORD;
      }
    }
    public static class ADDRESS {
      @JacksonXmlProperty(isAttribute = true)
      private String FROM;

      @JacksonXmlProperty(isAttribute = true)
      private String TO;

      @JacksonXmlProperty(isAttribute = true)
      private String SEQ;

      @JacksonXmlProperty(isAttribute = true)
      private String TAG;

      public ADDRESS() {
        super();
      }

      public ADDRESS(String fROM, String tO, String sEQ) {
        super();
        FROM = fROM;
        TO = tO;
        SEQ = sEQ;
      }
    }


    public static class SMS {
      @JacksonXmlProperty(isAttribute = true)
      private String UDH;

      @JacksonXmlProperty(isAttribute = true)
      private String CODING;

      @JacksonXmlProperty(isAttribute = true)
      private String TEXT;

      @JacksonXmlProperty(isAttribute = true)
      private String PROPERTY;

      @JacksonXmlProperty(isAttribute = true)
      private String ID;

      @JacksonXmlProperty
      private ADDRESS ADDRESS;
      
      public SMS() {
        super();
      }

      public SMS(String uDH, String cODING, String tEXT, String pROPERTY, String iD, ADDRESS aDDRESS) {
        super();
        UDH = uDH;
        CODING = cODING;
        TEXT = tEXT;
        PROPERTY = pROPERTY;
        ID = iD;
        ADDRESS = aDDRESS;
      }
    }
  }

  public static String getSMSMessage(String mobileNo, int pin) {
    String xml = "";
    USER user = new USER(USERNAME, PASSWORD);
    ADDRESS address = new ADDRESS(FROM, mobileNo, SEQ);
    SMS sms = new SMS("0", "1", String.format(SMS_TEXT, pin), "0",  Utils.uuidToBase64(UUID.randomUUID()), address);
    MESSAGE message = new MESSAGE(VER, user, sms);
    try {
      xml = xmlMapper.writeValueAsString(message);
      System.out.println("The XML is: " + xml);
      return URLEncoder.encode(xml, "UTF-8");

    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return xml;
  }

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    
    HttpClientOptions options = new HttpClientOptions();
    options.setDefaultHost("http://api.myvaluefirst.com");
    HttpClient httpClient = vertx.createHttpClient();
    String smsMsg = MessageBuilder.getSMSMessage("+919890128847".substring(1), 1735);
    
    String data ="data=" + smsMsg +"&action=send";
    System.out.println("Data: " + smsMsg);
    //httpClient.postAbs("http://api.myvaluefirst.com/psms/servlet/psms.Eservice2").exceptionHandler(t -> {
    httpClient.post(80, "api.myvaluefirst.com", "/psms/servlet/psms.Eservice2").exceptionHandler(t -> {
      log.error("Exception occured");
      t.printStackTrace();
    }).handler(response -> {
      System.out.println("statusCode from server is: " + response.statusCode());
      System.out.println("statusMessage from server is: " + response.statusMessage());
      response.bodyHandler(body -> {
        System.out.println(body.toString("ISO-8859-1"));
      });
    }).putHeader(HttpHeaders.CONTENT_LENGTH, data.length() + "").putHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded").write(data).end();

  }
}
