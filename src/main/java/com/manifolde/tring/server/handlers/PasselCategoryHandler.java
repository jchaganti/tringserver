package com.manifolde.tring.server.handlers;

import com.manifolde.tring.server.dao.PasselCategoryDAO;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
//import io.vertx.rxjava.core.http.HttpClient;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;

public class PasselCategoryHandler extends TringBaseHandler {

    private static final Logger log = LoggerFactory.getLogger(TringUserContextHandler.class);

    private PasselCategoryDAO passelCategoryDAO;

    //private HttpClient httpClient;

    public PasselCategoryHandler(Vertx vertx, PasselCategoryDAO categoryDAO) {
        super(vertx);
        this.passelCategoryDAO = categoryDAO;
    }

}
