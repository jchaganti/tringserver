package com.manifolde.tring.server.handlers;


import com.manifolde.tring.server.utils.ClusteredObjectStore;
import com.manifolde.tring.server.utils.TringConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;

import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.HandleDAO.HandleStatus;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpClient;
import io.vertx.rxjava.core.http.HttpClientRequest;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;

public class PasselGroupHandler extends TringBaseHandler {

    private static final Logger log = LoggerFactory.getLogger(PasselGroupHandler.class);

    private static final long SERENDIPITY_GROUPS_THRESHOLD = 0;

    private static final int PAGE_SIZE = 10;

    private GroupDAO passelGroupDAO;

    private HttpClient httpClient;

    private String SOLR_HOST;

    private int SOLR_PORT;

    public PasselGroupHandler(Vertx vertx, GroupDAO passelGroupDAO) {
        super(vertx);
        this.passelGroupDAO = passelGroupDAO;
        this.httpClient = vertx.createHttpClient();
        ClusteredObjectStore propertiesMap = new ClusteredObjectStore(vertx, TringConstants.TRING_PROPERTIES_MAP);
        propertiesMap.get(TringConstants.SERVER_PROPS).subscribe(_props -> {         
          JsonObject props = (JsonObject) _props;
          String host = props.getString("tring.server.solr.host");
          int port = Integer.valueOf((String) props.getString("tring.server.solr.port","8983"));
          this.SOLR_HOST = host;
          this.SOLR_PORT = port;
          log.debug("Solr host is " + host);
          log.debug("Solr port is "+port);
        });
    }
    
    public void getGroupsNextCircular(RoutingContext ctx){
      int offset, rows;
      offset = Integer.parseInt(ctx.request().getParam("offset"));
      rows = Integer.parseInt(ctx.request().getParam("rows"));
      JsonObject options = 
          new JsonObject()
          .put("out", 
              new JsonObject().put("inline",1));
      Observable<JsonObject> groups = passelGroupDAO.mapReduceForHandles("function(){ "
          + "if(this."+HandleDAO.STATUS+"==="+HandleStatus.ACTIVE.ordinal()+"){"
          + "emit(this."+HandleDAO.PARENT_GROUP_MEMBER+"."+HandleDAO.GID+",1);}"
          + "}",
      "function(k,varr){ "
          + "return {'gid': k, 'count': varr.reduce(function(a, b) { "
          + "if (isNaN(a)) {return b;} "
          + "else {return a + b;} "
          + "}"
          + ", 0)};"
          + "}", options)
      .flatMap(obj->{
        log.debug("mapReduce result is " + obj);
        return Observable.from(obj.getJsonArray("results"));
      })
      .flatMap(el->{
        Object valobj =((JsonObject)el).getValue("value");
        if (valobj instanceof JsonObject) {
          valobj = ((JsonObject) valobj).getDouble("count");
        }
        double val = (Double)valobj;
        long cnt = Math.round(val);
        if (cnt > SERENDIPITY_GROUPS_THRESHOLD) {
        ((JsonObject)el).put("count", cnt).remove("value");
        String gid = ((JsonObject)el).getString(HandleDAO.ID); //Note that it comes as _id in the results
        return passelGroupDAO.groupFromId(gid).map(obj->{
          if (obj != null && !obj.getBoolean(GroupDAO.CLOSED_GROUP)) {
            obj.remove(GroupDAO.THUMBNAIL_DATA); //which takes up a lot of bytes. We don't show any image for now
            obj.put("count", cnt);
            return obj;
          } else {
            return null;
          }
        });
        } else {
          return null;
        }
      })
      .filter(el->{
        return el != null;
      });
      groups.toList().subscribe(grps->{
        int siz = grps.size();
        List<JsonObject> lst = null;
        int off=0;
        if (siz > rows) {
          int from=offset;
          int to,to_more=0;
          if ((from+rows) <= siz) {
            to =  from+rows;
          } else {
            to = siz;
            to_more = rows-(siz-from);
          }     
          lst = grps.subList(from,to);
          lst.addAll(grps.subList(0,to_more));
          off = (to_more==0?to:to_more);
        } else {
          lst =grps;
        }
        log.debug("new offset is "+off);
        ctx.response().end(new JsonObject().put("offset", off).put("groups", new JsonArray(lst)).encode());
      },err->{
        log.error(err);
          ctx.fail(400);
        });
    }
    
    public void getSerendipitousGroups(RoutingContext ctx) {
      int rows;
      rows = Integer.parseInt(ctx.request().getParam("rows"));
      JsonObject options = 
          new JsonObject()
          .put("out", 
              new JsonObject().put("inline",1));
      Observable<JsonObject> groups = passelGroupDAO.mapReduceForHandles("function(){ "
          + "if(this."+HandleDAO.STATUS+"==="+HandleStatus.ACTIVE.ordinal()+"){"
          + "emit(this."+HandleDAO.PARENT_GROUP_MEMBER+"."+HandleDAO.GID+",1);}"
          + "}",
      "function(k,varr){ "
          + "return {'gid': k, 'count': varr.reduce(function(a, b) { "
          + "if (isNaN(a)) {return b;} "
          + "else {return a + b;} "
          + "}"
          + ", 0)};"
          + "}", options)
      .flatMap(obj->{
        log.debug("mapReduce result is " + obj);
        return Observable.from(obj.getJsonArray("results"));
      })
      .flatMap(el->{
        Object valobj =((JsonObject)el).getValue("value");
        if (valobj instanceof JsonObject) {
          valobj = ((JsonObject) valobj).getDouble("count");
        }
        double val = (Double)valobj;
        long cnt = Math.round(val);
        log.debug("getSerendipitousGroups - cnt size is "+cnt);

        if (cnt > SERENDIPITY_GROUPS_THRESHOLD) {
          ((JsonObject)el).put("count", cnt).remove("value");
          String gid = ((JsonObject)el).getString(HandleDAO.ID); //Note that it comes as _id in the results
          return passelGroupDAO.groupFromId(gid).map(obj->{
            if (obj != null && !obj.getBoolean(GroupDAO.CLOSED_GROUP)) {
              obj.remove(GroupDAO.THUMBNAIL_DATA); //which takes up a lot of bytes. We don't show any image for now
              obj.put("count", cnt);
              return obj;
            } else {
              return null;
            }
          });
        } else {
          return null;
        }
      })
      .filter(el->{
        return el != null;
      });
      groups.toList().subscribe(grps->{
        int siz = grps.size();
        log.debug("getSerendipitousGroups - grps size is "+siz);
        List<JsonObject> lst = null;
        int off=0;
        if (siz > rows) {
          int from=(int)Math.round((siz-1)* Math.random());
          int to,to_more=0;
          if ((from+rows) <= siz) {
            to =  from+rows;
          } else {
            to = siz;
            to_more = rows-(siz-from);
          }     
          lst = grps.subList(from,to);
          lst.addAll(grps.subList(0,to_more));

          log.debug("getSerendipitousGroups: result lst size is "+lst.size());
          off = (to_more==0?to:to_more);
        } else {
          lst =grps;
        }
        log.debug("new offset is "+off);
        ctx.response().end(new JsonObject().put("offset", off)
            .put("groups", new JsonArray(lst))
            .put("size", siz).encode());
      },err->{
        log.error(err);
        ctx.fail(400);
      });
    }
    
    public void getCategories(RoutingContext ctx){
      int offset, rows;
      offset = Integer.parseInt(ctx.request().getParam("offset"));
      rows = Integer.parseInt(ctx.request().getParam("rows"));
      JsonObject options = 
          new JsonObject()
          .put("out", 
              new JsonObject().put("inline",1));
      passelGroupDAO.mapReduceForGroups("function(){ "
          + "if(!this.closed && this.categories){"
          + "this.categories.forEach(function(t){"
          + "emit(t,1);});"
          + "}"
          + "}",
      "function(k,varr){ "
          + "return {'tag': k, 'count': varr.reduce(function(a, b) { "
          + "if (isNaN(a)) {return b;} "
          + "else {return a + b;} "
          + "}"
          + ", 0)};"
          + "}", options)
      .flatMap(obj->{
        log.debug("mapReduce result is " + obj);
        return Observable.from(obj.getJsonArray("results"));
      })
      .flatMap(el->{
        Object valobj =((JsonObject)el).getValue("value");
        if (valobj instanceof JsonObject) {
          valobj = ((JsonObject) valobj).getDouble("count");
        }
        double val = (Double)valobj;
        ((JsonObject)el).put("count",Math.round(val)).remove("value");
        return Observable.just(el);
      })
      .toSortedList((o1,o2)->{
        Long other= ((JsonObject)o2).getLong("count");
        Long self = ((JsonObject)o1).getLong("count");
        return self < other?1:(self > other? -1:0);
      })
      .skip(offset)
      .limit(rows)
      .subscribe(objs->{       
        ctx.response().end(new JsonArray(objs).encode());
      }, err->{
        log.error("ERROR in getCategories err is " + err);
        ctx.fail(405);
      });
    }

    public void indexGroup(RoutingContext ctx) {
      JsonObject group = (JsonObject)ctx.get("group");
      if (group == null) {
        log.debug("Group is null in indexGroup");
        ctx.fail(401);
        return;
      }
      JsonObject grp = new JsonObject().put("groupname", group.getString(GroupDAO.GROUP_NAME));
      Object categories = group.getValue(GroupDAO.CATEGORIES);
      if (categories != null) {
        grp.put("categories", categories);
      }
      grp.put("id", group.getValue(GroupDAO.ID));
      
      Object desc= group.getValue(GroupDAO.GROUP_DESCRIPTION);
      if (desc !=null) {
        grp.put("description", desc);
      }
      Object rules = group.getValue(GroupDAO.GROUP_RULES);
      if (rules !=null) {
        grp.put("rules", rules);
      }
      Object country=group.getValue(GroupDAO.GROUP_COUNTRY);
      if (country !=null) {
        grp.put("country", country);
      }
      Object state = group.getValue(GroupDAO.GROUP_STATE);
      if (state !=null) {
        grp.put("state", state);
      }
      Object city = group.getValue(GroupDAO.GROUP_CITY);
      if (city !=null) {
        grp.put("city", city);
      }
      Object o2m = group.getValue(GroupDAO.GROUP_O2M);
      if (o2m !=null) {
        grp.put("o2m", o2m);
      }     
      String grpStr = grp.encode();
      String grpStrForSolr = new JsonArray().add(grp).encode();
      log.debug("Grpstr sent to Solr is "+grpStr);
      if(Utils.isDevMode() && SOLR_HOST == null) {
      //if(false) {
        JsonObject partResponse = (JsonObject)ctx.get("partialResponse");
        if (partResponse == null) {
          partResponse = new JsonObject();
        }
        ctx.response().end(partResponse.put("success", true).encode());
      }
      else {
        HttpClientRequest request = httpClient
            .request(HttpMethod.POST, SOLR_PORT, SOLR_HOST, "/solr/collection1/update?debug=query")
            .putHeader("Content-type","application/json") .handler(resp->{
              
            })
            .putHeader("Content-Length", Integer.toString(grpStrForSolr.length()))
            .write(grpStrForSolr);
        request.toObservable().subscribe(
            response -> {
              // Process the response
              log.debug(response);
              JsonObject partResponse = (JsonObject)ctx.get("partialResponse");
              if (partResponse == null) {
                partResponse = new JsonObject();
              }
              ctx.response().end(partResponse.put("success", true).encode());
              if (ctx.get("init") != null || ctx.get("increment") != null) {
                ctx.next();
              }
            },
            error -> {
              // Could not connect
              ctx.fail(402);
            }
        );
        request.end();
      }
    }

    public void getRandomGroups(RoutingContext ctx) {
      String offset;
      int rows;
      offset = ctx.request().getParam("offset");
      String rowsStr = ctx.request().getParam("rows");
      rows = rowsStr != null ? Integer.parseInt(rowsStr):10;
      
      String query = "membercount:["+SERENDIPITY_GROUPS_THRESHOLD+"+TO+*]";
      String cursorMark = offset == null ? "*":offset;
      String finalQuery = "/solr/collection1/select?debug=query&q="+query+"&cursorMark="+cursorMark+"&wt=json&sort=groupname+asc&indent=true&fl=*&rows="+rows;
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              JsonObject jobj=obj.getJsonObject("response");
              String nextCursorMark = obj.getString("nextCursorMark");
              if ( nextCursorMark != null) {
                try {
                  nextCursorMark = URLEncoder.encode(nextCursorMark,"UTF-8");
                } catch (Exception e) {
                  // TODO Auto-generated catch block
                  log.error(e.getStackTrace());
                }
              }
              final String offsetMark = nextCursorMark;
              int siz = jobj.getInteger("numFound");
              Observable.from(jobj.getJsonArray("docs")).map(grp->{
                JsonObject jobject = (JsonObject)grp;
                return new JsonObject().put(GroupDAO.GROUP_NAME, jobject.getString("groupname"))
                    .put("count", jobject.getInteger("membercount"))
                    .put(GroupDAO.GROUP_O2M, jobject.getBoolean("o2m"))
                    .put(GroupDAO.CATEGORIES, jobject.getJsonArray("categories"))
                    .put(GroupDAO.GROUP_DESCRIPTION, jobject.getString("description"))
                    .put(GroupDAO.GROUP_RULES, jobject.getString("rules"));
              })
              .toList()
              .subscribe(grps ->{
                Collections.shuffle(grps);
                JsonObject retVal =
                    new JsonObject().put("offset", offsetMark)
                    .put("groups", grps)
                    .put("size", siz);
                ctx.response().end(retVal.encode());
              },err->{
                log.error(err);
                ctx.fail(401);
              });        
            });
          },
          error -> {
            // Could not connect
            log.error(error);
            ctx.fail(402);
          }
          );
      request.end();
    }
    
    
    public void pagedSearchGroupsByKeyword(RoutingContext ctx) {
      String keyword = ctx.getBodyAsJson().getJsonObject("map").getString("keyword");
      if (keyword == null || keyword.isEmpty()) {
        ctx.fail(403);
        return;
      }
      int rows;
      rows = Integer.parseInt(ctx.request().getParam("rows"));
      String offset = ctx.request().getParam("offset");

      String cursorMark = offset == null ? "*":offset;

      String finalQuery = null;
      try {
        finalQuery = "/solr/collection1/select?debug=query&q=text_en:("+URLEncoder.encode(keyword,"UTF-8")+")"+ "&cursorMark="+cursorMark+"&sort=groupname+asc&wt=json&indent=true&fl=*&rows="+rows;
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              String nextCursorMark = obj.getString("nextCursorMark");
              if ( nextCursorMark != null) {
                try {
                  nextCursorMark = URLEncoder.encode(nextCursorMark,"UTF-8");
                } catch (Exception e) {
                  log.error(e.getStackTrace());
                }
              }
              final String offsetMark = nextCursorMark;
              JsonObject jobj=obj.getJsonObject("response");
              int siz = jobj.getInteger("numFound");
              Observable.from(jobj.getJsonArray("docs")).flatMap(grp->{
                JsonObject jobject = (JsonObject)grp;
                final JsonObject resultGrpObj=new JsonObject().put(GroupDAO.GROUP_NAME, jobject.getString("groupname"))
                    .put("count", jobject.getInteger("membercount"))
                    .put(GroupDAO.GROUP_O2M, jobject.getBoolean("o2m"))
                    .put(GroupDAO.CATEGORIES, jobject.getJsonArray("categories"))
                    .put(GroupDAO.GROUP_DESCRIPTION, jobject.getString("description"))
                    .put(GroupDAO.GROUP_RULES, jobject.getString("rules"));
                return passelGroupDAO.getThumbnalData( jobject.getString("groupname")).map(gptemp->{
                  if (gptemp != null) {
                   resultGrpObj.put(GroupDAO.THUMBNAIL_DATA, gptemp.getString(GroupDAO.THUMBNAIL_DATA));
                  }
                  return resultGrpObj;
                });
              })
              .toList()
              .subscribe(grps ->{
                JsonObject retVal =
                    new JsonObject().put("offset", offsetMark)
                    .put("groups", grps)
                    .put("size", siz);
                ctx.response().end(retVal.encode());
                log.debug(retVal);
              },err->{
                log.error(err);
                ctx.fail(401);
              }); 
            });
          },
          error -> {
            // Could not connect
            ctx.fail(402);
          }
          );
      request.end();
    }

    public void searchGroupsByKeyword(RoutingContext ctx) {
      String keyword = ctx.getBodyAsJson().getJsonObject("map").getString("keyword");
      if (keyword == null || keyword.isEmpty()) {
        ctx.fail(403);
        return;
      }
      String finalQuery = null;
      try {
        finalQuery = "/solr/collection1/select?debug=query&q=text_en:("+URLEncoder.encode(keyword,"UTF-8")+")&wt=json&indent=true&fl=*&rows=20000";
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              ctx.response()
              .end(obj.getJsonObject("response").getJsonArray("docs").encode());
            });
          },
          error -> {
            // Could not connect
            ctx.fail(402);
          }
          );
      request.end();
    }

    public void filterGroupsByCategory(RoutingContext ctx) {
      JsonObject map = ctx.getBodyAsJson().getJsonObject("map");
      String keyword = map.getString("keyword");
      String category = map.getString("category");
      if (keyword == null || keyword.isEmpty()) {
        ctx.fail(403);
        return;
      }
      if (category == null || category.isEmpty()) {
        ctx.fail(403);
        return;
      }
      String finalQuery=null;
      try {
        finalQuery = "/solr/collection1/select?debug=query&q=text_en:("+URLEncoder.encode(keyword,"UTF-8")+")&fq=categories:("+URLEncoder.encode(category,"UTF-8")+")&wt=json&indent=true&fl=*&rows=20000";
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              ctx.response()
              .end(obj.getJsonObject("response").getJsonArray("docs").encode());
            });
          },
          error -> {
            // Could not connect
            ctx.fail(402);
          }
          );
      request.end();
    }

    public void filterGroupsByLocation(RoutingContext ctx) {
      JsonObject map = ctx.getBodyAsJson().getJsonObject("map");
      String keyword = map.getString("keyword");

      String country = map.getString("country");
      String state = map.getString("state");
      String city = map.getString("city");
      if (keyword == null || keyword.isEmpty() || 
          country == null || country.isEmpty()) {
        ctx.fail(403);
        return;
      }
      String locationQuery=null;
      String finalQuery =null;
      try {
        locationQuery = getLocationQuery(country,state,city);
        finalQuery = "/solr/collection1/select?debug=query&q=text_en:("+URLEncoder.encode(keyword,"UTF-8")+")&fq="+locationQuery+"&wt=json&indent=true&fl=*&rows=20000";

      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }        

      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json")
          .handler(resp->{

          });
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              ctx.response()
              .end(obj.getJsonObject("response").getJsonArray("docs").encode());
            });
          },
          error -> {
            // Could not connect
            ctx.fail(402);
          }
          );
      request.end();
    }

    private String getLocationQuery(String country, String state, String city) throws UnsupportedEncodingException {
      String query = null, stateQuery="", cityQuery="";
      if (country != null) {
        query ="country:("+ URLEncoder.encode(country,"UTF-8") +")";
        if (state != null) {
          stateQuery = "%20AND%20state:(" + URLEncoder.encode(state,"UTF-8")+")";
        }
        if (city != null) {
          cityQuery = "%20AND%20city:("+URLEncoder.encode(city,"UTF-8")+")";
        }
        query = query + stateQuery + cityQuery;
      }
      return query;
    }

    public void browseGroupsByCategory(RoutingContext ctx){
      JsonObject map = ctx.getBodyAsJson().getJsonObject("map");
      String category=map.getString("category");
      if (category == null || category.isEmpty()) {
        ctx.fail(403);
        return;
      }
      String finalQuery=null;
      try {
        finalQuery = "/solr/collection1/select?debug=query&q=categories:("+URLEncoder.encode(category, "UTF-8")+")&wt=json&indent=true&fl=*&rows=20000";
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              ctx.response()
              .end(obj.getJsonObject("response").getJsonArray("docs").encode());
            });
          },
          error -> {
            // Could not connect
            log.error("browseGroupsByCategory - error 402");
            ctx.fail(402);
          }
          );
      request.end();
    }

    public void browseGroupsByLocation(RoutingContext ctx){
      JsonObject map = ctx.getBodyAsJson().getJsonObject("map");

      String country = map.getString("country");
      String state = map.getString("state");
      String city = map.getString("city");
      if (country  == null || country.isEmpty()) {
        ctx.fail(403);
        ctx.response().end(new JsonObject().put("success", false).encode());
        return;
      }
      String locationQuery=null;
      try {
        locationQuery = getLocationQuery(country,state,city);
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      String finalQuery = "/solr/collection1/select?debug=query&q="+locationQuery+"&wt=json&indent=true&fl=*&rows=20000";
      HttpClientRequest request = httpClient
          .request(HttpMethod.GET, SOLR_PORT, SOLR_HOST, finalQuery)
          .putHeader("Content-type","application/json");
      request.toObservable().subscribe(
          response -> {
            // Process the response
            response.bodyHandler(buffer->{
              JsonObject obj = new JsonObject(buffer.getString(0,buffer.length()));
              log.debug(obj);
              ctx.response()
              .end(obj.getJsonObject("response").getJsonArray("docs").encode());
            });
          },
          error -> {
            // Could not connect
            ctx.fail(402);
          }
          );
      request.end();
      //        passelGroupDAO.browseGroupsByLocation(location).subscribe(groups -> {
      //            if (null == groups) {
      //                ctx.response().end(new JsonObject().encode());
      //            } else {
      //                ctx.response().end(new JsonObject().put("groups", groups).encode());
      //            }
      //        });
    }
    
    public void changeGroupMembershipCount(RoutingContext ctx) {
      if(Utils.isDevMode() && SOLR_HOST == null) {
        return;
      }
      JsonObject group = (JsonObject)ctx.get("group");
      Boolean incrementCount = (Boolean)ctx.get("increment");
      Boolean initCount = (Boolean)ctx.get("init");
      if (group == null || (incrementCount == null && initCount == null)) {
        log.debug("group or increment is null in changeGroupMembershipCount");
        return;
      }
      String gid = group.getString(GroupDAO.ID);
      String name = group.getString(GroupDAO.GROUP_NAME);
      Observable<String> nameObs = null;
      if (name == null) {
        log.debug("changeGroupMembershipCount - name not passed, hence finding from dbase");
        nameObs = passelGroupDAO.findById(gid, new JsonObject().put(GroupDAO.GROUP_NAME, 1).put(GroupDAO.CLOSED_GROUP,1)).map(grp->{
          if (grp.getBoolean(GroupDAO.CLOSED_GROUP)) {
            log.debug("changeGroupMembershipCount called for closed group");
            return null;
          }
          else
            return grp.getString(GroupDAO.GROUP_NAME);
        });
      } else {
        nameObs = Observable.just(name);
      }
      nameObs.subscribe(gname->{
        if (gname==null) {
          return;
        }
        JsonObject updateObj;
        if (incrementCount != null) {
          if (incrementCount){
            updateObj = new JsonObject().put("membercount", new JsonObject().put("inc", 1))
                .put("groupname", gname);
          } else {
            updateObj = new JsonObject().put("membercount", new JsonObject().put("inc", -1))
                .put("groupname", gname);
          }
        } else {
          if (initCount) {
            updateObj = new JsonObject().put("membercount", new JsonObject().put("set", 1))
                .put("groupname", gname);
          } else {
            //It should never come here
            log.error("Error: changeGroupMembershipCount - can't have initCount false");
            updateObj = null;
            return;
          }
        }
        String updateObjStr = updateObj.encode();
        String updateObjStrForSolr = new JsonArray().add(updateObj).encode();
        log.debug("Grpstr sent to Solr is "+updateObjStr);
        HttpClientRequest request = httpClient
            .request(HttpMethod.POST, SOLR_PORT, SOLR_HOST, "/solr/collection1/update?debug=query")
            .putHeader("Content-type","application/json") .handler(resp->{

            })
            .putHeader("Content-Length", Integer.toString(updateObjStrForSolr.length()))
            .write(updateObjStrForSolr);
        request.toObservable().subscribe(
            response -> {
              // Process the response
              log.debug(response);
            },
            error -> {
              // Could not connect
              log.debug("Problem getting to solr. error is "+error);
            }
            );
        request.end();
      },err->{
        log.error("changeGroupMembershipCount error- "+ err);
      });
    }

    public void getFeaturedGroups(RoutingContext ctx){
      passelGroupDAO.getFeaturedGroups().subscribe(groups -> {
        if (null == groups) {
          ctx.response().end(new JsonObject().put("success", false).encode());
        } else {
         /* groups.forEach(grp->{
            grp.remove(GroupDAO.THUMBNAIL_DATA); //which takes up a lot of bytes. We don't show any image for now
          }); */      
          ctx.response().end(new JsonObject().put("success", true).put("groups", groups).encode());
        }
      });
    }
}
