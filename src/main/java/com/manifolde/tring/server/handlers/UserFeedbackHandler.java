package com.manifolde.tring.server.handlers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.google.common.base.Strings;
import com.manifolde.tring.server.dao.ClientFeedbackDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.RoutingContext;

public class UserFeedbackHandler extends TringBaseHandler {
  private static final String HEADER_AUTHORIZATION = "Authorization";

  private static final Logger log = LoggerFactory.getLogger(TringUserContextHandler.class);

  private ClientFeedbackDAO clientFeedbackDAO;
  
  public UserFeedbackHandler(Vertx vertx, ClientFeedbackDAO clientFeedbackDAO) {
    super(vertx);
    this.clientFeedbackDAO = clientFeedbackDAO;
  }
  
  
  public void storeClientFeedback(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject usr = ctx.get("user");
    String uid = null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    String feedback = ctx.request().getParam(TringWebApiServer.FEEDBACK);
    log.debug("storeClientFeedback from "+uid+" - feedback is '"+feedback+"'");
    if (!Strings.isNullOrEmpty(feedback)) {
      try {
        feedback = URLDecoder.decode(feedback, "UTF-8");
        JsonObject obj = new JsonObject().put(ClientFeedbackDAO.UID,uid).put(ClientFeedbackDAO.FEEDBACK, feedback);  
        clientFeedbackDAO.insert(obj).subscribe(id->{
          if (id != null) {
            ctx.response().end(new JsonObject().put(ClientFeedbackDAO.ID, id).put("success",true).encode());
          } else {
            ctx.fail(400);
          }
        }, err->{
          log.debug("storeClientFeedback error is "+err);
          ctx.fail(401);
        });
      } catch (UnsupportedEncodingException e) {
        ctx.fail(405);
        e.printStackTrace();
      }
    } else {
      ctx.fail(404);
    }
  }

}
