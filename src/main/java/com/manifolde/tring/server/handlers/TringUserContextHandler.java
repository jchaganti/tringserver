package com.manifolde.tring.server.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.google.common.base.Strings;
import com.google.common.net.HttpHeaders;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ISet;
import com.manifolde.tring.server.constants.Constants;
import com.manifolde.tring.server.dao.BaseUserToSyncDAO;
import com.manifolde.tring.server.dao.ContactsDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.GroupProfileChangedSyncRecordDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserToSyncAcceptRejectMembershipDAO;
import com.manifolde.tring.server.dao.UserToSyncAdminRoleDAO;
import com.manifolde.tring.server.dao.UserToSyncContactDAO;
import com.manifolde.tring.server.dao.UserToSyncModeratorMessagesDAO;
import com.manifolde.tring.server.dao.UserToSyncProfileDAO;
import com.manifolde.tring.server.dao.UserToSyncSubGroupDAO;
import com.manifolde.tring.server.sms.MessageBuilder;
import com.manifolde.tring.server.utils.ClusteredObjectStore;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.utils.async.AsyncSerialTasksTransaction;
import com.manifolde.tring.server.utils.async.MongoAsyncSerialTasksTransaction;
import com.manifolde.tring.server.verticles.TringVerticleUtils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Future;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpClient;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;

public class TringUserContextHandler extends TringBaseHandler {
  private static final String HEADER_AUTHORIZATION = "Authorization";

  private static final Logger log = LoggerFactory.getLogger(TringUserContextHandler.class);

  private UserDAO usersDAO;
  private ContactsDAO contactsDAO;
  private UserToSyncContactDAO userToSyncContactDAO;
  private UserToSyncProfileDAO userToSyncProfileDAO;
  private HandleDAO handlesDAO;
  private UserToSyncModeratorMessagesDAO userToSyncModeratorMessagesDAO;
  private UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO;
  private UserToSyncSubGroupDAO userToSyncSubGroupDAO;
  private UserToSyncAdminRoleDAO userToSyncAdminRoleDAO;
  private GroupProfileChangedSyncRecordDAO groupProfileChangedSyncRecordDAO;
  private TransactionDAO transactionDAO;
  private ClusteredObjectStore deviceToUser;
  private ClusteredObjectStore mobileToLastSeen;
  private JsonObject serverProperties;
  private HttpClient httpClient;


  public TringUserContextHandler(Vertx vertx, UserDAO usersDAO, ContactsDAO contactsDAO,
      UserToSyncContactDAO userToSyncContactDAO, UserToSyncProfileDAO userToSyncProfileDAO,
      HandleDAO handlesDAO, UserToSyncModeratorMessagesDAO userToSyncModeratorMessagesDAO,
      UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO,
      TransactionDAO transactionDAO, UserToSyncSubGroupDAO userToSyncSubGroupDAO,
      UserToSyncAdminRoleDAO userToSyncAdminRoleDAO, GroupProfileChangedSyncRecordDAO groupProfileChangedSyncRecordDAO,
      JsonObject serverProperties) {
    super(vertx);
    this.usersDAO = usersDAO;
    this.contactsDAO = contactsDAO;
    this.userToSyncContactDAO = userToSyncContactDAO;
    this.userToSyncProfileDAO = userToSyncProfileDAO;
    this.handlesDAO = handlesDAO;
    this.userToSyncModeratorMessagesDAO = userToSyncModeratorMessagesDAO;
    this.userToSyncSubGroupDAO = userToSyncSubGroupDAO;
    this.userToSyncAdminRoleDAO = userToSyncAdminRoleDAO;
    this.transactionDAO = transactionDAO;
    this.deviceToUser = new ClusteredObjectStore(vertx, TringConstants.DEVICE_TO_USER_MAP);
    this.mobileToLastSeen = new ClusteredObjectStore(vertx, TringConstants.MOBILE_TO_LAST_SEEN);
    this.serverProperties = serverProperties;
    this.userToSyncAcceptRejectMembershipDAO = userToSyncAcceptRejectMembershipDAO;
    this.groupProfileChangedSyncRecordDAO = groupProfileChangedSyncRecordDAO;
    httpClient = vertx.createHttpClient();
  }

  public void validateDevice(RoutingContext ctx) {
    _fromDeviceId(ctx, true);
  }

  public void verifyDevice(RoutingContext ctx) {
    String did = verifyArgs(ctx, true);
    Observable<JsonObject> userObservable = usersDAO.userByDeviceId(did);
    userObservable.subscribe(user -> {
      if (user != null) {
        handlePINVerification(ctx, user);
      } else {
        ctx.fail(401);
      }
    });
  }
  
  public void addPushNotificationId(RoutingContext ctx) {
    String did = verifyArgs(ctx, false);
    Observable<JsonObject> userObservable = usersDAO.userByDeviceId(did);
    userObservable.subscribe(user -> {
      if (user != null) {
        JsonObject pnInfo = ctx.getBodyAsJson();
        String pnId = pnInfo.getString("pnId");
        usersDAO.updatePushNotificationId(user.getString("_id"), pnId).subscribe(v -> {
          addToDeviceIdsMap(ctx, did, pnId);
        });
      } else {
        ctx.fail(401);
      }
    });
  }
  
  private String verifyArgs(RoutingContext ctx, boolean verifyIfPinExists) {
    String did = ctx.request().getHeader(HEADER_AUTHORIZATION);
    log.debug("Device id from Auth Header is " + did);
    if (Strings.isNullOrEmpty(did)) {
      ctx.fail(403);
    }

    if (verifyIfPinExists) {
      JsonObject deviceInfo = ctx.getBodyAsJson();
      Integer pin = deviceInfo.getInteger("pin");
      if (pin == null) {
        ctx.fail(403);
      }
    }
    return did;
  }

  private void _fromDeviceId(RoutingContext ctx, boolean onlyValidation) {
    String did = verifyArgs(ctx, false);
    // Get the user object from cache..
    Observable<Object> userObservable = deviceToUser.get(did);
    userObservable.flatMap(obj -> {
      if (obj != null) {
        return Observable.just(obj);
      } else {
        return usersDAO.userByDeviceId(did);
      }
    }).subscribe(user -> {
      JsonObject _user = (JsonObject) user;
      JsonArray devices = _user.getJsonArray("devices");
      devices.forEach(_device -> {
        String _did = ((JsonObject) _device).getString("did");
        if (_did.equals(did)) {
          Boolean isActive = ((JsonObject) _device).getBoolean("active");
          if (isActive.booleanValue()) {
            if (onlyValidation) {
              ctx.response().end(new JsonObject().put("validDevice", true).encode());
            } else {
              ctx.put("user", user);
              ctx.next();
            }
          } else {
            log.debug("Found de-activated device with id: " + did);
            ctx.response().end(new JsonObject().put("validDevice", false).encode());
          }
          return;
        }
      });

    } , err -> {
      err.printStackTrace();
      ctx.fail(401);
    });

  }

  public void fromDeviceId(RoutingContext ctx) {
    _fromDeviceId(ctx, false);
  }

  public void registerDevice(RoutingContext ctx) {
    JsonObject userInfo = ctx.getBodyAsJson();
    String mobileNo = userInfo.getString("mobileNo");
    log.debug("The mobileNo for registration: " + mobileNo);
    if (mobileNo == null) {
      ctx.fail(401);
      return;
    }
    Observable<String> userByMobileNumberObservable = usersDAO.userByMobileNumber(mobileNo);
    userByMobileNumberObservable.flatMap(uid -> {
      return usersDAO.addDevice(uid, userInfo.getString("imei"), userInfo.getString("platform"));
    }).subscribe(json -> {
      if(json != null) {
        String did = json.getString("did");
        int pin = json.getInteger("pin");
        if (log.isDebugEnabled()) {
          log.debug("The device id being added : " + did);
        }
        JsonObject device = new JsonObject().put("did", did);
        if (Utils.isDevMode()) {
          device.put("pin", pin);
        }
        else {
          sendSMS(mobileNo, pin);
        }
        //device.put("pin", pin);
        ctx.response().end(device.encode());
      }
      else {
        log.error("Could not get the user with the added device");
        ctx.fail(401);
      }
    } , err -> {
      log.error("Error during addDevice");
      log.error(Arrays.toString(err.getStackTrace()));
      ctx.fail(401);
    });
  }

  private void handlePINVerification(RoutingContext ctx, JsonObject user) {
    JsonObject deviceInfo = ctx.getBodyAsJson();
    String did = ctx.request().getHeader(HEADER_AUTHORIZATION);
    Integer pin = deviceInfo.getInteger("pin");
    JsonArray devices = user.getJsonArray("devices");

    Optional<Object> optional = devices.stream().filter(json -> {
      JsonObject o = (JsonObject) json;
      return (did.equals(o.getString("did")));
    }).findFirst();

    if (optional.isPresent()) {
      JsonObject _deviceInfo = (JsonObject) optional.get();
      int _pin = _deviceInfo.getInteger("pin");
      if (_pin == pin.intValue()) {
        // Update the device as verified.
        usersDAO.updateDevice(user.getString("_id"), did).subscribe(v -> {
          List<Observable<?>> deleteObservables = new ArrayList<Observable<?>>();
          devices.forEach(_device -> {
            JsonObject device = (JsonObject) _device;
            String _did = device.getString("did");
            if (!did.equals(_did)) {
              deleteObservables.add(deviceToUser.delete(_did));
              //deleteFromDeviceIdsMap(_did);
            }
          });
          Observable.merge(deleteObservables).subscribe(o -> {
          } , err -> {
            log.debug("some error while deleting from cache");
          } , () -> {
            Observable<Void> putObservable = deviceToUser.put(did, user);
            putObservable.subscribe(v1 -> {
              log.debug("Added to store, device id: " + did + " and user: " + user);
              TringVerticleUtils.sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_CONTACT, user,
                  null, vertx);
            });
            ctx.response()
                .end(new JsonObject().put("success", true)
                    .put("p2p", user.getString(UserDAO.TOPIC_PREFIX) + user.getString("_id"))
                    .put("host", serverProperties.getString("tring.server.mqttbroker.host"))
                    .put("port",
                        Integer
                            .valueOf(serverProperties.getString("tring.server.mqttbroker.ws.port")))
                .encode());
          });
        });
      } else {
        setFailureStatus("PIN did not match", ctx);
      }
    }
  }

  private void deleteFromDeviceIdsMap(String _did) {
    log.debug("Removing clientId: " + _did);
    vertx.executeBlocking(fut -> {
      HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
      IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
      map.delete(_did);
      fut.complete((Void)null);
    },  res -> {
      log.debug("Removed clientId: " + _did);
    });
  }
  
  private void addToDeviceIdsMap(RoutingContext ctx, String did, String pnDeviceId) {
    log.debug("Adding clientId: " + did + " and pnDeviceId: " + pnDeviceId);
    vertx.executeBlocking(fut -> {
      HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
      IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
      map.set(did, pnDeviceId);
      fut.complete((Void)null);
    },  res -> {
      log.debug("Added clientId: " + did + " and pnDeviceId: " + pnDeviceId);
      ctx.response().end(new JsonObject().put("success", true).encode());
    });
  }
  
  private void sendSMS(String mobileNo, int pin) {
    String smsMsg = MessageBuilder.getSMSMessage(mobileNo.substring(1), pin);
    String data ="data=" + smsMsg +"&action=send";
    httpClient.post(80, "api.myvaluefirst.com", "/psms/servlet/psms.Eservice2").exceptionHandler(t -> {
      log.error("Exception occured");
      t.printStackTrace();
    }).handler(response -> {
      log.debug("statusCode from server is: " + response.statusCode());
      log.debug("statusMessage from server is: " + response.statusMessage());
      response.bodyHandler(body -> {
        log.debug(body.toString("ISO-8859-1"));
      });
    }).putHeader(HttpHeaders.CONTENT_LENGTH, data.length() + "").putHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded").write(data).end();
  }

  public void fromUserId(RoutingContext ctx) {
    verifyUser(ctx);
    String userId = ctx.request().getParam("userId");
    if (userId != null) {
      usersDAO.findById(userId,
          new JsonObject().put("mobileNo", 1).put("customMessage", 1).put(UserDAO.TOPIC_PREFIX, 1).put(UserDAO.PN_ID, 1))
          .subscribe(user -> {
            user.put("p2p", user.getString(UserDAO.TOPIC_PREFIX) + user.getString("_id"));
            ctx.response().end(user.encode());
          } , err -> {
            ctx.fail(401);
          });
    } else {
      ctx.fail(401);
    }
  }

  public void fromUsersMobileNos(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    if (log.isDebugEnabled()) {
      log.debug("Requesting mobileNos info for " + data);
    }
    JsonArray mobileNos = data.getJsonArray("mobileNos");
    String syncId = data.getString("syncId");
    JsonObject user = ctx.get("user");
    Observable<JsonObject> contactObservable =
        contactsDAO.findOne(new JsonObject().put(ContactsDAO.USER_ID, user.getString("_id")), null);
    contactObservable.subscribe(contact -> {
      if (contact == null) {
        contact = new JsonObject().put(ContactsDAO.USER_ID, user.getString("_id"))
            .put(ContactsDAO.MOBILE_NOS, mobileNos);
        contactsDAO.save(contact).subscribe(cid -> {
          _fromUserMobileNos(ctx, mobileNos, syncId);
        });
      } else {
        // contactsDAO.updateContact(contact.getString("_id"), mobileNos)
        contactsDAO
            .findOneAndUpdate(new JsonObject().put(ContactsDAO.USER_ID, user.getString("_id")),
                new JsonObject()
                    .put("$addToSet",
                        new JsonObject().put(ContactsDAO.MOBILE_NOS,
                            new JsonObject().put("$each", mobileNos))),
                true, false)
            .subscribe(v -> {
          _fromUserMobileNos(ctx, mobileNos, syncId);
        });
      }
    });
  }

  public void userProfilesFromUsersMobileNos(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    if (log.isDebugEnabled()) {
      log.debug("Requesting mobileNos profile info for " + data);
    }
    JsonArray mobileNos = data.getJsonArray("mobileNos");
    JsonObject profilesInfo = new JsonObject();
    if (mobileNos.size() > 0) {
      JsonObject fields = new JsonObject().put(UserDAO.MOBILE_NO, 1).put(UserDAO.THUMBNAIL_DATA, 1)
          .put(UserDAO.NAME, 1).put(UserDAO.CONTENT_ID, 1);
      usersDAO.usersByMobileNumbers(mobileNos, fields).subscribe(users -> {
        JsonArray usersInfo = new JsonArray();
        log.debug("# of Users obtained from query: " + users.size());
        users.parallelStream().forEach(user -> {
          usersInfo.add(user);
        });
        profilesInfo.put("profilesInfo", usersInfo);
        ctx.response().end(profilesInfo.encode());
      } , err -> ctx.fail(401));
    } else {
      ctx.response().end(profilesInfo.encode());
    }
  }

  private void _fromUserMobileNos(RoutingContext ctx, JsonArray mobileNos, String syncId) {
    JsonObject fields = new JsonObject().put(UserDAO.NAME, 1).put(UserDAO.MOBILE_NO, 1)
        .put(UserDAO.CUSTOM_MESSAGE, 1).put(UserDAO.TOPIC_PREFIX, 1);
    Observable<List<JsonObject>> usersByMobileNumbers =
        usersDAO.usersByMobileNumbers(mobileNos, fields);
    usersByMobileNumbers.subscribe(objs -> {
      JsonObject usersInfoJson = new JsonObject();
      JsonArray usersInfo = new JsonArray();
      objs.parallelStream().forEach(obj -> {
        obj.put("p2p", obj.getString(UserDAO.TOPIC_PREFIX) + obj.getString("_id"));
        usersInfo.add(obj);
      });
      usersInfoJson.put("usersInfo", usersInfo);
      if (log.isDebugEnabled()) {
        log.debug("UsersInfo obtained = " + usersInfoJson);
      }
      if (syncId != null) {
        JsonObject query = new JsonObject().put("_id", syncId);
        deleteSyncStatus(ctx, usersInfoJson, query);
      } else if (mobileNos.size() == 1) {
        String tgtMobileNo = mobileNos.getString(0);
        usersDAO.userByMobileNumber(tgtMobileNo).subscribe(tgtUid -> {
          JsonObject user = ctx.get("user");
          String srcUid = user.getString("_id");
          JsonObject query = new JsonObject().put("srcUid", srcUid).put("tgtUid", tgtUid);
          deleteSyncStatus(ctx, usersInfoJson, query);
        });
      } else {
        ctx.response().end(usersInfoJson.encode());
      }
    } , err -> ctx.fail(401));
  }

  private void deleteSyncStatus(RoutingContext ctx, JsonObject usersInfoJson, JsonObject query) {
    userToSyncContactDAO.delete(query).subscribe(updated -> {
      ctx.response().end(usersInfoJson.encode());
    } , err -> {
      ctx.response().end(usersInfoJson.encode());
      log.error("Could not delete syncContact for the query: " + query);
    });
  }

  public void updateUserProfile(RoutingContext ctx) {
    verifyUser(ctx);

    JsonObject data = ctx.getBodyAsJson();
    String customMessage = data.getString(UserDAO.CUSTOM_MESSAGE);
    String name = data.getString(UserDAO.NAME);
    String contentId = data.getString(UserDAO.CONTENT_ID);
    String thumbNailData = data.getString(UserDAO.THUMBNAIL_DATA);
    JsonObject notifications =  data.getJsonObject(UserDAO.NOTIFICATIONS);
    String occupation = data.getString(UserDAO.OCCUPATION);
    String interest = data.getString(UserDAO.INTEREST);
    String website = data.getString(UserDAO.WEBSITE);
    String paypalId = data.getString(UserDAO.PAYPALID);
    JsonObject user = ctx.get("user");
    
    if (user != null) {
      String userId = user.getString("_id");
      JsonObject query = new JsonObject().put("_id", userId);
      JsonObject update = new JsonObject();
    
      if (!Strings.isNullOrEmpty(name)) {
        update.put(UserDAO.NAME, name);
      }
      
      if (!Strings.isNullOrEmpty(customMessage)) {
        update.put(UserDAO.CUSTOM_MESSAGE, customMessage);
      }
      
      if (!Strings.isNullOrEmpty(contentId)) {
        update.put(UserDAO.CONTENT_ID, contentId);
      }
      
      if(!Strings.isNullOrEmpty(thumbNailData)) {
        update.put(UserDAO.THUMBNAIL_DATA, thumbNailData);
      }
      
      HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();

      if(notifications != null && !notifications.isEmpty()) {
        update.put(UserDAO.NOTIFICATIONS, notifications);
      }
      
      if(!Strings.isNullOrEmpty(occupation)) {
          update.put(UserDAO.OCCUPATION, occupation);
      }
      
      if(!Strings.isNullOrEmpty(website)) {
          update.put(UserDAO.WEBSITE, website);
      }
      
      if(!Strings.isNullOrEmpty(interest)) {
          update.put(UserDAO.INTEREST, new JsonArray(Arrays.asList(interest.replaceAll(Constants.REGEX_WHITE_SPACE, "").split(","))));
      }
      if(!Strings.isNullOrEmpty(paypalId)) {
        update.put(UserDAO.PAYPALID, paypalId);
       }
      update.put(UserDAO.PROFILE_HANDLED, false);

      JsonObject updateJson = new JsonObject().put("$set", update);
      log.debug("updateUserProfile - update object is "+updateJson);
      
      if (TringConstants.TRING_PROFILE_REWRITE) {
        JsonObject profileCounter = new JsonObject().put(UserDAO.PROFILE_COUNTER, 1);
        updateJson.put("$inc", profileCounter);
      }

      usersDAO.findOneAndUpdate(query, updateJson, true, false).subscribe(modifiedUser -> {
        log.debug("Sending message to Sync Profile of modifiedUser: "
            + modifiedUser.getJsonObject("value"));
        JsonObject obj = modifiedUser.getJsonObject("value");
        obj.remove(UserDAO.PAYPALID);
        TringVerticleUtils.sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_CONTACT_PROFILE,
            obj, null, vertx);
        if (update.containsKey(UserDAO.NOTIFICATIONS)) {
          ISet<String> notifiableIdSet  = hazelcastInstance.getSet(TringConstants.TRING_NOTIFIABLE_IDS);  
          JsonArray devices = obj.getJsonArray(UserDAO.DEVICES);
          int siz=devices.size();
          if(update.getJsonObject(UserDAO.NOTIFICATIONS).getBoolean(UserDAO.NOTI_GROUP)) {
            if (siz >0) {
              notifiableIdSet.add(((JsonObject)devices.getValue(siz-1)).getString("did"));
            }
          } else {
            if (siz >0) {
              notifiableIdSet.remove(((JsonObject)devices.getValue(siz-1)).getString("did"));
            }
          }
        }
        ctx.response().end(new JsonObject().put("success", true).encode());
      } , err -> {
        setFailureStatus("Error while updating user profile", ctx);
      });
    } else {
      setFailureStatus("User not found while updating user profile", ctx);
    }
  }

  public void deleteSyncRecord(RoutingContext ctx, BaseUserToSyncDAO syncDAO) {
    verifyUser(ctx);
    String syncId = ctx.request().getParam("syncId");
    if (log.isDebugEnabled()) {
      log.debug("syncId is " + syncId);
      log.debug("syncDAO is "+ syncDAO);
      log.debug("Deleting syncId " + syncId + " using syncDAO: " + syncDAO.getClass().getName());
    }
    JsonObject query = new JsonObject().put("_id", syncId);
    syncDAO.delete(query).subscribe(updated -> {
      ctx.response().end(new JsonObject().put("success", true).encode());
    } , err -> {
      setFailureStatus("Could not delete syncRecord for the query: " + query, ctx);
    });
  }

  public void deleteProfileSync(RoutingContext ctx) {
    deleteSyncRecord(ctx, userToSyncProfileDAO);
  }

  public void deleteRequestMembershipSync(RoutingContext ctx) {
    deleteSyncRecord(ctx, userToSyncAcceptRejectMembershipDAO);
  }

  public void deleteModeratorMessageSync(RoutingContext ctx) {
    deleteSyncRecord(ctx, userToSyncModeratorMessagesDAO);
  }

  public void deleteSubgroupMessageSync(RoutingContext ctx) {
    deleteSyncRecord(ctx, userToSyncSubGroupDAO);
  }

  public void deleteHandleAdminMessageSync(RoutingContext ctx) {
    deleteSyncRecord(ctx, userToSyncAdminRoleDAO);
  }

  public void deleteGroupProfileChangedSyncRecord(RoutingContext ctx) {
    deleteSyncRecord(ctx, groupProfileChangedSyncRecordDAO);
  }
  
  public void updateUserReputation(RoutingContext ctx) {
    verifyUser(ctx);
    // Add check if this user is owner/moderator of the group
    JsonObject data = ctx.getBodyAsJson();
    if (log.isDebugEnabled()) {
      log.debug("Reputation data: " + data);
    }
    String reputationKey = data.getString("reputationKey");
    int reputationValue = data.getInteger("reputationValue");
    String moderatorMessage = data.getString("moderatorMessage");
    if (isValidReputationKey(reputationKey)) {
      String moderatorHandleName = (data.getString("moderatorHandleName")).toLowerCase();
      String gid = data.getString(HandleDAO.GID);
      handlesDAO.getHandleRoleInGroup(moderatorHandleName, gid).subscribe(role -> {
        log.debug("Role found for the user: " + role);
        if (isValidRole(reputationKey, role)) {
          String handleName = (data.getString("handleName")).toLowerCase();
          handlesDAO.handleFromHandleNameAndGroupId(handleName, gid,
              new JsonObject().put("_id", 1).put(HandleDAO.UID, 1).put(HandleDAO.PARENT_GROUP_MEMBER, 1)).subscribe(handle -> {
            if (handle != null) {
              JsonObject membership = handle.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER);
              log.debug("The membership for handleName: " + handleName + " is: " + membership);
              int type = membership.getInteger(HandleDAO.TYPE);
              if(type > HandleDAO.HandleType.ADMIN.ordinal()) {
                String uid = handle.getString(HandleDAO.UID);
                if (uid != null) {
                  MongoAsyncSerialTasksTransaction transaction =
                      new MongoAsyncSerialTasksTransaction(transactionDAO);
                  if (UserDAO.EVICTIONS.equals(reputationKey)
                      && !Strings.isNullOrEmpty(moderatorMessage)) {
                    // 1. Update handle record and mark its status as EVICTED
                    // 2. Update user record to bump up the reputation value
                    // 3. Create sync record for warning/eviction message
                    // 4. Send message
                    AsyncSerialTasksTransaction userReputationTasks =
                        transaction.task((t, future, obj) -> {
                      log.debug("updateHandleEvictionStatus");
                      updateHandleEvictionStatus(t, moderatorMessage, handle,
                          HandleDAO.HandleStatus.EVICTED, transaction, future);
                    }).rollbackTask((t, future, obj) -> {
                      updateHandleEvictionStatus(t, "", handle, HandleDAO.HandleStatus.ACTIVE,
                          transaction, future);
                    });
                    userReputationTasks = addUserReputationTasks(ctx, reputationKey, reputationValue,
                        moderatorMessage, gid, uid, transaction);
                    userReputationTasks.task((t,future,obj)->{
                      log.debug("update solr member count due to eviction");
                      future.complete();
                      ctx.put("group", new JsonObject().put(GroupDAO.ID, gid)).put("increment", false);
                      ctx.next();
                    });
                    userReputationTasks.execute();
                  } else if (UserDAO.WARNINGS.equals(reputationKey)
                      && !Strings.isNullOrEmpty(moderatorMessage)) {
                    AsyncSerialTasksTransaction userReputationTasks = addUserReputationTasks(ctx,
                        reputationKey, reputationValue, moderatorMessage, gid, uid, transaction);
                    userReputationTasks.execute();
                  } else {
                    updateReputationValueForUser(ctx, reputationKey, reputationValue, gid, handleName,
                        uid);
                  }
                } else {
                  setFailureStatus("User could not be found: " + handleName + " in group: " + gid,
                      ctx);
                }
              }
              else {
                setFailureStatus("The handle being moderated cannot be owner or admin",
                    ctx);
              }
              
              
            } else {
              setFailureStatus("Handle could not be found: " + handleName + " in group: " + gid,
                  ctx);
            }
          } , err -> {
            err.printStackTrace();
            ctx.fail(401);
          });
        } else {
          log.debug("Invalid role for changing user reputation: " + role);
        }
      } , err -> {
        err.printStackTrace();
        ctx.fail(401);
      });

    } else {
      setFailureStatus("reputationKey or  reputationValue could not be found", ctx);
    }

  }

  private AsyncSerialTasksTransaction addUserReputationTasks(RoutingContext ctx,
      String reputationKey, int reputationValue, String moderatorMessage, String gid, String uid,
      MongoAsyncSerialTasksTransaction transaction) {
    return transaction.task((t, future, obj) -> {
      if(isInvalidEviction(reputationKey, t)) {
        log.debug("This is isInvalidEviction");
        future.complete();
      }
      else {
        updateReputationValueForUser(reputationKey, reputationValue, uid, transaction, t, future);
      }
    }).rollbackTask((t, future, obj) -> {
      updateReputationValueForUser(reputationKey, (-1 * reputationValue), uid, transaction, t,
          future);
    }).task((t, future, obj) -> {
      if(isInvalidEviction(reputationKey, t)) {
        future.complete();
      }
      else {
        String transactionId =
            (String) transaction.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
        String syncId = Utils.uuidToBase64(UUID.randomUUID());
        JsonObject syncModeratorRecord =
            getSyncModeratorRecord(ctx, uid, syncId, moderatorMessage, gid, reputationKey);
        Observable<String> syncRecordObservable =
            userToSyncModeratorMessagesDAO.insertTransactional(syncModeratorRecord, transactionId);
        syncRecordObservable.subscribe(syncRecordId -> {
          if (syncRecordId != null) {
            t.setValue("syncId", syncId);
            future.complete();
          } else {
            future.fail("Updating eviction status failed since syncRecord could not be created");
          }
        });
      }
      
    }).rollbackTask((t, future, obj) -> {
      String transactionId =
          (String) transaction.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
      String syncId = (String) t.getValue("syncId");
      Observable<Void> syncRecordDeleteObservable = userToSyncModeratorMessagesDAO
          .deleteTransactional(new JsonObject().put("_id", syncId), transactionId);
      syncRecordDeleteObservable.subscribe(v -> {
        future.complete();
      } , err -> {
        future.fail("rollback insert syncRecord failed");
      });

    }).task((t, future, obj) -> {
      if(isInvalidEviction(reputationKey, t)) {
        future.complete();
      }
      else {
        String topic = (String) t.getValue("topic");
        String syncId = (String) t.getValue("syncId");
        log.debug("addUserReputationTasks " + " Topic: " + topic + "  syncId: " + syncId);
        JsonObject syncModeratorMessage = TringVerticleUtils.getSyncModeratorMessage(topic,
            reputationKey, gid, moderatorMessage, syncId);
        TringVerticleUtils._sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null,
            syncModeratorMessage, vertx, success -> {
          if (success) {
            future.complete();
            // ctx.response().end(new JsonObject().put("success", true).encode());
          } else {
            future.fail("Updating eviction status failed since user syncMessage could not be sent");
          }
        });
      }
    }).onSuccess(() -> {
      if(isInvalidEviction(reputationKey, transaction)) {
        log.debug("This is isInvalidEviction and hence success is false");
        ctx.response().end(new JsonObject().put("success",false).encode());
      }
      else {
        ctx.response().end(new JsonObject().put("success", true).encode());
      }
      
    }).onFailure((rollbackSuccess) -> {
      ctx.response().end(new JsonObject().put("success", false).encode());
    });
  }

  private boolean isInvalidEviction(String reputationKey, AsyncSerialTasksTransaction t) {
    JsonObject updatedHandle = (JsonObject) t.getValue("updatedHandle");
    boolean isEviction = UserDAO.EVICTIONS.equals(reputationKey);
    return updatedHandle == null && isEviction;
  }

  private void updateReputationValueForUser(String reputationKey, int reputationValue, String uid,
      MongoAsyncSerialTasksTransaction transaction, AsyncSerialTasksTransaction t,
      Future<Void> future) {
    String transactionId =
        (String) transaction.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
    log.debug("updateReputationValueForUser for uid: " + uid + " transactionId: " + transactionId);
    Observable<JsonObject> modifiedUserObservable =
        _updateReputationValueForUser(reputationKey, reputationValue, uid, transactionId);
    modifiedUserObservable.subscribe(result -> {
      if (result != null) {
        JsonObject updatedUser = result.getJsonObject("value");
        String topic =
            updatedUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + uid;
        t.setValue("topic", topic);
        future.complete();
      } else {
        future.fail("Updating failed since user could not be updated for the reputationKey: " + reputationKey + " for uid: " + uid);
      }
    });
  }

  private void updateHandleEvictionStatus(AsyncSerialTasksTransaction t, String moderatorMessage, JsonObject handle,
      HandleDAO.HandleStatus handleStatus, MongoAsyncSerialTasksTransaction transaction,
      Future<Void> future) {
    String transactionId =
        (String) transaction.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
    Observable<JsonObject> modifiedHandleObservable =
        updateHandleEvictionStatus(handle, handleStatus, moderatorMessage, transactionId);
    modifiedHandleObservable.subscribe(result -> {
      if (result != null) {
        JsonObject modifiedHandle = result.getJsonObject("value");
        log.debug("modifiedHandle for evictionStatus: " + modifiedHandle );
        t.setValue("updatedHandle", modifiedHandle);
        future.complete();
      } else {
        future.fail("Updating eviction status failed");
      }
    }, err -> {
      log.debug("Got exception during updateHandleEvictionStatus");
      t.setValue("updatedHandle", null);
      future.complete();
    });
  }

  private JsonObject getSyncModeratorRecord(RoutingContext ctx, String uid, String syncId,
      String message, String gid, String reputationKey) {
    JsonObject srcUser = ctx.get("user");
    JsonObject syncModeratorMessage = new JsonObject();
    syncModeratorMessage.put("_id", syncId);
    syncModeratorMessage.put(BaseUserToSyncDAO.SRC_UID, srcUser.getString("_id"));
    syncModeratorMessage.put(BaseUserToSyncDAO.TGT_UID, uid);
    syncModeratorMessage.put(UserDAO.MODERATOR_MESSAGE, message);
    syncModeratorMessage.put(UserDAO.MODERATOR_MESSAGE_TYPE, reputationKey);
    syncModeratorMessage.put(GroupDAO.GID, gid);
    syncModeratorMessage.put(BaseUserToSyncDAO.SYNC_STATUS, false);
    return syncModeratorMessage;
  }

  private Observable<JsonObject> updateHandleEvictionStatus(JsonObject handle,
      HandleDAO.HandleStatus status, String evictionMessage, String transactionId) {
//    JsonObject query = new JsonObject().put("_id", handle.getString("_id")).put(HandleDAO.STATUS,
//        new JsonObject().put("$ne",status.ordinal()));
    JsonObject query = new JsonObject().put("_id", handle.getString("_id")).put(HandleDAO.STATUS,
        new JsonObject().put("$eq", HandleDAO.HandleStatus.ACTIVE.ordinal()));
    JsonObject update = new JsonObject();
    update.put(HandleDAO.STATUS, status.ordinal());
    update.put(HandleDAO.EVICTION_MESSAGE, evictionMessage);
    JsonObject updateJson = new JsonObject().put("$set", update);
    Observable<JsonObject> modifiedHandleObservable =
        handlesDAO.updateTransactional(query, updateJson, transactionId);
    return modifiedHandleObservable;
  }

  private void setFailureStatus(String message, RoutingContext ctx) {
    log.error(message);
    ctx.response().end(new JsonObject().put("success", false).encode());
  }

  private void updateReputationValueForUser(RoutingContext ctx, String reputationKey,
      int reputationValue, String gid, String handleName, String uid) {
    Observable<JsonObject> modifiedUserObservable =
        _updateReputationValueForUser(reputationKey, reputationValue, uid, null);
    modifiedUserObservable.subscribe(modifiedUser -> {
      JsonObject modifiedUserVal = modifiedUser.getJsonObject("value");
      int newReputationValue = modifiedUserVal.getInteger(reputationKey);
      if (newReputationValue < 0) {
        log.debug("User cannot have negative reputation. Hence resetting back!!");
        handleResetReputationValue(ctx, reputationKey, gid, handleName, uid, newReputationValue);
      } else {
        ctx.response().end(new JsonObject().put("success", true).encode());
        log.debug("The user has been successfully updated for newly added reputation");
      }

    });
  }

  private void handleResetReputationValue(RoutingContext ctx, String reputationKey, String gid,
      String handleName, String uid, int newReputationValue) {
    Observable<JsonObject> resetUserObservable =
        _updateReputationValueForUser(reputationKey, (-1 * newReputationValue), uid, null);
    resetUserObservable.subscribe(resetUser -> {
      setFailureStatus("Invalid reputation value: " + handleName + " in group: " + gid, ctx);
    });
  }

  private Observable<JsonObject> _updateReputationValueForUser(String reputationKey,
      int reputationValue, String uid, String transactionId) {
    JsonObject update = new JsonObject();
    update.put(reputationKey, Integer.valueOf(reputationValue));
    Observable<JsonObject> modifiedUserObservable;
    if (transactionId != null) {
      modifiedUserObservable = usersDAO.updateTransactional(new JsonObject().put("_id", uid),
          new JsonObject().put("$inc", update), transactionId);
    } else {
      modifiedUserObservable = usersDAO.findOneAndUpdate(new JsonObject().put("_id", uid),
          new JsonObject().put("$inc", update), true, false);
    }
    return modifiedUserObservable;
  }

  private boolean isValidRole(String reputationKey, int role) {
    if (reputationKey.equals(UserDAO.ABUSES)) {
      return true;
    } else {
      return role != HandleDAO.HandleType.NONMEMBER.ordinal();
    }
  }

  private boolean isValidReputationKey(String reputationKey) {
    return !Strings.isNullOrEmpty(reputationKey) && (reputationKey.equals(UserDAO.ABUSES)
        || reputationKey.equals(UserDAO.WARNINGS) || reputationKey.equals(UserDAO.EVICTIONS));
  }

  public void userLastSeen(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    String mobileNo = data.getString(UserDAO.MOBILE_NO);
    mobileToLastSeen.get(mobileNo).flatMap(obj -> {
      if (obj != null) {
        return Observable.just(obj);
      } else {
        Observable<JsonObject> userLastSeenByMobileNumber =
            usersDAO.userLastSeenByMobileNumber(mobileNo);
        return userLastSeenByMobileNumber.flatMap(lastSeen -> {
          if (log.isDebugEnabled()) {
            log.debug("Adding to store, mobileNo: " + mobileNo + ", lastseen: " + lastSeen);
          }
          mobileToLastSeen.put(mobileNo, lastSeen).subscribe();
          return Observable.just(lastSeen);
        });
      }
    }).subscribe(obj -> ctx.response().end(new JsonObject().put("user", obj).encode()),
        err -> ctx.fail(401));
  }

  public void getUserReputation(RoutingContext ctx) {
    JsonObject usr = ctx.get("user");
    String uid = null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    usersDAO.findById(uid,
        new JsonObject().put(UserDAO.ABUSES, 1).put(UserDAO.EVICTIONS, 1).put(UserDAO.WARNINGS, 1))
        .subscribe(user -> {
          if (user != null) {
            if (user.getInteger(UserDAO.ABUSES.toString()) == null) {
              user.put(UserDAO.ABUSES, 0);
            }
            if (user.getInteger(UserDAO.EVICTIONS.toString()) == null) {
              user.put(UserDAO.EVICTIONS, 0);
            }
            if (user.getInteger(UserDAO.WARNINGS.toString()) == null) {
              user.put(UserDAO.WARNINGS, 0);
            }
            ctx.response().end(user.encode());
          } else {
            ctx.fail(400);
          }
        } , err -> {
          ctx.fail(400);
        });
  }
}
