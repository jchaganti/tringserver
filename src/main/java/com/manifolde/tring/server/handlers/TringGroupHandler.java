package com.manifolde.tring.server.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bson.types.ObjectId;

import com.manifolde.tring.server.dao.BaseModelDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.HandleDAO.HandleStatus;
import com.manifolde.tring.server.dao.HandleDAO.HandleType;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.utils.async.MongoAsyncSerialTasksTransaction;
import com.manifolde.tring.server.verticles.TringVerticleUtils;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Future;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;
import rx.functions.Action1;


public class TringGroupHandler extends TringBaseHandler {
  private static final Logger log = LoggerFactory.getLogger(TringGroupHandler.class);

  public static final String GROUP_DATA = "groupData";
  public static final String OWNER_HANDLE_NAME = "ownerHandleName";
  private static final String MEMBERSHIP_TYPE = "membershipType";

  private GroupDAO groupsDAO;

  private HandleDAO handlesDAO;

  private TransactionDAO transactionDAO;


  public TringGroupHandler(Vertx vertx, GroupDAO groupsDAO, HandleDAO handlesDAO,
      TransactionDAO transactionDAO) {
    super(vertx);
    this.groupsDAO = groupsDAO;
    this.handlesDAO = handlesDAO;
    this.transactionDAO = transactionDAO;
  }
  public void getGroups(RoutingContext ctx) {
    JsonObject user = (JsonObject) ctx.get("user");
    String uid = user.getString("_id");
    // Simple ArrayList won't do. we get a ConcurrentModificationException.
    // Note that grps is a shared, mutable sequence for several subscribe threads
    // below.
    // CopyOnWriteArrayList is too expensive since we have many 'add's.
    // Hence this class.
    ConcurrentLinkedQueue<JsonObject> grps = new ConcurrentLinkedQueue<>();

    handlesDAO.getParentGroupsForUser(uid).subscribe(groups -> {
      if (null == groups) {
        ctx.response().end(new JsonObject().encode());
      } else {
        // List<JsonObject> gr = groups;
        // Observable<JsonObject> gr1 = Observable.from(groups);
        Future<Void> future = Future.future();
        future.setHandler((a)->{
          if (log.isDebugEnabled()) {
            log.debug("Groups are :");
            grps.forEach(g -> {
              log.debug(g);
            });
          }
          System.out.println("extended group info:"+grps.size());
          System.out.println(grps);
          JsonArray arr = new JsonArray(Arrays.asList(grps.toArray()));
          System.out.println("group info as JsonArray:");
          System.out.println(arr);
          ctx.response().end(arr.encode());
        });
        System.out.println("Group found: "+groups.size());
        Observable.from(groups).subscribe(group -> {
          System.out.println(group);
          JsonObject jobj = group.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER);
          String gid = jobj.getString(HandleDAO.GID);
          String handleName = group.getString(HandleDAO.NAME);
          int type = jobj.getInteger(HandleDAO.TYPE, -1);
          if(type == -1) {
            type = HandleDAO.HandleType.MEMBER.ordinal();
          }
          int membershipStatus = group.getInteger(HandleDAO.STATUS, HandleStatus.ACTIVE.ordinal());
          final int fType = type;
          Observable<JsonObject> groupObs = groupsDAO.groupFromId(gid);
          groupObs.subscribe(grp -> {
            String topicPrefix = grp.getString(GroupDAO.MQTT_TOPIC_PREFIX);
            String topic = topicPrefix + gid;
            grp.put(TringWebApiServer.HANDLE_NAME, handleName)
            .put(MEMBERSHIP_TYPE, fType).put(GroupDAO.MQTT_TOPIC, topic)
            .put(TringWebApiServer.MEMBERSHIP_STATUS, membershipStatus)
            .remove(GroupDAO.MQTT_TOPIC_PREFIX);
            grp.remove("transactions");
            grps.add(grp);
            if(grps.size() == groups.size()) {
              future.complete();
            }
          } , err -> {
          ctx.fail(400);
          future.fail("");
          });
        },
         err-> {
           ctx.fail(401);
           future.fail("");
           },
         ()->{
          if (groups.size() <= 0) {
            future.complete();
          }
        });
      }
    } , err -> {
      ctx.fail(402);
    });
  }
  public void isGroupNameAvailable(RoutingContext ctx) {
    String groupName = ctx.request().getParam(TringWebApiServer.GROUP_NAME);
    groupName = groupName.toLowerCase();
    
    groupsDAO.groupFromName(groupName).subscribe( group -> {
      if (null == group) {
        ctx.response().end(new JsonObject().put("success", true).encode());
      } else {
        ctx.response().end(new JsonObject().put("success", false).encode());
      }
    } , err -> {
      ctx.response().end(new JsonObject().put("success", true).encode());
    });
  }

  public void isGroupNameInDatabase(String groupName, Action1<JsonObject> action,
      Action1<Throwable> errHandler) {
    groupsDAO.groupFromName(groupName).subscribe(action, errHandler);
  }

  public void groupFromName(RoutingContext ctx) {
    String groupName = ctx.request().getParam(TringWebApiServer.GROUP_NAME);
    groupName = groupName.toLowerCase();

    JsonObject user = (JsonObject) ctx.get("user");
    String userId = user !=null?user.getString(UserDAO.ID):null;
    groupsDAO.groupFromName(groupName).subscribe(groupInfo -> {
      if (groupInfo == null) {
        ctx.response().end(new JsonObject().put("success", false).encode());
      } else {
        String gid = groupInfo.getString(GroupDAO.ID);
        if (null != userId) {
          handlesDAO.handleFromUidAndParentGid(userId, gid, 
              new JsonObject().put(HandleDAO.STATUS, HandleDAO.HandleStatus.ACTIVE.ordinal()), 
              new JsonObject().put(HandleDAO.NAME, 1).put(HandleDAO.PARENT_GROUP_MEMBER,1)).subscribe(hdl->{
                JsonObject resultObj = groupInfo.put("success", true);
                if (null != hdl){
                  JsonObject pgObj = hdl.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER);
                  int type = pgObj.getInteger(HandleDAO.TYPE, -1);
                  if (type == -1) {
                    type = HandleType.MEMBER.ordinal();
                  }
                  JsonObject handleInfo = new JsonObject().put(TringWebApiServer.HANDLE_NAME, hdl.getString(HandleDAO.NAME))
                      .put(HandleDAO.TYPE, type);
                  resultObj.mergeIn(handleInfo);
                  resultObj.remove("transactions");
                  //Remove these fields since we are getting the same from a query.
                  resultObj.remove(TringWebApiServer.RATINGS_AND_REVIEWS);
                  resultObj.remove(TringWebApiServer.MEAN_RATING);
                  resultObj.remove(TringWebApiServer.RATING_COUNT);
                }
                ctx.response().end(resultObj.encode());
              }, err->{
                ctx.fail(401);
              });
        } else {
          ctx.response().end(groupInfo.put("success", true).encode());
        }
      }
    } , err -> ctx.fail(400));
  }

  private void changeJsonObjectForSolrUpdate(JsonObject obj, Set<String> ignoreKeys) {
    obj.stream().forEach(el->{
      String key=el.getKey();
      if (!ignoreKeys.contains(key)) {
        el.setValue(new JsonObject().put("set", el.getValue()));
      }
    });
  }

  public void updateGroup(RoutingContext ctx) {
    log.error("Inside updateGroup");
    JsonObject body = ctx.getBodyAsJson();
    JsonObject groupData = body.getJsonObject(GROUP_DATA);
    String gid= groupData.getString(GroupDAO. ID);
    JsonObject usr = ctx.get("user");
    String uid=null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    groupData.remove(BaseModelDAO.MODIFIED);
    handlesDAO.getUserRoleInGroup(uid, gid).
    subscribe(role -> {
      log.error("Role found for the requesting user in getUserReputation: " + role);
      if(Utils.isAdmin(role) || Utils.isOwner(role)) {
        groupsDAO.updateGroup(groupData).subscribe(grp->{
          log.error("Inside updateGroup 1");
          if(grp != null){
            //ctx.response().end(new JsonObject().put("success", true).encode());
            groupData.remove(BaseModelDAO.MODIFIED);
            JsonObject groupDataCopy = groupData.copy();
            changeJsonObjectForSolrUpdate(groupData,new HashSet<String>(Arrays.asList(GroupDAO.GROUP_NAME)));
            log.debug("updateGroup: groupData is "+groupData);
            ctx.put("partialResponse",new JsonObject());
            groupsDAO.findById(gid, null).subscribe(group->{
              //Send sync message
              log.debug("Sending message to Sync Profile of modifiedUser: "
                  + groupDataCopy);
              if (group.getBoolean(GroupDAO.CLOSED_GROUP)) {
                ctx.response().end(((JsonObject)ctx.get("partialResponse")).put("success", true).encode());
              }else{
                ctx.put("group", groupData);
                ctx.next();
              }
              TringVerticleUtils.sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_GROUP_PROFILE,
                  group.put("groupData", groupDataCopy), null, vertx).subscribe(r->{
                    log.debug("Group profile update message sent successfully");
                  }, err->{
                    log.error(err);
                    ctx.fail(403);
                  });

            },err->{
              log.error(err);
              ctx.fail(402);
            });          
          }else {
            log.error("err- updateGroup");
            ctx.fail(401);
          }
        }, err->{
          log.error(err);
          ctx.fail(400);
        });
      }
    }, err->{
      log.error(err);
      ctx.fail(401);
    });
  }
  
  public void createGroup(RoutingContext ctx) {
    JsonObject body = ctx.getBodyAsJson();
    JsonObject groupData = body.getJsonObject(GROUP_DATA);
    String ownerHandle = (body.getString(OWNER_HANDLE_NAME)).toLowerCase();
    String hid = ObjectId.get().toHexString();
    groupData.put(GroupDAO.ID, ObjectId.get().toHexString()).put(GroupDAO.HID, hid);
    String gname = groupData.getString((GroupDAO.GROUP_NAME)).toLowerCase();
    groupData.put(GroupDAO.GROUP_NAME, gname);

    MongoAsyncSerialTasksTransaction trans = new MongoAsyncSerialTasksTransaction(transactionDAO);
    if (log.isDebugEnabled()) {
      log.debug("Created MongoAsyncSerialTasksTransaction object");
    }
    trans.task((t, future, obj) -> {
      String transactionId =
          (String) trans.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
      if (log.isDebugEnabled()) {
        log.debug("Calling Task createGroup- Transaction id is " + transactionId);
      }
      addTopicPrefix(groupData);
      groupsDAO.createGroup(groupData, ownerHandle, transactionId).subscribe(jobj -> {
        if (null == jobj) {
          future.fail("Problem creating Group");
        } else {
          String id = jobj.getString(GroupDAO.ID);
          //System.out.println("Group topic prefix is " + groupData.getString(GroupDAO.MQTT_TOPIC_PREFIX));
          String topic = groupData.getString(GroupDAO.MQTT_TOPIC_PREFIX) + id;
          //Long modified = jobj.getLong(BaseModelDAO.MODIFIED);
          if (log.isDebugEnabled()) {
            log.debug("Create Group id is " + id);
            //log.debug("Create Group topic is " + topic);
            //log.debug("Create Group modified is " + modified);
          }
          t.setValue(GroupDAO.GID, id);
          t.setValue(GroupDAO.MQTT_TOPIC, topic);
          t.setValue("group", jobj);
          future.complete();
        }
      } , err -> {
        log.error("problem creating group");
        future.fail("Problem creating Group");
      } , () -> {
        if (log.isDebugEnabled()) {
          log.debug("onComplete for createGroup");
        }
      });
    }).rollbackTask((t, future, obj) -> {
      String id = (String) t.getValue(GroupDAO.GID);
      // groupsDAO.delete(id);
      if (log.isDebugEnabled()) {
        log.debug("Deleted group with id " + id);
      }
    }).task((t, future, obj) -> {
      String transactionId =
          (String) trans.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
      String gid = (String) t.getValue(GroupDAO.GID);
      JsonObject user = (JsonObject) ctx.get("user");
      String uid = user.getString("_id");
      handlesDAO.createOwnerHandle(ownerHandle, hid, gid, uid, transactionId).subscribe(jobj -> {
        if (null == jobj) {
          future.fail("Problem creating owner handle " + ownerHandle);
        } else {
          String id = jobj.getString(HandleDAO.ID);
          if (log.isDebugEnabled()) {
            log.debug("Created owner handle with id " + id);
          }
          t.setValue(HandleDAO.HID, id);
          future.complete();
        }
      } , err -> future.fail("Problem creating owner handle " + ownerHandle));
    }).onSuccess(() -> {
      String gid = (String) trans.getValue(GroupDAO.GID);
      if (log.isDebugEnabled()) {
        log.debug("onSuccess- Group id is " + gid);
      }
      //Long modifyDate = (Long)trans.getValue(BaseModelDAO.MODIFIED);
      String topic = (String)trans.getValue(GroupDAO.MQTT_TOPIC);
      ctx.put("partialResponse",new JsonObject().put(GroupDAO.GID, gid)
          .put(HandleDAO.HID, hid) // .put(BaseModelDAO.MODIFIED, modifyDate)
          .put(GroupDAO.MQTT_TOPIC, topic )); 
      JsonObject grp = (JsonObject) trans.getValue("group");
      if (grp.getBoolean(GroupDAO.CLOSED_GROUP)) {
        ctx.response().end(((JsonObject)ctx.get("partialResponse")).put("success", true).encode());
      }else{
        ctx.put("group", grp);
        ctx.put("init", true);
        ctx.next();
      }
    }).onFailure(rollbackSuccess -> {
      if (log.isDebugEnabled()) {
        log.debug("onFailure-  rollbackSuccess is " + rollbackSuccess);
      }
      if (rollbackSuccess) {
        ctx.response().end(new JsonObject().put("success", false).put("rolledBack", true).encode());
      } else {
        ctx.response()
            .end(new JsonObject().put("success", false).put("rolledBack", false).encode());
      }
    }).execute();
  }

  private void addTopicPrefix(JsonObject groupData) {
    groupData.put(GroupDAO.MQTT_TOPIC_PREFIX, "/groups/1/");
  }
  
  public void getAllGroups(RoutingContext ctx) {
    JsonObject user = (JsonObject) ctx.get("user");
    String uid = user.getString("_id");
    JsonObject groupData = new JsonObject();
    
    handlesDAO.getAllHandlesForUser(uid).subscribe(handles -> {
      if (handles != null && handles.size() > 0) {
        JsonObject parentGroups = new JsonObject();
        JsonObject asModeratorSubGroups = new JsonObject();
        JsonObject asMemberSubGroups = new JsonObject();
        final JsonObject groupIds = new JsonObject().put("subGroupMemberIds", new JsonArray())
                                    .put("subGroupModeratorIds", new JsonArray());
        
        List<Observable<JsonObject>> groupObservables = new ArrayList<>();
        handles.forEach(handle -> {
          // populateParentGroupData(parentGroups, group);
          int membershipStatus = handle.getInteger(HandleDAO.STATUS);
          if (membershipStatus != HandleDAO.HandleStatus.INACTIVE.ordinal()
              && membershipStatus != HandleDAO.HandleStatus.PENDING.ordinal()) {
            JsonObject jobj = handle.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER);
            String gid = jobj.getString(HandleDAO.GID);
            int type = jobj.getInteger(HandleDAO.TYPE, -1);
            if (type == -1) {
              type = HandleDAO.HandleType.MEMBER.ordinal();
            }
            JsonObject group = new JsonObject().put(TringWebApiServer.HANDLE_NAME, handle.getString(HandleDAO.NAME))
                                               .put(MEMBERSHIP_TYPE, type).put(TringWebApiServer.MEMBERSHIP_STATUS, membershipStatus);
            parentGroups.put(gid, group);
            
            JsonObject fields = new JsonObject().put(GroupDAO.ID, 1).put(GroupDAO.GROUP_NAME, 1).put(GroupDAO.GROUP_O2M, 1).put(GroupDAO.CLOSED_GROUP, 1)
                .put(GroupDAO.THUMBNAIL_DATA, 1).put(GroupDAO.PARENT_GID, 1).put(GroupDAO.TOPIC_PREFIX, 1);
            groupObservables.add(groupsDAO.findById(gid, fields));
            
            JsonArray subGroupModeratorIds = handle.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
            if(subGroupModeratorIds != null && subGroupModeratorIds.size() > 0) {
              groupIds.getJsonArray("subGroupModeratorIds").addAll(subGroupModeratorIds);
              subGroupModeratorIds.forEach(subGroupModeratorId -> {
                groupObservables.add(groupsDAO.findById((String)subGroupModeratorId, fields));
              });
            }
            
            JsonArray subGroupMemberIds = handle.getJsonArray(HandleDAO.SUB_GROUP_MEMBER);
            if(subGroupMemberIds != null && subGroupMemberIds.size() > 0) {
              groupIds.getJsonArray("subGroupMemberIds").addAll(subGroupMemberIds);
              subGroupMemberIds.forEach(subGroupMemberId -> {
                groupObservables.add(groupsDAO.findById((String)subGroupMemberId, fields));
              });
            }
            
          }
        });
        
        if(log.isDebugEnabled()) {
          log.debug("parentGroups: " + parentGroups);
          log.debug("subGroupMemberIds: " + groupIds.getJsonArray("subGroupMemberIds"));
          log.debug("subGroupModeratorIds: " + groupIds.getJsonArray("subGroupModeratorIds"));
        }
        
        Observable.merge(groupObservables).subscribe(groupObj-> {
          if(log.isDebugEnabled()) {
            log.debug("GroupObj: " + groupObj);
          }
          String pgid = groupObj.getString(GroupDAO.PARENT_GID);
          log.debug("pgid: " + pgid);
          String topicPrefix = groupObj.getString(GroupDAO.MQTT_TOPIC_PREFIX);
          String gid = groupObj.getString(GroupDAO.ID);
          String topic = topicPrefix + gid;
          
          if(pgid == null) {
            updateParentGroupData(parentGroups, groupObj, topic);
          }
          else {
            updateSubGroupData(asModeratorSubGroups, asMemberSubGroups, groupIds, groupObj, pgid,
                gid, topic);
          }
        }, err->{
          err.printStackTrace();
          log.error("Error occured while obtaining getAllGroups: " + err.getMessage());
        }, () -> {
          groupData.put("parentGroups", parentGroups);
          groupData.put("asModeratorSubGroups", asModeratorSubGroups);
          groupData.put("asMemberSubGroups", asMemberSubGroups);
          ctx.response().end(groupData.encode());
        });
        
      } else {
        ctx.response().end(groupData.encode());
      }

    });
  }
  private void updateSubGroupData(JsonObject asModeratorSubGroups, JsonObject asMemberSubGroups,
      final JsonObject groupIds, JsonObject groupObj, String pgid, String gid, String topic) {
    JsonObject subGroup = new JsonObject();
    subGroup.put(GroupDAO.PARENT_GID, pgid);
    subGroup.put(GroupDAO.MQTT_TOPIC, topic);
    subGroup.put(GroupDAO.GROUP_NAME, groupObj.getString(GroupDAO.GROUP_NAME));
    JsonArray subGroupMemberIds =  groupIds.getJsonArray("subGroupMemberIds");
    JsonArray subGroupModeratorIds =  groupIds.getJsonArray("subGroupModeratorIds");
    if(subGroupMemberIds.contains(gid)) {
      asMemberSubGroups.put(gid, subGroup);
    }
    else if(subGroupModeratorIds.contains(gid)) {
      asModeratorSubGroups.put(gid, subGroup);
    }
    else {
      log.error("Subgroup Id could not be found: " + gid);
    }
  }
  private void updateParentGroupData(JsonObject parentGroups, JsonObject groupObj, String topic) {
    JsonObject parentGroup = parentGroups.getJsonObject(groupObj.getString(GroupDAO.ID) );
    parentGroup.put(GroupDAO.MQTT_TOPIC, topic);
    parentGroup.put(GroupDAO.GROUP_NAME, groupObj.getString(GroupDAO.GROUP_NAME));
    parentGroup.put(GroupDAO.GROUP_O2M, groupObj.getBoolean(GroupDAO.GROUP_O2M));
    parentGroup.put(GroupDAO.CLOSED_GROUP, groupObj.getBoolean(GroupDAO.CLOSED_GROUP));
    parentGroup.put(GroupDAO.THUMBNAIL_DATA, groupObj.getString(GroupDAO.THUMBNAIL_DATA));
    parentGroup.put("contentId", groupObj.getString("contentId"));
  }
  
}
