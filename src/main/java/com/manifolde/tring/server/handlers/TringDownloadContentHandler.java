package com.manifolde.tring.server.handlers;

import com.manifolde.tring.server.dao.ContentDAO;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.rxjava.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
// import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;

public class TringDownloadContentHandler extends TringBaseHandler {
  private static final Logger log = LoggerFactory.getLogger(TringDownloadContentHandler.class);
  public static final String WEB_ROOT = "/static";

  private ContentDAO contentDAO;

  // private StaticHandler staticHandler = StaticHandler.create();
  public TringDownloadContentHandler(Vertx vertx, ContentDAO contentDAO) {
    super(vertx);
    this.contentDAO = contentDAO;
  }

  public void downloadContent(RoutingContext ctx) {
    log.debug("handleContent...");
    verifyUser(ctx);
    String id = ctx.request().getParam(TringWebApiServer.CONTENT_ID);
    Observable<JsonObject> content = contentDAO.getContent(id);
    content.subscribe(obj -> {
      String fPath = obj.getString(ContentDAO.PATH);
      ctx.response().putHeader("Content-Disposition", "attachment; filename=" + obj.getString(ContentDAO.NAME));
      ctx.response().sendFileObservable(fPath).subscribe(r -> {
        if (log.isDebugEnabled()) {
          log.debug("downloadContent: r is "+r);
        }
      } , t -> {
        log.error("Error during sendFileObservable" + t.getMessage());
        t.printStackTrace();
        ctx.fail(400);
      });
    } , (t) -> {
      log.error("Error during getContent");
      ctx.fail(400);
    });
  }
}
