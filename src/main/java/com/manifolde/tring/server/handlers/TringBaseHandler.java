package com.manifolde.tring.server.handlers;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.RoutingContext;

public class TringBaseHandler {

  protected Vertx vertx;
  private static final Logger log = LoggerFactory.getLogger(TringBaseHandler.class);
  
  protected TringBaseHandler(Vertx vertx) {
    this.vertx = vertx;
  }
  
  protected void verifyUser(RoutingContext ctx) {
    JsonObject user = ctx.get("user");
    if(log.isDebugEnabled()) {
      if(user != null) {
        log.debug("verifying user: " + user.getString("_id"));
      }
      else {
        log.debug("verifying user: null" );
      }
     
    }
    if(user == null) {
      ctx.fail(403);
    }
  }
}
