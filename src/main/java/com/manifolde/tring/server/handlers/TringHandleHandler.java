package com.manifolde.tring.server.handlers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;
import com.manifolde.tring.server.dao.BaseUserToSyncDAO;
import com.manifolde.tring.server.dao.ContactsDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.HandleDAO.HandleStatus;
import com.manifolde.tring.server.dao.HandleDAO.HandleType;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.utils.async.AsyncFailure;
import com.manifolde.tring.server.utils.async.AsyncSuccess;
import com.manifolde.tring.server.utils.async.MongoAsyncSerialTasksTransaction;
import com.manifolde.tring.server.verticles.TringVerticleUtils;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.RoutingContext;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.observables.MathObservable;

public class TringHandleHandler extends TringBaseHandler {
  private static final Logger log = LoggerFactory.getLogger(TringHandleHandler.class);

  public static final String ID = "_id";

  private static final String UID = "uid";

  private static final String DUPLICATE_ERROR_CODE = "11000";
  private static final int PAGE_LENGTH = 50;

  public static final int CONFLICT_ERROR = 409;
  private HandleDAO handlesDAO;
  private UserDAO usersDAO;
  private GroupDAO groupsDAO;
  private ContactsDAO contactsDAO;
  private TransactionDAO transactionDAO;

  public TringHandleHandler(Vertx vertx, GroupDAO groupsDAO, UserDAO usersDAO, HandleDAO handlesDAO,
      ContactsDAO contactsDAO, TransactionDAO transactionDAO) {

    super(vertx);
    this.handlesDAO = handlesDAO;
    this.usersDAO = usersDAO;
    this.groupsDAO = groupsDAO;
    this.contactsDAO = contactsDAO;
    this.transactionDAO = transactionDAO;
  }

  public void isHandlerNameForGroupNameAvailable(RoutingContext ctx) {
    String groupName = ctx.request().getParam(TringWebApiServer.GROUP_NAME);

    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);

    try {
      groupName = (URLDecoder.decode(groupName, "UTF-8")).toLowerCase();
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    final String hName = handleName;
    groupsDAO.findOne(new JsonObject().put(GroupDAO.GROUP_NAME, groupName),
        new JsonObject().put(GroupDAO.ID, 1)).subscribe(group -> {
          if (group == null) {
            ctx.fail(400);
          }
          String gid = group.getString(GroupDAO.ID);
          isHandlerNameInDatabase(hName, gid, obj -> {
            if (obj == null) {
              ctx.response().end(new JsonObject().put("success", true).encode());
            } else {
              ctx.response().end(new JsonObject().put("success", false).encode());
            }
          } , err -> {
            err.printStackTrace();
            ctx.fail(500);
          });
        });
  }

  public void isHandlerNameAvailable(RoutingContext ctx) {
    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    String groupId = ctx.request().getParam(TringWebApiServer.GID);
    isHandlerNameInDatabase(handleName, groupId, obj -> {
      if (obj == null) {
        ctx.response().end(new JsonObject().put("success", true).encode());
      } else {
        ctx.response().end(new JsonObject().put("success", false).encode());
      }
    } , err -> {
      err.printStackTrace();
      ctx.fail(500);
    });
  }

  public void userInfoByHandleNameandGroupId(RoutingContext ctx) {
    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    String subGroupId = ctx.request().params().get(HandleDAO.SUBGROUP_GID);
    log.debug("userInfoByHandleNameandGroupId Parent group id: " + gid);
    log.debug("userInfoByHandleNameandGroupId Sub group id: " + subGroupId);
    log.debug("userInfoByHandleNameandGroupId handleName: " + handleName);
    if(!Strings.isNullOrEmpty(subGroupId)) {
      gid = subGroupId;
    }
    handlesDAO.handleFromHandleNameAndGroupId(handleName, gid,
        new JsonObject().put("_id", 1).put(HandleDAO.UID, 1)
            .put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE, 1)
            .put(HandleDAO.SUB_GROUP_MODERATOR, 1))
        .subscribe(handle -> {
          if (handle != null) {
            if(log.isDebugEnabled()) {
              log.debug("Handle found: " + handle);
            }
            String uid = handle.getString(HandleDAO.UID);
            int membershipType = handle.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE);
            if (uid != null) {
              usersDAO.findById(uid, new JsonObject().put(UserDAO.TOPIC_PREFIX, 1))
                  .subscribe(user -> {
                boolean isModerator = false;
                if (Strings.isNullOrEmpty(subGroupId)) {
                  isModerator = membershipType == HandleDAO.HandleType.OWNER.ordinal() || membershipType == HandleDAO.HandleType.ADMIN.ordinal();
                } else {
                  JsonArray moderatingSubGroups = handle.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
                  if(log.isDebugEnabled()) {
                    log.debug("Moderating SubGroups found: " + moderatingSubGroups);
                  }
                  isModerator = moderatingSubGroups != null && moderatingSubGroups.contains(subGroupId);
                }
                ctx.response()
                    .end(new JsonObject().put("p2p", user.getString(UserDAO.TOPIC_PREFIX) + uid)
                        .put("isModerator", isModerator).encode());
              } , err -> {
                err.printStackTrace();
                ctx.fail(401);
              });
            } else {
              ctx.fail(401);
            }
          } else {
            ctx.fail(401);
          }

        } , err -> {
          err.printStackTrace();
          ctx.fail(401);
        });

  }

  public void isHandlerNameInDatabase(String handleName, String groupId, Action1<JsonObject> action,
      Action1<Throwable> errHandler) {
    if (log.isDebugEnabled()) {
      log.debug("isHandlerNameInDatabase: handleName is " + handleName + " groupId is " + groupId);
    }
    Observable<JsonObject> handleObservable =
        handlesDAO.handleFromHandleNameAndGroupId(handleName, groupId);
    handleObservable.subscribe(action, errHandler);
  }


  public void getMembersOfGroup(RoutingContext ctx) {
    verifyUser(ctx);
    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);
    String gid = ctx.request().getParam(HandleDAO.GID);
    int page = Utils.parseString(ctx.request().params().get(HandleDAO.PAGE), 0);
    int pageSize = Utils.parseString(ctx.request().params().get(HandleDAO.PAGE_SIZE), -1);

    if (handleName != null) {
      handleName = handleName.toLowerCase();
    }
    
    if (log.isDebugEnabled()) {
      log.debug("getMembersOfGroup handleName: " + handleName);
      log.debug("getMembersOfGroup gid: " + gid);
      log.debug("getMembersOfGroup page: " + page);
      log.debug("getMembersOfGroup pageSize: " + pageSize);
    }
    if (gid != null && handleName != null) {
      handlesDAO.getHandleRoleInGroup(handleName, gid).subscribe(role -> {
        log.debug("Role found for the user in getMembersOfGroup: " + role);
        if (Utils.isOwner(role) || Utils.isAdmin(role)) {
          groupsDAO.getGroupAndSubGroups(gid).subscribe(groups -> {
            Map<String, String> groupIdToNames = new HashMap<>();
            groups.forEach(group -> {
              groupIdToNames.put(group.getString(GroupDAO.ID),
                  group.getString(GroupDAO.GROUP_NAME));
            });
            if (log.isDebugEnabled()) {
              log.debug("groupIdToNames: " + groupIdToNames);
            }
            String parentGroupName = groupIdToNames.get(gid);
            if (parentGroupName != null) {
              JsonObject fields = new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER, 1)
                  .put(HandleDAO.NAME, 1).put(HandleDAO.ID, 1).put(HandleDAO.SUB_GROUP_MODERATOR, 1)
                  .put(HandleDAO.SUB_GROUP_MEMBER, 1);
              handlesDAO.getMembersOfGroup(gid, fields, page, pageSize).subscribe(members -> {
                if (log.isDebugEnabled()) {
                  log.debug("Members of group are: " + members);
                }
                handleMembers(ctx, gid, groupIdToNames, members);
              } , err -> {
                err.printStackTrace();
                ctx.fail(401);
              });
            } else {
              log.error("Parent group's name found to be null");
              ctx.response().end(new JsonObject().put("success", false)
                  .put("message", "Parent group's name found to be null").encode());
            }
          } , err -> {
            err.printStackTrace();
            ctx.fail(401);
          });
        } else {
          log.error("User does not have moderator role. Hence failing.");
          ctx.response().end(new JsonObject().put("success", false)
              .put("message", "User does not have owner/admin role").encode());
        }
      });
    } else {
      log.error("Invalid inputs");
      ctx.response()
          .end(new JsonObject().put("success", false).put("message", "Invalid inputs").encode());
    }

  }

  public void getMembersOfSubGroup(RoutingContext ctx) {
    verifyUser(ctx);
    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);
    String pgid = ctx.request().getParam(HandleDAO.GID);
    String sgid = ctx.request().getParam(HandleDAO.SUBGROUP_GID);
    String includeNonMembersParam = ctx.request().params().get("includeNonMembers");
    boolean includeNonMembers =
        includeNonMembersParam != null && "true".equals(includeNonMembersParam) ? true : false;
    int page = Utils.parseString(ctx.request().params().get(HandleDAO.PAGE), 0);
    int pageSize = Utils.parseString(ctx.request().params().get(HandleDAO.PAGE_SIZE), -1);

    if (handleName != null) {
      handleName = handleName.toLowerCase();
    }
    if (log.isDebugEnabled()) {
      log.debug("getMembersOfSubGroup handleName: " + handleName);
      log.debug("getMembersOfSubGroup parent gid: " + pgid);
      log.debug("getMembersOfSubGroup subGroupId: " + sgid);
      log.debug("getMembersOfSubGroup page: " + page);
      log.debug("getMembersOfSubGroup pageSize: " + pageSize);
    }
    if (pgid != null && handleName != null) {
      handlesDAO.getHandleRoleInGroup(handleName, pgid).subscribe(role -> {
        log.debug("Role found for the user in getMembersOfSubGroup: " + role);
        if (Utils.isOwner(role) || Utils.isAdmin(role)) {
          groupsDAO.getGroupAndSubGroups(pgid).subscribe(groups -> {
            Map<String, String> groupIdToNames = new HashMap<>();
            groups.forEach(group -> {
              groupIdToNames.put(group.getString(GroupDAO.ID),
                  group.getString(GroupDAO.GROUP_NAME));
            });
            if (log.isDebugEnabled()) {
              log.debug("groupIdToNames: " + groupIdToNames);
            }
            String parentGroupName = groupIdToNames.get(pgid);
            if (parentGroupName != null) {
              JsonObject fields = new JsonObject().put(HandleDAO.NAME, 1).put(HandleDAO.ID, 1)
                  .put(HandleDAO.SUB_GROUP_MODERATOR, 1).put(HandleDAO.SUB_GROUP_MEMBER, 1).put(HandleDAO.STATUS, 1);
              handlesDAO.getMembersOfSubGroup(includeNonMembers ? pgid : null, sgid, fields, page,
                  pageSize).subscribe(members -> {
                if (log.isDebugEnabled()) {
                  log.debug("Members of subgroup are: " + members);
                }
                handleSubGroupMembers(ctx, pgid, sgid, groupIdToNames, members, includeNonMembers);
              } , err -> {
                err.printStackTrace();
                ctx.fail(401);
              });
            } else {
              log.error("Parent group's name found to be null");
              ctx.response().end(new JsonObject().put("success", false)
                  .put("message", "Parent group's name found to be null").encode());
            }
          } , err -> {
            err.printStackTrace();
            ctx.fail(401);
          });
        } else {
          log.error("User does not have moderator role. Hence failing.");
          ctx.response().end(new JsonObject().put("success", false)
              .put("message", "User does not have owner/admin role").encode());
        }
      });
    } else {
      log.error("Invalid inputs");
      ctx.response()
          .end(new JsonObject().put("success", false).put("message", "Invalid inputs").encode());
    }

  }

  public void acceptHandle(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    String handleName = (ctx.request().getParam(TringWebApiServer.HANDLE_NAME)).toLowerCase();

    JsonObject self = (JsonObject) ctx.get("user");
    String userId = self.getString(UserDAO.ID);
    handlesDAO.getUserRoleInGroup(userId, gid).subscribe(role -> {
      log.debug("Role found for the requesting user in getUserReputation: " + role);
      if (Utils.isAdmin(role) || Utils.isOwner(role)) {
        // Check if user has the right role to do this
        Observable<JsonObject> myHandle =
            handlesDAO.handleFromUidAndParentGid(userId, gid, new JsonObject(), new JsonObject());
        myHandle.subscribe(myHdl -> {

          boolean isReject = ctx.request().absoluteURI().endsWith("reject");
          Func2<String, String, Observable<Void>> func;
          if (isReject) {
            func = handlesDAO::markHandleAsRejectedGivenHandleNameAndGid;
          } else {
            func = handlesDAO::updateHandleToNonPending;
          }
          groupsDAO.findById(gid, new JsonObject()).subscribe(grp->{
            Boolean requiresApproval = grp.getBoolean(GroupDAO.MEMBERSHIP_NEEDS_APPROVAL);
            JsonObject moreQuery = new JsonObject();
            if (!isReject) {
              moreQuery = moreQuery.put(HandleDAO.STATUS, HandleDAO.HandleStatus.PENDING.ordinal());
            } else {
              //When membershipNeedsApproval is in place we may need to change this to conditionally use the "$or"
              //below only for membershipNeedsApproval=false
              if (requiresApproval != null && requiresApproval) {
                moreQuery = moreQuery.put(HandleDAO.STATUS, HandleDAO.HandleStatus.PENDING.ordinal());
              } else {
                moreQuery = moreQuery.put(HandleDAO.STATUS, HandleDAO.HandleStatus.ACTIVE.ordinal());
              }
            }
            getUserFromHandleAndParentGid(handleName, gid, moreQuery).subscribe(user -> {
              if (user == null) {
                log.error("Error finding user in sendMessageToSync");
                ctx.fail(401);
                return;
              }
              System.out.println("In acceptHandle, user is " + user);
              acceptHandleDbChange(func, user, handleName, gid, isReject).subscribe(handle -> {
                if (!grp.getBoolean(GroupDAO.CLOSED_GROUP) ) {
                  String gname = grp.getString(GroupDAO.GROUP_NAME);

                  if (!isReject && requiresApproval != null && requiresApproval) {
                    ctx.put("group", new JsonObject().put(GroupDAO.ID, gid).put(GroupDAO.GROUP_NAME, gname));
                    ctx.put("increment", true);
                  }
                  else if (isReject && (requiresApproval == null  || !requiresApproval)) {
                    ctx.put("group", new JsonObject().put(GroupDAO.ID, gid).put(GroupDAO.GROUP_NAME, gname));
                    ctx.put("increment", false);
                  }
                  ctx.next();
                }
                ctx.response().end(new JsonObject().put("success", true).encode());
              } , err -> {
                log.error("Error deleting/updating handle");
                ctx.fail(400);
              });
            } , err -> {
              log.error("Problem finding user in sendMessageToSync");
              ctx.fail(401);
            });
          });
        } , err -> {
          log.error("Unauthorized access to accept membership functionality");
          ctx.fail(400);
        });
      } else {
        log.error("Unauthorized access to accept membership functionality");
        ctx.fail(400);
      }
    });
  }

  private Observable<JsonObject> getUserFromHandleAndParentGid(String handleName, String gid,
      JsonObject moreQuery) {
    ObservableFuture<JsonObject> result = new ObservableFuture<>();
    JsonObject query = new JsonObject().put(HandleDAO.NAME, handleName)
        .put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid).mergeIn(moreQuery);
    handlesDAO.findOne(query, new JsonObject()).subscribe(handle -> {
      if (handle == null) {
        result.toHandler().handle(new AsyncFailure<>());
        return;
      }
      String uid = handle.getString(UID);
      System.out.println("From getUserFromHandleAndParentGid uid is " + uid);
      usersDAO.findById(uid, new JsonObject()).subscribe(user -> {
        System.out.println("From getUserFromHandleAndParentGid user is " + user);
        result.toHandler().handle(new AsyncSuccess<>(user));
      } , err -> {
        result.toHandler().handle(new AsyncFailure<>());
      });
    } , err -> {
      result.toHandler().handle(new AsyncFailure<>());
    });
    return result;
  }

  private Observable<Boolean> acceptHandleDbChange(
      final Func2<String, String, Observable<Void>> func, JsonObject user, String handleName,
      String gid, boolean isReject) {
    //
    MongoAsyncSerialTasksTransaction trans = new MongoAsyncSerialTasksTransaction(transactionDAO);
    ObservableFuture<Boolean> result = new ObservableFuture<>();
    trans.task((t, future, obj) -> {
      func.call(handleName, gid).subscribe(jobj -> {
        future.complete();
      } , err -> {
        future.fail("accepHandleDbChange failed");
      });
    }).task((t, future, obj) -> {
      groupsDAO.groupFromId(gid).subscribe(group -> {
        Boolean requestNeedsApproval = group.getBoolean(GroupDAO.MEMBERSHIP_NEEDS_APPROVAL);
        if (isReject || requestNeedsApproval != null && requestNeedsApproval) {

          JsonObject syncInfo = new JsonObject();
          System.out.println("In acceptHandleDbChange, user is " + user);
          syncInfo // .put("_id", Utils.uuidToBase64(UUID.randomUUID()))
          .put(BaseUserToSyncDAO.TGT_UID, user.getString(UserDAO.ID))
          .put(GroupDAO.TOPIC_PREFIX, group.getString(GroupDAO.TOPIC_PREFIX))
          .put(TringWebApiServer.USER_TOPIC_PREFIX, user.getString(UserDAO.TOPIC_PREFIX))
          .put(UserDAO.MOBILE_NO, user.getString(UserDAO.MOBILE_NO))
          .put(BaseUserToSyncDAO.HANDLE_NAME, handleName)
          .put(GroupDAO.GROUP_NAME, group.getString(GroupDAO.GROUP_NAME))
          .put(BaseUserToSyncDAO.SYNC_STATUS, false).put(BaseUserToSyncDAO.REJECTED, isReject)
          .put(BaseUserToSyncDAO.GID, gid)
          .put(GroupDAO.GROUP_O2M, group.getBoolean(GroupDAO.GROUP_O2M))
          .put(GroupDAO.CLOSED_GROUP, group.getBoolean(GroupDAO.CLOSED_GROUP));
          System.out.println("In acceptHandleDbChange, syncInfo is "+syncInfo);
          TringVerticleUtils
          .sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_MEMBERSHIP_REQUEST_PROCESSED,
              syncInfo, null, vertx)
          .subscribe(success -> {
            if (success) {
              future.complete();
            } else {
              future.fail("sendMessageToSync failed");
            }
          } , err -> {
            future.fail("Problem in sendMessageToSync");
          });
        }
      });
    }).onSuccess(() -> {
      result.toHandler().handle(new AsyncSuccess<>(true));
    }).onFailure(rollbackSuccess -> {
      if (log.isDebugEnabled()) {
        log.debug("onFailure-  rollbackSuccess is " + rollbackSuccess);
      }
      if (rollbackSuccess) {
        result.toHandler().handle(new AsyncFailure<>());
      } else {
        result.toHandler().handle(new AsyncFailure<>());
      }
    }).execute();
    return result;
  }

  private void handleMembers(RoutingContext ctx, String gid, Map<String, String> groupIdToNames,
      List<JsonObject> members) {
    JsonArray membersArr = new JsonArray();
    JsonArray adminsArr = new JsonArray();
    JsonArray ownerArr = new JsonArray();
    members.forEach(member -> {
      JsonObject parentMemberShip = member.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER);
//      String parentGroupName = groupIdToNames.get(gid);
      String hid = member.getString(HandleDAO.ID);
      int ptype = parentMemberShip.getInteger(HandleDAO.TYPE);
      final JsonArray arr = (HandleDAO.HandleType.MEMBER.ordinal() == ptype
          || HandleDAO.HandleType.NONMEMBER.ordinal() == ptype) ? membersArr
              : (HandleDAO.HandleType.ADMIN.ordinal() == ptype) ? adminsArr : ownerArr;
      JsonObject memberDetails = new JsonObject();
      memberDetails.put(HandleDAO.NAME, member.getString(HandleDAO.NAME));

      JsonArray asMemberArr = new JsonArray();
      memberDetails.put(HandleDAO.AS_MEMBER, asMemberArr);
      // if (HandleDAO.HandleType.MEMBER.ordinal() == ptype) {
      // asMemberArr.add(new JsonObject().put(gid, parentGroupName));
      // }

      JsonArray asModeratorArr = new JsonArray();
      memberDetails.put(HandleDAO.AS_MODERATOR, asModeratorArr);
      JsonObject memberData = new JsonObject();
      memberData.put(hid, memberDetails);
      arr.add(memberData);

      JsonArray asModeratorGids = member.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
      if (asModeratorGids != null) {
        asModeratorGids.forEach(asModeratorGid -> {
          // String subGroupFullName =
          // parentGroupName + "/" + groupIdToNames.get((String) asModeratorGid);
          String subGroupFullName = groupIdToNames.get((String) asModeratorGid);
          asModeratorArr.add(new JsonObject().put((String) asModeratorGid, subGroupFullName));
        });
      }

      JsonArray asMemberGids = member.getJsonArray(HandleDAO.SUB_GROUP_MEMBER);
      if (asMemberGids != null) {
        asMemberGids.forEach(asMemberGid -> {
          // String subGroupFullName =
          // parentGroupName + "/" + groupIdToNames.get((String) asMemberGid);
          String subGroupFullName = groupIdToNames.get((String) asMemberGid);
          asMemberArr.add(new JsonObject().put((String) asMemberGid, subGroupFullName));
        });
      }

    });
    JsonObject _membersData = new JsonObject().put("success", true)
        .put(HandleDAO.HandleType.MEMBER.toString(), membersArr)
        .put(HandleDAO.HandleType.ADMIN.toString(), adminsArr)
        .put(HandleDAO.HandleType.OWNER.toString(), ownerArr);
    log.debug("The membersData: " + _membersData);
    ctx.response().end(_membersData.encode());
  }

  private void handleSubGroupMembers(RoutingContext ctx, String pgid, String sgid,
      Map<String, String> groupIdToNames, JsonArray members, boolean includeNonMembers) {
    JsonArray adminsAsModeratorArr = new JsonArray();
    JsonArray adminsAsNonMembersArr = includeNonMembers ? new JsonArray() : null;
    JsonArray membersAsModeratorArr = new JsonArray();
    JsonArray membersArr = new JsonArray();
    JsonArray nonMembersArr = includeNonMembers ? new JsonArray() : null;
    members.forEach(_member -> {
      JsonObject member = (JsonObject) _member;
      boolean isOwner = member.getBoolean(HandleDAO.IS_OWNER);
      boolean isAdmin = member.getBoolean(HandleDAO.IS_ADMIN);
      boolean addedAsModerator = member.getBoolean(HandleDAO.ADDED_AS_MODERATOR);
      boolean addedAsMember = member.getBoolean(HandleDAO.ADDED_AS_MEMBER);
      int status = member.getInteger(HandleDAO.STATUS);

      if ((isAdmin && addedAsMember) || status > 0) {
        if(status > 0) {
          log.debug("Ignoring the member due to invalid status: " + member);
        }
        else {
          log.error("Something wrong with this member. Hence ignoring! " + member);
        }
      } else {
        JsonArray arr = null;
        JsonObject memberDetails = new JsonObject();
        if (isOwner || isAdmin) {
          if (addedAsModerator) {
            arr = adminsAsModeratorArr;
            memberDetails.put("role", HandleType.MODERATOR.ordinal());
          } else if (includeNonMembers) {
            memberDetails.put("role", HandleType.NONMEMBER.ordinal());
            arr = adminsAsNonMembersArr;
          }
        } else {
          if (addedAsModerator) {
            memberDetails.put("role", HandleType.MODERATOR.ordinal());
            arr = membersAsModeratorArr;
          } else if (addedAsMember) {
            memberDetails.put("role", HandleType.MEMBER.ordinal());
            arr = membersArr;
          } else if (includeNonMembers) {
            memberDetails.put("role", HandleType.NONMEMBER.ordinal());
            arr = nonMembersArr;
          }
        }
        String hid = member.getString(HandleDAO.ID);

        memberDetails.put(HandleDAO.NAME, member.getString(HandleDAO.NAME));

        JsonArray asMemberArr = new JsonArray();
        memberDetails.put(HandleDAO.AS_MEMBER, asMemberArr);

        JsonArray asModeratorArr = new JsonArray();
        memberDetails.put(HandleDAO.AS_MODERATOR, asModeratorArr);
        JsonObject memberData = new JsonObject();
        memberData.put(hid, memberDetails);
        arr.add(memberData);

        JsonArray asModeratorGids = member.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
        if (asModeratorGids != null) {
          asModeratorGids.forEach(asModeratorGid -> {
            if (!asModeratorGid.equals(sgid)) {
              String subGroupFullName = groupIdToNames.get((String) asModeratorGid);
              asModeratorArr.add(new JsonObject().put((String) asModeratorGid, subGroupFullName));
            }

          });
        }

        JsonArray asMemberGids = member.getJsonArray(HandleDAO.SUB_GROUP_MEMBER);
        if (asMemberGids != null) {
          asMemberGids.forEach(asMemberGid -> {
            if (!asMemberGid.equals(sgid)) {
              String subGroupFullName = groupIdToNames.get((String) asMemberGid);
              asMemberArr.add(new JsonObject().put((String) asMemberGid, subGroupFullName));
            }
          });
        }
      }
    });

    JsonObject _membersData =
        new JsonObject().put("success", true).put("adminsAsModerator", adminsAsModeratorArr)
            .put("adminsAsNonMembers", adminsAsNonMembersArr)
            .put("membersAsModerator", membersAsModeratorArr).put("members", membersArr)
            .put("nonMembers", nonMembersArr);
    log.debug("The membersData: " + _membersData);
    ctx.response().end(_membersData.encode());
  }

  public void createGroupTempMember(RoutingContext ctx) {
    String handleName = ctx.getBodyAsJson().getString(TringWebApiServer.HANDLE_NAME);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    final String hName = handleName;
    String groupName = (ctx.request().getParam(TringWebApiServer.GROUP_NAME)).toLowerCase();
    JsonObject user = (JsonObject) ctx.get("user");
    String userId = user.getString(UserDAO.ID);
    String uMessage = ctx.getBodyAsJson().getString(TringWebApiServer.MESSAGE_DATA);
    final String userMessage = uMessage == null ? "" : uMessage;
    groupsDAO.groupFromName(groupName).subscribe(group -> {
      if (group == null) {
        ctx.fail(400);
        return;
      }
      Boolean requestNeedsApproval = group.getBoolean(GroupDAO.MEMBERSHIP_NEEDS_APPROVAL);
      String gid = group.getString(GroupDAO.ID);
      handlesDAO.handleFromUidAndParentGid(userId, gid, new JsonObject(),
          new JsonObject().put(HandleDAO.STATUS, 1).put(HandleDAO.NAME, 1)
              .put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE, 1))
          .subscribe(hdl -> {
        Observable<Void> result = Observable.just(null);
        if (hdl != null) {
          String hdlName = hdl.getString(HandleDAO.NAME);
          String hid = hdl.getString(HandleDAO.ID);
          int status = hdl.getInteger(HandleDAO.STATUS);
          int type = hdl.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE);
          log.debug("createTempMember:- handle already exists, membership status is "+status);
          if (status == HandleStatus.LEFT.ordinal()) {
            if (requestNeedsApproval != null && requestNeedsApproval) {
              status = HandleStatus.PENDING.ordinal();
            } else {
              status = HandleStatus.ACTIVE.ordinal();
            }
            result = handlesDAO.updateHandleToGivenStatus(hdlName, gid, status);
            TringVerticleUtils.sendGroupMembershipRequestMessage(group, hdlName, userId,
                userMessage, vertx, null);
          }
          final int retStatus = status;
          result.subscribe((r) -> {
            boolean performIncrement = (requestNeedsApproval == null || !requestNeedsApproval) && retStatus == HandleStatus.ACTIVE.ordinal();
            respondAfterCreateGroupMember(false, ctx, hid, group, hdlName, retStatus, type, performIncrement);
          } , err -> {
            log.error(err);
            log.error("Fatal error: updating handle status to PENDING");
            ctx.fail(401);
          });
        } else {
          boolean isTempMember = false;
          if (requestNeedsApproval != null && requestNeedsApproval) {
            isTempMember =true;
          }
          final boolean isTempMem = isTempMember;
          Observable<JsonObject> groupMemberObservable =
              handlesDAO.createGroupMember(hName, gid, userId, isTempMember);
          groupMemberObservable.subscribe(obj -> {
            if (obj == null) {
              ctx.fail(400);
            } else {
              String hid = obj.getString(ID);
              TringVerticleUtils.sendGroupMembershipRequestMessage(group, hName, userId,
                  userMessage, vertx, null);
              respondAfterCreateGroupMember(true, ctx, hid, group, hName,
                  obj.getInteger(HandleDAO.STATUS),
                  obj.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE), !isTempMem);
            }
          } , err -> {
            boolean handleAlreadyCreated = err.getMessage().contains(DUPLICATE_ERROR_CODE);
            err.printStackTrace();
            if (handleAlreadyCreated) {
              log.error(
                  "Serious error : handle already exists for uid and gid " + userId + " " + gid);
              ctx.fail(402);
            } else {
              log.error("Serious error : Random error for uid and gid " + userId + " " + gid);
              ctx.fail(400);
            }
          });
        }
      } , err -> {
        log.error(
            "Fatal error: Finding if handle for uid, gid " + userId + " " + gid + "already exists");
        ctx.fail(400);
      });
    } , err -> {
      log.error("Problem finding group " + groupName);
      err.printStackTrace();
      ctx.fail(400);
    });
  }

  public void getNonMembersFromContacts(RoutingContext ctx) {
    verifyUser(ctx);
    String handleName = ctx.request().getParam(TringWebApiServer.HANDLE_NAME);
    String gid = ctx.request().getParam(HandleDAO.GID);
    if (log.isDebugEnabled()) {
      log.debug("getNonMembersFromContacts handleName: " + handleName);
      log.debug("getNonMembersFromContacts gid: " + gid);
    }

    if (gid != null && handleName != null) {
      handleName = handleName.toLowerCase();
      handlesDAO.getHandleRoleInGroup(handleName, gid).subscribe(role -> {
        log.debug("Role found for the user in getNonMembersFromContacts: " + role);
        if (Utils.isModerator(role)) {
          JsonObject user = ctx.get("user");
          contactsDAO
              .findOne(new JsonObject().put(ContactsDAO.USER_ID, user.getString(ID)),
                  new JsonObject().put(ContactsDAO.USER_ID, 1).put(ContactsDAO.MOBILE_NOS, 1))
              .subscribe(contact -> {
            if (contact != null) {
              log.debug("Contact found: " + contact);
              JsonArray contactsNos = contact.getJsonArray(ContactsDAO.MOBILE_NOS);
              if (log.isDebugEnabled()) {
                log.debug("Input MobileNos: " + contactsNos);
              }
              if (contactsNos.size() > 0) {
                JsonObject query = new JsonObject().put(UserDAO.MOBILE_NO,
                    new JsonObject().put("$in", contactsNos));
                JsonObject fields = new JsonObject().put(ID, 1).put(UserDAO.MOBILE_NO, 1);
                usersDAO.find(query, fields).subscribe(users -> {
                  JsonArray uids = new JsonArray();
                  Map<String, String> uidToMobileNos = new HashMap<>();
                  users.forEach(_user -> {
                    String uid = _user.getString(ID);
                    uids.add(uid);
                    uidToMobileNos.put(uid, _user.getString(UserDAO.MOBILE_NO));
                  });
                  if (log.isDebugEnabled()) {
                    log.debug("Total contacts: " + uids);
                  }

                  handlesDAO.getMembersOfGroupWithUsersAs(gid, uids,
                      new JsonObject().put(HandleDAO.UID, 1)).subscribe(members -> {
                    members.forEach(member -> {
                      contactsNos.remove(uidToMobileNos.get(member.getString(HandleDAO.UID)));
                    });
                    if (log.isDebugEnabled()) {
                      log.debug("Non-member Mobile Nos of the contact: " + contactsNos);
                    }
                    ctx.response().end(new JsonObject().put("success", true)
                        .put("nonMembers", contactsNos).encode());
                  } , err -> {
                    err.printStackTrace();
                    ctx.fail(401);
                  });
                } , err -> {
                  err.printStackTrace();
                  ctx.fail(401);
                });
              } else {
                log.error("Zero contacts found for this user");
                ctx.response().end(new JsonObject().put("success", false)
                    .put("message", "Zero contacts found for this user").encode());
              }
            } else {
              log.error("Contacts could not be found for this user");
              ctx.response().end(new JsonObject().put("success", false)
                  .put("message", "Contacts could not be found for this user").encode());
            }
          } , err -> {
            err.printStackTrace();
            ctx.fail(401);
          });

        } else {
          log.error("User does not have moderator role. Hence failing.");
          ctx.response().end(new JsonObject().put("success", false)
              .put("message", "User does not have owner/admin role").encode());
        }
      } , err -> {
        err.printStackTrace();
        ctx.fail(401);
      });
    }
  }

  private void respondAfterCreateGroupMember(boolean success, RoutingContext ctx, String hid,
      JsonObject group, String hdlName, int status, int type, Boolean performIncrement) {
    String gid = group.getString(GroupDAO.ID);
    String topic = group.getString(GroupDAO.MQTT_TOPIC_PREFIX) + gid;
    log.debug("respondAfterCreateGroupMember - temp- " + hdlName + " " + status + " " + type + " "
        + gid + " " + topic + " " + hid);
    JsonObject response = new JsonObject().put(TringWebApiServer.HANDLE_NAME, hdlName)
        .put(TringWebApiServer.MEMBERSHIP_STATUS, status)
        .put(TringWebApiServer.MEMBERSHIP_TYPE, type).put(TringWebApiServer.GID, gid)
        .put(TringWebApiServer.TOPIC, topic)
        .put(TringWebApiServer.GROUP_O2M, group.getBoolean(GroupDAO.GROUP_O2M))
        .put(TringWebApiServer.THUMBNAIL_DATA, group.getString(GroupDAO.THUMBNAIL_DATA))
        .put(TringWebApiServer.HID, hid);

    if (!success) {
      response.put("error",
          new JsonObject().put("code", CONFLICT_ERROR).put("message", "Conflict"));
    } else {
      response.put("success", true);
    }
    if(!group.getBoolean(GroupDAO.CLOSED_GROUP) && performIncrement) {
      ctx.put("group", new JsonObject().put(GroupDAO.ID, gid).put(GroupDAO.GROUP_NAME, group.getString(GroupDAO.GROUP_NAME)));
      ctx.put("increment", true);
      ctx.next();
    }
    ctx.response().end(response.encode());
  }

  private void respondAfterCreateGroupMember(boolean success, RoutingContext ctx, String hid,
      String groupId, String hdlName, int status, int type, Boolean performIncrement) {
    groupsDAO.groupFromId(groupId).subscribe(group -> {
      String topic = group.getString(GroupDAO.MQTT_TOPIC_PREFIX) + groupId;
      log.debug("respondAfterCreateGroupMember - temp- " + hdlName + " " + status + " " + type + " "
          + groupId + " " + topic + " " + hid);
      JsonObject response = new JsonObject().put(TringWebApiServer.HANDLE_NAME, hdlName)
          .put(TringWebApiServer.MEMBERSHIP_STATUS, status)
          .put(TringWebApiServer.MEMBERSHIP_TYPE, type).put(TringWebApiServer.GID, groupId)
          .put(TringWebApiServer.GROUP_O2M, group.getBoolean(GroupDAO.GROUP_O2M))
          .put(TringWebApiServer.THUMBNAIL_DATA, group.getString(GroupDAO.THUMBNAIL_DATA))
          .put(TringWebApiServer.TOPIC, topic).put(TringWebApiServer.HID, hid);
      if (!success) {
        response.put("error",
            new JsonObject().put("code", CONFLICT_ERROR).put("message", "Conflict"));
      } else {
        response.put("success", true);
      }
      if(!group.getBoolean(GroupDAO.CLOSED_GROUP) && performIncrement) {
        ctx.put("group", new JsonObject().put(GroupDAO.ID, groupId).put(GroupDAO.GROUP_NAME, group.getString(GroupDAO.GROUP_NAME)));
        ctx.put("increment", true);
        ctx.next();
      }
      ctx.response().end(response.encode());
    } , err -> {
      log.error(err);
      ctx.fail(400);
    });
  }

  public void createGroupMember(RoutingContext ctx) {
    String handleName = ctx.getBodyAsJson().getString(TringWebApiServer.HANDLE_NAME);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    // boolean tempMember = ctx.request().absoluteURI().contains("tempmember");

    String groupId = ctx.request().getParam(TringWebApiServer.GID);
    JsonObject user = (JsonObject) ctx.get("user");
    String userId = user.getString("_id");
    final String hName = handleName;

    // String userMessage = ctx.getBodyAsJson().getString(TringWebApiServer.MESSAGE_DATA);
    handlesDAO.handleFromUidAndParentGid(userId, groupId, new JsonObject(),
        new JsonObject().put(HandleDAO.STATUS, 1).put(HandleDAO.NAME, 1)
            .put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE, 1))
        .subscribe(hdl -> {
          Observable<Void> result = null;
          if (hdl != null) {
            String hdlName = hdl.getString(HandleDAO.NAME);
            String hid = hdl.getString(HandleDAO.ID);
            int status = hdl.getInteger(HandleDAO.STATUS);
            final int oldStatus = status;
            int type = hdl.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE);
            if (status == HandleStatus.LEFT.ordinal() || status == HandleStatus.PENDING.ordinal()) {
              result = handlesDAO.updateHandleToGivenStatus(hdlName, groupId,
                  HandleStatus.ACTIVE.ordinal());
              status = HandleStatus.ACTIVE.ordinal();
            } else {
              result = Observable.just(null);
            }
            final int retStatus = status;
            result.subscribe((r) -> {
              if (oldStatus == HandleStatus.LEFT.ordinal() || oldStatus == HandleStatus.PENDING.ordinal()) {
                respondAfterCreateGroupMember(false, ctx, hid, groupId, hdlName, retStatus, type,true);
              } else {
                respondAfterCreateGroupMember(false, ctx, hid, groupId, hdlName, retStatus, type,false);
              }
            } , err -> {
              log.error(err);
              log.error("Fatal error: updating handle status to ACTIVE");
              ctx.fail(400);
            });
          } else {
            Observable<JsonObject> groupMemberObservable =
                handlesDAO.createGroupMember(hName, groupId, userId, false);
            groupMemberObservable.subscribe(obj -> {
              if (obj == null) {
                ctx.fail(400);
              } else {
                String hid = obj.getString(ID);
                String gid =
                    obj.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getString(HandleDAO.GID);
                respondAfterCreateGroupMember(true, ctx, hid, gid, hName,
                    obj.getInteger(HandleDAO.STATUS),
                    obj.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE),true);
              }
            } , err -> { // Should never reach here
              boolean handleAlreadyCreated = err.getMessage().contains(DUPLICATE_ERROR_CODE);
              err.printStackTrace();
              if (handleAlreadyCreated) {
                log.error("Serious error : handle already exists for uid and gid " + userId + " "
                    + groupId);
              } else {
                log.error("Serious error : Random error for uid and gid " + userId + " " + groupId);
                ctx.fail(400);
              }
            });
          }
        });
  }

  public void handleCreateEditSubGroup(RoutingContext ctx) {
    // 1. Get the action - Create/Edit, parentGroup id
    // 2. Get all handleIds that are part of this group and their roles

    // 3. If the action is create:
    // 3.1 Create group record with the parentGroup id
    // 3.1. Bulk Update Handle records for moderators
    // 3.1.1 Add the subgroup id to membership as moderator
    // 3.1.2 Add the moderator as the member of parent group
    // 3.2. Bulk Update Handle records for members
    // 3.2.1 add the subgroup id to membership
    // 3.2.2 Make the members, non-members of parent group

    // 4. If the action is edit, get the subgroup id
    // 4.1 Update each handle (Assuming that we will not have lot of handles for edit)

    // 4.1.1 If the member or admin/owner is made moderator:
    // 4.1.1.1 Modify the handle's subgroup membership
    // 4.1.1.2 Add the handle as member of parent group if he was a non-member.

    // 4.1.2 If the non-member is made moderator:
    // 4.1.2.1 Add the handle's subgroup membership
    // 4.1.2.2 Add the handle as member of parent group if he was a non-member.

    // 4.1.3 If the member is being made non-member:
    // 4.1.3.1 Remove the handle's subgroup membership
    // 4.1.3.2 Check if the handle is member of at least one subgroup. If not add the handle as
    // member of parent group if he is non-member.

    // 4.1.4 If the moderator is being made non-member:
    // 4.1.4.1 Remove the handle's subgroup membership
    // 4.1.4.2 Check if the handle is moderator of at least one other subgroup or if he is not be a
    // member of any subgroup.
    // 4.1.4.2.1 If yes, add the handle as member of parent group.
    // 4.1.4.2.2 If no, remove the handle as member of parent group.

    // 4.1.5 If the admin/owner is being made non-member, remove the handle's subgroup membership

    // 4.1.6 If the moderator is being made member:
    // 4.1.6.1 Modify the handle's subgroup membership
    // 4.1.6.2 Remove the handle as member of parent group if the handle is not moderator of at
    // least one other subgroup

    // 4.1.7 If the non-member is being made member, add the subgroup id to membership as moderator

    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    if (log.isDebugEnabled()) {
      log.debug("handleSubGroup data: " + data);
    }
    String handleName = data.getString(TringWebApiServer.HANDLE_NAME);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    String gid = data.getString(HandleDAO.GID);
    if (gid != null && handleName != null) {
      handlesDAO.getHandleRoleInGroup(handleName, gid).subscribe(role -> {
        log.debug("Role found for the user in handleSubGroup: " + role);
        if (Utils.isModerator(role)) {
          String subGroupName = (data.getString(GroupDAO.GROUP_NAME)).toLowerCase();
          String action = data.getString("action");
          final String ADD_AS_MODERATOR = "addAsModeratorOfSubGroup";
          final String ADD_AS_MEMBER = "addAsMemberOfSubGroup";
          final String REMOVE_AS_MEMBER = "removeAsMemberOfSubGroup";
          final String REMOVE_AS_MODERATOR = "removeAsModeratorOfSubGroup";
          final String REMOVE_ADMIN_AS_MODERATOR = "removeAdminAsModeratorOfSubGroup";

          JsonObject resultData = new JsonObject();
          JsonArray warnings = new JsonArray();
          if ("CREATE".equals(action)) {
            JsonArray asModerators = data.getJsonArray(HandleDAO.AS_MODERATOR);
            JsonArray adminAsModerators = data.getJsonArray(HandleDAO.ADMIN_AS_MODERATOR);
            JsonArray asMembers = data.getJsonArray(HandleDAO.AS_MEMBER);

            if (!Strings.isNullOrEmpty(subGroupName)
                && ((adminAsModerators != null && adminAsModerators.size() > 0)
                    || (asModerators != null && asModerators.size() > 0))) {
              JsonObject subGroup = new JsonObject().put(GroupDAO.PARENT_GID, gid)
                  .put(GroupDAO.GROUP_NAME, subGroupName)
                  .put(GroupDAO.MQTT_TOPIC_PREFIX, "/groups/1/");
              Observable<String> subGroupIdObservable = groupsDAO.save(subGroup);

              handleAddAsMemberAndModerator(gid, asModerators, adminAsModerators, asMembers,
                  ADD_AS_MODERATOR, ADD_AS_MEMBER, resultData, warnings, subGroupIdObservable,
                  false).subscribe(bulkUpdateResult -> {
                logBulkUpdateResults("", ADD_AS_MEMBER);
                resultData.put(GroupDAO.MQTT_TOPIC_PREFIX, "/groups/1/");
                resultData.put("action", action);
                handleSendSyncSubGroupMessage(ctx, data, gid, subGroupName, asModerators,
                    adminAsModerators, asMembers, new JsonArray(), resultData, warnings);

              } , err -> {
                err.printStackTrace();
                ctx.response().end(new JsonObject().put("success", false)
                    .put("error", err.getMessage()).put("warning", warnings).encode());
              });

            } else {
              log.error("Subgroup name is null or empty");
              ctx.response()
                  .end(new JsonObject().put("success", false)
                      .put("error",
                          "Subgroup name is null or empty or no moderator has been assigned")
                  .encode());
            }
          } else if ("EDIT".equals(action)) {
            String subGroupId = data.getString(HandleDAO.SUBGROUP_GID);
            if (!Strings.isNullOrEmpty(subGroupId)) {
              Observable<String> subGroupIdObservable = Observable.just(subGroupId);
              JsonArray adminAsModerators = data.getJsonArray(HandleDAO.ADMIN_AS_MODERATOR); // addAsModeratorOfSubGroup
                                                                                             // -
                                                                                             // isAdmin=true,
                                                                                             // edit=true
              JsonArray asModerators = data.getJsonArray(HandleDAO.AS_MODERATOR); // addAsModeratorOfSubGroup
                                                                                  // -
                                                                                  // isAdmin=false,
                                                                                  // edit=true
              JsonArray asMembers = data.getJsonArray(HandleDAO.AS_MEMBER); // addAsMemberOfSubGroup
                                                                            // - call twice
              JsonArray asNonMembers = data.getJsonArray(HandleDAO.AS_NON_MEMBER); // removeAsMemberOfSubGroup
                                                                                   // - call twice
              JsonArray adminAsNonMembers = data.getJsonArray(HandleDAO.ADMIN_AS_NONMEMBER); // removeAdminAsModeratorOfSubGroup
              if (log.isDebugEnabled()) {
                log.debug("adminAsModerators: " + adminAsModerators);
                log.debug("asModerators: " + asModerators);
                log.debug("asMembers: " + asMembers);
                log.debug("asNonMembers: " + asNonMembers);
                log.debug("adminAsNonMembers: " + adminAsNonMembers);
              }
              handleAddAsMemberAndModerator(gid, asModerators, adminAsModerators, asMembers,
                  ADD_AS_MODERATOR, ADD_AS_MEMBER, resultData, warnings, subGroupIdObservable, true)
                      .flatMap(bulkUpdateResult -> {
                logBulkUpdateResults("", ADD_AS_MEMBER);
                return removeAsMemberOfSubGroup(gid, asNonMembers, subGroupId, true);
              }).flatMap(bulkUpdateResult -> {
                logBulkUpdateResults("checkAlreadyMembers true", ADD_AS_MEMBER);
                return removeAsMemberOfSubGroup(gid, asNonMembers, subGroupId, false);
              }).flatMap(bulkUpdateResult -> {
                logBulkUpdateResults("checkAlreadyMembers false", REMOVE_AS_MEMBER);
                return removeAsModeratorOfSubGroup(gid, asNonMembers, subGroupId, true);
              }).flatMap(bulkUpdateResult -> {
                logBulkUpdateResults("CheckAlreadyModerators", REMOVE_AS_MODERATOR);
                return removeAsModeratorOfSubGroup(gid, asNonMembers, subGroupId, false);
              }).flatMap(bulkUpdateResult -> {
                logBulkUpdateResults("No CheckAlreadyModerators", REMOVE_AS_MODERATOR);
                return removeAdminAsModeratorOfSubGroup(gid, adminAsNonMembers, subGroupId);
              }).subscribe(bulkUpdateResult -> {
                logBulkUpdateResults("adminAsNonMembers", "removeAdminAsModeratorOfSubGroup");
                JsonArray allNonMembers = new JsonArray();
                if (adminAsNonMembers != null) {
                  allNonMembers.addAll(adminAsNonMembers);
                }
                if (asNonMembers != null) {
                  allNonMembers.addAll(asNonMembers);
                }
                resultData.put("action", action);
                handleSendSyncSubGroupMessage(ctx, data, gid, subGroupName, asModerators,
                    adminAsModerators, asMembers, allNonMembers, resultData, warnings);
              } , err -> {
                err.printStackTrace();
                ctx.response().end(
                    new JsonObject().put("success", false).put("error", err.getMessage()).encode());
              });
            } else {
              log.error("Subgroup name is null or empty");
              ctx.response().end(new JsonObject().put("success", false)
                  .put("error", "Subgroup id is null or empty").encode());
            }
          } else {
            log.error("Invalid action in handleSubGroup: " + action);
            ctx.response().end(new JsonObject().put("success", false)
                .put("message", "Invalid action in handleSubGroup: " + action).encode());
          }
        }
      });
    }

  }

  public void handleAdmin(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    if (log.isDebugEnabled()) {
      log.debug("handleAdmin data: " + data);
    }
    String handleName = data.getString(TringWebApiServer.HANDLE_NAME);
    String gid = data.getString(HandleDAO.GID);
    String action = data.getString("action");
    String adminHid = data.getString("adminHid");
    if (gid != null && handleName != null && action != null && adminHid != null) {
      handleName = handleName.toLowerCase();
      handlesDAO.getHandleRoleInGroup(handleName, gid).subscribe(role -> {
        if (role == HandleDAO.HandleType.OWNER.ordinal()) {
          if ("add".equals(action)) {
            handlesDAO.addAsGroupAdmin(adminHid).subscribe(adminObj -> {
              handleAdminMessage(ctx, gid, action, adminObj);

            });
          } else if ("remove".equals(action)) {
            handlesDAO.removeAsGroupAdmin(adminHid).subscribe(adminObj -> {
              handleAdminMessage(ctx, gid, action, adminObj);
            });
          } else {
            ctx.response().end(new JsonObject().put("success", false)
                .put("error", "Invalid action: " + action).encode());
          }
        }
      });
    } else {
      ctx.response().end(new JsonObject().put("success", false)
          .put("error", "Invalid inputs: " + action).encode());
    }
  }

  private void handleAdminMessage(RoutingContext ctx, String gid, String action,
      JsonObject adminObj) {
    JsonObject admin = adminObj.getJsonObject("value");
    log.debug("The admin has been added: " + admin);
    JsonObject msg = getHandleAdminMessage(ctx, gid, action, admin);
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_ADMIN_ROLE, msg)
        .subscribe(replyMsg -> {
          JsonObject reply = (JsonObject) replyMsg.body();
          Boolean success = reply.getBoolean("success");
          if (success) {
            ctx.response().end(new JsonObject().put("success", true).encode());
          } else {
            ctx.response().end(new JsonObject().put("success", false)
                .put("error", "Error while sending MQTT messages.").encode());
          }
        } , err -> {
          log.error("Error while Message being sent to topic "
              + TringConstants.TRING_SERVER_HANDLE_ADMIN_ROLE + " :" + err.getMessage());
          err.printStackTrace();
        });
  }

  private JsonObject getHandleAdminMessage(RoutingContext ctx, String gid, String action,
      JsonObject admin) {
    JsonObject msg = new JsonObject();
    msg.put("action", action);
    msg.put("srcUser", (JsonObject) ctx.get("user"));
    msg.put(HandleDAO.GID, gid);
    msg.put("tgtUid", admin.getString(HandleDAO.UID));
    return msg;
  }

  private Observable<Void> handleAddAsMemberAndModerator(String gid, JsonArray asModerators,
      JsonArray adminAsModerators, JsonArray asMembers, final String ADD_AS_MODERATOR,
      final String ADD_AS_MEMBER, JsonObject resultData, JsonArray warnings,
      Observable<String> subGroupIdObservable, boolean edit) {
    return subGroupIdObservable.flatMap(subGroupId -> {
      resultData.put(HandleDAO.SUBGROUP_GID, subGroupId);
      return addAsModeratorOfSubGroup(gid, adminAsModerators, subGroupId, true, edit);
    }).flatMap(bulkUpdateResult -> {
      logBulkUpdateResults("Edit: " + edit + " As admin", ADD_AS_MODERATOR);
      return addAsModeratorOfSubGroup(gid, asModerators, resultData.getString(HandleDAO.SUBGROUP_GID),
          false, edit);
    }).flatMap(bulkUpdateResult -> {
      logBulkUpdateResults("Edit: " + edit + " Not As admin", ADD_AS_MODERATOR);
      log.debug(
          "Handles being added as members with checkAlreadyModerators as true : " + asMembers);
      return addAsMemberOfSubGroup(gid, asMembers, resultData.getString(HandleDAO.SUBGROUP_GID), true,
          edit);// Observable.just("");
    }).flatMap(bulkUpdateResult -> {
      logBulkUpdateResults("CheckAlreadyModerators", ADD_AS_MEMBER);
      log.debug(
          "Handles being added as members with checkAlreadyModerators as false : " + asMembers);
      return addAsMemberOfSubGroup(gid, asMembers, resultData.getString(HandleDAO.SUBGROUP_GID), false,
          edit);
    });
  }

  private void handleSendSyncSubGroupMessage(RoutingContext ctx, JsonObject data, String gid,
      String subGroupName, JsonArray asModerators, JsonArray adminAsModerators, JsonArray asMembers,
      JsonArray asNonMembers, JsonObject resultData, JsonArray warnings) {
    String hid = data.getString("hid");
    boolean addedSelf = hid != null && adminAsModerators.contains(hid) ? true : false;
    boolean removedSelf = hid != null && asNonMembers != null && asNonMembers.contains(hid);
    JsonObject msg = getSyncSubGroupMessage(ctx, gid, subGroupName, asModerators, adminAsModerators,
        asMembers, asNonMembers, resultData);
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_SUBGROUP, msg)
        .subscribe(replyMsg -> {
          JsonObject reply = (JsonObject) replyMsg.body();
          Boolean success = reply.getBoolean("success");
          if (success) {
            ctx.response()
                .end(new JsonObject().put("success", true)
                    .put(HandleDAO.SUBGROUP_GID, resultData.getString(HandleDAO.SUBGROUP_GID))
                    .put(GroupDAO.MQTT_TOPIC,
                        "/groups/1/" + resultData.getString(HandleDAO.SUBGROUP_GID))
                .put("addedSelf", addedSelf).put("removedSelf", removedSelf).encode());
          } else {
            ctx.response()
                .end(new JsonObject().put("success", false)
                    .put("error", "Error while sending MQTT messages.").put("warning", warnings)
                    .encode());
          }
        } , err -> {
          log.error("Error while Message being sent to topic "
              + TringConstants.TRING_SERVER_HANDLE_SUBGROUP + " :" + err.getMessage());
          err.printStackTrace();
        });
  }

  private JsonObject getSyncSubGroupMessage(RoutingContext ctx, String gid, String subGroupName,
      JsonArray asModerators, JsonArray adminAsModerators, JsonArray asMembers,
      JsonArray asNonMembers, JsonObject resultData) {
    JsonObject msg = new JsonObject();
    JsonArray allModerators = new JsonArray();
    if (adminAsModerators != null) {
      allModerators.addAll(adminAsModerators);
    }
    if (asModerators != null) {
      allModerators.addAll(asModerators);
    }
    msg.put(HandleDAO.AS_MODERATOR, allModerators);
    msg.put(HandleDAO.AS_MEMBER, asMembers);
    msg.put(HandleDAO.AS_NON_MEMBER, asNonMembers);
    msg.put(HandleDAO.SUBGROUP_GID, resultData.getString(HandleDAO.SUBGROUP_GID));
    msg.put(GroupDAO.MQTT_TOPIC, "/groups/1/" + resultData.getString(HandleDAO.SUBGROUP_GID));
    msg.put(GroupDAO.PARENT_GID, gid);
    msg.put(GroupDAO.GROUP_NAME, subGroupName);
    msg.put("action", resultData.getString("action"));
    msg.put("srcUser", (JsonObject) ctx.get("user"));
    return msg;
  }


  private void logBulkUpdateResults(String prefix, String ctx) {
    log.debug("bulkUpdateResult in context: " + ctx + " " + prefix);
  }

  private Observable<Void> addAsMemberOfSubGroup(String gid, JsonArray members, String subGroupId,
      boolean checkAlreadyModerators, boolean edit) {
    if (members == null || members.size() == 0) {
      return Observable.just(null);
    }
    JsonObject idQuery = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", members));
    idQuery.put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid);
    JsonArray andConds = new JsonArray();
    andConds.add(idQuery);
    andConds.add(new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
        new JsonObject().put("$ne", HandleDAO.HandleType.ADMIN.ordinal())));
    andConds.add(new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
        new JsonObject().put("$ne", HandleDAO.HandleType.OWNER.ordinal())));
    JsonObject query = new JsonObject().put("$and", andConds);
    JsonObject update = new JsonObject().put("$addToSet",
        new JsonObject().put(HandleDAO.SUB_GROUP_MEMBER, subGroupId));
    if (checkAlreadyModerators) {
      query = getSubGroupModeratorSizeCondition(
          "this." + HandleDAO.SUB_GROUP_MODERATOR + ".length > 0", query);
    } else {
      query = getSubGroupModeratorSizeCondition(
          "this." + HandleDAO.SUB_GROUP_MODERATOR + ".length == 0", query);
      update.put("$set", new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
          HandleDAO.HandleType.NONMEMBER.ordinal()));
    }

    if (edit) {
      update.put("$pull", new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, subGroupId));
    }
    return handlesDAO.bulkUpdate(query, update);
  }

  private Observable<Void> removeAsMemberOfSubGroup(String gid, JsonArray members, String subGroupId, boolean checkAlreadyMembers) {
    if (members == null || members.size() == 0) {
      return Observable.just(null);
    }
    JsonObject query = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", members));
    query.put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid);
    JsonObject update =
        new JsonObject().put("$pull", new JsonObject().put(HandleDAO.SUB_GROUP_MEMBER, subGroupId));

    if (checkAlreadyMembers) {
      // All members will be removed in this pass since there will be non-zero subGroupMember array size
      query.put(HandleDAO.SUB_GROUP_MEMBER, new JsonObject().put("$exists", true)).put("$where", "this." + HandleDAO.SUB_GROUP_MEMBER + ".length > 0");
    } else {
      // Update parent group membership to member only for those members whose subGroupMember array size is 0
      query.put(HandleDAO.SUB_GROUP_MEMBER, new JsonObject().put("$exists", true)).put("$where", "this." + HandleDAO.SUB_GROUP_MEMBER + ".length == 0");
      update.put("$set", new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
          HandleDAO.HandleType.MEMBER.ordinal()));
    }
    return handlesDAO.bulkUpdate(query, update);
  }

  private Observable<Void> removeAsModeratorOfSubGroup(String gid, JsonArray members, String subGroupId, boolean checkAlreadyModerators) {
    if (members == null || members.size() == 0) {
      return Observable.just(null);
    }
    JsonObject query = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", members));
    query.put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid);
    
    JsonObject update = new JsonObject().put("$pull",
        new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, subGroupId));

    if (checkAlreadyModerators) {
      // All moderators will be removed in this pass since there will be non-zero subGroupModerator array size 
      query = getSubGroupModeratorSizeCondition(
          "this." + HandleDAO.SUB_GROUP_MODERATOR + ".length > 0", query);
    } else {
      // Update parent group membership to non-member only for those members whose subGroupModerator array
      // size is 0 and whose subGroupMember array size is > 0
      query = getSubGroupMemberExistsCondition(
          "this." + HandleDAO.SUB_GROUP_MODERATOR + ".length == 0", "this." + HandleDAO.SUB_GROUP_MEMBER + ".length > 0", query);
      update.put("$set", new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
          HandleDAO.HandleType.NONMEMBER.ordinal()));
    }
    return handlesDAO.bulkUpdate(query, update);
  }

  private JsonObject getSubGroupMemberExistsCondition(String moderatorCondStr, String memberCondStr, JsonObject query) {
    JsonObject subGroupModeratorsSizeCond = new JsonObject().put("$where", moderatorCondStr);
    JsonObject subGroupModeratorsExistsCond =
        new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, new JsonObject().put("$exists", true));
    JsonArray andConds = new JsonArray().add(subGroupModeratorsExistsCond).add(subGroupModeratorsSizeCond);
    
    JsonObject subGroupMembersSizeCond = new JsonObject().put("$where", memberCondStr);
    JsonObject subGroupMembersExistsCond =
        new JsonObject().put(HandleDAO.SUB_GROUP_MEMBER, new JsonObject().put("$exists", true));
    JsonArray _andConds = new JsonArray().add(subGroupMembersExistsCond).add(subGroupMembersSizeCond);
    
    JsonArray andConditions = new JsonArray().add(query).add(new JsonObject().put("$and", andConds)).add(new JsonObject().put("$and", _andConds));
    query = new JsonObject().put("$and", andConditions);
    return query;
  }
  
  private JsonObject getSubGroupModeratorSizeCondition(String condStr, JsonObject query) {
    JsonObject subGroupSizeCond = new JsonObject().put("$where", condStr);
    JsonObject subGroupExistsCond =
        new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, new JsonObject().put("$exists", true));
    JsonObject subGroupNotExistsCond =
        new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, new JsonObject().put("$exists", false));
    JsonArray andConds = new JsonArray().add(subGroupExistsCond).add(subGroupSizeCond);
    JsonObject subGroupExistsButEmpty = new JsonObject().put("$and", andConds);
    JsonArray orConds = new JsonArray().add(subGroupNotExistsCond).add(subGroupExistsButEmpty);
    JsonArray andConditions = new JsonArray().add(query).add(new JsonObject().put("$or", orConds));
    query = new JsonObject().put("$and", andConditions);
    return query;
  }

  private Observable<Void> addAsModeratorOfSubGroup(String gid, JsonArray moderators, String subGroupId,
      boolean isAdmin, boolean edit) {
    if (moderators == null || moderators.size() == 0) {
      return Observable.just(null);
    }
    JsonObject query = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", moderators));
    query.put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid);
    JsonObject update = new JsonObject();
    update.put("$addToSet", new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, subGroupId));
    // If the moderators are not admins then add them as members of the parent group
    if (!isAdmin) {
      update.put("$set", new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.TYPE,
          HandleDAO.HandleType.MEMBER.ordinal()));
      if (edit) {
        update.put("$pull", new JsonObject().put(HandleDAO.SUB_GROUP_MEMBER, subGroupId));
      }
    }
    return handlesDAO.bulkUpdate(query, update);

  }

  private Observable<Void> removeAdminAsModeratorOfSubGroup(String gid, JsonArray moderators,
      String subGroupId) {
    if (moderators == null || moderators.size() == 0) {
      return Observable.just(null);
    }
    JsonObject query = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", moderators));
    query.put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid);
    JsonObject update = new JsonObject();
    update.put("$pull", new JsonObject().put(HandleDAO.SUB_GROUP_MODERATOR, subGroupId));
    return handlesDAO.bulkUpdate(query, update);
  }

  public void getUserReputation(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    String handleName = (ctx.request().getParam(TringWebApiServer.HANDLE_NAME)).toLowerCase();
    JsonObject usr = ctx.get("user");
    String uid = null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    
    // final String userId =uid;
    handlesDAO.getUserRoleInGroup(uid, gid).subscribe(role -> {
      log.debug("Role found for the requesting user in getUserReputation: " + role);
      if (Utils.isAdmin(role) || Utils.isOwner(role)) {
        handlesDAO.handleFromHandleNameAndGroupId(handleName, gid, new JsonObject())
            .subscribe(hdl -> {
          String userId = hdl.getString(HandleDAO.UID);
          usersDAO.findById(userId, new JsonObject().put(UserDAO.ABUSES, 1)
              .put(UserDAO.EVICTIONS, 1).put(UserDAO.WARNINGS, 1)).subscribe(user -> {
            if (user != null) {
              if (user.getInteger(UserDAO.ABUSES.toString()) == null) {
                user.put(UserDAO.ABUSES, 0);
              }
              if (user.getInteger(UserDAO.EVICTIONS.toString()) == null) {
                user.put(UserDAO.EVICTIONS, 0);
              }
              if (user.getInteger(UserDAO.WARNINGS.toString()) == null) {
                user.put(UserDAO.WARNINGS, 0);
              }
              ctx.response().end(user.encode());
            } else {
              ctx.fail(400);
            }
          } , err -> {
            ctx.fail(400);
          });
        } , err -> {
          ctx.fail(401);
        });
      } else {
        ctx.fail(401);
      }
    } , err -> {
      ctx.fail(401);
    });
  }

  public void leaveGroup(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    JsonObject usr = ctx.get("user");
    String uid = null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    final String userId = uid;
    handlesDAO.getRolesForGroupAndSubGroups(uid, gid).subscribe(roles -> {
      if (roles != null) {
        Observable.from(roles).map(rl -> {
          return rl.getInteger(HandleDAO.TYPE);
        }).subscribe(type -> {
          if (HandleType.ADMIN.ordinal() == (type) || HandleType.OWNER.ordinal() == (type)
              || HandleType.MODERATOR.ordinal() == (type)) {
            ctx.fail(400);
          } else {
            JsonObject query = new JsonObject().put(HandleDAO.UID, userId)
                .put(HandleDAO.PARENT_GROUP_MEMBER + "." + HandleDAO.GID, gid)
                .put(HandleDAO.STATUS, HandleStatus.ACTIVE.ordinal());
            handlesDAO.findOneAndUpdate(query,
                new JsonObject().put("$set",
                    new JsonObject().put(HandleDAO.STATUS, HandleStatus.LEFT.ordinal())),
                true, false).subscribe(r -> {
                  //Tell Solr to update the member count
                  groupsDAO.findById(gid, null).subscribe(grp->{
                    if (!grp.getBoolean(GroupDAO.CLOSED_GROUP)) {
                      ctx.put("group", grp);
                      ctx.put("increment", false); //means decrement count
                      ctx.next();
                    }
                  },err->{
                    log.error(err);
                    //ctx.fail(401);
                  });
                  ctx.response().end(new JsonObject().put("success", true).encode());
            } , err -> {
              log.error(err);
              ctx.response().end(new JsonObject().put("success", false).encode());
            });
          }
        });
      } else {
        // ctx.response().end(new JsonArray().add(roles).encode());
        ctx.fail(402);
      }
    } , err -> {
      log.error(err);
      ctx.fail(403);
    });

  }



  public void setRatingAndReview(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    JsonObject body = ctx.getBodyAsJson();
    String review = body.getString(TringWebApiServer.REVIEW);
    Integer rating = body.getInteger(TringWebApiServer.RATING);
    JsonObject usr = ctx.get("user");
    String uid = null;
    if (null != usr) {
      uid = usr.getString(UserDAO.ID);
    }
    final String userId = uid;
    if (rating != null || review != null) {
      MongoAsyncSerialTasksTransaction trans = new MongoAsyncSerialTasksTransaction(transactionDAO);
      trans.task((t, future, obj) -> {
        String transactionId =
            (String) trans.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID);
        handlesDAO.setRatingAndReview(userId, gid, rating, review, transactionId)
            .subscribe(jobj -> {
          if (jobj != null) {
            future.complete();
          } else {
            future.fail("error");
          }
        } , err -> {
          future.fail("error");
        });
      })
          /*
           * .task((t, future, obj) -> { groupsDAO.getRatingAndCount(gid).subscribe(data->{ String
           * transactionId = (String)
           * trans.getValue(MongoAsyncSerialTasksTransaction.TRANSACTION_ID); Integer count =
           * data.getInteger(GroupDAO.RATING_COUNT); if (count == null) count =0; Double oldRating =
           * data.getDouble(GroupDAO.MEAN_RATING); if (oldRating == null) oldRating =0.0;
           * 
           * double rtng = Math.abs((oldRating*count + rating)/(++count)); final int cnt = count;
           * groupsDAO.setMeanRatingAndCount(gid,rtng,count,transactionId).subscribe(jobj->{ if
           * (jobj != null && jobj.getBoolean("success")) {
           * t.setValue(TringWebApiServer.MEAN_RATING,rtng);
           * t.setValue(TringWebApiServer.RATING_COUNT,cnt); future.complete(); }else {
           * future.fail("error"); } },err->{ future.fail("error"); }); }); })
           */
          .onSuccess(() -> {
            ctx.response()
                .end(new JsonObject().put("success", true)
                    /*
                     * .put("meanRating", trans.getValue(TringWebApiServer.MEAN_RATING))
                     * .put("ratingCount", trans.getValue(TringWebApiServer.RATING_COUNT))
                     */
                    .put("success", true).encode());
          }).onFailure(rollbackSuccess -> {
            ctx.fail(400);
          }).execute();
    } else {
      ctx.fail(400);
    }
  }

  public void getRatingAndReviews(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);

    JsonObject fields =
        new JsonObject().put(HandleDAO.RATING, 1).put(HandleDAO.REVIEW, 1).put(HandleDAO.NAME, 1);
    handlesDAO.handlesForParentGid(gid, new JsonObject(), fields).subscribe(lst -> {
      if (lst == null || lst.isEmpty()) {
        int count = 0;
        ctx.response().end(new JsonObject().put(TringWebApiServer.RATING_COUNT, count).encode());
        return;
      }
      Observable<List<JsonObject>> reviews = Observable.from(lst).flatMap(obj -> {
        String review = obj.getString(HandleDAO.REVIEW);
        Integer rating = obj.getInteger(HandleDAO.RATING);
        if ((review == null || review.isEmpty()) && (rating == null || rating == 0)) {
          return null;
        } else {
          String hname = obj.getString(HandleDAO.NAME);
          return Observable.just(new JsonObject().put(HandleDAO.REVIEW, review)
              .put(HandleDAO.RATING, rating).put(TringWebApiServer.HANDLE_NAME, hname));
        }
      }).filter(obj -> {
        return obj != null;
      }).limit(PAGE_LENGTH).toList();

      Observable<Double> ratings = Observable.from(lst).flatMap(obj -> {
        Integer rtng = obj.getInteger(HandleDAO.RATING);
        Double rating = (double) (rtng == null ? 0.0 : (double) rtng);
        if (rating != null && (rating != 0.0)) {
          return Observable.just(rating);
        } else {
          return null;
        }
      }).filter(obj -> {
        return obj != null;
      });
      Observable<Double> averageRating = MathObservable.averageDouble(ratings);

      reviews.subscribe(list ->{
        ratings.count().subscribe(cnt -> {
          if (cnt != null && cnt > 0) {
            averageRating.subscribe(a -> {
              ctx.response()
              .end(new JsonObject().put(TringWebApiServer.REVIEWS, new JsonArray(list))
                  .put(TringWebApiServer.MEAN_RATING, a).put(TringWebApiServer.RATING_COUNT, cnt)
                  .put(TringWebApiServer.PAGE_OFFSET,
                      list.size() < PAGE_LENGTH ? list.size() : PAGE_LENGTH)
                  .encode());
            } , err -> {
              ctx.fail(403);
            });
          } else {
            ctx.response().end(new JsonObject().put(TringWebApiServer.RATING_COUNT, 0).encode());
          }
        } , err -> {
          ctx.fail(400);
        });
      }, err -> {
        ctx.fail(401);
      });
    } , err -> {
      ctx.fail(402);
    });
  }

  public void getReviews(RoutingContext ctx) {
    String gid = ctx.request().getParam(TringWebApiServer.GID);
    String startStr = ctx.request().getParam(TringWebApiServer.OFFSET);
    int start = (startStr == null || startStr.isEmpty()) ? 0 : Integer.parseInt(startStr);

    JsonObject fields = new JsonObject().put(HandleDAO.REVIEW, 1).put(HandleDAO.NAME, 1);
    handlesDAO.handlesForParentGid(gid, new JsonObject(), fields).subscribe(lst -> {
      if (lst == null || lst.isEmpty()) {
        int count = 0;
        ctx.response().end(new JsonObject().put(TringWebApiServer.RATING_COUNT, count).encode());
        return;
      }
      Observable<List<JsonObject>> reviews = Observable.from(lst).flatMap(obj -> {
        String review = obj.getString(HandleDAO.REVIEW);
        Integer rating = obj.getInteger(HandleDAO.RATING);
        String hname = obj.getString(HandleDAO.NAME);

        if (review != null && !review.isEmpty() || rating != null && rating != 0) {
          return Observable.just(new JsonObject().put(HandleDAO.REVIEW, review)
              .put(HandleDAO.RATING, rating).put(TringWebApiServer.HANDLE_NAME, hname));

        } else {
          return null;
        }
      }).filter(obj -> {
        return obj != null;
      }).skip(start).limit(PAGE_LENGTH).toList();

      reviews.subscribe(list->{
      ctx.response()
          .end(new JsonObject().put(TringWebApiServer.REVIEWS, new JsonArray(list))
              .put(TringWebApiServer.PAGE_OFFSET,
                  list.size() < PAGE_LENGTH ? (start + list.size()) : (start + PAGE_LENGTH))
          .encode());
      }, err->{
        ctx.fail(401);
      });
    } , err -> {
      ctx.fail(400);
    });
  }
}
