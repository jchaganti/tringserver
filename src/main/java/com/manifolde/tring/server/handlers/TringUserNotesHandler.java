package com.manifolde.tring.server.handlers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.google.common.base.Strings;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserNotesDAO;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.RoutingContext;

public class TringUserNotesHandler extends TringBaseHandler {
  private static final Logger log = LoggerFactory.getLogger(TringUserNotesHandler.class);

  private UserNotesDAO userNotesDAO;
  private HandleDAO handlesDAO;

  public TringUserNotesHandler(Vertx vertx, UserNotesDAO userNotesDAO, HandleDAO handlesDAO) {
    super(vertx);
    this.userNotesDAO = userNotesDAO;
    this.handlesDAO = handlesDAO;
  }

  public void createNotes(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject moderator = ctx.get("user");
    JsonObject data = ctx.getBodyAsJson();
    String handleName = data.getString(UserNotesDAO.HANDLE_NAME);
    String gid = data.getString(HandleDAO.GID);
    String notes = data.getString(UserNotesDAO.NOTES);
    if(log.isDebugEnabled()) {
      log.debug("User Notes data: " + data);
    }
    if(!Strings.isNullOrEmpty(handleName) && !Strings.isNullOrEmpty(gid) && !Strings.isNullOrEmpty(notes) ) {
      handleName = handleName.toLowerCase();
      JsonObject userNotes = new JsonObject();
      userNotes.put(UserNotesDAO.MODERATOR_ID, moderator.getString("_id"));
      userNotes.put(UserNotesDAO.HANDLE_NAME, handleName);
      userNotes.put(HandleDAO.GID, gid);
      userNotes.put(UserNotesDAO.NOTES, notes);
      userNotesDAO.save(userNotes).
      subscribe(notesId -> {
        ctx.response().end(new JsonObject().put("success", true)
            .put("notesId", notesId)
            .encode());      
        }, err -> {
        log.error("User Notes could not be saved");
        ctx.fail(401);
      });
    }
    else {
      ctx.response().end(new JsonObject().put("success", false)
          .put("notesId", (String)null)
          .encode());
    }
  }
  
  public void getNotes(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject moderator = ctx.get("user");
    String handleName = ctx.request().getParam(UserNotesDAO.HANDLE_NAME);
    String gid = ctx.request().getParam(HandleDAO.GID);
    try {
      handleName = (URLDecoder.decode(handleName, "UTF-8")).toLowerCase();
      gid = URLDecoder.decode(gid, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      log.error(e);
    }
    
    String adminContext = ctx.request().params().get("adminContext");
    log.debug("Get Notes HandleName: " + handleName);
    log.debug("Get Notes gid: " + gid);
    JsonObject  handleWrapper = new JsonObject().put("handleName", handleName).put("gid", gid);
    if (!Strings.isNullOrEmpty(handleName) && !Strings.isNullOrEmpty(gid)) {
      if("true".equals(adminContext)) {
        getUserNotes(ctx, moderator, handleName, gid);
      }
      else {
        handlesDAO.handleFromHandleNameAndGroupId(handleName, gid,
            new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER, 1)
                .put(HandleDAO.SUB_GROUP_MODERATOR, 1))
            .subscribe(handle -> {
              if (handle != null) {
                if(log.isDebugEnabled()) {
                  log.debug("Handle found: " + handle);
                }
                int membershipType = handle.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER).getInteger(HandleDAO.TYPE);
                JsonArray moderatingSubGroups = handle.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
                if((membershipType < HandleDAO.HandleType.ADMIN.ordinal()) || (moderatingSubGroups != null && moderatingSubGroups.size() > 0)) {
                  JsonObject userNotesJson = new JsonObject();
                  userNotesJson.put("success", false);
                  userNotesJson.put("message", "This handle is a owner or admin of the group or moderator of one of the subgroup. Hence cannot be moderated");
                  ctx.response().end(userNotesJson.encode());
                }
                else {
                  getUserNotes(ctx, moderator, handleWrapper.getString("handleName"), handleWrapper.getString("gid"));
                }
              } else {
                ctx.fail(401);
              }
            } , err -> {
              err.printStackTrace();
              ctx.fail(401);
            });
      }
      
    } else {
      ctx.fail(401);
    }
  }

  private void getUserNotes(RoutingContext ctx, JsonObject moderator, String handleName,
      String gid) {
    JsonObject query = new JsonObject().put(UserNotesDAO.MODERATOR_ID, moderator.getString("_id"))
        .put(UserNotesDAO.HANDLE_NAME, handleName).put(HandleDAO.GID, gid);
    userNotesDAO.find(query, new JsonObject().put(UserNotesDAO.NOTES, 1)
        .put(UserNotesDAO.CREATED, 1).put(UserNotesDAO.MODIFIED, 1)).subscribe(userNotes -> {
          JsonObject userNotesJson = new JsonObject();
          JsonArray userNotesInfo = new JsonArray();
          userNotes.stream().forEach(userNote -> {
            userNotesInfo.add(userNote);
          });
          userNotesJson.put("userNotesInfo", userNotesInfo);
          userNotesJson.put("success", true);
          if (log.isDebugEnabled()) {
            log.debug("UserNotesInfo obtained = " + userNotesJson);
          }
          ctx.response().end(userNotesJson.encode());
        } , err -> {
          ctx.fail(401);
        });
  }
  
  public void updateNotes(RoutingContext ctx) {
    verifyUser(ctx);
    JsonObject data = ctx.getBodyAsJson();
    String notesId = data.getString("notesId");
    String notes = data.getString(UserNotesDAO.NOTES);
    JsonObject query = new JsonObject().put("_id", notesId);
    JsonObject update = new JsonObject();
    if(!Strings.isNullOrEmpty(notes) && !Strings.isNullOrEmpty(notesId)) {
      update.put(UserNotesDAO.NOTES, notes);
      userNotesDAO.findOneAndUpdate(query, new JsonObject().put("$set", update), true, false).
      subscribe(modifiedNote -> {
        ctx.response().end(new JsonObject().put("success", true).encode());
      }, err -> {
        ctx.fail(401);
      });
    }
    else {
      ctx.fail(401);
    }
  }
  
  public void deleteNotes(RoutingContext ctx) {
    verifyUser(ctx);
    String notesId = ctx.request().getParam("notesId");
    JsonObject query = new JsonObject().put("_id", notesId);
    userNotesDAO.delete(query).
    subscribe(updated -> {
      ctx.response().end(new JsonObject().put("success", true).encode());
    }, err -> {
      ctx.response().end(new JsonObject().put("success", false).encode());
      log.error("Could not delete notes for the query: " + query);
    });
  }
}
