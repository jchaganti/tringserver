package com.manifolde.tring.server.handlers;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;
import com.manifolde.tring.server.dao.ContentDAO;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.FileUpload;
import io.vertx.rxjava.ext.web.RoutingContext;

public class TringContentHandler extends TringBaseHandler {
  private ContentDAO contentDAO;
  private String vaultLocation;
  private static final Logger log = LoggerFactory.getLogger(TringContentHandler.class);
  
  public TringContentHandler(Vertx vertx, ContentDAO contentDAO, JsonObject serverProperties) {
    super(vertx);
    this.contentDAO = contentDAO;
    String vaultDir =  serverProperties.getString("tring.server.vault.dir");
    if(!Strings.isNullOrEmpty(vaultDir)) {
      if(vaultDir.endsWith(File.separator)) {
        vaultDir = StringUtils.substringBeforeLast(vaultDir, File.separator);
      }
      this.vaultLocation = vaultDir;
      log.debug("Vault location is "+vaultDir);
    }
    else {
      log.error("The vault location has not been configured");
    }
  }
  
  public void handleContent(RoutingContext ctx) {
    log.debug("handleContent...");
    verifyUser(ctx);
    JsonObject content = new JsonObject();
    for (FileUpload f : ctx.fileUploads()) {
      populateContent(ctx, content, f);
      contentDAO.save(content).
      flatMap(id -> {
        ctx.response().end(new JsonObject().put("contentId", id).put("contentType", f.contentType()).encode());
        JsonObject msg = getMessage(f, id);
        if(log.isDebugEnabled()) {
          log.debug("Message to address " + TringConstants.TRING_SERVER_COPY_CONTENT_TO_VAULT + ": " + msg);
        }
        return vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_COPY_CONTENT_TO_VAULT, msg);
      }).
      subscribe(msg -> {
        JsonObject reply = (JsonObject) msg.body();
        if(reply.getBoolean("success")) {
          String contentId = reply.getString("contentId");
          String targetPath = reply.getString("targetPath");
          contentDAO.updatePath(contentId, targetPath).subscribe(v -> {
            log.debug("The content with Id: " + contentId + " has been successfully vaulted");
            // Now delete the old content from uploaded directory
            deleteContentFromCache(f);
          }, err -> {
            log.error("The content with Id: " + contentId + " could not be updated for vault location");
          });
        }
        else {
          log.error("The content with Id: " + reply.getString("contentId") + " has failed to be vaulted");
        }
      } , err -> {
        log.error(Arrays.toString(err.getStackTrace()));
      });
    }
  }

  private void deleteContentFromCache(FileUpload f) {
    JsonObject deleteMessage = new JsonObject().put("uploadedFileName", f.uploadedFileName());
    deleteFile(deleteMessage, null, null);
  }

  private void deleteFile(JsonObject deleteMessage, String contentId, RoutingContext ctx) {
    vertx.eventBus().
    sendObservable(TringConstants.TRING_SERVER_DELETE_UPLOADED_CONTENT, deleteMessage).
    subscribe(deleteReplyMsg -> {
      JsonObject deleteReply = (JsonObject) deleteReplyMsg.body();
      if(deleteReply.getBoolean("success")) {
        log.debug("The  content has been removed for the delete message:" + deleteMessage);
        if(contentId != null) {
          contentDAO.delete(contentId).subscribe(v -> {
            log.debug("The contentId: " + contentId + " has been deleted");
            if(ctx != null) {
              ctx.response().end(new JsonObject().put("success", true).encode());
            }
          }, err-> {
            log.error("The contentId: " + contentId + " could not be deleted");
            if(ctx != null) {
              ctx.response().end(new JsonObject().put("success", false).encode());
            }
          });
        }
      }
      else {
        log.error("The content could not be removed for the delete message: " + deleteMessage);
        if(ctx != null) {
          ctx.response().end(new JsonObject().put("success", false).encode());
        }
      }
    });
  }

  private void populateContent(RoutingContext ctx, JsonObject content, FileUpload f) {
    JsonObject user = ctx.get("user");
    String userId = user.getString("_id");
    content.put("userId", userId);
    content.put("name", f.fileName());
    content.put("path", f.uploadedFileName());
    content.put("size", f.size());
    content.put("contentType", f.contentType());
    log.debug("The uploaded content being saved is: " + content);
  }

  private JsonObject getMessage(FileUpload f, String id) {
    String fileName = StringUtils.substringAfterLast(f.uploadedFileName(), File.separator);
    String newDir = Utils.computePathForFileName(fileName, (int)f.size());
    String targetPath = vaultLocation + newDir + fileName;
    JsonObject msg = new JsonObject().
        put("contentId", id).
        put("sourcePath", f.uploadedFileName()).
        put("targetPath", targetPath);
    return msg;
  }
  
  public void deleteContentById(RoutingContext ctx) {
    verifyUser(ctx);
    String contentId = ctx.request().getParam("contentId");
    if(contentId != null) {
      contentDAO.findById(contentId, null)
      .subscribe(content -> {
        JsonObject deleteMessage = new JsonObject().put("uploadedFileName", content.getString(ContentDAO.PATH));
        deleteFile(deleteMessage, contentId, ctx);
      }, err-> {
        ctx.fail(401);
      });
    }
    else {
      ctx.fail(401);
    }
  }
}
