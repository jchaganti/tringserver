package com.manifolde.tring.server.dao;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.netty.util.internal.ThreadLocalRandom;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

@SuppressWarnings("unused")
public class BaseUserToSyncDAO extends BaseModelDAO {

  private static final Logger log = LoggerFactory.getLogger(BaseUserToSyncDAO.class);
  public static final String SRC_MOBILE_NO = "srcMobileNo";
  public static final String TGT_UID = "tgtUid";
  public static final String SRC_UID = "srcUid";
  public static final String SYNC_STATUS = "syncStatus";
  public static final String HANDLE_NAME = TringWebApiServer.HANDLE_NAME;
  public static final String REJECTED = TringWebApiServer.REJECTED;
  public static final String GID = TringWebApiServer.GID;
  public static final String ID = "_id";
  
  public BaseUserToSyncDAO(String collection, MongoClient mongoClient) {
    super(collection, mongoClient);
  }
  
  public BaseUserToSyncDAO(String collection, MongoClient mongoClient, TransactionDAO transactionDAO) {
	    super(collection, mongoClient, transactionDAO);
	  }

  public   Observable<List<JsonObject>> unSyncRecords() {
	DateTime now = new DateTime(DateTimeZone.UTC);
	JsonObject dateField = new JsonObject().put("$date", now.toString());
	JsonObject query = new JsonObject().put(SYNC_STATUS, false).put(BaseModelDAO.MODIFIED, new JsonObject().put("$lt", dateField));
	return find(query, null);
  }
}
