package com.manifolde.tring.server.dao;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.netty.util.internal.ThreadLocalRandom;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

@SuppressWarnings("unused")
public class UserDAO extends BaseModelDAO {
  private static final Logger log = LoggerFactory.getLogger(UserDAO.class);
  public static final String CUSTOM_MESSAGE = "customMessage";
  public static final String NAME = "name";
  public static final String CONTENT_ID = "contentId";
  public static final String MODERATOR_MESSAGE = "moderatorMessage";
  public static final String THUMBNAIL_DATA = "thumbnailData";
  public static final String TOPIC_PREFIX = "topicPrefix";
  public static final String MOBILE_NO = "mobileNo";
  public static final String USER_VERIFICATION = "userVerification";
  public static final String PROFILE_COUNTER = "profileCounter";
  public static final String PROFILE_HANDLED = "profileHandled";
  public static final String MODERATOR_MESSAGE_TYPE = "moderatorMessageType";
  public static final String ABUSES = "abuses";
  public static final String WARNINGS = "warnings";
  public static final String EVICTIONS = "evictions";
  public static final String PN_ID = "pnId";
  // handledContacts field indicates that we can start sending messages to
  // the contacts which contain this user in their contact to update their
  // local database
  public static final String HANDLED_CONTACTS = "handledContacts";
  public static final String TOPIC_TAIL_NAME_PREFIX = "user_";
  public static final String ID = "_id";
  public static final String NOTIFICATIONS = "notifications";
  public static final String NOTI_INDIVIDUAL = "individual";
  public static final String NOTI_GROUP = "group";
  public static final String NOTI_ADMIN = "admin";
  public static final String OCCUPATION = "occupation";
  public static final String INTEREST = "interest";
  public static final String WEBSITE = "website";
  public static final String PAYPALID = "paypalId";
  public static final String DEVICES = "devices";
  
  private static String uuidFromBase64(String str) {
    byte[] bytes = Base64.decodeBase64(str);
    ByteBuffer bb = ByteBuffer.wrap(bytes);
    UUID uuid = new UUID(bb.getLong(), bb.getLong());
    return uuid.toString();
  }

  
  public UserDAO(MongoClient mongoClient, TransactionDAO transactionDAO) {
    super("users", mongoClient, transactionDAO);
  }

  private void addDevice(final JsonObject device, JsonObject existing) {
    JsonArray arr = existing.getJsonArray("devices");
    if (arr == null) {
      existing.put("devices", new JsonArray().add(device));
    } else {
      arr.forEach(_device -> {
        ((JsonObject)_device).put("active", false);
      });
      existing.put("devices", arr.add(device));
    }
  }

  public Observable<JsonObject> addDevice(String userId, String imei, String platform) {
    if (userId == null) {
      log.debug("addDevice: userId is null");
      return Observable.just((JsonObject)null);
    } else {
      log.debug("addDevice: userId is non-null");
      final JsonObject device = new JsonObject();
      String _did = Utils.uuidToBase64(UUID.randomUUID());
      log.debug("The platform of the device: " + platform);
      String did = platform != null && platform.indexOf("ios") >= 0? _did + "_a": _did + "_g";
      device.put("did", did);
      device.put("imei", imei);
      device.put("active", true);
      int pin;
      if(Utils.isDevMode()) {        
        pin = TringConstants.DEV_MODE_PIN;
      }
      else {
        pin = ThreadLocalRandom.current().nextInt(100001, 999999);
      }
      device.put("pin", pin);
      
      DateTime now = new DateTime( DateTimeZone.UTC ) ;
      JsonObject dateField = new JsonObject().put("$date", now.toString());
      device.put("pinCreated", dateField);
      device.put("verifiedTS", (JsonObject) null);
      device.put("verificationStatus", Boolean.FALSE);
      log.debug("About to update device");
      return update(userId,  existing  -> {
        addDevice(device, existing);
      }).flatMap(v -> {
        log.debug("Added device");
        return Observable.just(new JsonObject().put("did", did).put("pin", pin));
      });
    }
  }


  public Observable<String> newUser(String mobileNo) {
    final JsonObject user = new JsonObject();
    final JsonObject notifications = new JsonObject();
    
    user.put(MOBILE_NO, mobileNo);
    user.put(CUSTOM_MESSAGE, "Hi, I am using Tring Chat!");
    user.put(TOPIC_PREFIX, "/p2p/1/");
    user.put("devices", new JsonArray());
    user.put(USER_VERIFICATION, false);
    DateTime now = new DateTime( DateTimeZone.UTC ) ;
    JsonObject dateField = new JsonObject().put("$date", now.toString());
    user.put("lastSeen", dateField);
    
    notifications.put(NOTI_INDIVIDUAL, TringConstants.NOTI_INDIVIDUAL);
    notifications.put(NOTI_GROUP, TringConstants.NOTI_GROUP);
    notifications.put(NOTI_ADMIN, TringConstants.NOTI_ADMIN);
    user.put(NOTIFICATIONS, notifications);

    if(log.isDebugEnabled()) {
      log.debug("The user being saved is: " + user);
    }
    return save(user);
  }

  public Observable<Void> updateDevice(String userId, String deviceId) {
    if (userId == null || deviceId == null) {
      return Observable.just((Void)null);
    } else {
      return update(userId, existing -> {
        updateDeviceStatus(userId, deviceId, existing);
      });
    }
  }

  private void updateDeviceStatus(String userId, String deviceId, JsonObject existing) {
    JsonArray arr = existing.getJsonArray("devices");
    existing.put(HANDLED_CONTACTS, false);
    existing.put(USER_VERIFICATION, true);
    Optional<Object> optional = arr.stream().filter(json -> {
      JsonObject o = (JsonObject) json;
      return (deviceId.equals(o.getString("did")));
    }).findFirst();
    if (optional.isPresent()) {
      JsonObject json = (JsonObject) optional.get();
      DateTime now = new DateTime( DateTimeZone.UTC ) ;
      JsonObject dateField = new JsonObject().put("$date", now.toString());
      json.put("verifiedTS", dateField);
      json.put("verificationStatus", Boolean.TRUE);
    } else {
      Observable.just(new Throwable("Device id = " + deviceId + " not found for user id =" + userId));
    }
  }

  public Observable<JsonObject> userByDeviceId(String did) {
    return findOne(new JsonObject().put("devices.did", new JsonObject().put("$in",new JsonArray().add(did))), null);
  }

  public Observable<JsonObject> userByUid(String uid) {
      return findOne(new JsonObject().put(ID, uid), null);
  }
  
  public Observable<List<JsonObject>> usersByDeviceIds(JsonArray devices, JsonObject fields) {
    return find(new JsonObject().put("devices.did", new JsonObject().put("$in", devices)), fields);
  }
  
  public Observable<JsonObject> usersByPnId(String pnid) {
      return findOne(new JsonObject().put(PN_ID, pnid), null);
  }
  
  public Observable<List<JsonObject>> usersByUnhandledContacts() {
    JsonObject query = new JsonObject().put(HANDLED_CONTACTS, false);
    JsonObject projection = new JsonObject().put("_id", 1).put(TOPIC_PREFIX, 1).put(MOBILE_NO, 1);

    return find(query, projection);
  }
  
  public Observable<List<JsonObject>> usersByUnhandledProfiles() {
    JsonObject query = new JsonObject().put(PROFILE_HANDLED, false);
    return find(query, null);
  }

  public Observable<String> userByMobileNumber(String mobileNo) {
    JsonObject query = new JsonObject().put("mobileNo", mobileNo);
    Observable<JsonObject> userObservable = findOne(query, null);
    return userObservable.
    flatMap(obj -> {
      if(obj != null) {
        if(log.isDebugEnabled()) {
          log.debug("The user with Mobile No: " + mobileNo + " is already there.");
        }
        return Observable.just(obj.getString("_id"));
      }
      else {
        return newUser(mobileNo);
      }
    });
  }

  public Observable<List<JsonObject>> usersByMobileNumbers(JsonArray mobileNos, JsonObject fields) {
    return find(new JsonObject().put(USER_VERIFICATION, true).put("mobileNo", new JsonObject().put("$in", mobileNos)), 
        fields);
  }

  public Observable<JsonObject> userLastSeenByMobileNumber(String mobileNo) {
    return findOne(new JsonObject().put("mobileNo", mobileNo), new JsonObject().put("lastSeen", 1));
  }

  public   Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put("mobileNo", 1)).put("unique", true).put("name", "tring_user_mobile_no_index"));
    return createIndexes(jsonArray);
  }
  
  public Observable<Void> updatePushNotificationId(String userId, String pnId) {
    return update(userId , existing -> {
      existing.put(PN_ID, pnId);
    });
  }
}
