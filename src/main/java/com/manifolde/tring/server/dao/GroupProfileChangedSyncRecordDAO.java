package com.manifolde.tring.server.dao;

import io.vertx.rxjava.ext.mongo.MongoClient;

public class GroupProfileChangedSyncRecordDAO extends BaseUserToSyncDAO{

  public GroupProfileChangedSyncRecordDAO(MongoClient mongoClient) {
    super("syncGrpProfile", mongoClient);
  }

}
