package com.manifolde.tring.server.dao;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import java.util.List;
import rx.Observable;

//@SuppressWarnings("unused")
public class PasselCategoryDAO extends BaseModelDAO {

    private static final Logger log = LoggerFactory.getLogger(UserDAO.class);

    public PasselCategoryDAO(MongoClient mongoClient, TransactionDAO transactionDAO) {
        super("groups", mongoClient, transactionDAO);
    }

    public Observable<String> addCategories(JsonArray categoriesJson) {
        if (categoriesJson == null) {
            return Observable.just(null);
        }
        return insert(new JsonObject().put("categories", categoriesJson));
    }

    public Observable<List<JsonObject>> getCategories() {
        return find(new JsonObject(), null);
    }
    
}
