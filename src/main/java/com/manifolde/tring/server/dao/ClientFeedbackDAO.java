package com.manifolde.tring.server.dao;

import io.vertx.rxjava.ext.mongo.MongoClient;

public class ClientFeedbackDAO extends BaseModelDAO {

  public static final String FEEDBACK = "feedback";
  public static final String UID = "uid";
  public static final String ID = "_id";

  public ClientFeedbackDAO(MongoClient mongoClient) {
    super("clientFeedback", mongoClient);
  }

}
