package com.manifolde.tring.server.dao;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

public class ContentDAO extends BaseModelDAO {
  public static final String PATH = "path";
  public static final String ID = "_id";
  public static final String MIME_TYPE = "mimeType";
  public static final String NAME = "name";
  // size, name, type, path
  public ContentDAO(MongoClient mongoClient) {
    super("contents", mongoClient);
  }

  public Observable<Void> updatePath(String contentId, String newPath) {
    if (contentId == null) {
      return Observable.just((Void)null);
    } else {
      return update(contentId, existing -> {
        existing.put(PATH, newPath);
      });
    }
  }
  

  public Observable<JsonObject> getContent(String id) {
    return findOne(new JsonObject().put(ID, id), new JsonObject().put(PATH, 1).put(MIME_TYPE, 1).put(NAME, 1));
  }

  public Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(PATH, 1)).put("name", "tring_content_path_index"));
    return createIndexes(jsonArray);
  }

}
