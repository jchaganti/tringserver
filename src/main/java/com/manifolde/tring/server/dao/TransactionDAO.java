package com.manifolde.tring.server.dao;

import java.util.Arrays;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

public class TransactionDAO extends BaseModelDAO {

  public static final String STATE = "state";

  public static final String ID = "_id";

  public static final String COLLECTIONS = "collections";

  public static final String OPERATIONS = "operations";

  public static final String AFFECTED_IDS = "affectedIds";

  public static final String OLD_VALUES = "oldValues";

  public static final String TRANSACTION_ID = "tid";

  public static final class Operation {
    public static final String UPDATE = "update";
    public static final String INSERT = "insert";
    public static final String DELETE = "delete";

  }

  public TransactionDAO(MongoClient mongoClient) {
    super("transactions", mongoClient);

  }

  public Observable<String> createTransaction() {
    JsonObject transObj = new JsonObject().put(STATE, "new")
        .put(COLLECTIONS, new JsonArray(Arrays.asList(new String[] {})))
        .put(OPERATIONS, new JsonArray(Arrays.asList(new JsonObject[] {})))
        .put(AFFECTED_IDS, new JsonArray(Arrays.asList(new String[] {})))
        .put(OLD_VALUES, new JsonArray(Arrays.asList(new JsonObject[] {})));
    return insert(transObj);
  }

  public Observable<JsonObject> rollbackCommand(String collection, String operation, JsonObject oldVal, JsonObject jsonObject) {
    JsonObject command = new JsonObject();
    Observable<JsonObject> obsCommand=null;

    switch(operation) {
      case Operation.INSERT:
        command.put("insert", collection).put("documents", new JsonArray(Arrays.asList(oldVal)));
        obsCommand= mongoClient.runCommandObservable("insert", command);
        break;
      case Operation.UPDATE:
        command.put("findAndModify", collection).put("upsert", false)
        .put("new", true).put("query", jsonObject).put("update", oldVal);
        obsCommand= mongoClient.runCommandObservable("findAndModify", command);
        break;
      case Operation.DELETE:
        command.put("delete", collection).put("deletes", 
            new JsonArray(Arrays.asList(new JsonObject().put("q", jsonObject.put("limit",1)))));
        obsCommand= mongoClient.runCommandObservable("delete", command);
        break;
      default:
        break;
    }
    return obsCommand;
  }


}
