package com.manifolde.tring.server.dao;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

public class UserNotesDAO extends BaseModelDAO {

  public static final String MODERATOR_ID = "moderatorId";
  public static final String HANDLE_NAME = "handleName";
  public static final String NOTES = "notes";
  
  public UserNotesDAO(MongoClient mongoClient) {
    super("userNotes", mongoClient);
  }

  public Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(MODERATOR_ID, 1).put(HandleDAO.NAME, 1).put(HandleDAO.GID, 1))
        .put("name", "tring_user_notes_index"));
    return createIndexes(jsonArray);
  }
}
