package com.manifolde.tring.server.dao;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

public class GroupDAO extends BaseModelDAO {

  public static final String GID = "gid";
  public static final String PARENT_GID = "pgid";
  public static final String GROUP_NAME = "name";
  public static final String HID = "ownerHandleId";
  public static final String ID = "_id";
  public static final String GROUP_O2M = "o2m";
  public static final String CLOSED_GROUP = "closed";
  public static final String MQTT_TOPIC_PREFIX = "topicPrefix";
  public static final String CATEGORIES = "categories";
  public static final String COUNTRY = "country";
  public static final String STATE = "state";
  public static final String CITY = "city";
  public static final String GROUP_TOPIC_TAIL_NAME_PREFIX = "group_";
  public static final String TOPIC_PREFIX= "topicPrefix";
  public static final String PVT = "/pvt";
  public static final String MQTT_TOPIC = "topic";
  public static final String MEAN_RATING = "meanRating";
  public static final String RATING_COUNT = "ratingCount";
  public static final String THUMBNAIL_DATA = "thumbnailData";
  public static final String GROUP_DESCRIPTION = "description";
  public static final String GROUP_RULES = "rules";
  public static final String GROUP_COUNTRY = "country";
  public static final String GROUP_STATE = "state";
  public static final String GROUP_CITY = "city";
  public static final String MEMBERSHIP_NEEDS_APPROVAL = "membershipNeedsApproval";
  private static final String FEATURED = "featured";

  public GroupDAO(MongoClient mongoClient) {
    super("groups", mongoClient);
  }

  public GroupDAO(MongoClient mongoClient, TransactionDAO transactionDAO) {
    // TODO Auto-generated constructor stub
    super("groups", mongoClient, transactionDAO);
  }

  public Observable<JsonObject> groupIdByName(String groupName) {
    return findOne(new JsonObject().put(GROUP_NAME, groupName), new JsonObject().put(ID, 1));
  }

  public Observable<JsonObject> groupNameById(String id) {
      return findById(id, new JsonObject().put(GROUP_NAME, 1));
  }
  
  public List<Observable<JsonObject>> groupsFromIds(List<String> ids) {
    List<Observable<JsonObject>> groupsObservable = new ArrayList<>();
    for (String id : ids) {
      Observable<JsonObject> groupObservable = findOne(new JsonObject().put(ID, id), null);
      groupsObservable.add(groupObservable);
    }
    return groupsObservable;
  }

  public Observable<JsonObject> groupFromName(String groupName) {
    return findOne(new JsonObject().put(GROUP_NAME, groupName), new JsonObject());
  }

  public Observable<JsonObject> createGroup(JsonObject groupData, String ownerHandle,
      String transactionId) {
    String gname = groupData.getString(GROUP_NAME);
    // JsonObject query = new JsonObject().put(GROUP_NAME, gname);
    // return findOneAndUpdate(query, new JsonObject().put("$setOnInsert", groupData), true, true)
    return insertTransactional(groupData, transactionId).flatMap(obj -> {
      return findOne(new JsonObject().put(GROUP_NAME, gname), new JsonObject());
    });
  }

  // public Observable<JsonObject> removeGroup(JsonObject groupData) {
  // return findOneAndUpdate(groupData, new JsonObject().put("$setOnInsert", new
  // JsonObject().put("status", BaseModelDAO.DELETED)), false, false);
  // }

  public Observable<JsonObject> setOwnerHandle(JsonObject query, String handleId) {
    return findOneAndUpdate(query,
        new JsonObject().put("$set", new JsonObject().put(HID, handleId)), false, true)
            .flatMap(obj -> {
              return (obj != null) ? Observable.just((JsonObject) null) : Observable.just(obj);
            });
  }

  public Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(GROUP_NAME, 1).put(PARENT_GID, 1))
        .put("unique", true).put("name", "tring_group_name_pgid_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(PARENT_GID, 1)).put("name", "tring_parent_group_name_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(COUNTRY, 1).put(STATE, 1).put(CITY, 1))
        .put("name", "tring_group_country_state_city_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(CATEGORIES, 1))
        .put("name", "tring_group_categories_index"));
    return createIndexes(jsonArray);
  }
  
  public Observable<List<JsonObject>> getGroupAndSubGroups(String gid) {
	  JsonObject orClause = new JsonObject().put("$or", new JsonArray().add(new JsonObject().put(ID, gid)).add(new JsonObject().put(PARENT_GID, gid)));
	  JsonObject fields = new JsonObject().put(ID, 1).put(PARENT_GID, 1).put(GROUP_NAME, 1);
	  return find(orClause, fields);	  
  }
  
  public Observable<List<JsonObject>> getSubGroups(JsonArray pgIds, JsonObject fields) {
    JsonObject orClause = new JsonObject().put(GroupDAO.PARENT_GID, new JsonObject().put("$in", pgIds));
    return find(orClause, fields);      
}

  public Observable<JsonObject> groupFromId(String gid) {
    return findOne(new JsonObject().put(ID, gid), null);
  }

  public Observable<JsonObject> updateGroup(JsonObject groupData) {
    return findOneAndUpdate(new JsonObject().put(ID, groupData.getString(ID)), 
        new JsonObject().put("$set", groupData), true, false);
  }

  public Observable<JsonObject> setMeanRatingAndCount(String gid, double rating, int count,String transactionId) {
    JsonObject query = new JsonObject().put(ID,gid);
    JsonObject update = new JsonObject().put(MEAN_RATING, rating).put(RATING_COUNT, count).put("success", true);
    return updateTransactional(query, update, transactionId);
  }

  public Observable<JsonObject> getRatingAndCount(String gid) {
    
    return findById(gid, new JsonObject().put(MEAN_RATING,1).put(RATING_COUNT, 1));
  }
  
  public Observable<JsonObject> mapReduceForGroups(String mapFunc,
      String reduceFunc, JsonObject options) {
    return super.mapReduceForCollection("groups", mapFunc, reduceFunc, options);
  }

  public Observable<JsonObject> mapReduceForHandles(String mapFunc,
      String reduceFunc, JsonObject options) {
    return super.mapReduceForCollection("handles", mapFunc, reduceFunc, options);    
  }

  public Observable<List<JsonObject>> getFeaturedGroups() {
    return find(new JsonObject().put(FEATURED, new JsonObject().put("$exists", true)),new JsonObject())
        .flatMap(list->{
          return Observable.from(list).toSortedList((a,b)->{
            int featured_a = a.getBoolean(FEATURED)?0:1;
            int featured_b = b.getBoolean(FEATURED)?0:1;
            return (featured_a < featured_b) ? -1:(featured_a == featured_b?0:1);
          });
        });
  }

  public Observable<JsonObject> getThumbnalData(String str) {
    return findOne(new JsonObject().put(GROUP_NAME,str),new JsonObject().put(THUMBNAIL_DATA, 1));
  }
}
