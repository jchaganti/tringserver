package com.manifolde.tring.server.dao;
import com.manifolde.tring.server.constants.Constants;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import rx.Observable;

/**
 *
 * @author Amit Valse<amit.valse@blobcity.com>
 */
@SuppressWarnings("unused")
public class PasselGroupDAO extends BaseModelDAO {

    private static final Logger log = LoggerFactory.getLogger(UserDAO.class);

    public PasselGroupDAO(MongoClient mongoClient, TransactionDAO transactionDAO) {
        super("passelGroups", mongoClient, transactionDAO);
    }

    public Observable<String> addGroup(JsonObject groupJson) {
        if (groupJson == null) {
            return Observable.just(null);
        }
        return insert(groupJson);
    }

    public Observable<List<JsonObject>> searchGroupsByKeyword(String keyword) {
        JsonArray groupIds = searchGroup(keyword);
        JsonObject query = new JsonObject();
        query.put("_id", new JsonObject().put("$in", groupIds));
        return find(query, null);
    }

    public Observable<List<JsonObject>> filterGroupsByCategory(String keyword, String category) {
        JsonArray groupIds = searchGroup(keyword);
        JsonObject query = new JsonObject();
        query.put("_id", new JsonObject().put("$in", groupIds));
        query.put("categories", category);
        return find(query, null);
    }

    public Observable<List<JsonObject>> filterGroupsByLocation(String keyword, String location) {
        JsonArray groupIds = searchGroup(keyword);
        JsonObject query = new JsonObject();
        query.put("_id", new JsonObject().put("$in", groupIds));
        query.put("location", location);
        return find(query, null);
    }

    public Observable<List<JsonObject>> browseGroupsByCategory(String category) {
        JsonObject query = new JsonObject();
        query.put("categories", category);
        return find(query, null);
    }

    public Observable<List<JsonObject>> browseGroupsByLocation(String location) {
        JsonObject query = new JsonObject();
        query.put("location", location);
        return find(query, null);
    }

    public Observable<List<JsonObject>> getFeaturedGroups() {
        JsonObject query = new JsonObject();
        query.put("featured", new JsonObject().put("$exists", true));
        return find(query, null);
    }

    /**
     * This method takes user query text as searchParam breaks them into tokens
     * and searches the groupNames indexed in Solr returns a list of all the
     * matched groupNames from Solr
     *
     * @param searchParam
     * @return
     */
    private JsonArray searchGroup(String searchParam) {
        JsonArray groupIdList = new JsonArray();
        HttpSolrServer solrServer = new HttpSolrServer(Constants.SERVER_ADDRESS);
        try {
            String finalKeywords = "";
            String[] keywords = searchParam.split(" ");
            for (String keyword : keywords) {
                finalKeywords += "+" + keyword;
            }
            finalKeywords = finalKeywords.substring(1);
            QueryResponse response;
            SolrQuery query = new SolrQuery();
            query.setQuery("groupName:" + finalKeywords);
            query.set("defType", "edismax");
            query.setFields("groupId");
            response = solrServer.query(query);
            SolrDocumentList results = response.getResults();
            for (int i = 0; i < results.size(); ++i) {
                groupIdList.add(response.getResults().get(i).getFieldValue("groupId").toString());
            }
            solrServer.shutdown();
        } catch (SolrServerException ex) {
            solrServer.shutdown();
            log.info(ex);
        }
        return groupIdList;
    }

}
