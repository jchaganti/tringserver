package com.manifolde.tring.server.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.UpdateOptions;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

abstract public class BaseModelDAO {
  private static final Logger log = LoggerFactory.getLogger(BaseModelDAO.class);

  public static final String CREATED = "created";

  public static final String MODIFIED = "modified";

  protected String collection;
  protected Long created;
  protected Long modified;
  protected MongoClient mongoClient;

  private TransactionDAO transactionDAO;

  protected BaseModelDAO(String type, MongoClient mongoClient) {
    super();
    this.collection = type;
    this.mongoClient = mongoClient;
    this.created = new Date().getTime();
    this.modified = created;
    // this.transactionDAO = new TransactionDAO(mongoClient);
  }

  protected BaseModelDAO(String type, MongoClient mongoClient, TransactionDAO transactionDAO) {
    super();
    this.collection = type;
    this.mongoClient = mongoClient;
    this.created = new Date().getTime();
    this.modified = created;
    this.transactionDAO = transactionDAO;
  }

  public Long getCreated() {
    return created;
  }

  public Long getModified() {
    return modified;
  }

  public void setModified(Long modified) {
    this.modified = modified;
  }


  public Observable<String> save(JsonObject json) {
    DateTime now = new DateTime(DateTimeZone.UTC);
    JsonObject dateField = new JsonObject().put("$date", now.toString());
    json = json.put(CREATED, dateField).put(MODIFIED, dateField);
    return mongoClient.saveObservable(collection, json);
  }

  public Observable<String> insert(JsonObject json) {
    DateTime now = new DateTime(DateTimeZone.UTC);
    JsonObject dateField = new JsonObject().put("$date", now.toString());
    json = json.put(CREATED, dateField).put(MODIFIED, dateField);
    return mongoClient.insertObservable(collection, json);
  }

  private JsonObject constructTransactionUpdateObj(String operation, String id,
      JsonObject oldValue) {
    // Prepend rather than append the transactions related stuff, because rollback will happen in
    // the reserve
    // order
    return new JsonObject().put("$push", new JsonObject()
        .put(TransactionDAO.COLLECTIONS,
            new JsonObject().put("$each", new JsonArray(Arrays.asList(new String[] {collection})))
                .put("$position", 0))
        .put(TransactionDAO.OPERATIONS,
            new JsonObject().put("$each", new JsonArray(Arrays.asList(new String[] {operation})))
                .put("$position", 0))
        .put(TransactionDAO.AFFECTED_IDS,
            new JsonObject().put("$each", new JsonArray(Arrays.asList(new String[] {id})))
                .put("$position", 0))
        .put(TransactionDAO.OLD_VALUES,
            new JsonObject().put("$each", new JsonArray(Arrays.asList(new JsonObject[] {oldValue})))
                .put("$position", 0)));
  }
  
  public Observable<String> insertTransactional(JsonObject json, String transactionId) {
    final String docId = json.containsKey("_id")?json.getString("_id"):ObjectId.get().toHexString();
    json.put("_id", docId);
    List<String> transList = new ArrayList<>();
    transList.add(transactionId);
    json.put("transactions", new JsonArray(transList));
    Observable<String> result = insert(json);
    ObservableFuture<String> finalResult = new ObservableFuture<String>();
    
    JsonObject transUpdateObj =
        constructTransactionUpdateObj(TransactionDAO.Operation.DELETE, docId, new JsonObject());
    log.debug("Within insertTransactional,  transUpdateObj is " + transUpdateObj + " and transactionDAO is: " + transactionDAO +  " called on thread "
                    + Thread.currentThread().getName() + " for collection: " + collection);

    transactionDAO.findOneAndUpdate(new JsonObject().put(TransactionDAO.ID, transactionId),
        transUpdateObj, true, false).subscribe(t -> {
          if (log.isDebugEnabled()) {
            log.debug(
                "Within insertTransactional, transactionDAO.findOneAndUpdate subscribe called on thread "
                    + Thread.currentThread().getName());
            log.debug("arg object  is " + t);
          }
          if (t == null || !checkOK(t)) {
            if (log.isDebugEnabled()) {
              log.debug("During insertTransactional, Problem pushing " + collection
                  + " into collections field for transaction " + transactionId);
            }
            Throwable err=new Exception();
            finalResult.toHandler().handle(Future.failedFuture(err));
          } else {
            result.subscribe(id->{
              id = json.getString("_id");
              if (log.isDebugEnabled()) {
                log.debug(
                    "insertTransactional subscribe called on thread " + Thread.currentThread().getName());
                log.debug("insertTransactional subscribe - id is " + id);
                log.debug("transactionDAO is " + transactionDAO);
              }
              finalResult.toHandler().handle(Future.succeededFuture(id));
            }, err -> {
              log.error("During insertTransactional, Error pushing " + collection
                  + " into collections field for transaction " + transactionId);
              finalResult.toHandler().handle(Future.failedFuture(err));
            });
          }
        } , err -> {
          log.error("During insertTransactional, Error pushing " + collection
              + " into collections field for transaction " + transactionId);
          finalResult.toHandler().handle(Future.failedFuture(err));
        });
    return finalResult;
  }

  public Observable<Void> update(String id, JsonFieldUpdater updater) {
    return mongoClient.
        // Find existing object
        findOneObservable(collection, new JsonObject().put("_id", id), null).
        // Check if it is not null
        filter(existing -> existing != null).
        // Update the existing
        flatMap(existing -> {
          updater.update(existing);
          DateTime now = new DateTime(DateTimeZone.UTC);
          JsonObject dateField = new JsonObject().put("$date", now.toString());
          existing.put(MODIFIED, dateField);
          JsonObject updatedExisting = new JsonObject().put("$set", existing);
          return mongoClient.updateObservable(collection, new JsonObject().put("_id", id),
              updatedExisting);
        });
  }
  
  public Observable<JsonObject> bulkInsert(JsonArray documents, boolean ordered) {
    JsonObject command = new JsonObject().put("insert", collection).put("documents", documents).put("ordered", ordered);
    return mongoClient.runCommandObservable("insert", command);
  }

  public Observable<Void> bulkUpdate(JsonObject query, JsonObject update) {
		JsonObject set = update.getJsonObject("$set");
		if (null == set) {
			update.put("$set", new JsonObject().put(MODIFIED,
					new JsonObject().put("$date", (new DateTime(DateTimeZone.UTC)).toString())));
		} else {
			set.put(MODIFIED, new JsonObject().put("$date", (new DateTime(DateTimeZone.UTC)).toString()));
		}
		if(log.isDebugEnabled()) {
			log.debug("Bulk Update Query: " + query + " Update: " + update);
		}
		return mongoClient.updateWithOptionsObservable(collection, query, update, new UpdateOptions(false, true));
  }
  
  public Observable<Void> delete(String id) {
    return mongoClient.removeObservable(collection, new JsonObject().put("_id", id));
  }

  public Observable<Void> delete(JsonObject query) {
    return mongoClient.removeObservable(collection, query);
  }
  
  public Observable<JsonObject> findById(String id, JsonObject fields) {
    JsonObject query = new JsonObject();
    query.put("_id", id);
    return findOne(query, fields);
  }

  public Observable<JsonObject> findOne(JsonObject query, JsonObject fields) {
    return mongoClient.findOneObservable(collection, query, fields);
  }

  public Observable<List<JsonObject>> find(JsonObject query, JsonObject fields) {
    return find(query, fields, 0, -1, null);
  }
  
  public Observable<List<JsonObject>> find(JsonObject query, JsonObject fields, int skip, int limit, JsonObject sortBy) {
	  JsonObject findOptions = new JsonObject();
	  findOptions.put("fields", fields);
	  if(skip > 0) {
		  findOptions.put("skip", Integer.valueOf(skip));		  
	  }
	  if(limit > 0) {
		  findOptions.put("limit", Integer.valueOf(limit));
	  }
	  
	  if(sortBy != null) {
		  findOptions.put("sort", sortBy);
	  }
	  return mongoClient.findWithOptionsObservable(collection, query,
	    new FindOptions(findOptions));
  }
  
  public Observable<JsonObject> aggregate(JsonArray pipeline) {
    JsonObject command = new JsonObject().put("aggregate", collection).put("pipeline", pipeline);
    return mongoClient.runCommandObservable("aggregate", command);
  }

  public Observable<JsonObject> findOneAndUpdate(JsonObject query, JsonObject update, boolean isNew,
      boolean upsert) {
    //If update object already has a $set then add the MODIFIED action to it, else create one
    JsonObject set = update.getJsonObject("$set");
    if (null == set) {
      update.put("$set", new JsonObject().put(MODIFIED, new JsonObject()
          .put("$date", (new DateTime(DateTimeZone.UTC)).toString())));
    } else{
      set.put(MODIFIED, new JsonObject()
          .put("$date", (new DateTime(DateTimeZone.UTC)).toString()));
    }
    JsonObject command = new JsonObject().put("findAndModify", collection).put("upsert", upsert)
        .put("new", isNew).put("query", query).put("update", update);
    if (log.isDebugEnabled()) {
      log.debug("findOneAndUpdate: command is " + command);
      log.debug("mongoClient is " + mongoClient);
    }
    return mongoClient.runCommandObservable("findAndModify", command);
  }

  public Observable<JsonObject> updateTransactional(JsonObject query, JsonObject update,
      String transactionId) {
    ObservableFuture<JsonObject> finalResult = new ObservableFuture<JsonObject>();
    
    findOne(query,new JsonObject()).subscribe(fobj->{
      if (null == fobj) {
        log.error("updateTransactional - Null Document found");
        finalResult.toHandler().handle(Future.failedFuture("Null Document found"));
      }
      String id = fobj.getString("_id");
      log.debug("id being updated in transaction: " + id);
      JsonObject transUpdateObj =
          constructTransactionUpdateObj(TransactionDAO.Operation.UPDATE, id, fobj);
      
      transactionDAO.findOneAndUpdate(new JsonObject().put(TransactionDAO.ID, transactionId),
          transUpdateObj, true, false).subscribe(t -> {
        log.debug("findOneAndUpdateTransactional: transaction object is " + t);
        if (t == null || !checkOK(t)) {
          if (log.isDebugEnabled()) {
            log.debug(
                "During findOneAndUpdateTransactional, Problem pushing rollback info fields into transaction "
                    + transactionId);
          }
          finalResult.toHandler().handle(Future.failedFuture("Problem pushing rollback info fields into transaction"));
        } else {
          JsonObject push = update.getJsonObject("$push");
          if (null == push) {
            update.put("$push", new JsonObject().put("transactions", transactionId));
          } else{
            push.put("transactions", transactionId);
          }
          JsonObject set = update.getJsonObject("$set");
          if (null == set) {
            update.put("$set", new JsonObject().put(MODIFIED, new JsonObject()
                .put("$date", (new DateTime(DateTimeZone.UTC)).toString())));
          } else{
            set.put(MODIFIED, new JsonObject()
                .put("$date", (new DateTime(DateTimeZone.UTC)).toString()));
          }
          findOneAndUpdate(query, update, true, false).subscribe(obj->{
            if (obj == null || !checkOK(obj)) {
              log.error(
                  "During updateTransactional, Null result updating document for transaction " + transactionId);
              finalResult.toHandler().handle(Future.failedFuture("Null result updating document for transaction " + transactionId));
            } else {
              if (log.isDebugEnabled()){
                log.debug("updateTransactional: Successful update");
              }
              finalResult.toHandler().handle(Future.succeededFuture(obj));
            }
          }, err -> {
            log.error(
                "During updateTransactional, Error updating document for transaction " + transactionId);
            finalResult.toHandler().handle(Future.failedFuture(err));
          });
        }
      } , err -> {
        log.error(
            "During findOneAndUpdateTransactional, Problem pushing transaction id into transactions field for collection "
                + collection);
        finalResult.toHandler().handle(Future.failedFuture(err));
      });
    },err->{
      log.error("updateTransactional - Document to be updated not found");
      err.printStackTrace();
      finalResult.toHandler().handle(Future.failedFuture(err));
    });
    
    return finalResult;
  }

  public Observable<Void> deleteTransactional(JsonObject query,
      String transactionId) {
    ObservableFuture<Void> finalResult = new ObservableFuture<Void>();
    
    findOne(query,new JsonObject()).subscribe(fobj->{
      if (null == fobj) {
        log.error("deleteTransactional - Null Document found");
        finalResult.toHandler().handle(Future.failedFuture("Null Document found"));
      }
      String id = fobj.getString("_id");
      JsonObject transUpdateObj =
          constructTransactionUpdateObj(TransactionDAO.Operation.INSERT, id, fobj);
      
      transactionDAO.findOneAndUpdate(new JsonObject().put(TransactionDAO.ID, transactionId),
          transUpdateObj, true, false).subscribe(t -> {
        log.debug("deleteTransactional: transaction object is " + t);
        if (t == null || !checkOK(t)) {
          if (log.isDebugEnabled()) {
            log.debug(
                "During deleteTransactional, Problem pushing rollback info fields into transaction "
                    + transactionId);
          }
          finalResult.toHandler().handle(Future.failedFuture("Problem pushing rollback info fields into transaction"));
        } else {
          delete(id).subscribe((obj)->{
            if (obj == null) {
              log.error(
                  "During deleteTransactional, Null result deleting document "+id+" for transaction " + transactionId);
              finalResult.toHandler().handle(Future.failedFuture("Null result updating document for transaction " + transactionId));
            } else {
              if (log.isDebugEnabled()){
                log.debug("deleteTransactional: Successful delete");
              }
              finalResult.toHandler().handle(Future.succeededFuture(obj));
            }
          }, err -> {
            log.error(
                "During deleteTransactional, Error deleteing document "+id+"  for transaction " + transactionId);
            finalResult.toHandler().handle(Future.failedFuture(err));
          });
        }
      } , err -> {
        log.error(
            "During deleteTransactional, Problem pushing transaction id into transactions field for collection "
                + collection);
        finalResult.toHandler().handle(Future.failedFuture(err));
      });
    },err->{
      log.error("deleteTransactional - Document to be deleted not found");
      finalResult.toHandler().handle(Future.failedFuture(err));
    });
    
    return finalResult;
  }
  public Observable<JsonObject> createIndexes(JsonArray indexes) {
    JsonObject command = new JsonObject().put("createIndexes", collection).put("indexes", indexes);
    return mongoClient.runCommandObservable("createIndexes", command);
  }

  @FunctionalInterface
  protected interface JsonFieldUpdater {
    void update(JsonObject existing);
  }

  public static boolean checkOK(JsonObject res) {
    int val = res.getInteger("ok");
    if (val == 1) {
      return true;
    }
    return false;
  }

  public Observable<JsonObject> findOneAndUpdateForCollection(String coll, JsonObject query,
      JsonObject update, boolean isNew, boolean upsert) {
    JsonObject set = update.getJsonObject("$set");
    if (null == set) {
      update.put("$set", new JsonObject().put(MODIFIED, new JsonObject()
          .put("$date", (new DateTime(DateTimeZone.UTC)).toString())));
    } else{
      set.put(MODIFIED, new JsonObject()
          .put("$date", (new DateTime(DateTimeZone.UTC)).toString()));
    }
    JsonObject command = new JsonObject().put("findAndModify", coll).put("upsert", upsert)
        .put("new", isNew).put("query", query).put("update", update);
    return mongoClient.runCommandObservable("findAndModify", command);
  }

  public Observable<JsonObject> mapReduceForCollection(String coll, String string,
      String string2, JsonObject options) {
    JsonObject command = new JsonObject().put("mapReduce", coll).put("map", string)
        .put("reduce", string2).mergeIn(options);
    System.out.println("mapReduceForCollection command is "+ command);
    return mongoClient.runCommandObservable("mapReduce", command);
 
  }
}
