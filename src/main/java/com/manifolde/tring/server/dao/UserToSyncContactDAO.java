package com.manifolde.tring.server.dao;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.netty.util.internal.ThreadLocalRandom;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

@SuppressWarnings("unused")
public class UserToSyncContactDAO extends BaseUserToSyncDAO {
  private static final Logger log = LoggerFactory.getLogger(UserToSyncContactDAO.class);
  
  public UserToSyncContactDAO(MongoClient mongoClient) {
    super("userToSyncContacts", mongoClient);
  }  
}
