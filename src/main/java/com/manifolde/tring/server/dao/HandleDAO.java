package com.manifolde.tring.server.dao;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

public class HandleDAO extends BaseModelDAO {

  
  private static final Logger log = LoggerFactory.getLogger(HandleDAO.class);
  public static final String ID = "_id";
  public static final String NAME = "name";
  public static final String GID = "gid";
  public static final String SUBGROUP_GID = "subGroupId";
  public static final String UID = "uid";
  public static final String TYPE = "type";
  public static final String STATUS = "status";
  public static final String PAGE = "page";
  public static final String PAGE_SIZE = "pageSize";
  public static final String EVICTION_MESSAGE = "evictionMessage";

  // This is used when Handle ID is referred as a foreign key in a different collection
  public static final String HID = "hid";
  public static final String PARENT_GROUP_MEMBER = "parentGroupMember";
  public static final String SUB_GROUP_MODERATOR = "subGroupModerator";
  public static final String SUB_GROUP_MEMBER = "subGroupMember";
  public static final String MEMBERS_TYPE = "memberType";
  public static final String AS_MEMBER = "asMember";
  public static final String AS_MODERATOR = "asModerator";
  public static final String ADMIN_AS_MODERATOR = "adminAsModerator";
  public static final String ADMIN_AS_NONMEMBER = "adminAsNonMember";
  public static final String AS_NON_MEMBER = "asNonMember";
  public static final String RATING = "rating";
  public static final String REVIEW = "review";
  public static final String ADDED_AS_MEMBER = "addedAsMember";
  public static final String ADDED_AS_MODERATOR = "addedAsModerator";
  //public static final String IS_ADMIN = PARENT_GROUP_MEMBER + "." + TYPE;
  public static final String IS_ADMIN = "isAdmin";
  public static final String IS_OWNER = "isOwner";


  // If any handle is a member of a group (parent group or a sub-group), by convention there 
  // is no type associated with it in DB. When a handle is moved from parent group to any
  // sub-group, h/she will be made NONMEMBER of the parent group and there will be no type
  // associated when h/she is a member of the sub-group.
  public static enum HandleType {
    OWNER, ADMIN, MODERATOR, MEMBER, NONMEMBER
  };
  
  public static enum HandleStatus {
	  ACTIVE, INACTIVE, EVICTED,PENDING, REJECTED, LEFT
  }

  public HandleDAO(MongoClient mongoClient, TransactionDAO transactionDAO) {
    super("handles", mongoClient, transactionDAO);
  }

  public Observable<JsonObject> handleFromHandleNameAndGroupId(String handleName,
      String groupId) {
    return handleFromHandleNameAndGroupId(handleName, groupId, new JsonObject().put(ID, 1));
  }
  
  public Observable<JsonObject> handleFromHandleNameAndGroupId(String handleName,
      String groupId, JsonObject fields) {
	  JsonArray orConditions = getGroupQuery(groupId);
	  return findOne(new JsonObject().put(NAME, handleName).put("$or", orConditions), fields);
  }

  private JsonArray getGroupQuery(String groupId) {
	JsonArray gidArray = new JsonArray().add(groupId);
	JsonObject parentGroupCondition = new JsonObject();
	parentGroupCondition.put(PARENT_GROUP_MEMBER + "." + GID, groupId);

	JsonObject subGroupModeratorCondition = new JsonObject();
	subGroupModeratorCondition.put(SUB_GROUP_MODERATOR, new JsonObject().put("$in", gidArray));

	JsonObject subGroupMemberCondition = new JsonObject();
	subGroupMemberCondition.put(SUB_GROUP_MEMBER, new JsonObject().put("$in", gidArray));

	JsonArray orConditions = new JsonArray().add(parentGroupCondition).add(subGroupModeratorCondition)
			.add(subGroupMemberCondition);
	return orConditions;
  }

  public Observable<JsonObject> createGroupMember(String handleName, String gid, String uid, boolean tempMember) {
    JsonObject handleObj = new JsonObject().put(NAME, handleName).put(UID, uid)
        .put(PARENT_GROUP_MEMBER, new JsonObject().put(GID, gid).put(TYPE, HandleType.MEMBER.ordinal()));
    if (tempMember) {
      handleObj.put(STATUS, HandleStatus.PENDING.ordinal());
    } else {
      handleObj.put(STATUS, HandleStatus.ACTIVE.ordinal());
    }
    return insert(handleObj).flatMap(obj -> {
      return findOne(new JsonObject().put("_id", obj), new JsonObject());
      //return Observable.just(new JsonObject().put(UID, uid).put(GID, gid).put(NAME,handleName));
    });
  }

  public Observable<JsonObject> addAsGroupAdmin(String hid) {
	  JsonObject subGroupEmptyCond =  new JsonObject().put("$where", "this.subGroupMember.length == 0");
	  
	  JsonObject subGroupExistsCond = new JsonObject().put(SUB_GROUP_MEMBER, new JsonObject().put("$exists", true));
	  JsonObject subGroupNotExistsCond = new JsonObject().put(SUB_GROUP_MEMBER, new JsonObject().put("$exists", false));
	  
	  JsonArray andConds = new JsonArray().add(subGroupExistsCond).add(subGroupEmptyCond);
	  JsonObject subGroupExistsButEmpty = new JsonObject().put("$and", andConds);
	  JsonArray orConds = new JsonArray().add(subGroupNotExistsCond).add(subGroupExistsButEmpty);
	  
	  JsonObject notOwnerCond = new JsonObject().put(PARENT_GROUP_MEMBER+"." + TYPE, new JsonObject().put("$ne", HandleType.OWNER.ordinal()));
	  andConds = new JsonArray().add(new JsonObject().put(ID, hid)).add(notOwnerCond).add(new JsonObject().put("$or", orConds));
	  JsonObject query = new JsonObject().put("$and", andConds);
	  
	  JsonObject update = new JsonObject().put("$set", new JsonObject().put(PARENT_GROUP_MEMBER+"." + TYPE, HandleType.ADMIN.ordinal()));
	  return findOneAndUpdate(query, update, true, false);	    
  }
  
  public Observable<JsonObject> removeAsGroupAdmin(String hid) {
	  JsonObject hidCond = new JsonObject().put(ID, hid);
	  JsonObject notOwnerCond = new JsonObject().put(PARENT_GROUP_MEMBER+"." + TYPE, new JsonObject().put("$ne", HandleType.OWNER.ordinal()));
      JsonArray andConds = new JsonArray().add(hidCond).add(notOwnerCond);
	  JsonObject query = new JsonObject().put("$and", andConds);
	  JsonObject update = new JsonObject().put("$set", new JsonObject().put(PARENT_GROUP_MEMBER+"." + TYPE, HandleType.MEMBER.ordinal()));
	  return findOneAndUpdate(query, update, true, false);	    
  }
  
  public Observable<JsonObject> createOwnerHandle(String ownerHandle, String hid, String gid,
      String uid, String transactionId) {
    JsonObject handleObj = new JsonObject().put(NAME, ownerHandle).put(UID, uid)
        .put(PARENT_GROUP_MEMBER, new JsonObject().put(GID, gid).put(TYPE, HandleType.OWNER.ordinal()));
    handleObj.put(STATUS, HandleStatus.ACTIVE.ordinal());
    if (null != hid) {
      handleObj.put(ID, hid);
    }
    return insertTransactional(handleObj, transactionId).flatMap(obj -> {
      //return findOne(new JsonObject().put(ID, hid), new JsonObject());
      return Observable.just(new JsonObject().put(ID, hid).put(UID, uid).put(GID, gid).put(TYPE, HandleType.OWNER.ordinal()).put(NAME,ownerHandle));
    });
  }

  public Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, 1))
        .put("name", "tring_handle_gid_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(NAME, 1).put(PARENT_GROUP_MEMBER+"."+GID, 1))
        .put("unique", true).put("name", "tring_handle_name_gid_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(UID, 1).put(PARENT_GROUP_MEMBER+"."+GID, 1))
        .put("unique", true).put("name", "tring_handle_user_gid_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(UID, 1).put(PARENT_GROUP_MEMBER+"."+TYPE, 1))
        .put("name", "tring_handle_user_type_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(UID, 1))
        .put("name", "tring_handle_user_index"));
    return createIndexes(jsonArray);
  }

  public Observable<List<JsonObject>> getParentGroupsForUser(String uid) {
    JsonObject query = new JsonObject().put(UID, uid);
    JsonObject projection = new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, 1).put(NAME, 1)
        .put(PARENT_GROUP_MEMBER+"."+TYPE, 1).put(STATUS, 1);
    return find(query, projection);
  }
  
  public Observable<List<JsonObject>> getAllHandlesForUser(String uid) {
    JsonObject query = new JsonObject().put(UID, uid);
    JsonObject projection = new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, 1).put(NAME, 1)
        .put(PARENT_GROUP_MEMBER+"."+TYPE, 1).put(STATUS, 1).put(SUB_GROUP_MEMBER, 1).put(SUB_GROUP_MODERATOR, 1);
    return find(query, projection);
  }
  
  public Observable<List<JsonObject>> getMembersOfGroup(String gid, JsonObject fields, int page, int pageSize) {
  	int skip = page > 0? (page - 1) * pageSize: 0;
  	int limit = pageSize;
  	JsonObject query = new JsonObject().put(PARENT_GROUP_MEMBER + "." + GID, gid).put(STATUS, 0);
    return find(query, fields, skip, limit, new JsonObject().put(PARENT_GROUP_MEMBER+"."+TYPE, 1));
  }
  
  public Observable<JsonArray> getMembersOfSubGroup(String pgid, String sgid, JsonObject fields, int page, int pageSize) {
    int _skip = page > 0? (page - 1) * pageSize: 0;
    int _limit = pageSize;
    JsonArray pipeline = new JsonArray();
    JsonObject parentGroupCondition = null;
    
    if(pgid != null) {
      parentGroupCondition = new JsonObject();
      parentGroupCondition.put(PARENT_GROUP_MEMBER + "." + GID, pgid);
    }
    
    JsonArray gidArray = new JsonArray().add(sgid);
    JsonObject subGroupModeratorCondition = new JsonObject();
    subGroupModeratorCondition.put(SUB_GROUP_MODERATOR, new JsonObject().put("$in", gidArray));

    JsonObject subGroupMemberCondition = new JsonObject();
    subGroupMemberCondition.put(SUB_GROUP_MEMBER, new JsonObject().put("$in", gidArray));

    JsonArray orConditions = new JsonArray();
    
    if(parentGroupCondition != null) {
      orConditions.add(parentGroupCondition);
    }
    orConditions.add(subGroupModeratorCondition).add(subGroupMemberCondition);
    JsonObject match = new JsonObject().put("$match", new JsonObject().put("$or", orConditions));

    JsonObject ownerCond = new JsonObject().put("$eq",new JsonArray().add("$" + PARENT_GROUP_MEMBER + "." + TYPE).add(0));
    fields.put(IS_OWNER, ownerCond);
    
    JsonObject _typeOrConds = new JsonObject().put("$eq",new JsonArray().add("$" + PARENT_GROUP_MEMBER + "." + TYPE).add(1));
    fields.put(IS_ADMIN, _typeOrConds);
       
    JsonObject subGroupModeratorArray = new JsonObject().put("$ifNull", new JsonArray().add("$"+SUB_GROUP_MODERATOR).add(new JsonArray()));
    JsonObject setIsSubsetOfModerator = new JsonObject().put("$setIsSubset", new JsonArray().add(gidArray).add(subGroupModeratorArray));
    fields.put(ADDED_AS_MODERATOR, setIsSubsetOfModerator);
    
    JsonObject subGroupMemberArray = new JsonObject().put("$ifNull", new JsonArray().add("$"+SUB_GROUP_MEMBER).add(new JsonArray()));
    JsonObject setIsSubsetOfMember = new JsonObject().put("$setIsSubset", new JsonArray().add(gidArray).add(subGroupMemberArray));
    fields.put(ADDED_AS_MEMBER, setIsSubsetOfMember);
    
    JsonObject sort = new JsonObject();
    sort.put(IS_OWNER, -1);
    sort.put(IS_ADMIN, -1);
    sort.put(ADDED_AS_MODERATOR, -1);
    sort.put(ADDED_AS_MEMBER, -1);
    
    JsonObject skip = new JsonObject().put("$skip", _skip);
    JsonObject limit = new JsonObject().put("$limit", _limit);
    
    pipeline.add(match).add(new JsonObject().put("$project", fields)).add(limit).add(skip).add(new JsonObject().put("$sort", sort));
    log.debug("Pipeline : " + pipeline);
    return aggregate(pipeline).flatMap(res -> {
        if(checkOK(res)) {
          return Observable.just(res.getJsonArray("result"));
        }
        else {
          return Observable.just(null);
        }
    });
  }
  
  public Observable<List<JsonObject>> getMembersOfGroupWithUsersAs(String gid, JsonArray uids, JsonObject fields) {
    JsonObject andClause = new JsonObject().put("$and",
        new JsonArray().add(new JsonObject().put(PARENT_GROUP_MEMBER + "." + GID, gid))
                       .add(new JsonObject().put(UID, new JsonObject().put("$in", uids))));
    return find(andClause, fields);
  }
  
  public Observable<Integer> getHandleRoleInGroup(String handleName, String gid) {
	  return handleFromHandleNameAndGroupId(handleName, gid, new JsonObject().put(PARENT_GROUP_MEMBER, 1))
		      .flatMap(handle -> {
		    	  JsonObject membership = handle.getJsonObject(PARENT_GROUP_MEMBER);
		    	  log.debug("The groupData for group id: " + gid + " is: " + membership);
		    	  return Observable.just(membership.getInteger(TYPE));
		      });
  }

  public Observable<Void> updateHandleToNonPending(String handleName, String gid) {
    ObservableFuture<Void> finalResult=new ObservableFuture<>();
    JsonObject query =
        new JsonObject().put(HandleDAO.NAME, handleName)
        .put(PARENT_GROUP_MEMBER+"."+GID,gid)
        .put(STATUS, HandleStatus.PENDING.ordinal());
     findOneAndUpdate(query, 
        new JsonObject().put("$set",new JsonObject().put(STATUS, HandleStatus.ACTIVE.ordinal())), true, false).subscribe(obj->{
          finalResult.toHandler().handle(Future.succeededFuture());
        },err->{
          finalResult.toHandler().handle(Future.failedFuture("Problem updateHandleToNonPending"));
        });
     return finalResult;
  }
 
  public  Observable<Void> markHandleAsRejectedGivenHandleNameAndGid(String handleName, String gid) {
    JsonObject query =
        new JsonObject().put(HandleDAO.NAME, handleName)
        .put(PARENT_GROUP_MEMBER+"."+GID,gid);
        //.put(STATUS, HandleStatus.PENDING.ordinal());
    return findOneAndUpdate(query, new JsonObject().put("$set", new JsonObject().put(STATUS, HandleStatus.REJECTED.ordinal())), true, false)
        .flatMap(obj->{return Observable.just(null);});
  }

  public Observable<JsonObject> handleFromUidAndParentGid(String userId, String gid,JsonObject moreQuery,
      JsonObject fields) {
    return findOne(new JsonObject().put(UID, userId).put(PARENT_GROUP_MEMBER+"."+GID,  gid).mergeIn(moreQuery),fields);
  }

  public Observable<Void> updateHandleToGivenStatus(String handleName, String gid,int status) {
    ObservableFuture<Void> finalResult=new ObservableFuture<>();
    JsonObject query =
        new JsonObject().put(HandleDAO.NAME, handleName)
        .put(PARENT_GROUP_MEMBER+"."+GID,gid);
     findOneAndUpdate(query,
        new JsonObject().put("$set",new JsonObject().put(STATUS, status)), true, false).subscribe(obj->{
          finalResult.toHandler().handle(Future.succeededFuture());
        },err->{
          finalResult.toHandler().handle(Future.failedFuture("Problem updateHandleToGiveStatus"));
        });
     return finalResult;
  }
 
  public Observable<Integer> getUserRoleInGroup(String uid, String gid) {
    JsonObject query = new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, gid).put(UID, uid);
        
    return findOne(query, new JsonObject().put(PARENT_GROUP_MEMBER+"."+TYPE, 1)).flatMap(obj->{
      int role = obj.getJsonObject(PARENT_GROUP_MEMBER).getInteger(TYPE, -1);
      return Observable.just(role != -1 ?role: HandleType.MEMBER.ordinal());
    });
  }
  
  public Observable<List<JsonObject>> getRolesForGroupAndSubGroups(String uid, String gid) {
  return handleFromUidAndParentGid(uid, gid, 
      new JsonObject(), new JsonObject().put(HandleDAO.PARENT_GROUP_MEMBER, 1)
      .put(HandleDAO.SUB_GROUP_MEMBER,1).put(HandleDAO.SUB_GROUP_MODERATOR,1)).flatMap(mbrshipData-> {
        List<JsonObject> roles=new ArrayList<>();
        roles.add(mbrshipData.getJsonObject(HandleDAO.PARENT_GROUP_MEMBER));
        JsonArray data = mbrshipData.getJsonArray(HandleDAO.SUB_GROUP_MODERATOR);
        if (data != null){
          for (Object groupId : data) {
            int type = HandleType.MODERATOR.ordinal();
            roles.add(new JsonObject().put(HandleDAO.GID, groupId).put(HandleDAO.TYPE, type));
          }
        }
        data=mbrshipData.getJsonArray(HandleDAO.SUB_GROUP_MEMBER);
        if(data != null) {
          for (Object groupId : mbrshipData.getJsonArray(HandleDAO.SUB_GROUP_MEMBER)) {
            int type = HandleType.MEMBER.ordinal();
            roles.add(new JsonObject().put(HandleDAO.GID, groupId).put(HandleDAO.TYPE, type));
          }
        }
        return Observable.just(roles);
      });
  }

  public Observable<JsonObject> setRatingAndReview(String uid, String gid, Integer rating,
      String review, String transactionId) {
    JsonObject query = new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, gid).put(UID, uid).put(STATUS, HandleStatus.ACTIVE.ordinal());
    JsonObject update = new JsonObject().put("$set", new JsonObject().put(RATING, rating).put(REVIEW, review));
    return updateTransactional(query, update, transactionId);
  }

  public Observable<List<JsonObject>> handlesForParentGid(String gid, JsonObject moreQuery,JsonObject fields) {
    return find(new JsonObject().put(PARENT_GROUP_MEMBER+"."+GID, gid).mergeIn(moreQuery),fields);
  }

  
  
}
