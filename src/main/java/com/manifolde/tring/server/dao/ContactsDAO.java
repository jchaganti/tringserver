package com.manifolde.tring.server.dao;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;

import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.netty.util.internal.ThreadLocalRandom;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;

@SuppressWarnings("unused")
public class ContactsDAO extends BaseModelDAO {

  public static final String MOBILE_NOS = "mobileNos";

  private static final Logger log = LoggerFactory.getLogger(ContactsDAO.class);
  
  public static final String USER_ID = "uid";
  
  public ContactsDAO(MongoClient mongoClient) {
    super("contacts", mongoClient);
  }

  public Observable<List<JsonObject>> usersContainingMobileNumber(String mobileNo) {
    JsonObject query = new JsonObject().put("$text", new JsonObject().put("$search", "\""+mobileNo+"\""));
    //JsonObject query = new JsonObject().put(MOBILE_NOS, new JsonObject().put("$elemMatch", elemMatchClause));
    JsonObject projection = new JsonObject().put("uid", 1);
    return find(query, projection);
  }

  public Observable<Void> updateContact(String id, JsonArray mobileNos) {
    if (id == null) {
      return Observable.just((Void)null);
    } else {
      return update(id, existing -> {
        JsonArray existingMobileNos = existing.getJsonArray(MOBILE_NOS);
        existingMobileNos.add(mobileNos);
        existing.put(MOBILE_NOS, existingMobileNos);
      });
    }
  }
  public   Observable<JsonObject> createIndexes() {
    JsonArray jsonArray = new JsonArray();
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(MOBILE_NOS, "text"))
        .put("default_language", "none").put("name", "tring_contacts_mobile_nos_index"));
    jsonArray.add(new JsonObject().put("key", new JsonObject().put(USER_ID, 1)).put("name", "tring_contacts_uid_index"));
    return createIndexes(jsonArray);
  }
}
