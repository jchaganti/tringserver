package com.manifolde.tring.server.verticles;

import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.common.base.Strings;
import com.manifolde.tring.server.dao.BaseUserToSyncDAO;
import com.manifolde.tring.server.dao.ContactsDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.GroupProfileChangedSyncRecordDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserToSyncAcceptRejectMembershipDAO;
import com.manifolde.tring.server.dao.UserToSyncAdminRoleDAO;
import com.manifolde.tring.server.dao.UserToSyncContactDAO;
import com.manifolde.tring.server.dao.UserToSyncProfileDAO;
import com.manifolde.tring.server.dao.UserToSyncSubGroupDAO;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;


public class TringContactsHandlerVerticle extends AbstractVerticle {
  private static final Logger log = LoggerFactory.getLogger(TringContactsHandlerVerticle.class);
  private JsonObject config;
  private ContactsDAO contactsDAO;
  private UserToSyncContactDAO userToSyncContactsDAO;
  private UserToSyncProfileDAO userToSyncProfileDAO;
  private UserToSyncSubGroupDAO userToSyncSubGroupDAO;
  private UserToSyncAdminRoleDAO userToSyncAdminRoleDAO;
  private HandleDAO handleDAO;
  private UserDAO userDAO;
  private UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO;
  private GroupProfileChangedSyncRecordDAO groupProfileChangedSyncRecordDAO;
  
  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    config = context.config();
  }
  
  @Override
  public void start(Future<Void> future) {
    log.debug("TringContactsHandlerVerticle::start:begin");
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_CONTACT, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_CONTACT);
      handleContact(message);
    });
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_CONTACT_PROFILE, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_CONTACT_PROFILE);
      handleContactProfile(message);
    });
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_SUBGROUP, message -> {
        log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_SUBGROUP);
        handleSubGroup(message);
      });
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_ADMIN_ROLE, message -> {
        log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_ADMIN_ROLE);
        handleAdminRole(message);
      });
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_MEMBERSHIP_REQUEST_PROCESSED, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_MEMBERSHIP_REQUEST_PROCESSED);
      handleMembershipRequestProcessed(message);
    });
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_GROUP_PROFILE, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_GROUP_PROFILE);
      handleGroupProfileChange(message);
    });
    MongoClient mongoClient = MongoClient.createShared(vertx, config.getJsonObject("mongo"));
    contactsDAO = new ContactsDAO(mongoClient);
    userToSyncContactsDAO = new UserToSyncContactDAO(mongoClient);
    TransactionDAO transactionDAO = new TransactionDAO(mongoClient);
    userDAO = new UserDAO(mongoClient, transactionDAO);
    userToSyncProfileDAO = new UserToSyncProfileDAO(mongoClient);
    handleDAO = new HandleDAO(mongoClient, transactionDAO);
    userToSyncSubGroupDAO = new UserToSyncSubGroupDAO(mongoClient, transactionDAO);
    userToSyncAcceptRejectMembershipDAO = new UserToSyncAcceptRejectMembershipDAO(mongoClient);
    userToSyncAdminRoleDAO = new UserToSyncAdminRoleDAO(mongoClient);
    groupProfileChangedSyncRecordDAO = new GroupProfileChangedSyncRecordDAO(mongoClient);
    future.complete();
    log.debug("TringContactsHandlerVerticle::start:end");
  }

  private void handleGroupProfileChange(Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject srcGrp = msgBody.getJsonObject("srcUser");
    
    createGroupProfileChangedSyncRecord(message, groupProfileChangedSyncRecordDAO).subscribe(syncId->{
      TringVerticleUtils.sendGroupProfileChangedToSync(syncId, srcGrp, vertx,success->{
        if(success){
          log.debug("success in createGroupProfileChangedSyncRecord");
          message.reply(new JsonObject().
              put("success", true));
        } else {
          log.debug("error in createGroupProfileChangedSyncRecord");
          message.reply(new JsonObject().
              put("success", false));         
        }
      });
    },err->{
      log.debug(err);
      message.reply(new JsonObject().
          put("success", false));         
    });
  }

  private Observable<String> createGroupProfileChangedSyncRecord(Message<Object> message,
      GroupProfileChangedSyncRecordDAO groupProfileChangedSyncRecordDAO) {
    // TODO Auto-generated method stub
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject grp = msgBody.getJsonObject("srcUser");
      // TODO Auto-generated method stub
      JsonObject syncGrp = new JsonObject();
      syncGrp.mergeIn(grp.getJsonObject("groupData"));
      syncGrp.put(GroupDAO.GID, syncGrp.getString(GroupDAO.ID));
      syncGrp.remove(GroupDAO.ID);
      //syncUser.put("_id", Utils.uuidToBase64(UUID.randomUUID()));
      syncGrp.put(BaseUserToSyncDAO.SYNC_STATUS, false);
      return groupProfileChangedSyncRecordDAO.insert(syncGrp);
  }

  private void handleAdminRole(Message<Object> message) {
	  JsonObject msgBody = (JsonObject) message.body();
	  String gid = msgBody.getString(HandleDAO.GID);
	  JsonObject srcUser = msgBody.getJsonObject("srcUser");
	  String action = msgBody.getString("action");
	  String tgtUid = msgBody.getString("tgtUid");
	  
	  JsonObject query1 = new JsonObject().put(HandleDAO.ID, tgtUid);
	  JsonObject fields1 = new JsonObject().put("_id", 1).put(UserDAO.TOPIC_PREFIX, 1);
	  userDAO.find(query1, fields1).subscribe(users -> {
		  if(log.isDebugEnabled()) {
			  log.debug("handleAdminRole target users: " + users);
		  }
		  _handleSendSyncMessage(message, srcUser, userToSyncAdminRoleDAO, false, (syncRecord, _tgtUser, _timerId) -> {
		        TringVerticleUtils.sendSyncSubGroupMessageToTargetUser(syncRecord, srcUser, _tgtUser, _timerId, vertx);
	      }, users, "_id",  syncRecord -> {
				syncRecord.put("action", action);
				syncRecord.put(HandleDAO.GID, gid);
				syncRecord.put("type", 2);
				syncRecord.put("appMsgType", 128);
				DateTime nowUtc = DateTime.now( DateTimeZone.UTC);
                syncRecord.put("ts", String.valueOf(nowUtc.getMillis()));
	      });
	  });
}

private void handleSubGroup(Message<Object> message) {
	  JsonObject msgBody = (JsonObject) message.body();
	  JsonArray asModerators = msgBody.getJsonArray(HandleDAO.AS_MODERATOR);
	  JsonArray asMembers = msgBody.getJsonArray(HandleDAO.AS_MEMBER);
	  JsonArray asNonMembers = msgBody.getJsonArray(HandleDAO.AS_NON_MEMBER);
	  String subGroupId = msgBody.getString(HandleDAO.SUBGROUP_GID);
	  String pgid = msgBody.getString(GroupDAO.PARENT_GID);
	  String groupName = msgBody.getString(GroupDAO.GROUP_NAME);
	  String topic = msgBody.getString(GroupDAO.MQTT_TOPIC);
	  JsonObject srcUser = msgBody.getJsonObject("srcUser");
	  String action = msgBody.getString("action");
	  JsonArray allMembers = new JsonArray();
	  if(asModerators != null) {
		  allMembers.addAll(asModerators);
	  }
	  if(asMembers != null) {
		  allMembers.addAll(asMembers);
	  }
	  if(log.isDebugEnabled()) {
		  log.debug("Non-Member HandleIds: " + asNonMembers);
	  }
	  if(asNonMembers != null) {
		  allMembers.addAll(asNonMembers);
	  }
	  if(!Strings.isNullOrEmpty(subGroupId)) {
		  if(allMembers.size() > 0) {
			  JsonObject query = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", allMembers));
			  JsonObject fields = new JsonObject().put(HandleDAO.UID, 1).put(HandleDAO.ID, 1);
			  if(log.isDebugEnabled()) {
				  log.debug("Finding all the affected handles: " + allMembers);
			  }
			  
			  handleDAO.find(query, fields).subscribe(handles -> {
				  if(log.isDebugEnabled()) {
					  log.debug("handleSubGroup handles: " + handles);
				  }
				  JsonArray tgtUids = new JsonArray();
				  JsonArray asModeratorUids = new JsonArray();
				  JsonArray asNonMemberUids = new JsonArray();
				  handles.forEach(handle -> {
					  if(asModerators != null && asModerators.contains(handle.getString(HandleDAO.ID))) {
						  asModeratorUids.add(handle.getString(HandleDAO.UID));
					  }
					  else if(asNonMembers != null && asNonMembers.contains(handle.getString(HandleDAO.ID))) {
						  asNonMemberUids.add(handle.getString(HandleDAO.UID));
					  }
					  tgtUids.add(handle.getString(HandleDAO.UID));
				  });
				  JsonObject query1 = new JsonObject().put(HandleDAO.ID, new JsonObject().put("$in", tgtUids));
				  JsonObject fields1 = new JsonObject().put("_id", 1).put(UserDAO.TOPIC_PREFIX, 1);
				  userDAO.find(query1, fields1).subscribe(users -> {
					  _handleSendSyncMessage(message, srcUser, userToSyncSubGroupDAO, false, (syncRecord, _tgtUser, _timerId) -> {
					        TringVerticleUtils.sendSyncSubGroupMessageToTargetUser(syncRecord, srcUser, _tgtUser, _timerId, vertx);
				      }, users, "_id",  syncRecord -> {
							String tgtUid = syncRecord.getString(BaseUserToSyncDAO.TGT_UID);
							int tgtMemberType = asModeratorUids.contains(tgtUid)
									? HandleDAO.HandleType.MODERATOR.ordinal()
									: asNonMemberUids.contains(tgtUid) ? HandleDAO.HandleType.NONMEMBER.ordinal()
											: HandleDAO.HandleType.MEMBER.ordinal();
							syncRecord.put(HandleDAO.MEMBERS_TYPE, tgtMemberType);
							syncRecord.put(HandleDAO.SUBGROUP_GID, subGroupId);
							syncRecord.put(GroupDAO.PARENT_GID, pgid);
							syncRecord.put(GroupDAO.GROUP_NAME, groupName);
							syncRecord.put(GroupDAO.MQTT_TOPIC, topic);
							syncRecord.put("action", action);
							syncRecord.put("type", 2);
							DateTime nowUtc = DateTime.now( DateTimeZone.UTC);
							syncRecord.put("ts", String.valueOf(nowUtc.getMillis()));
							syncRecord.put("appMsgType", 256);
				      });
				  });
			  });
		  }
		  else {
			  message.reply(new JsonObject().put("success", true).put("message", "No handle ids for sending message"));
		  }
		  
	  }
	  else {
		  message.reply(new JsonObject().put("success", false).put("message", "subGroupId is null or empty"));
	  }
	
  }
  private void handleMembershipRequestProcessed(Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject tgtUser = msgBody.getJsonObject("srcUser");
    createMembershipProcessedSyncRecord(message, tgtUser, userToSyncAcceptRejectMembershipDAO).subscribe(syncId->{
      TringVerticleUtils.sendMembershipProcessedMessageToSync(syncId, tgtUser, vertx,success->{
        if(success){
          message.reply(new JsonObject().
              put("success", true));
        } else {
          message.reply(new JsonObject().
              put("success", false));         
        }
      });
    });
  }

  private Observable<String> createMembershipProcessedSyncRecord(Message<Object> message, JsonObject tgtUser,
      UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO) {
    // TODO Auto-generated method stub
    JsonObject syncUser = new JsonObject();
    syncUser.mergeIn(tgtUser);
    //syncUser.put("_id", Utils.uuidToBase64(UUID.randomUUID()));
    syncUser.put(BaseUserToSyncDAO.SYNC_STATUS, false);
    return userToSyncAcceptRejectMembershipDAO.insert(syncUser);
  }

private void handleContact(Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject srcUser = msgBody.getJsonObject("srcUser");
    String srcMobileNo = srcUser.getString(UserDAO.MOBILE_NO);
    if(!Strings.isNullOrEmpty(srcMobileNo)) {
      handleSendSyncMessage(message, srcUser, userToSyncContactsDAO, true, (syncRecord, _tgtUser, _timerId) -> {
    	  String _syncId = syncRecord.getString("_id");
    	  TringVerticleUtils.sendSyncContactMessageToTargetUser(_syncId, srcUser, _tgtUser, _timerId, vertx);
      });
    }
    else {
      log.debug("srcMobileNo is null");
    }
  }

  private void handleSendSyncMessage(Message<Object> message, JsonObject srcUser,
      BaseUserToSyncDAO baseUserToSyncDAO, boolean updateUserAsHandled, MessageHandler messageHandler) {
    Observable<List<JsonObject>> contactsObservable = contactsDAO.usersContainingMobileNumber(srcUser.getString(UserDAO.MOBILE_NO));
    contactsObservable.
    subscribe(contacts -> {
      if(contacts != null && contacts.size() > 0) {
        _handleSendSyncMessage(message, srcUser, baseUserToSyncDAO, updateUserAsHandled, messageHandler, contacts, ContactsDAO.USER_ID, null);
      }
      else {
        handleUserAndReply(message, srcUser, updateUserAsHandled);
      }
    });
  }

private void _handleSendSyncMessage(Message<Object> message, JsonObject srcUser, BaseUserToSyncDAO baseUserToSyncDAO,
		boolean updateUserAsHandled, MessageHandler messageHandler, List<JsonObject> users, String uidKey, SyncRecordFieldAdder syncRecordFieldAdder) {
	JsonArray syncRecords = new JsonArray();
	users.forEach(user -> {
	  // Create a record that tracks sync message has been sent to each user
	  // who has this contactNo
	  JsonObject syncRecord = new JsonObject();
	  syncRecord.put("_id", Utils.uuidToBase64(UUID.randomUUID()));
	  syncRecord.put(BaseUserToSyncDAO.SRC_UID, srcUser.getString("_id"));
	  syncRecord.put(BaseUserToSyncDAO.TGT_UID, user.getString(uidKey));
	  syncRecord.put(BaseUserToSyncDAO.SRC_MOBILE_NO, srcUser.getString(UserDAO.MOBILE_NO));
	  syncRecord.put(BaseUserToSyncDAO.SYNC_STATUS, false);
	  if(syncRecordFieldAdder != null) {
		  syncRecordFieldAdder.add(syncRecord);
	  }
	  syncRecords.add(syncRecord);
	});
	baseUserToSyncDAO.bulkInsert(syncRecords, false).subscribe(obj -> {            
	  sendMQTTMessage(message, srcUser, updateUserAsHandled, messageHandler, syncRecords);
	}, err -> {
	  log.error("Error during" +  baseUserToSyncDAO.getClass().getName() + ".bulkInsert");
      sendMQTTMessage(message, srcUser, updateUserAsHandled, messageHandler, syncRecords);
      err.printStackTrace();
	});
}

private void sendMQTTMessage(Message<Object> message, JsonObject srcUser,
    boolean updateUserAsHandled, MessageHandler messageHandler, JsonArray syncRecords) {
  syncRecords.forEach(_syncRecord -> {
    log.debug("The _syncRecord: " + _syncRecord);
    JsonObject syncRecord = (JsonObject)_syncRecord;
    userDAO.findById(syncRecord.getString(UserToSyncContactDAO.TGT_UID), null).subscribe(tgtUser -> {
      // Send Message
      vertx.setTimer(100, id -> {
        messageHandler.sendMessageToTargetUser(syncRecord, tgtUser, id);
      });
    });
  });
  handleUserAndReply(message, srcUser, updateUserAsHandled);
}

  private void handleUserAndReply(Message<Object> message, JsonObject srcUser,
      boolean updateUserAsHandled) {
    if(updateUserAsHandled) {
      setUserAsHandled(message, srcUser);
    }
    else {
    	if(message != null) {
    		message.reply(new JsonObject().
		          put("success", true)
		          );
    	}
    }
  }

  private void handleContactProfile(Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject srcUser = msgBody.getJsonObject("srcUser");
    String srcMobileNo = srcUser.getString(UserDAO.MOBILE_NO);
    if(!Strings.isNullOrEmpty(srcMobileNo)) {
      if(TringConstants.TRING_PROFILE_REWRITE) {
        TringVerticleUtils.sendSyncProfileMessageToSrcUserStatus(srcUser, vertx, success -> {
          if(success) {
            JsonObject query = new JsonObject().put("_id", srcUser.getString("_id"));
            JsonObject update = new JsonObject();
            update.put(UserDAO.PROFILE_HANDLED, true);
            userDAO.findOneAndUpdate(query, new JsonObject().put("$set", update), true, false).
            subscribe(modifiedUser -> {
              if(log.isDebugEnabled()) {
                if(modifiedUser != null) {
                  log.debug("Modified User for profileHandled: " + modifiedUser.getJsonObject("value").getString("_id"));
                }
                else {
                  log.debug("Modified User for profileHandled: null");
                }
              }
              message.reply(new JsonObject().put("success", true));
            }, err -> {
              log.error("Error while updating user for profileHandled");
              err.printStackTrace();
            });
          }
        });
      }
      else {
        handleSendSyncMessage(message, srcUser, userToSyncProfileDAO, false, (syncRecord, _tgtUser, _timerId) -> {
        	String _syncId = syncRecord.getString("_id");
        	TringVerticleUtils.sendSyncProfileMessageToTargetUser(_syncId, srcUser, _tgtUser, _timerId, vertx);
        });
      }
      
    }
    else {
      log.debug("srcMobileNo is null");
    }
  }
  
  private void setUserAsHandled(Message<Object> message, JsonObject srcUser) {
    String _id = srcUser.getString("_id");
    JsonObject update =
        new JsonObject().put("$set", new JsonObject().put(UserDAO.HANDLED_CONTACTS, true));
    userDAO.findOneAndUpdate(new JsonObject().put("_id", _id), update, true, false).subscribe(updatedObj -> {
      log.debug("The user id : " + _id + " has been set as handled");
      if(message != null) {
		  message.reply(new JsonObject().
		          put("success", true)
		          ); 
      }
    });
  }

  @Override
  public void stop(Future<Void> future) {
    log.debug("TringContactsHandlerVerticle::end:begin");
    log.debug("TringContactsHandlerVerticle::end:end");
    future.complete();
  }
  
  @FunctionalInterface
  protected interface MessageHandler {
    void sendMessageToTargetUser(JsonObject syncRecord, JsonObject tgtUser, Long timerId);
  }
  
  @FunctionalInterface
  protected interface SyncRecordFieldAdder {
    void add(JsonObject syncRecord);
  }
}
