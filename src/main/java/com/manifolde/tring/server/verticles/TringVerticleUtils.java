package com.manifolde.tring.server.verticles;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import com.manifolde.tring.server.dao.BaseUserToSyncDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.utils.TringConstants;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rx.java.ObservableFuture;
import io.vertx.rxjava.core.Vertx;
import rx.Observable;

public class TringVerticleUtils {
  
  private static final Logger log = LoggerFactory.getLogger(TringVerticleUtils.class);
  
  public static Observable<Boolean> sendMessageToSync(String syncMsgTopic, JsonObject srcUser, Long id, Vertx vertx) {
    JsonObject msg = new JsonObject();
    msg.put("srcUser", srcUser);
    String userId = srcUser.getString("_id");
    String userMobileNo = srcUser.getString(UserDAO.MOBILE_NO);
    log.debug("sendMessageToHandleContact userId: " + userId + " userMobileNo: " + userMobileNo);
    ObservableFuture<Boolean> result=new ObservableFuture<>();
    vertx.eventBus().sendObservable(syncMsgTopic, msg).subscribe(replyMsg-> {
      if(id != null) {
        vertx.cancelTimer(id);
      }
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      if (success){
        result.toHandler().handle(Future.succeededFuture(true));
      }else {
        result.toHandler().handle(Future.failedFuture("failed sendMessageToSync"));
      }
      log.debug("Message sent to " + syncMsgTopic + " with mobile No: " + userMobileNo + " ands user id: " + userId + " is: " + success);
    }, err -> {
      log.error("Error while Message being sent to topic " + syncMsgTopic + " :" + err.getMessage() + " with mobile No: " + userMobileNo + " ands user id: " + userId);
      err.printStackTrace();
      result.toHandler().handle(Future.failedFuture("failed sendMessageToSync"));
    });
    return result;
  }
  
  public static void sendSyncContactMessageToTargetUser(String syncId, JsonObject srcUser, JsonObject tgtUser, Long id, Vertx vertx) {
    JsonObject data = new JsonObject().put("syncUpMobileNo", srcUser.getString(UserDAO.MOBILE_NO)).put("syncId", syncId);
    String topic = tgtUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + tgtUser.getString("_id");
    JsonObject msg = getMessage( srcUser, topic, 8, data);
    
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, id, msg, vertx, null);
  }
  
  public static void sendSyncProfileMessageToTargetUser(String syncId, JsonObject srcUser, JsonObject tgtUser, Long id, Vertx vertx) {
    JsonObject data = new JsonObject().
                      put(UserDAO.CUSTOM_MESSAGE, srcUser.getString(UserDAO.CUSTOM_MESSAGE)).
                      put("syncId", syncId).
                      put("syncUpMobileNo", srcUser.getString(UserDAO.MOBILE_NO)).
                      put(UserDAO.CONTENT_ID, srcUser.getString(UserDAO.CONTENT_ID)).
                      put(UserDAO.NAME, srcUser.getString(UserDAO.NAME)).
                      put(UserDAO.THUMBNAIL_DATA, srcUser.getString(UserDAO.THUMBNAIL_DATA));
    String topic = tgtUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + tgtUser.getString("_id");
    JsonObject msg = getMessage(srcUser, topic, 32, data);
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, id, msg, vertx, null);
  }
  
  public static void sendGroupMembershipRequestMessage(JsonObject group, String handleName, String requestorUid, String userMessage, Vertx vertx, MQTTMessageReplyHandler replyHandler) {
    String gid = group.getString(GroupDAO.ID);
    String groupName = group.getString(GroupDAO.GROUP_NAME);
    JsonObject otherData = new JsonObject().
        put(TringWebApiServer.HANDLE_NAME,handleName).
        put(TringWebApiServer.GROUP_NAME,groupName).
        put(TringWebApiServer.GID,gid).
        put(TringWebApiServer.SID,GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid).
        put(TringWebApiServer.REQUESTOR_UID,requestorUid).
        put(TringWebApiServer.MESSAGE_DATA, userMessage+"\nRequest for membership to group "+
           groupName + " by handle "+handleName);
    
    String topic = group.getString(GroupDAO.TOPIC_PREFIX) + GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid+GroupDAO.PVT;
    JsonObject msg = buildMessage(null, topic, 8, "pvtMsgType", 4, otherData);
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null, msg, vertx, replyHandler);
  }
  public static void sendSyncProfileMessageToSrcUserStatus(JsonObject srcUser, Vertx vertx, MQTTMessageReplyHandler replyHandler) {
    JsonObject data = new JsonObject().
                      put(UserDAO.CUSTOM_MESSAGE, srcUser.getString(UserDAO.CUSTOM_MESSAGE)).
                      put(UserDAO.PROFILE_COUNTER, srcUser.getInteger(UserDAO.PROFILE_COUNTER)).
                      put("syncUpMobileNo", srcUser.getString(UserDAO.MOBILE_NO)).
                      put(UserDAO.CONTENT_ID, srcUser.getString(UserDAO.CONTENT_ID)).
                      put(UserDAO.NAME, srcUser.getString(UserDAO.NAME)).
                      put(UserDAO.THUMBNAIL_DATA, srcUser.getString(UserDAO.THUMBNAIL_DATA));
    String topic = srcUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + srcUser.getString("_id") + "/profile";
    JsonObject msg = getMessage(srcUser, topic, 32, data);
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null, msg, vertx, replyHandler);
  }

  public static void _sendMessage(String eventBusTopic, Long id, JsonObject msg, Vertx vertx, MQTTMessageReplyHandler replyHandler) {
    vertx.eventBus().sendObservable(eventBusTopic, msg)
    .subscribe(replyMsg-> {
      if(id != null) {
        vertx.cancelTimer(id);
      }
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      log.debug("The message status sent on eventBusTopic " + eventBusTopic + " with message: " + msg + " with success status: " + success);
      if(replyHandler != null) {
        replyHandler.handleReply(reply.getBoolean("success"));
      }
    }, err -> {

      log.error("Error during sending the message" + err.getMessage());
      err.printStackTrace();
      if (replyHandler != null) {
        replyHandler.handleReply(false);
      }
    });
  }

  public static JsonObject getMessage(JsonObject srcUser, String topic, int appMsgType, JsonObject data) {
    JsonObject msg = new JsonObject();
    msg.put("topic", topic);
    JsonObject _msg = new JsonObject();
    _msg.put("type", 2);
    _msg.put("appMsgType", appMsgType);
    _msg.put("data", data);
    msg.put("msg", _msg);
    return msg;
  }
  public static JsonObject buildMessage(JsonObject srcUser, String topic, int msgType, String msgSubTypeProp, int msgSubType, JsonObject otherData) {
    JsonObject msg = new JsonObject();
    msg.put("topic", topic);
    JsonObject _msg = new JsonObject();
    _msg.put("type", msgType);
    _msg.put(msgSubTypeProp, msgSubType);
    _msg.mergeIn(otherData);
    DateTime nowUtc = DateTime.now( DateTimeZone.UTC);
    long now = nowUtc.getMillis();
    _msg.put("ts", String.valueOf(now));
    _msg.put("_id",  _msg.hashCode()+""+now);
    msg.put("msg", _msg);
    log.debug("buildMessage - msg is "+msg);
    return msg;
  }
  public static JsonObject getSyncModeratorMessage(String topic, String reputationKey, String gid, String moderatorMessage, String syncId) {
    JsonObject msg = new JsonObject();
    msg.put("topic", topic);
    JsonObject _msg = new JsonObject();
    _msg.put("type", 16);
    if(UserDAO.EVICTIONS.equals(reputationKey)) {
    	_msg.put("userReputationMsgType", 4);
    }
    else if(UserDAO.WARNINGS.equals(reputationKey)){
    	_msg.put("userReputationMsgType", 2);
    }
    else {
    	log.error("Unexpected reputationKey..");
    }
    _msg.put("sid", GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid);
    _msg.put("data", moderatorMessage);
    DateTime nowUtc = DateTime.now( DateTimeZone.UTC);
    _msg.put("ts", String.valueOf(nowUtc.getMillis()));
    _msg.put("syncId", syncId);
    msg.put("msg", _msg);
    return msg;
  }
  
  @FunctionalInterface
  public interface MQTTMessageReplyHandler {
    void handleReply(boolean success);
  }

  public static void sendSyncModeratorMessageToTargetUser(String syncId, String moderatorMessage, String reputationKey, String gid, JsonObject srcUser, JsonObject tgtUser, Long id,
		io.vertx.rxjava.core.Vertx vertx) {
	String topic = tgtUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + tgtUser.getString("_id");
	JsonObject syncModeratorMessage = getSyncModeratorMessage(topic, reputationKey, gid, moderatorMessage, syncId);
	_sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null, syncModeratorMessage, vertx, success -> {
		if(success) {
			log.debug("Sync Moderator Message send to mobile No: " + tgtUser.getString(UserDAO.MOBILE_NO));
		}
		else {
			log.debug("Failed to send Sync Moderator Message to mobile No: " + tgtUser.getString(UserDAO.MOBILE_NO));
		}
	});
	
  }

  public static void sendSyncSubGroupMessageToTargetUser(JsonObject syncRecord, JsonObject srcUser, JsonObject tgtUser,
		Long timerId, io.vertx.rxjava.core.Vertx vertx) {
	  String topic = tgtUser.getString(UserDAO.TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + tgtUser.getString("_id");
	  JsonObject syncSubGroupMessage = getSyncSubGroupMessage(topic, syncRecord);
	  _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, timerId, syncSubGroupMessage, vertx, success -> {
			if(success) {
				log.debug("Sync Moderator Message send to mobile No: " + tgtUser.getString(UserDAO.MOBILE_NO));
			}
			else {
				log.debug("Failed to send Sync Moderator Message to mobile No: " + tgtUser.getString(UserDAO.MOBILE_NO));
			}
	  });
	
  }

  private static JsonObject getSyncSubGroupMessage(String topic, JsonObject syncRecord) {
	JsonObject msg = new JsonObject();
    msg.put("topic", topic);
    JsonObject _msg = syncRecord;
    msg.put("msg", _msg);
    return msg;
  }
  
  public static void sendMembershipProcessedMessageToSync(
      String  syncId, JsonObject tgtUser, Vertx vertx, MQTTMessageReplyHandler func) {
    String gid=tgtUser.getString(HandleDAO.GID);
    String handleName=tgtUser.getString(TringWebApiServer.HANDLE_NAME);
    String name=tgtUser.getString(GroupDAO.GROUP_NAME);
    Boolean isReject=tgtUser.getBoolean(TringWebApiServer.REJECTED);
    String topic = tgtUser.getString(TringWebApiServer.USER_TOPIC_PREFIX) + UserDAO.TOPIC_TAIL_NAME_PREFIX + tgtUser.getString(BaseUserToSyncDAO.TGT_UID);
    System.out.println("In sendMembershipProcessedMessageToSync topic is "+topic);
    String groupTopic = tgtUser.getString(GroupDAO.TOPIC_PREFIX) + GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid;
    Boolean o2m = tgtUser.getBoolean(GroupDAO.GROUP_O2M);
    Boolean closed = tgtUser.getBoolean(GroupDAO.CLOSED_GROUP);
    JsonObject syncMessage = getSyncMembershipProcessedMessage(topic, gid, name, groupTopic,handleName, o2m, closed,syncId,isReject);
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null, syncMessage, vertx, func);  
  }

  private static JsonObject getSyncMembershipProcessedMessage(String topic, String gid, String name,String groupTopic,
      String handleName, Boolean o2m, Boolean closed, String syncId, boolean isReject) {
    JsonObject data = new JsonObject().
        put(HandleDAO.GID, gid)
        .put("sid", GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid)
        .put(TringWebApiServer.HANDLE_NAME, handleName)
        .put(TringWebApiServer.REJECTED,isReject)
        .put("syncId", syncId)
        .put(TringWebApiServer.GROUP_TOPIC,groupTopic)
        .put(TringWebApiServer.GROUP_O2M,o2m)
        .put(TringWebApiServer.GROUP_CLOSED, closed)
        .put(TringWebApiServer.GROUP_NAME, name);
    return buildMessage(null, topic, 8,"pvtMsgType", 8, data);
  }

  public static void sendGroupProfileChangedToSync(String syncId,JsonObject srcGrp, Vertx vertx, MQTTMessageReplyHandler replyHandler) {
    String topic = srcGrp.getString(UserDAO.TOPIC_PREFIX) + GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + srcGrp.getString("_id") + "/status";
    
    JsonObject syncMessage = getGroupProfileChangedMessage(topic, syncId, srcGrp);
    _sendMessage(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, null, syncMessage, vertx, replyHandler);
  }

  private static JsonObject getGroupProfileChangedMessage(String topic, String syncId, JsonObject srcGrp) {
    String gid = srcGrp.getString(GroupDAO.ID);
    JsonObject groupData = srcGrp.getJsonObject("groupData");
    String thumbnailData = groupData.getString(TringWebApiServer.THUMBNAIL_DATA);
    String contentId = groupData.getString(TringWebApiServer.CONTENT_ID);
    String description = groupData.getString(GroupDAO.GROUP_DESCRIPTION);
    String rules = groupData.getString(GroupDAO.GROUP_RULES);
    JsonObject data = new JsonObject()
        .put(HandleDAO.GID, gid)
        .put("sid", GroupDAO.GROUP_TOPIC_TAIL_NAME_PREFIX + gid)
        .put("syncId", syncId)
        .put(TringWebApiServer.THUMBNAIL_DATA,thumbnailData)
        .put(TringWebApiServer.CONTENT_ID, contentId);
        //.put(GroupDAO.GROUP_DESCRIPTION,description)
        //.put(GroupDAO.GROUP_RULES,rules);

    log.debug("getGroupProfileChangedMessage - msg data is "+ data);
    return buildMessage(null, topic, 2, "appMsgType", 512, data);
  }
}
