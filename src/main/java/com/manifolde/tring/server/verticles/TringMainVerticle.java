package com.manifolde.tring.server.verticles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.manifolde.tring.server.utils.ClusteredObjectStore;
import com.manifolde.tring.server.utils.ExecuteOnceThreadSafe;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.TringHazelcastClusterManager;
import com.manifolde.tring.server.utils.TringRunner;
import com.manifolde.tring.server.utils.Utils;
import com.manifolde.tring.server.utils.async.MultipleFutures;

import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.file.FileSystem;



/**
 * Main verticle, orchestrate the instantiation of other verticles
 * 
 */
public class TringMainVerticle extends AbstractVerticle {


  private List<String> deploymentIds;
  private static ClusteredObjectStore propertiesMap;
  private static Logger log;
  private static ClusterManager mgr;

  static {
    System.setProperty(LoggerFactory.LOGGER_DELEGATE_FACTORY_CLASS_NAME,
        "io.vertx.core.logging.SLF4JLogDelegateFactory");
    log = LoggerFactory.getLogger(TringMainVerticle.class);
  }

  public static void main(String[] args) {
//    if(!Utils.isDevMode()) {
//      log.debug("Initing TringHazelcastClusterManager");
//      mgr = new TringHazelcastClusterManager();
//    }
    TringRunner.run(TringMainVerticle.class, !Utils.isDevMode(), mgr);
  }


  @Override
  public void init(Vertx vertx, Context context) {
    log.debug("Initing TringMainVerticle");
//    if(mgr == null) {
//      mgr = new TringHazelcastClusterManager();
//    }
//    context.put(TringConstants.TRING_CLUSTER_MANAGER, mgr);
    super.init(vertx, context);
    
    deploymentIds = new ArrayList<>(3);
    propertiesMap = new ClusteredObjectStore(super.vertx, TringConstants.TRING_PROPERTIES_MAP);
  }

  @Override
  public void start(Future<Void> future) {
    log.debug("Current Main verticle is running in thread" + Thread.currentThread().getName());
    MultipleFutures mfut = new MultipleFutures();
    if (Utils.isDevMode()) {
      log.debug("Tring server starting in dev mode");
    }

    mfut.add(fut -> {
      new ExecuteOnceThreadSafe(vertx).run(TringConstants.PROPS_FILE_LOCK, this::loadPropertiesFile,
          fut);
    });

    mfut.setHandler(res -> {
      log.debug("Deploying workers");
      deployAppVerticles(future, this::deployWebApiServer);
    });
    mfut.start();
  }

  private void loadPropertiesFile(Future<Void> future) {
    FileSystem fs = vertx.fileSystem();
    fs.readFile(TringConstants.TRING_SERVER_PROPERTIES_FILE_NAME, res -> {
      if (res.succeeded()) {
        loadPropertiesFile(res, future);
      } else {
        log.error("Tring Props file could not be read");
      }
    });
  }

  private void loadPropertiesFile(AsyncResult<Buffer> res, Future<Void> future) {
    Buffer buffer = res.result();
    String str = buffer.toString("UTF-8");
    System.out.println(str);
    List<String> props = Arrays.asList(str.split("\n"));
    JsonObject json = new JsonObject();
    props.stream().forEach(kv -> {
      String[] _kv = kv.split("=");
      json.put(_kv[0], _kv[1].replaceAll("\\r\\n|\\r|\\n", ""));
    });
    log.debug("Putting the tring properties into map");
    propertiesMap.put(TringConstants.SERVER_PROPS, json).subscribe(o -> {
      log.debug("Putting the tring properties into map finished");
      future.complete();
    });
  }

  private void deployAppVerticles(Future<Void> future, Handler<Future<Void>> whatsNext) {
    MultipleFutures appVerticlesDeploymentsFuture = new MultipleFutures();
    appVerticlesDeploymentsFuture.add(this::deployTringMQTTClient);
    appVerticlesDeploymentsFuture.add(this::deployTringContentVault);//TringPushNotificationVerticle
    //appVerticlesDeploymentsFuture.add(this::deployTringPushNotification);
    //appVerticlesDeploymentsFuture.add(this::deployTringSolrVerticle);
    //appVerticlesDeploymentsFuture.add(this::deployTringVertxJerseyVerticle);
    //appVerticlesDeploymentsFuture.add(this::deploySolrServiceVerticle);
    

    appVerticlesDeploymentsFuture.setHandler(result -> {
      if (result.failed()) {
        future.fail(result.cause());
      } else {
        whatsNext.handle(future);
        log.info("All workers deployed successfully");
      }
    });
    appVerticlesDeploymentsFuture.start();
  }

  private void deployTringMQTTClient(Future<Void> future) {
    deployAppverticle(TringMQTTClientVerticle.class.getName(), future, true);
  }

  private void deployTringContentVault(Future<Void> future) {
    deployAppverticle(TringContentVaultVerticle.class.getName(), future, false);
  }
  
  private void deployTringSolrVerticle(Future<Void> future) {
    deployAppverticle(TringSolrVerticle.class.getName(), future, true);
  }
  
//  private void deployTringVertxJerseyVerticle(Future<Void> future) {
//    JsonObject config = new JsonObject();
//    config.put("port", 9001);
//    config.put("resources", new JsonArray(Arrays.asList(new String[]{"com.englishtown.vertx.jersey.resources", "com.englishtown.vertx.jersey.resources2"})));
//    config.put("features", new JsonArray(Arrays.asList(new String[]{"org.glassfish.jersey.jackson.JacksonFeature"})));
//    config.put("binders", new JsonArray(Arrays.asList(new String[]{"com.englishtown.vertx.jersey.AppBinder"})));
//    vertx.setPeriodic(50, id -> {
//      propertiesMap.get(TringConstants.SERVER_PROPS).subscribe(props -> {
//          if(props != null) {
//            vertx.cancelTimer(id);
//            _deployServiceVerticle("java-hk2:"+JerseyVerticle.class.getName(), future, config,props, false);
//          }
//      });
//    });
//  }
//  
//  private void deploySolrServiceVerticle(Future<Void> future) {
//    JsonObject config = new JsonObject();
//    config.put("server_url", "http://localhost:8983/solr")
//            .put("address", "tring.solr.service");
//    vertx.setPeriodic(50, id -> {
//      propertiesMap.get(TringConstants.SERVER_PROPS).subscribe(props -> {
//          if(props != null) {
//            vertx.cancelTimer(id);
//            _deployServiceVerticle("java-hk2:"+SolrServiceVerticle.class.getName(), future, config,props, false);
//          }
//      });
//    });
//  }
//  
  private void deployAppverticle(String className, Future<Void> future, boolean worker) {
    vertx.setPeriodic(50, id -> {
      propertiesMap.get(TringConstants.SERVER_PROPS).subscribe(props -> {
          if(props != null) {
            vertx.cancelTimer(id);
            _deployAppVerticle(className, future, props, worker);
          }
      });
    });
  }


  private void _deployAppVerticle(String className, Future<Void> future,
      Object props, boolean worker) {
    DeploymentOptions options = new DeploymentOptions();
    options.setInstances(4);
    options.setWorker(worker);
    JsonObject config = new JsonObject();
    config.put(TringConstants.TRING_PROPERTIES_MAP, props);
    options.setConfig(config);
    vertx.deployVerticle(className, options, result -> {
      if (result.failed()) {
        log.error(className + " deployment failed");
        future.fail(result.cause());
      } else {
        future.complete();
        log.debug(className + " successfully deployed");
      }
    });
  }

//  private void _deployServiceVerticle(String className, Future<Void> future,
//     JsonObject config, Object props, boolean worker) {
//    DeploymentOptions options = new DeploymentOptions();
//    options.setInstances(4);
//    options.setWorker(worker);
//    config.put(TringConstants.TRING_PROPERTIES_MAP, props);
//    options.setConfig(config);
//    vertx.deployVerticle(className, options, result -> {
//      if (result.failed()) {
//        log.error(className + " deployment failed");
//        future.fail(result.cause());
//      } else {
//        future.complete();
//        log.debug(className + " successfully deployed");
//      }
//    });
//  }
  private void deployWebApiServer(Future<Void> future) {
    propertiesMap.get(TringConstants.SERVER_PROPS).flatMap(props -> {
      if (log.isDebugEnabled()) {
        log.debug("In deployWebApiServer : props is " + props);
      }
      if (props == null) {
        future.fail("In deployWebApiServer : props is null");
      }
      JsonObject _props = (JsonObject) props;
      Boolean singledbVertxCluster = Boolean.valueOf(_props.getString("tring.server.singledb.vertxcluster"));
      JsonObject dbConfig = null;
      if(Utils.isDevMode() || singledbVertxCluster != null && singledbVertxCluster) {
          dbConfig=mongoConfig(props);
          } else {
         dbConfig=mongoConfigProd(props);
      }
      JsonObject config = new JsonObject();
      config.put("mongo", dbConfig);
      config.put(TringConstants.TRING_PROPERTIES_MAP, props);
      DeploymentOptions webserverOptions = new DeploymentOptions();
      webserverOptions.setConfig(config);
      webserverOptions.setInstances(2);
      return vertx.deployVerticleObservable(TringWebApiServer.class.getName(), webserverOptions);
    }).subscribe(id -> deploymentIds.add(id), err -> future.fail(err), () -> {
      future.complete();
      log.debug("TringWebApiServer successfully deployed");
    });
  }


  private JsonObject mongoConfig(Object props) {
    JsonObject _props = (JsonObject) props;
    JsonObject config = new JsonObject();
    config.put("host", _props.getString("tring.server.db.host"));
    config.put("port", Integer.valueOf(
        (String) _props.getString("tring.server.db.port")));
    config.put("db_name",
        _props.getString("tring.server.db.name"));
    return config;
  }

  private JsonObject mongoConfigProd(Object props) {
    JsonObject _props = (JsonObject) props;
    JsonObject config = new JsonObject();
    config.put("db_name",
        _props.getString("tring.server.db.name"));
    config.put("username", _props.getString("tring.server.db.user"));
    config.put("password", _props.getString("tring.server.db.password"));
    config.put("authMechanism", _props.getString("tring.server.db.authMechanism"));
    config.put("authSource", _props.getString("tring.server.db.authSource"));
    JsonArray hosts = new JsonArray(Arrays.asList(new JsonObject[]{
        new JsonObject().put("host", _props.getString("tring.server.db.host1"))
        .put("port",Integer.valueOf(_props.getString("tring.server.db.port1"))),
        new JsonObject().put("host", _props.getString("tring.server.db.host2"))
        .put("port",Integer.valueOf(_props.getString("tring.server.db.port2"))),
        new JsonObject().put("host", _props.getString("tring.server.db.host0"))
        .put("port",Integer.valueOf(_props.getString("tring.server.db.port0")))
    }));
    config.put("hosts", hosts);
    config.put("replicaSet", _props.getString("tring.server.db.replSet"));
    return config;
  }

  @Override
  public void stop(Future<Void> future) {
    MultipleFutures futures = new MultipleFutures(future);
    deploymentIds.forEach(deploymentId -> {
      futures.add(fut -> {
        undeploy(deploymentId, fut);
      });
    });
    futures.start();
  }

  private void undeploy(String deploymentId, Future<Void> future) {
    vertx.undeploy(deploymentId, res -> {
      if (res.succeeded()) {
        future.complete();
      } else {
        future.fail(res.cause());
      }
    });
  }
}
