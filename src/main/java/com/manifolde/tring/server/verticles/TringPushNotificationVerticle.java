package com.manifolde.tring.server.verticles;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ISet;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.utils.TringConstants;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Observable;


public class TringPushNotificationVerticle extends AbstractVerticle {
  private static final Logger log = LoggerFactory.getLogger(TringPushNotificationVerticle.class);
  private static final String GCM_API_KEY = "AIzaSyDooJ__zfPLL7Yy7CfeNArQgCmflyl28Hg";
  private @Nullable JsonObject config;
  private UserDAO userDAO;
  private ApnsService apnsService;

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    InputStream certStream = this.getClass().getClassLoader().getResourceAsStream("your_certificate.p12"); //TODO: change to actual file name.
    apnsService = APNS.newService().withCert(certStream, "you_cert_password").withSandboxDestination().build(); //TODO: actual password
    apnsService.start();
    config = context.config();
  }

  @Override
  public void start(Future<Void> future) {
    log.debug("TringPushNotificationVerticle::start:begin");
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_PUSH_NOTIFICATION, message -> {
      log.debug(
          "Message received on address: " + TringConstants.TRING_SERVER_HANDLE_PUSH_NOTIFICATION);
      handlePushNotification(message);
    });
    MongoClient mongoClient = MongoClient.createShared(vertx, config.getJsonObject("mongo"));
    TransactionDAO transactionDAO = new TransactionDAO(mongoClient);
    userDAO = new UserDAO(mongoClient, transactionDAO);
    future.complete();
    log.debug("TringPushNotificationVerticle::start:end");
  }

  private void handlePushNotification(io.vertx.rxjava.core.eventbus.Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    int count = msgBody.getInteger(TringConstants.COUNT);
    log.debug("The handlePushNotification count: " + count);
    JsonArray clientIds = msgBody.getJsonArray(TringConstants.IDS);// Integer(TringConstants.IDS);
    log.debug("The handlePushNotification clientIds: " + clientIds);
    HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
    IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
    ISet<String> notifiableIdSet  = hazelcastInstance.getSet(TringConstants.TRING_NOTIFIABLE_IDS);
    Set<String> clientIdSet = new HashSet<>(clientIds.getList());
    clientIdSet.retainAll(notifiableIdSet);
    boolean isAndroid=false;
    if(clientIds.size() > 0) {
      String oneClientId = clientIds.getString(0);
      if (oneClientId.endsWith("_g")) {
        isAndroid = true;
      }
    }
    final boolean isAndroidDevice = isAndroid;
    vertx.executeBlocking(fut -> {
      log.debug("The hazelcastInstance map: " + map);
      fut.complete(map.values(getPredicate(clientIdSet)));
    },  res -> {
      Collection<String> _res = (Collection<String>)res.result();
      log.debug("The map.values: " + _res);
      log.debug("The map.size: " + _res.size());
      if(_res.size() > 0) {
        sendPushNotification(count, _res, isAndroidDevice);
      }
      
      vertx.executeBlocking(fut1 -> {
       // HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
       // IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
        fut1.complete(map.keySet(getPredicate(clientIdSet)));
      }, res1 -> {
        Set<String> processedClientIds = (Set<String>)res1.result();
        log.debug("The processedClientIds: " + processedClientIds);
        Set<String> allClientIds = clientIdSet;//new HashSet<>(clientIds.getList());
        Set<String> pendingClientIds = Sets.difference(allClientIds, processedClientIds);
        log.debug("The pendingClientIds: " + pendingClientIds);
        if(pendingClientIds.size() >0) {
          JsonArray pendingClientIdsArr = new JsonArray(new ArrayList<>(pendingClientIds));
          userDAO.usersByDeviceIds(pendingClientIdsArr, new JsonObject().put(UserDAO.PN_ID, 1).put("devices", 1)).subscribe(objs -> {
            Collection<String> pendingRegIds = new ArrayList<String>();
            final Map<String, String> deviceMap = new HashMap<String, String> ();
            objs.forEach(obj -> {
              JsonArray _devices = obj.getJsonArray("devices");
              _devices.forEach(_device-> {
                JsonObject device = (JsonObject)_device;
                boolean verificationStatus = device.getBoolean("verificationStatus");
                if(verificationStatus) {
                  deviceMap.put(device.getString("did"), obj.getString(UserDAO.PN_ID));
                }
              });
              pendingRegIds.add(obj.getString(UserDAO.PN_ID));
            });
            log.debug("The pendingRegIds: " + pendingRegIds);
            if(pendingClientIds.size() > 0) {
              sendPushNotification(count, pendingRegIds,isAndroidDevice);
            }
            log.debug("The device map to be added to Hazelcast: " + deviceMap);
            vertx.executeBlocking(fut3 -> {
              //HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
              //IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
              log.debug("The existing hazelcastInstance map 1: " + map);
              map.putAll(deviceMap);
              fut3.complete(Boolean.TRUE);
            }, res3 -> {
              log.debug("The device has been SUCCESSFULLY added to Hazelcast");
              if(log.isDebugEnabled()) {
                vertx.executeBlocking(fut4 -> {
                  //HazelcastInstance hazelcastInstance = Hazelcast.getAllHazelcastInstances().stream().findFirst().get();
                  //IMap<String, String> map = hazelcastInstance.getMap(TringConstants.TRING_DEVICE_IDS);
                  log.debug("The existing hazelcastInstance map 2: " + map);
                  fut4.complete(map.entrySet());
                }, res4-> {
                  log.debug("All entries of map: " + res4.result());
                });
              }
            });
          });
        }
      });
    });
  }

  private void sendPushNotification(int count, Collection<String> deviceIds, boolean isAndroid) {
    try {
      log.debug("sendPushNotification");
      for (String id: deviceIds) {
        if (isAndroid) {
      pushMessageToAndroidClient("You have new unread messages.", "xyz", 1, deviceIds);
        } else {
          pushMessageToAppleClient("You have new unread messages.", deviceIds);
        }
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
 }
  

  private Predicate getPredicate(Set<String> clientIds) {
    EntryObject e = new PredicateBuilder().getEntryObject();
    return e.key().in((String[]) Iterables.toArray(clientIds, String.class));
  }


  public void pushMessageToAndroidClient(String msg, String token, int lastUpdate, Collection<String> devices)
      {
    try {
      Sender sender = new Sender(GCM_API_KEY);
      log.debug("pushMessageToAndroidClient");
      Set<String> uniqueDevices = new HashSet<>(devices);
      log.debug("Unique Devices: " + uniqueDevices);
      Message message = new Message.Builder().collapseKey("unreadMessages").delayWhileIdle(false)
          .addData("message", msg).build();
      log.debug("Message constructed");
      MulticastResult result = sender.send(message, new ArrayList<String>(uniqueDevices), 1);
      log.debug("Message being sent to GCM: " + result.toString());
      if (result.getResults() != null) {
        int canonicalRegId = result.getCanonicalIds();
        log.debug("canonicalRegId from GCM: " + canonicalRegId);
        if (canonicalRegId != 0) {
        }
      } else {
        int error = result.getFailure();
        log.debug("Error from GCM: " + error);
        log.error(error);
      }
    }
    catch (InvalidRequestException e) {
      log.error("Invalid Request", e);
    } catch (IOException e) {
       log.error("IO Exception", e);
    }
    
  }

  @Override
  public void stop(Future<Void> future) {
    log.debug("TringPushNotificationVerticle::end:begin");
    log.debug("TringPushNotificationVerticle::end:end");
    future.complete();
  }

  public void pushMessageToAppleClient(String message, Collection<String> devices) {   
    log.debug("pushMessageToAppleClient");
    PayloadBuilder payloadBuilder = APNS.newPayload()
        .alertBody(message)
        .alertTitle("Tring");
    // check if the message is too long (it won't be sent if it is)
    // and trim it if it is.
    if (payloadBuilder.isTooLong()) {
      payloadBuilder = payloadBuilder.shrinkBody();
    }
    String payload = payloadBuilder.build();
    log.debug("Sending push notification");
    apnsService.push(devices, payload);
    log.debug("Sent push notification");
  }
  
}
