package com.manifolde.tring.server.verticles;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;
import com.manifolde.tring.server.utils.TringConstants;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;
import io.vertx.rxjava.core.file.FileSystem;


public class TringContentVaultVerticle extends AbstractVerticle {
  
  
  private static final Logger log = LoggerFactory.getLogger(TringContentVaultVerticle.class);
  
  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
  }
  
  @Override
  public void start(Future<Void> future) {
    log.debug("TringContentVaultVerticle::start:begin");
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_COPY_CONTENT_TO_VAULT, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_COPY_CONTENT_TO_VAULT);
      handleCopyContentFromCacheToVault(message);
    });
    
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_DELETE_UPLOADED_CONTENT, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_DELETE_UPLOADED_CONTENT);
      handleDeleteContentFromCache(message);
    });
    future.complete();
    log.debug("TringContentVaultVerticle::start:end");
  }

  private void handleDeleteContentFromCache(Message<Object> message) {
    JsonObject json = (JsonObject) message.body();
    String fileName = json.getString("uploadedFileName");
    vertx.fileSystem().deleteObservable(fileName).
    subscribe(res -> {
      message.reply(new JsonObject().put("success", true));
    }, err -> {
      message.reply(new JsonObject().put("success", false));
    });
  }

  private void handleCopyContentFromCacheToVault(Message<Object> message) {
    JsonObject json = (JsonObject) message.body();
    String source = json.getString("sourcePath");
    String target = json.getString("targetPath");
    if(!Strings.isNullOrEmpty(source) && !Strings.isNullOrEmpty(target)) {
      FileSystem fs = vertx.fileSystem();
      String dir = StringUtils.substringBeforeLast(target, File.separator);
      log.debug("The target directory: " + dir);
      if(!fs.existsBlocking(dir)) {
        fs.mkdirsBlocking(dir);
      }
      fs.copyObservable(source, target).subscribe(res -> {
        message.reply(new JsonObject().
            put("success", true).
            put("contentId", json.getString("contentId")).
            put("targetPath", target)
            );
      }, err -> {
        message.reply(new JsonObject().
            put("success", false).
            put("contentId", json.getString("contentId")).
            put("targetPath", target)
            );
      });
    }
  }

  @Override
  public void stop(Future<Void> future) {
    log.debug("TringContentVaultVerticle::end:begin");
    log.debug("TringContentVaultVerticle::end:end");
    future.complete();
  }
}
