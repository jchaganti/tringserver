package com.manifolde.tring.server.verticles;

import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserToSyncAcceptRejectMembershipDAO;
import com.manifolde.tring.server.dao.UserToSyncContactDAO;
import com.manifolde.tring.server.dao.UserToSyncModeratorMessagesDAO;
import com.manifolde.tring.server.dao.UserToSyncProfileDAO;
import com.manifolde.tring.server.utils.TringConstants;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;
import io.vertx.rxjava.ext.mongo.MongoClient;


public class TringBootstrapVerticle extends AbstractVerticle {
  
  private static final Logger log = LoggerFactory.getLogger(TringBootstrapVerticle.class);
  private JsonObject config;
  private UserDAO userDAO;
  private UserToSyncContactDAO userToSyncContactsDAO;
  private UserToSyncProfileDAO userToSyncProfileDAO;
  private UserToSyncModeratorMessagesDAO userToSyncModeratorMessagesDAO;
  private UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO;
  
  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    config = context.config();
  }
  
  @Override
  public void start(Future<Void> future) {
    log.debug("TringBootstrapVerticle::start:begin");
    
    // This is to handle the UNHANDLED contacts (the users table will have handleContacts flag to false
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_UNHANDLED_CONTACTS, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_UNHANDLED_CONTACTS);
      handleUnHandledContacts(message);
    });
    
    // This is to handle the UNSYNCED contacts (the sync contact record will have syncStatus flag to false)
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_UNSYNC_CONTACTS, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_UNSYNC_CONTACTS);
      handleUnSyncContacts(message);
    });
    
    // This is to handle the UNSYNCED profiles (the sync profile record will have syncStatus flag to false)
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_UNSYNC_PROFILES, message -> {
      log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_UNSYNC_PROFILES);
      if(TringConstants.TRING_PROFILE_REWRITE) {
        handleUnHandledProfiles(message);
      }
      else {
        handleUnSyncProfiles(message);
      }
      
    });
    
    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_UNSYNC_MODERATOR_MESSAGES, message -> {
	  log.debug("Message received on address: " + TringConstants.TRING_SERVER_HANDLE_UNSYNC_MODERATOR_MESSAGES);
	  handleUnhandledModeratorMessages(message);  
        
    });
    
    MongoClient mongoClient = MongoClient.createShared(vertx, config.getJsonObject("mongo"));
    TransactionDAO transactionDAO = new TransactionDAO(mongoClient);
    userDAO = new UserDAO(mongoClient, transactionDAO);
    userToSyncContactsDAO = new UserToSyncContactDAO(mongoClient);
    userToSyncProfileDAO = new UserToSyncProfileDAO(mongoClient);
    userToSyncModeratorMessagesDAO = new UserToSyncModeratorMessagesDAO(mongoClient, transactionDAO);
    userToSyncAcceptRejectMembershipDAO = new UserToSyncAcceptRejectMembershipDAO(mongoClient);
    future.complete();
    log.debug("TringBootstrapVerticle::start:end");
  }

  private void handleUnhandledModeratorMessages(Message<Object> message) {
	  userToSyncModeratorMessagesDAO.unSyncRecords().subscribe(unSyncContacts -> {
      unSyncContacts.forEach(unSyncModeratorMessage -> {
        handleUnSyncModeratorMessage(unSyncModeratorMessage);
      });
    });
    message.reply(new JsonObject().
        put("success", true)
        );
	
  }

  private void handleUnSyncModeratorMessage(JsonObject unSyncModeratorMessage) {
	  userDAO.findById(unSyncModeratorMessage.getString(UserToSyncContactDAO.TGT_UID), null).subscribe(tgtUser -> {
	      // Send Message
	      userDAO.findById(unSyncModeratorMessage.getString(UserToSyncContactDAO.SRC_UID), null).subscribe(srcUser -> {
	        vertx.setTimer(100, id -> {
					TringVerticleUtils.sendSyncModeratorMessageToTargetUser(unSyncModeratorMessage.getString("_id"),
							unSyncModeratorMessage.getString(UserDAO.MODERATOR_MESSAGE),
							unSyncModeratorMessage.getString(UserDAO.MODERATOR_MESSAGE_TYPE),
							unSyncModeratorMessage.getString(GroupDAO.GID), srcUser, tgtUser, id, vertx);
		        });
	      });
    });
	
  }

private void handleUnSyncContacts(Message<Object> message) {
    userToSyncContactsDAO.unSyncRecords().subscribe(unSyncContacts -> {
      unSyncContacts.forEach(unSyncContact -> {
        handleUnSyncContact(unSyncContact);
      });
    });
    message.reply(new JsonObject().
        put("success", true)
        );
  }
  
  private void handleUnSyncProfiles(Message<Object> message) {
    userToSyncProfileDAO.unSyncRecords().subscribe(unSyncContacts -> {
      unSyncContacts.forEach(unSyncContact -> {
        handleUnSyncProfile(unSyncContact);
      });
    });
    message.reply(new JsonObject().
        put("success", true)
        );
  }

  private void handleUnSyncContact(JsonObject unSyncContact) {
    userDAO.findById(unSyncContact.getString(UserToSyncContactDAO.TGT_UID), null).subscribe(tgtUser -> {
      // Send Message
      userDAO.findById(unSyncContact.getString(UserToSyncContactDAO.SRC_UID), null).subscribe(srcUser -> {
        vertx.setTimer(100, id -> {
          TringVerticleUtils.sendSyncContactMessageToTargetUser(unSyncContact.getString("_id"), srcUser, tgtUser, id, vertx);
        });
      });
    });
    
  }
  
  private void handleUnSyncProfile(JsonObject unSyncContact) {
    userDAO.findById(unSyncContact.getString(UserToSyncContactDAO.TGT_UID), null).subscribe(tgtUser -> {
      // Send Message
      userDAO.findById(unSyncContact.getString(UserToSyncContactDAO.SRC_UID), null).subscribe(srcUser -> {
        vertx.setTimer(100, id -> {
          TringVerticleUtils.sendSyncProfileMessageToTargetUser(unSyncContact.getString("_id"), srcUser, tgtUser, id, vertx);
        });
      });
    });
  }

  private void handleUnHandledContacts(Message<Object> message) {
    userDAO.usersByUnhandledContacts().subscribe(users -> {
      users.forEach(user -> {
        vertx.setTimer(100, id -> {
          log.debug("Unhandled Contact: " + user);
          TringVerticleUtils.sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_CONTACT, user, id, vertx);
        });
      });
    });
    message.reply(new JsonObject().
        put("success", true)
        );
  }
  
  private void handleUnHandledProfiles(Message<Object> message) {
    userDAO.usersByUnhandledProfiles().subscribe(users -> {
      users.forEach(user -> {
        vertx.setTimer(100, id -> {
          log.debug("Unhandled Contact: " + user);
          TringVerticleUtils.sendMessageToSync(TringConstants.TRING_SERVER_HANDLE_CONTACT_PROFILE, user, id, vertx);
        });
      });
    });
    message.reply(new JsonObject().
        put("success", true)
        );
  }
    
  @Override
  public void stop(Future<Void> future) {
    log.debug("TringBootstrapVerticle::end:begin");
    log.debug("TringBootstrapVerticle::end:end");
    future.complete();
  }
}
