package com.manifolde.tring.server.verticles;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

import com.manifolde.tring.server.dao.BaseModelDAO;
import com.manifolde.tring.server.dao.ClientFeedbackDAO;
import com.manifolde.tring.server.dao.ContactsDAO;
import com.manifolde.tring.server.dao.ContentDAO;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.GroupProfileChangedSyncRecordDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.TransactionDAO;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserNotesDAO;
import com.manifolde.tring.server.dao.UserToSyncAcceptRejectMembershipDAO;
import com.manifolde.tring.server.dao.UserToSyncAdminRoleDAO;
import com.manifolde.tring.server.dao.UserToSyncContactDAO;
import com.manifolde.tring.server.dao.UserToSyncModeratorMessagesDAO;
import com.manifolde.tring.server.dao.UserToSyncProfileDAO;
import com.manifolde.tring.server.dao.UserToSyncSubGroupDAO;
import com.manifolde.tring.server.handlers.PasselGroupHandler;
import com.manifolde.tring.server.handlers.TringContentHandler;
import com.manifolde.tring.server.handlers.TringDownloadContentHandler;
import com.manifolde.tring.server.handlers.TringGroupHandler;
import com.manifolde.tring.server.handlers.TringHandleHandler;
import com.manifolde.tring.server.handlers.TringUserContextHandler;
import com.manifolde.tring.server.handlers.TringUserNotesHandler;
import com.manifolde.tring.server.handlers.UserFeedbackHandler;
import com.manifolde.tring.server.utils.ExecuteOnceThreadSafe;
import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.async.MultipleFutures;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.mongo.MongoClient;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import io.vertx.rxjava.ext.web.handler.ErrorHandler;
import io.vertx.rxjava.ext.web.handler.sockjs.SockJSHandler;

public class TringWebApiServer extends AbstractVerticle {

  public static final String GID = "gid";
  public static final String HANDLE_NAME = "handleName";
  public static final String GROUP_NAME = "groupName";
  public static final String ATTR = "attr";
  public static final String INFO = "info";
  public static final String IS_AVAILABLE = "isavailable";
  public static final String CONTENT_ID = "contentId";
  public static final String REQUESTOR_UID = "requestorUid";
  public static final String MESSAGE_DATA = "data";

  private HttpServer server;

  private UserDAO userDAO;
  private HandleDAO handlesDAO;
  private GroupDAO groupsDAO;
  private ContentDAO contentDAO;
  private ContactsDAO contactsDAO;
  private UserToSyncContactDAO userToSyncContactDAO;
  private UserToSyncProfileDAO userToSyncProfileDAO;
  private UserToSyncSubGroupDAO userToSyncSubGroupDAO;
  private UserNotesDAO userNotesDAO;
  private UserToSyncModeratorMessagesDAO userToSyncModeratorMessagesDAO;
  private UserToSyncAdminRoleDAO userToSyncAdminRoleDAO;
  private ClientFeedbackDAO clientFeedbackDAO;
  public static final String SID = "sid";
  public static final String ACCEPT = "accept";
  public static final String REJECT = "reject";
  public static final String REJECTED = "rejected";
  
  private TringUserContextHandler userContextHandler;
  private JsonObject config;

  private TringGroupHandler groupsHandler;

  private TringHandleHandler handlesHandler;

  private TringContentHandler contentHandler;

  private TringDownloadContentHandler downloadContentHandler;
  
  private TringUserNotesHandler userNotesHandler;

  private PasselGroupHandler passelGroupHandler;

  private UserFeedbackHandler userFeedbackHandler;

  private TransactionDAO transactionDAO;
    
  private GroupProfileChangedSyncRecordDAO groupProfileChangedSyncRecordDAO;
  
  //private HttpClient httpClient;

  private static final Logger log = LoggerFactory.getLogger(TringWebApiServer.class);
  public static final String TOPIC = "topic";
  public static final String GROUP_TOPIC = "groupTopic";
  public static final String GROUP_O2M = "groupO2M";
  public static final String REVIEW = "review";
  public static final String RATING="rating";
  public static final String MEAN_RATING="meanRating";
  public static final String RATING_COUNT = "ratingCount";
  public static final String REVIEWS = "reviews";
  public static final String PAGE_OFFSET = "pageOffset";
  public static final String OFFSET = "offset";
  public static final String RATINGS_AND_REVIEWS = "ratingsAndReviews";
  public static final String MEMBERSHIP_STATUS = "membershipStatus";
  public static final String MEMBERSHIP_TYPE = "membershipType";
  public static final String HID = "hid";
  public static final String THUMBNAIL_DATA = "thumbnailData";
  public static final String USER_TOPIC_PREFIX = "userTopicPrefix";
  public static final String GROUP_CLOSED = "groupClosed";
  public static final String FEEDBACK = "feedback";

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    config = context.config();
  }

  @Override
  public void start(Future<Void> future) {
    log.debug(
        "Current Web Api verticle is running in thread"
            + Thread.currentThread().getName());
    MongoClient mongoClient = MongoClient.createShared(vertx, config.getJsonObject("mongo"));
    transactionDAO = new TransactionDAO(mongoClient);

    userDAO = new UserDAO(mongoClient, transactionDAO);
    contentDAO = new ContentDAO(mongoClient);
    contentHandler = new TringContentHandler(vertx, contentDAO,
        config.getJsonObject(TringConstants.TRING_PROPERTIES_MAP));
    groupsDAO = new GroupDAO(mongoClient, transactionDAO);
    handlesDAO = new HandleDAO(mongoClient, transactionDAO);
    contactsDAO = new ContactsDAO(mongoClient);
    userToSyncContactDAO = new UserToSyncContactDAO(mongoClient);
    userToSyncProfileDAO = new UserToSyncProfileDAO(mongoClient);
    userNotesDAO = new UserNotesDAO(mongoClient);
    clientFeedbackDAO = new ClientFeedbackDAO(mongoClient);
    userToSyncModeratorMessagesDAO = new UserToSyncModeratorMessagesDAO(mongoClient, transactionDAO);
    UserToSyncAcceptRejectMembershipDAO userToSyncAcceptRejectMembershipDAO = new UserToSyncAcceptRejectMembershipDAO(mongoClient);
    userToSyncSubGroupDAO = new UserToSyncSubGroupDAO(mongoClient, transactionDAO);
    userToSyncAdminRoleDAO = new UserToSyncAdminRoleDAO(mongoClient);
    userContextHandler = new TringUserContextHandler(vertx, userDAO , contactsDAO, userToSyncContactDAO, userToSyncProfileDAO, handlesDAO, userToSyncModeratorMessagesDAO,  userToSyncAcceptRejectMembershipDAO,transactionDAO, userToSyncSubGroupDAO, userToSyncAdminRoleDAO, groupProfileChangedSyncRecordDAO, config.getJsonObject(TringConstants.TRING_PROPERTIES_MAP));
    
    groupProfileChangedSyncRecordDAO = new GroupProfileChangedSyncRecordDAO(mongoClient);
    
    groupsHandler = new TringGroupHandler(vertx, groupsDAO, handlesDAO, transactionDAO);

    handlesHandler = new TringHandleHandler(vertx, groupsDAO, userDAO, handlesDAO, contactsDAO,transactionDAO);

    
    downloadContentHandler = new TringDownloadContentHandler(vertx, contentDAO);
    userNotesHandler = new TringUserNotesHandler(vertx, userNotesDAO, handlesDAO);
    userFeedbackHandler = new UserFeedbackHandler(vertx, clientFeedbackDAO);
    passelGroupHandler = new PasselGroupHandler(vertx, groupsDAO);
    MultipleFutures createIndexesFuture = new MultipleFutures();
    createIndexesFuture.add(fut -> {
      new ExecuteOnceThreadSafe(vertx).run(TringConstants.CREATE_INDEXES_LOCK,
          this::createIndexes, fut);
    });
    
    createIndexesFuture.setHandler(result -> {
      log.debug("createIndexesFuture Handler..");
      if (result.failed()) {
        future.fail(result.cause());
      } else {
        MultipleFutures bootstrapDeploymentFuture = new MultipleFutures();
        bootstrapDeploymentFuture.add(this::deployContactsHandlerVerticle);
        bootstrapDeploymentFuture.add(this::deployBootStrapVerticle);
        bootstrapDeploymentFuture.add(this::deployTringPushNotification);
        
        bootstrapDeploymentFuture.setHandler(result1 -> {
          if(result1.failed()) {
            future.fail(result1.cause());
          }
          else {
            log.info("All bootstrap Verticles deployed successfully");
            new ExecuteOnceThreadSafe(vertx).run(TringConstants.UNHANDLED_CONTACTS_LOCK, this::sendHandleUnHandledContactsEvent,
                Future.future());
            new ExecuteOnceThreadSafe(vertx).run(TringConstants.UNSYNCED_CONTACTS_LOCK, this::sendHandleUnSyncedContactsEvent,
                Future.future());
            new ExecuteOnceThreadSafe(vertx).run(TringConstants.UNSYNCED_PROFILES_LOCK, this::sendHandleUnSyncedProfilesEvent,
                Future.future());
            new ExecuteOnceThreadSafe(vertx).run(TringConstants.UNSYNCED_MODERATOR_MESSAGES_LOCK, this::sendHandleUnSyncedModeratorMessagesEvent,
                    Future.future());
            startHttpServer(future);
          }
        });
        bootstrapDeploymentFuture.start();
      }
    });
    createIndexesFuture.start();
  }
  
  private void startHttpServer(Future<Void> future) {
    log.debug("About to create HttpSever");
    server = vertx.createHttpServer(createOptions());
    server.requestHandler(createRouter()::accept);
    server.listen(result -> {
      if (result.succeeded()) {
        log.debug("Started listening to HttpSever");
        future.complete();
        
      } else {
        log.debug("Failed to listen HttpSever");
        future.fail(result.cause());
      }
    });
  }

  private void deployVerticle(String className, Future<Void> future, boolean worker) {
    DeploymentOptions options = new DeploymentOptions();
    options.setConfig(config);
    //String className = TringContactsHandlerVerticle.class.getName();
    options.setWorker(worker);
    vertx.deployVerticle(className, options, result -> {
      if (result.failed()) {
        log.error(className + " deployment failed");
        future.fail(result.cause());
      } else {
        future.complete();
        log.debug(className + " successfully deployed");
      }
    });
  }
  
  private void deployContactsHandlerVerticle(Future<Void> future) {
    deployVerticle(TringContactsHandlerVerticle.class.getName(), future, false);
  }
  
  private void deployBootStrapVerticle(Future<Void> future) {
    deployVerticle(TringBootstrapVerticle.class.getName(), future, false);
  }

  private void deployTringPushNotification(Future<Void> future) {
    deployVerticle(TringPushNotificationVerticle.class.getName(), future, true);
  }
  
  @Override
  public void stop(Future<Void> future) {
    if (server == null) {
      future.complete();
      return;
    }
    server.close(result -> {
      if (result.failed()) {
        future.fail(result.cause());
      } else {
        future.complete();
      }
    });
  }

  private HttpServerOptions createOptions() {
    HttpServerOptions options = new HttpServerOptions();
    options.setHost("0.0.0.0");
    options.setPort(9000);
    return options;
  }

  private Router createRouter() {
    Router router = Router.router(vertx);
    router.route().failureHandler(ErrorHandler.create(true));

    /* API */
    router.mountSubRouter("/tring/api", apiRouter());

    /* SockJS / EventBus */
    router.route("/tring/eventbus/*").handler(eventBusHandler());

    return router;
  }

  private SockJSHandler eventBusHandler() {
    log.debug("In eventBusHandler start");
    SockJSHandler handler = SockJSHandler.create(vertx);
    BridgeOptions options = new BridgeOptions();
    PermittedOptions permitted = new PermittedOptions().setAddress(TringConstants.TRING_SERVER_HANDLE_PUSH_NOTIFICATION);
    options.addOutboundPermitted(permitted);
    options.addInboundPermitted(permitted);
    permitted = new PermittedOptions().setAddress("testEventBusBridge");
    options.addOutboundPermitted(permitted);
    options.addInboundPermitted(permitted);
    handler.bridge(options);
    return handler;
  }



  private Router apiRouter() {
    Router router = Router.router(vertx);
    router.route("/user*").consumes("application/json");
    router.route("/device*").consumes("application/json");
    router.route().produces("application/json");
    JsonObject tringProps = config.getJsonObject(TringConstants.TRING_PROPERTIES_MAP);
    String uploadDirectory = tringProps.getString("tring.server.upload.dir");
//    if(Utils.isDevMode()) {
//      uploadDirectory = "/Users/jchaganti/content/upload";
//    }
    router.route().handler(
        BodyHandler.create().setUploadsDirectory(uploadDirectory));
    router.route().handler(context -> {
      context.response().headers().add(CONTENT_TYPE.toString(), "application/json");
      context.response().setChunked(true);
      context.next();
    });
    router.post("/device").handler(userContextHandler::registerDevice);
    router.post("/device/verify").handler(userContextHandler::verifyDevice);
    router.get("/device/validate").handler(userContextHandler::validateDevice);

    /* API to deal with users info : device id required */
    router.route("/user*").handler(userContextHandler::fromDeviceId);
    router.route("/content*").handler(userContextHandler::fromDeviceId);
    router.get("/user/id/:userId").handler(userContextHandler::fromUserId);
    router.post("/user/profile").handler(userContextHandler::updateUserProfile);
    router.post("/user/reputation").handler(userContextHandler::updateUserReputation);
    router.post("/user/pushNotification").handler(userContextHandler::addPushNotificationId);
    router.post("/user/reputation").handler(passelGroupHandler::changeGroupMembershipCount);
    router.delete("/user/reputation/:syncId").handler(userContextHandler::deleteModeratorMessageSync);
    router.get("/user/reputation/group/:gid/handle/:handleName").handler(handlesHandler::getUserReputation);
    router.get("/user/reputation").handler(userContextHandler::getUserReputation);
    router.post("/user/profiles").handler(userContextHandler::userProfilesFromUsersMobileNos);
    router.delete("/user/profile/:syncId").handler(userContextHandler::deleteProfileSync);
    router.delete("/user/membership/:syncId").handler(userContextHandler::deleteRequestMembershipSync);
    router.delete("/user/group-profile/:syncId").handler(userContextHandler::deleteGroupProfileChangedSyncRecord);
    router.post("/user/contacts").handler(userContextHandler::fromUsersMobileNos);
    router.post("/user/lastseen").handler(userContextHandler::userLastSeen);
    router.post("/user/group").handler(groupsHandler::createGroup);
    router.post("/user/group").handler(passelGroupHandler::indexGroup);
    router.post("/user/group").handler(passelGroupHandler::changeGroupMembershipCount);
    router.post("/user/group-update").handler(groupsHandler::updateGroup);    
    router.post("/user/group-update").handler(passelGroupHandler::indexGroup);
    router.post("/user/group/:" + GID + "/member").handler(handlesHandler::createGroupMember);
    router.post("/user/group/:" + GID + "/member").handler(passelGroupHandler::changeGroupMembershipCount);
    router.get("/group/:"+GID+"/rating-reviews").handler(handlesHandler::getRatingAndReviews);
    router.get("/group/:"+GID+"/reviews/:"+OFFSET).handler(handlesHandler::getReviews);
    router.post("/user/group/:"+GID+"/rating-review").handler(handlesHandler::setRatingAndReview);
    router.get("/user/members/:" + HandleDAO.GID +"/:" + HANDLE_NAME).handler(handlesHandler::getMembersOfGroup);
    router.get("/user/members/:" + HandleDAO.GID +"/:" + HANDLE_NAME + "/:" + HandleDAO.SUBGROUP_GID).handler(handlesHandler::getMembersOfSubGroup);
    router.delete("/user/members/:syncId").handler(userContextHandler::deleteSubgroupMessageSync);
    router.get("/user/nonmembers/:" + HandleDAO.GID +"/:" + HANDLE_NAME).handler(handlesHandler::getNonMembersFromContacts);
    router.post("/user/members").handler(handlesHandler::handleCreateEditSubGroup);
    router.post("/user/admin").handler(handlesHandler::handleAdmin);
    router.delete("/user/admin/:syncId").handler(userContextHandler::deleteHandleAdminMessageSync);
    router.post("/user/group/:" + GROUP_NAME + "/tempmember").handler(handlesHandler::createGroupTempMember);
    router.post("/user/group/:" + GROUP_NAME + "/tempmember").handler(passelGroupHandler::changeGroupMembershipCount);
    router.post("/user/group/:" +GID+"/leave").handler(handlesHandler::leaveGroup);
    router.post("/user/group/:" +GID+"/leave").handler(passelGroupHandler::changeGroupMembershipCount);
    router.get("/user/groups").handler(groupsHandler::getGroups);
    // Below gets both parent and subgroups
    router.get("/user/allgroups").handler(groupsHandler::getAllGroups);
    router.get("/user/notes/:handleName/:" + GID).handler(userNotesHandler::getNotes);
    router.post("/user/notes").handler(userNotesHandler::createNotes);
    router.post("/user/notes/update").handler(userNotesHandler::updateNotes);
    router.delete("/user/notes/:notesId").handler(userNotesHandler::deleteNotes);
    router.post("/content").handler(contentHandler::handleContent);
    router.delete("/content/id/:contentId").handler(contentHandler::deleteContentById);
    router.get("/group/" + ":" + GROUP_NAME + "/" + IS_AVAILABLE)
    .handler(groupsHandler::isGroupNameAvailable);
    router.get("/group/" + ":" + GROUP_NAME + "/" + INFO)
    .handler(groupsHandler::groupFromName);
    router.get("/user/group/" + ":" + GROUP_NAME + "/" + INFO)
    .handler(groupsHandler::groupFromName);
    router.get("/group/:" + GID + "/member/:" + HANDLE_NAME + "/" + IS_AVAILABLE)
    .handler(handlesHandler::isHandlerNameAvailable);
    router.get("/group-name/:" + GROUP_NAME + "/member/:" + HANDLE_NAME + "/" + IS_AVAILABLE)
    .handler(handlesHandler::isHandlerNameForGroupNameAvailable);
    router.post("/user/group/:" + GID + "/member/:" + HANDLE_NAME + "/" + ACCEPT)
    .handler(handlesHandler::acceptHandle);
    router.post("/user/group/:" + GID + "/member/:" + HANDLE_NAME + "/" + ACCEPT).handler(passelGroupHandler::changeGroupMembershipCount);
    router.post("/user/group/:" + GID + "/member/:" + HANDLE_NAME + "/" + REJECT)
    .handler(handlesHandler::acceptHandle);
    router.post("/user/group/:" + GID + "/member/:" + HANDLE_NAME + "/" + REJECT)
    .handler(passelGroupHandler::changeGroupMembershipCount);

    router.get("/group/:" + GID + "/member/:" + HANDLE_NAME)
    .handler(handlesHandler::userInfoByHandleNameandGroupId);
    router.get("/content/:contentID").handler(downloadContentHandler::downloadContent);
    
    /* Discover Group Queries*/
    //router.post("/insert-categories").handler(passelCategoryHandler::addCategories);
    router.get("/get-categories/:offset/:rows").handler(passelGroupHandler::getCategories);
    //router.post("/insert-group").handler(passelGroupHandler::addGroup);
    router.post("/search-groups-by-keyword").handler(passelGroupHandler::searchGroupsByKeyword);
    router.post("/paged-search-groups-by-keyword/:offset/:rows").handler(passelGroupHandler::pagedSearchGroupsByKeyword);
    router.post("/filter-groups-by-category").handler(passelGroupHandler::filterGroupsByCategory);
    router.post("/filter-groups-by-location").handler(passelGroupHandler::filterGroupsByLocation);
    router.post("/browse-groups-by-category").handler(passelGroupHandler::browseGroupsByCategory);
    router.post("/browse-groups-by-location").handler(passelGroupHandler::browseGroupsByLocation);
    router.get("/get-featured-groups").handler(passelGroupHandler::getFeaturedGroups);
    router.get("/browse-groups-random/:rows").handler(passelGroupHandler::getSerendipitousGroups);
    router.get("/browse-groups-next-circular/:offset/:rows").handler(passelGroupHandler::getGroupsNextCircular);
    
    /*Contact Tring*/
    router.get("/user/contact-tring/:feedback").handler(userFeedbackHandler::storeClientFeedback);
    return router;
  }

  private void createIndexes(Future<Void> future) {
    log.debug("Creating indexes");
    MultipleFutures mfut = new MultipleFutures(future);
    try {
      mfut.add(fut -> {
        log.debug("Running createIndexes command for groups");
        groupsDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for groups");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for Group collection");
          }
        } , err -> {
          handleCreateIndexError("group", err);
        });
      });

      mfut.add(fut -> {
        log.debug("Running createIndexes command for users");
        userDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for users");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for User collection");
          }
        } , err -> {
          handleCreateIndexError("user", err);
        });
      });
      mfut.add(fut -> {
        log.debug("Running createIndexes command for handles");
        handlesDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for handles");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for Handle collection");
          }
        } , err -> {
          handleCreateIndexError("content", err);
        });
      });
      mfut.add(fut -> {
        log.debug("Running createIndexes command for contents");
        contentDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for contents");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for Content collection");
          }
        } , err -> {
          handleCreateIndexError("content", err);
        });
      });
      mfut.add(fut -> {
        log.debug("Running createIndexes command for contacts");
        contactsDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for contacts");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for Contacts collection");
          }
        } , err -> {
          handleCreateIndexError("contacts", err);
        });
      });
      mfut.add(fut -> {
        log.debug("Running createIndexes command for user notes");
        userNotesDAO.createIndexes().subscribe(res -> {
          boolean success = BaseModelDAO.checkOK(res);
          if (success) {
            log.debug("Created indexes for user notes");
            fut.complete();
          } else {
            fut.fail("Creation of indexes failed for userNotes collection");
          }
        } , err -> {
          handleCreateIndexError("userNotes", err);
        });
      });
      mfut.setHandler(res -> {
        if(res.succeeded()) {
          log.debug("All indexes created successfully");
          future.complete();
        }
        else {
          log.debug("All indexes creation failed");
        }
      });
      mfut.start();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
  
  private void sendHandleUnHandledContactsEvent(Future<Void> future) {
    JsonObject msg = new JsonObject();
    log.debug("sendHandleUnHandledContactsEvent");
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_UNHANDLED_CONTACTS, msg)
    .subscribe(replyMsg-> {
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      log.debug("The message status of sendHandleUnHandledContactsEvent: " + success);
    }, err -> {
      log.error("Error during sending the message" + err.getMessage());
      err.printStackTrace();
    });
  }

  private void sendHandleUnSyncedContactsEvent(Future<Void> future) {
    JsonObject msg = new JsonObject();
    log.debug("sendHandleUnSyncedContactsEvent");
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_UNSYNC_CONTACTS, msg)
    .subscribe(replyMsg-> {
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      log.debug("The message status of sendHandleUnHandledContactsEvent: " + success);
    }, err -> {
      log.error("Error during sending the message" + err.getMessage());
      err.printStackTrace();
    });
  }
  
  private void sendHandleUnSyncedProfilesEvent(Future<Void> future) {
    JsonObject msg = new JsonObject();
    log.debug("sendHandleUnHandledProfilesEvent");
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_UNSYNC_PROFILES, msg)
    .subscribe(replyMsg-> {
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      log.debug("The message status of sendHandleUnHandledProfilesEvent: " + success);
    }, err -> {
      log.error("Error during sending the message" + err.getMessage());
      err.printStackTrace();
    });
  }
  
  private void sendHandleUnSyncedModeratorMessagesEvent(Future<Void> future) {
    JsonObject msg = new JsonObject();
    log.debug("sendHandleUnSyncedModeratorMessagesEvent");
    vertx.eventBus().sendObservable(TringConstants.TRING_SERVER_HANDLE_UNSYNC_MODERATOR_MESSAGES, msg)
    .subscribe(replyMsg-> {
      JsonObject reply = (JsonObject) replyMsg.body();
      Boolean success = reply.getBoolean("success");
      log.debug("The message status of sendHandleUnSyncedModeratorMessagesEvent: " + success);
    }, err -> {
      log.error("Error during sending the message" + err.getMessage());
      err.printStackTrace();
    });
  }
  
  private void handleCreateIndexError(String indexName, Throwable err) {
    log.error("Could not create index for " + indexName);
    log.error(err.getMessage());
  }
}
