package com.manifolde.tring.server.verticles;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

import com.manifolde.tring.server.utils.TringConstants;
import com.manifolde.tring.server.utils.Utils;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;


public class TringMQTTClientVerticle extends AbstractVerticle {
  private String broker = "tcp://104.155.216.240:1883";
  private String clientId = "membership.server";
  private MqttClient client;
  private JsonObject config;
  private static volatile int counter;
  private String hostAddress;
  private static final Logger log = LoggerFactory.getLogger(TringMQTTClientVerticle.class);

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    config = context.config();
    try {
      hostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
      log.error("Error while obtaining hostAddress");
      e.printStackTrace();
    }
  }
  
  @Override
  public void start(Future<Void> future) {
    log.debug("TringMQTTClientVerticle::start:begin");
    JsonObject serverProperties = config.getJsonObject(TringConstants.TRING_PROPERTIES_MAP);
    String brokerHost = serverProperties.getString("tring.server.mqttbroker.internal.host");
    String port = serverProperties.getString("tring.server.mqttbroker.tcp.port");
    if(brokerHost == null || port == null) {
      future.fail("Broker host or port is null");
    }
    else {
      broker = "tcp://" + brokerHost + ":" + port;
      log.debug("Broker url: " + broker);      
      try {
        counter++;
        clientId = hostAddress + "_" + counter;
        log.debug("The clientId generated is: " + clientId);
        establishMQTTConnection();
        vertx.eventBus().consumer(TringConstants.TRING_SERVER_MQTT_SYNC_MSG, message -> {
          log.debug("Message received on address: " + TringConstants.TRING_SERVER_MQTT_SYNC_MSG);
          handleSendingMessage(message);
        });
        
        log.debug("TringMQTTClientVerticle isConnected: " + client.isConnected());
        future.complete();
        log.debug("TringMQTTClientVerticle::start:end");
      } catch (MqttException me) {
        log.error("TringMQTTClientVerticle::start:error reason = " + me.getReasonCode());
        log.error("TringMQTTClientVerticle::start:error message = " + me.getMessage());
        log.error("TringMQTTClientVerticle::start:error cause = " + me.getCause());
        log.error("TringMQTTClientVerticle::start:error exception = " + me);
      }
    }
  }

  private void establishMQTTConnection()
      throws MqttException, MqttSecurityException {
    String _clientId = clientId;
    if(Utils.isDevMode()) {
      _clientId = _clientId + "_dev";
    }
    log.debug("The  MQTT client id: " + _clientId);
    client = new MqttClient(broker, _clientId);
    MqttConnectOptions connOpts = new MqttConnectOptions();
    connOpts.setKeepAliveInterval(8*3600);
    connOpts.setConnectionTimeout(0);
    connOpts.setCleanSession(false);
    client.connect(connOpts);
  }

  private void handleSendingMessage(Message<Object> _message) {
    JsonObject json = (JsonObject) _message.body();
    String topic = json.getString("topic");
    JsonObject msg = json.getJsonObject("msg");
    log.debug("The message being sent to MQTT broker: " + msg);
    log.debug("The message being sent to topic: " + topic);
    MqttMessage message = new MqttMessage(msg.toString().getBytes());
    message.setQos(1);
    try {
      if(!client.isConnected()) {
        log.debug("MQTT Client is not connected. Hence establishing connection.");
        establishMQTTConnection();
      }
      publishMessage(topic,message,_message);
    } catch (MqttPersistenceException e) {
      log.error("handleSendingMessage MqttPersistenceException occured!!");
      log.error(e);
      disconnectAndConnect();
      try {
        publishMessage(topic,message,_message);
      } catch (MqttPersistenceException e1) {
        log.error(e1);
      } catch (MqttException e1) {
        log.error(e1);
      }   
    } catch (MqttException e) {
      log.error("handleSendingMessage MqttException occured!!");
      log.error(e);
      disconnectAndConnect();
      try {
        publishMessage(topic,message,_message);
      } catch (MqttPersistenceException e1) {
        log.error(e1);
      } catch (MqttException e1) {
        log.error(e1);
      }
    } 
  }

  private void publishMessage(String topic, MqttMessage message, Message<Object> _message) throws MqttPersistenceException, MqttException {
    log.debug("Publishing message.");
    client.publish(topic, message);
    _message.reply(new JsonObject().
        put("success", true)
        );
  }
  private void disconnectAndConnect() {
    log.debug("Trying to disconnect and connect to MQTT broker");
    try {
      client.disconnectForcibly(60000, 60000);
      log.debug("disconnectForcibly success");
      establishMQTTConnection();
      log.debug("Succeded tring to disconnect and connect to MQTT broker");
    } catch (MqttException e) {
      log.error("Error during disconnectAndConnect: " + e.getMessage());
      e.printStackTrace();
    }
  }
  
  @Override
  public void stop(Future<Void> future) {
    try {
      log.debug("TringMQTTClientVerticle::end:begin");
      client.disconnect();
      log.debug("TringMQTTClientVerticle::end:end");
    } catch (MqttException e) {
      
      e.printStackTrace();
    }
   
    future.complete();
  }
}
