package com.manifolde.tring.server.verticles;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import com.manifolde.tring.server.constants.Constants;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.utils.TringConstants;

import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;
import rx.Observable;


public class TringSolrVerticle extends AbstractVerticle {
  private JsonObject config;
  HttpSolrServer server;
  private static final Logger log = LoggerFactory.getLogger(TringSolrVerticle.class);

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    config = context.config();
  }

  @Override
  public void start(Future<Void> future) {
    log.debug("TringSolrVerticle::start:begin");

    vertx.eventBus().consumer(TringConstants.TRING_SERVER_HANDLE_SOLR_GROUP_INDEX, message -> {
      log.debug(
          "Message received on address: " + TringConstants.TRING_SERVER_HANDLE_SOLR_GROUP_INDEX);
      handleUpdatingSolrIndx(message);
    });
    server = new HttpSolrServer(Constants.SERVER_ADDRESS);
    future.complete();
    log.debug("TringSolrVerticle::start:end");
  }


  private void handleUpdatingSolrIndx(Message<Object> message) {
    JsonObject msgBody = (JsonObject) message.body();
    JsonObject group = msgBody.getJsonObject("group");
    try {
      SolrInputDocument doc = new SolrInputDocument();
      //doc.addField("groupId", group.toString());
      doc.addField("groupname", group.getString(GroupDAO.GROUP_NAME));
      Observable.from(group.getJsonArray(GroupDAO.CATEGORIES)).forEach(cat->{
        doc.addField("categories", cat);
      });
      String country=group.getString(GroupDAO.COUNTRY);
      String state= group.getString(GroupDAO.STATE);
      String city=group.getString(GroupDAO.CITY);
      if (country != null && !country.isEmpty()) {
        doc.addField(GroupDAO.COUNTRY, country);
      }
      if (state != null && !state.isEmpty()) {
        doc.addField(GroupDAO.STATE, state);
      }
      if (city != null && !city.isEmpty()) {
        doc.addField(GroupDAO.CITY, city);
      }
      server.add(doc);
      server.commit();
      message.reply(new JsonObject().put("success", true));
    } catch (SolrServerException | IOException ex) {
      ex.printStackTrace();
      log.error(ex.getMessage());
    }
  }

  @Override
  public void stop(Future<Void> future) {
    log.debug("TringSolrVerticle::end:begin");
    server.shutdown();
    log.debug("TringSolrVerticle::end:end");
    future.complete();
  }
}
