package com.manifolde.tring.server.utils;

import java.util.Collection;

import com.hazelcast.core.IMap;
import com.hazelcast.query.Predicate;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

public class TringHazelcastAsyncMap<K, V>  {
  private final Vertx vertx;
  private IMap<String, String> map;

  public TringHazelcastAsyncMap(Vertx vertx, IMap<String, String> map) {
    this.map = map;
    this.vertx = vertx;
  }

  public void values(Predicate predicate, Handler<AsyncResult<Collection<String>>> resultHandler) {
    vertx.executeBlocking(fut -> fut.complete((map.values(predicate))), resultHandler);
  }

  public void delete(String key, Handler<AsyncResult<Void>> resultHandler) {
    vertx.executeBlocking(fut -> {
      map.delete(key);
      fut.complete((Void) null);
    } , resultHandler);
  }
  
  public void set(String key, String value,  Handler<AsyncResult<Void>> resultHandler) {
    vertx.executeBlocking(fut -> {
      map.set(key, value);
      fut.complete((Void) null);
    } , resultHandler);
  }
}
