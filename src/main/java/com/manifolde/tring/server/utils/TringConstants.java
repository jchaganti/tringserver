package com.manifolde.tring.server.utils;

public class TringConstants {

  public static final String SERVER_PROPS = "serverProps";
  public static final String TRING_SERVER_PROPERTIES_FILE_NAME = "tring-server.properties";
  public static final String TRING_PROPERTIES_MAP = "tringProperties";
  public static final String TRING_CLUSTER_MANAGER = "tringClusterManager";
  public static final String PROPS_FILE_LOCK = "propsFileLock";
  public static final String UNHANDLED_CONTACTS_LOCK = "unhandledContactsLock";
  public static final String UNSYNCED_CONTACTS_LOCK = "unsyncedContactsLock";
  public static final String UNSYNCED_PROFILES_LOCK = "unsyncedProfilesLock";
  public static final String UNSYNCED_MODERATOR_MESSAGES_LOCK = "unsyncedModeratorMessagesLock";
  public static final String CREATE_INDEXES_LOCK = "createIndexesLock";
  public static final String DEVICE_TO_USER_MAP = "deviceToUser";
  public static final String MOBILE_TO_LAST_SEEN = "mobileToLastSeen";
  public static final int DEV_MODE_PIN = 1729;
  public static final String TRING_SERVER_COPY_CONTENT_TO_VAULT = "tring.server.copy.content.to.vault";
  public static final String TRING_SERVER_HANDLE_CONTACT = "tring.server.handle.contact";
  public static final String TRING_SERVER_HANDLE_SUBGROUP = "tring.server.handle.subgroup";
  public static final String TRING_SERVER_HANDLE_ADMIN_ROLE = "tring.server.handle.admin.role";
  public static final String TRING_SERVER_HANDLE_CONTACT_PROFILE = "tring.server.handle.contact.profile";
  public static final String TRING_SERVER_HANDLE_UNHANDLED_CONTACTS = "tring.server.unhandled.contacts";
  public static final String TRING_SERVER_HANDLE_SOLR_GROUP_INDEX = "tring.server.handle.solr.group.index";
  public static final String TRING_SERVER_MQTT_SYNC_MSG = "tring.server.mqtt.sync.msg";
  public static final String TRING_SERVER_DELETE_UPLOADED_CONTENT = "tring.server.delete.uploaded.content";
  public static final String TRING_SERVER_HANDLE_UNSYNC_CONTACTS = "tring.server.unsync.contacts";
  public static final String TRING_SERVER_HANDLE_UNSYNC_PROFILES = "tring.server.unsync.profiles";
  public static final String TRING_SERVER_HANDLE_UNSYNC_MODERATOR_MESSAGES = "tring.server.unsync.moderator.messages";
  public static final String TRING_SERVER_HANDLE_UNSYNC_SUBGROUP = "tring.server.unsync.subgroup";
  public static final String TRING_SERVER_HANDLE_PUSH_NOTIFICATION = "tring.server.push.notification";
  public static final String COUNT = "count";
  public static final String IDS = "ids";
  public static final String DEVICE_TYPE = "deviceType";
  public static final String DEVICE_IDS_MAP = "deviceIdsMap";
  public static final boolean TRING_PROFILE_REWRITE = true;
  public static final String TRING_SERVER_HANDLE_GROUP_PROFILE = "tring.server.handle.group.profile";
  public static String TRING_SERVER_HANDLE_MEMBERSHIP_REQUEST_PROCESSED="tring.server.membership.request.processed";
  public static final String TRING_DEVICE_IDS = "tring.device_ids";
  
  //notifications default settings
  public static final boolean NOTI_INDIVIDUAL = true;
  public static final boolean NOTI_GROUP = false;
  public static final boolean NOTI_ADMIN = true;
  public static final String TRING_NOTIFIABLE_IDS = "tring.notifiable.device_ids";
}
