package com.manifolde.tring.server.utils.async;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.manifolde.tring.server.dao.BaseModelDAO;
import com.manifolde.tring.server.dao.TransactionDAO;

import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Async Serial task Transaction emulation manager for cases where some or all steps of the
 * transaction involve Mongo db changes
 * 
 * @author ssashita
 *
 */
public class MongoAsyncSerialTasksTransaction extends AsyncSerialTasksTransaction {
  private static final Logger log = LoggerFactory.getLogger(MongoAsyncSerialTasksTransaction.class);

  public static final String TRANSACTION_ID = "transactionId";
  // We use a transactions collection for this Transaction manager
  private TransactionDAO transactionDAO;

  /**
   * The maximum time given for a transaction to complete after which it will be attempted to be
   * rolled back from the database
   */
  public static final long MAX_TRANSACTION_TIME = 60000L;

  /**
   * Constructor
   * 
   * @param transactionDAO
   */
  public MongoAsyncSerialTasksTransaction(TransactionDAO transactionDAO) {
    super();
    this.transactionDAO = transactionDAO;
  }

  /**
   * Constructor
   * 
   * @param tasks
   * @param args
   * @param undoTasks
   * @param undoArgs
   * @param transactionDAO
   */
  public MongoAsyncSerialTasksTransaction(List<AsyncSerialTask> tasks, List<List<Object>> args,
      List<AsyncSerialTask> undoTasks, List<List<Object>> undoArgs, TransactionDAO transactionDAO) {
    super(tasks, args, undoTasks, undoArgs);
    this.transactionDAO = transactionDAO;
  }

  /**
   * Constructor
   * 
   * @param tasks
   * @param args
   * @param transactionDAO
   */
  public MongoAsyncSerialTasksTransaction(List<AsyncSerialTask> tasks, List<List<Object>> args,
      TransactionDAO transactionDAO) {
    super(tasks, args);
    this.transactionDAO = transactionDAO;
  }

  /**
   * Execute the transaction
   */
  @Override
  public void execute() {
    transactionDAO.createTransaction().subscribe(id -> {
      if (null != id) {
        this.setValue(TRANSACTION_ID, id);
        super.execute();
      } else {
        log.error("Error creating MongoAsyncSerialTasksTransaction - transaction id null");
      }
    } , err -> {
      log.error("Error creating MongoAsyncSerialTasksTransaction transaction");
    });
  }

  /**
   * This is called when the transaction either fails mid-way or completes successfully. On failure
   * the rollback tasks corresponding to the successful steps are executed, followed by
   * rollbackTransaction for the database. On success, commitTransaction is called
   */
  @Override
  protected void transactionComplete(AsyncResult<Void> result) {
    completed = true;
    if (log.isDebugEnabled()) {
      log.debug("transactionComplete: Entered");
    }
    if (null != result && result.failed()) {
      List<AsyncSerialTask> serialTasks;
      List<List<Object>> serialTaskArgs;
      Object[] returnValues = getValidUndoTasksAndArgs();
      serialTasks = (List<AsyncSerialTask>) returnValues[0];
      serialTaskArgs = (List<List<Object>>) returnValues[1];
      if (serialTasks.size() > 0) {
        AsyncSerialTasksTransaction rollBackTransaction =
            new AsyncSerialTasksTransaction(serialTasks, serialTaskArgs);
        rollBackTransaction.onFailure(onFailureHandler);
        rollBackTransaction.onSuccess(() -> {
          onFailureHandler.call(true);
        });
        rollBackTransaction.execute();
      } else {
        if (null != onFailureHandler) {
          onFailureHandler.call(false);
        }
      }
      String id = (String) getValue(TRANSACTION_ID);
      rollbackTransaction(transactionDAO, id, onSuccessHandler, onFailureHandler);
    } else {
      commitTransaction();
    }
  }

  /**
   * Do all the database actions required to commit the transaction The transactions collection will
   * already have a document for the transaction This document has a 'created' time field. If the
   * created time is less than MAX_TRANSACTION_TIME before the current time then change the state of
   * the transaction from 'new' to 'commit' and call retireTransaction() on it
   * 
   */
  private void commitTransaction() {
    if (log.isDebugEnabled()) {
      log.debug("commitTransaction: Entered");
    }
    DateTime now = new DateTime(DateTimeZone.UTC);
    DateTime cutOff = now.minus(MAX_TRANSACTION_TIME);
    // DateTimeFormatter parser = ISODateTimeFormat.dateTime();
    // parser.parseDateTime(cutOff.toString());
    String id = (String) getValue(TRANSACTION_ID);
    if (log.isDebugEnabled()) {
      log.debug("commitTransaction: cutoff time is " + cutOff.toString());
    }
    JsonObject query = new JsonObject().put(TransactionDAO.ID, id).put(TransactionDAO.STATE, "new")
        .put(BaseModelDAO.CREATED,
            new JsonObject().put("$gt", new JsonObject().put("$date", cutOff.toString())));
    JsonObject update =
        new JsonObject().put("$set", new JsonObject().put(TransactionDAO.STATE, "commit"));
    if (log.isDebugEnabled()) {
      log.debug("commitTransaction: query is " + query);
      log.debug("commitTransaction: update is " + update);
    }
    transactionDAO.findOneAndUpdate(query, update, true, false).subscribe(t -> {
      if (log.isDebugEnabled()) {
        log.debug("transaction object is " + t);
      }
      retireTransaction(transactionDAO, id, onSuccessHandler, onFailureHandler);
    } , err -> {
      onFailureHandler.call(false);
    });
  }

  /**
   * Retire the transaction. This involves getting the collections field from the transaction and
   * pulling out the transaction id from the transactions array field for every document of the
   * collection that has the transaction id in its transactions array field. This is a static method
   * because it may be called from a separate verticle during database cleanup.
   * 
   * @param transactionDAO
   * @param transactionId
   * @param onSuccessHandler
   * @param onFailureHandler
   */
  private static void retireTransaction(TransactionDAO transactionDAO, String transactionId,
      Action0 onSuccessHandler, Action1<Boolean> onFailureHandler) {
    JsonObject query =
        new JsonObject().put(TransactionDAO.ID, transactionId).put(TransactionDAO.STATE, "commit");
    JsonObject projection = new JsonObject().put(TransactionDAO.COLLECTIONS, 1);
    if (log.isDebugEnabled()) {
      log.debug("retireTransaction: Entered");
      log.debug("query is " + query);
      log.debug("projection is " + projection);
    }
    transactionDAO.findOne(query, projection).subscribe(obj -> {
      JsonArray collect = obj.getJsonArray(TransactionDAO.COLLECTIONS);
      Observable.from(collect).subscribe(collection -> {
        JsonObject q = new JsonObject().put("transactions", new JsonObject().put("$in",
            new JsonArray(Arrays.asList(new String[] {transactionId}))));
        JsonObject update =
            new JsonObject().put("$pull", new JsonObject().put("transactions", transactionId));

        transactionDAO.findOneAndUpdateForCollection((String) collection, q, update, true, false)
            .subscribe((c) -> {
        } , err -> {
          log.error("Error pulling out transaction id from " + collect);
          onFailureHandler.call(false);
        });
      } , err -> {
        log.error("Error extracting collections from collections obj, for transaction id "
            + transactionId);
        onFailureHandler.call(false);
      } , () -> {
        // onComplete
        transactionDAO.delete(transactionId).subscribe((t) -> {
          if (null != onSuccessHandler)
            onSuccessHandler.call();
        } , err -> {
          log.error("Error deleting document for transaction id " + transactionId);
          onFailureHandler.call(false);
        });
      });

    } , err -> {
      log.error("Error getting collections for transaction id " + transactionId);
      onFailureHandler.call(false);
    });
  }

  /**
   * Rollback the transaction. Only those steps can be rolled back by this method which are related
   * to mongo db operations and which were performed using either of insertTransactional,
   * updateTransactional or removeTransactional methods of the DAO's. Whenever these methods are
   * called they store enough information in the transaction document, that is required for their
   * rollback, if the need arises. Please note that if the transaction comprises a mix of
   * non-database and database operations, then rollback actions for the non-database steps need to
   * be specified either as rollback tasks using this Transaction Manager constructor or via the
   * rollbackTask() chain method defined on the super class. No rollback action should be specified
   * this way for the database operations but instead the insertTransactional, updateTransactional
   * or removeTransactional methods should be used to ensure their rollback from database. Rollback
   * action will be kicked in whenever any of the steps of the Transaction (database or non-database
   * related) fails. This is a static method because it may be called from a separate verticle
   * during database cleanup.
   * 
   * @param transactionDAO
   * @param transactionId
   * @param onSuccessHandler
   * @param onFailureHandler
   */
  public static void rollbackTransaction(TransactionDAO transactionDAO, String transactionId,
      Action0 onSuccessHandler, Action1<Boolean> onFailureHandler) {
    JsonObject projection =
        new JsonObject().put(TransactionDAO.COLLECTIONS, 1).put(TransactionDAO.AFFECTED_IDS, 1)
            .put(TransactionDAO.OPERATIONS, 1).put(TransactionDAO.OLD_VALUES, 1);
    if (log.isDebugEnabled()) {
      log.debug("In rollbackTransaction: projection is " + projection);
    }
    transactionDAO.findOne(new JsonObject().put(TransactionDAO.ID, transactionId), projection)
        .subscribe(obj -> {
          if (null == obj) {
            log.error("Null Transaction found with id " + transactionId);
          } else {
            AsyncSerialTasksTransaction serialTasks = new AsyncSerialTasksTransaction();
            JsonArray collect = obj.getJsonArray(TransactionDAO.COLLECTIONS);
            JsonArray ops = obj.getJsonArray(TransactionDAO.OPERATIONS);
            JsonArray oldValues = obj.getJsonArray(TransactionDAO.OLD_VALUES);
            JsonArray affectedIds = obj.getJsonArray(TransactionDAO.AFFECTED_IDS);
            Observable.zip(Observable.from(collect), Observable.from(ops),
                Observable.from(oldValues), Observable.from(affectedIds), (col, op, ov, id) -> {
              return new Object[] {col, op, ov, id};
            }).subscribe(rollbackRecipe -> {
              Object[] recipe = (Object[]) rollbackRecipe;
              String collection = (String) recipe[0];
              String operation = (String) recipe[1];
              JsonObject oldVal = (JsonObject) recipe[2];
              String id = (String) recipe[3];
              if (log.isDebugEnabled()) {
                log.debug("rollbackTransaction: (col,op,ov,id) is " + collection + " " + operation
                    + " " + oldVal + " " + id);
              }
              serialTasks.task((t, f, o) -> {
                transactionDAO
                    .rollbackCommand(collection, operation, oldVal,
                        new JsonObject().put("_id", id).put("transactions",
                            new JsonObject().put("$in",
                                new JsonArray(Arrays.asList(new String[] {transactionId})))))
                    .subscribe(r -> {
                  if (log.isDebugEnabled()) {
                    log.debug("database rolled back successfully for command " + operation
                        + " for document " + id + " of collection " + collection);
                  }
                  f.complete();
                } , err -> {
                  f.complete();
                  log.error("database rollback command " + operation
                      + " failed to execute properly for document " + id + " of collection "
                      + collection);
                });
              });
            } , err -> {
              log.error("rollbackTransaction: Error getting rollbackRecipe");
            } , () -> {
              serialTasks.onSuccess(() -> {
                if (log.isDebugEnabled()) {
                  log.debug("database rolled back successfully for transaction " + transactionId);
                }
                onSuccessHandler.call();
              }).onFailure(s -> {
                if (log.isDebugEnabled()) {
                  log.debug("database rollback failed for transaction " + transactionId);
                }
                onFailureHandler.call(s);
              }).execute();
            });
          }
        } , err -> {
          log.error("rollbackTransaction: Error getting transaction with id " + transactionId);
          onFailureHandler.call(false);
        });
  }

  /**
   * This static method will typically be called at server restart time to ensure that transactions
   * in committed or new state are appropriately cleaned up by calling retireTransaction or
   * rollbackTransaction respectively.
   * 
   * @param transactionDAO
   * @param transactionId
   * @param onSuccessHandler
   * @param onFailureHandler
   */
  public static void cleanup(TransactionDAO transactionDAO, Action0 onSuccessHandler,
      Action1<Boolean> onFailureHandler) {
    transactionDAO.find(new JsonObject().put("state", "commit"), new JsonObject().put("_id", 1))
        .subscribe(tobjs -> {
          Observable<JsonObject> tobj = Observable.from(tobjs);
          tobj.subscribe(t -> {
            String transactionId = t.getString("_id");
            retireTransaction(transactionDAO, transactionId, onSuccessHandler, onFailureHandler);
          });
        });
    DateTime now = new DateTime(DateTimeZone.UTC);
    DateTime cutOff = now.minus(MAX_TRANSACTION_TIME);
    if (log.isDebugEnabled()) {
      log.debug("cleanup: cutoff time is " + cutOff.toString());
    }
    JsonObject query = new JsonObject().put(TransactionDAO.STATE, "new").put(BaseModelDAO.CREATED,
        new JsonObject().put("$lte", new JsonObject().put("$date", cutOff.toString())));
    transactionDAO.find(query, new JsonObject().put("_id", 1)).subscribe(tobjs -> {
      Observable<JsonObject> tobj = Observable.from(tobjs);
      tobj.subscribe(t -> {
        JsonObject update =
            new JsonObject().put("$set", new JsonObject().put(TransactionDAO.STATE, "rollback"));
        String tid = t.getString("_id");
        if (log.isDebugEnabled()) {
          log.debug("cleanup: update is " + update);
        }
        transactionDAO.findOneAndUpdate(new JsonObject().put("_id", tid), update, true, false)
            .subscribe(tRollback -> {
          rollbackTransaction(transactionDAO, tid, onSuccessHandler, onFailureHandler);
        });

      });
    });
  }
}
