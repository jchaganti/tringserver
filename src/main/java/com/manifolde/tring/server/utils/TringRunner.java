package com.manifolde.tring.server.utils;

import java.util.function.Consumer;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;

public class TringRunner {

  private static final String TRING_JAVA_DIR = "src/main/java/";
  private static final Logger log = LoggerFactory.getLogger(TringRunner.class);

  public static void run(Class clazz, boolean clustered, ClusterManager mgr) {
    VertxOptions options = new VertxOptions();
    if (clustered) {
      options.setClusterManager(mgr);
    }
    run(clazz, new VertxOptions().setClustered(clustered));
  }

  public static void run(Class clazz, VertxOptions options) {
    String exampleDir = TRING_JAVA_DIR + clazz.getPackage().getName().replace(".", "/");
    if (Utils.isDevMode()) {
      run(exampleDir, clazz.getName(), options, null);
    } else {
      run(exampleDir, clazz.getName(), options, new DeploymentOptions().setInstances(4));
    }
    log.debug("Inside Tring " + Thread.currentThread().getName());
  }

  public static void run(String runDir, String verticleID, VertxOptions options,
      DeploymentOptions deploymentOptions) {
    System.setProperty("vertx.cwd", runDir);
    Consumer<Vertx> runner = vertx -> {
      try {
        if (deploymentOptions != null) {
          vertx.deployVerticle(verticleID, deploymentOptions);
        } else {
          vertx.deployVerticle(verticleID);
        }
      } catch (Throwable t) {
        t.printStackTrace();
      }
    };
    if (options.isClustered()) {
      Vertx.clusteredVertx(options, res -> {
        if (res.succeeded()) {
          Vertx vertx = res.result();
          runner.accept(vertx);
        } else {
          res.cause().printStackTrace();
        }
      });
    } else {
      Vertx vertx = Vertx.vertx(options);
      runner.accept(vertx);
    }
  }
}
