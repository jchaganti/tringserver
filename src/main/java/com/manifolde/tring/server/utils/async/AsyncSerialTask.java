package com.manifolde.tring.server.utils.async;

import io.vertx.rxjava.core.Future;

@FunctionalInterface
public interface AsyncSerialTask {
  void perform(AsyncSerialTasksTransaction transaction, Future<Void> future, Object... objects);
}
