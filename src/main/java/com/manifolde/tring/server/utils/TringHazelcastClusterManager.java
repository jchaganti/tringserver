package com.manifolde.tring.server.utils;

import com.hazelcast.core.IMap;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class TringHazelcastClusterManager extends HazelcastClusterManager {
	private Vertx vertx;
	private static final Logger log = LoggerFactory.getLogger(TringHazelcastClusterManager.class);
	public TringHazelcastClusterManager() {
	    super();
	    log.debug("Creating TringHazelcastClusterManager");
	}
	
	public void getTringAsyncMap(String name, Handler<AsyncResult<TringHazelcastAsyncMap<String, String>>> resultHandler) {
	    vertx.executeBlocking(fut -> {
	      IMap<String, String> map = getHazelcastInstance().getMap(name);
	      fut.complete(new TringHazelcastAsyncMap<>(vertx, map));
	    }, resultHandler);
	}
}
