package com.manifolde.tring.server.utils;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.commons.codec.binary.Base64;

import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.dao.HandleDAO.HandleType;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class Utils {
  private static final Logger log = LoggerFactory.getLogger(Utils.class);
  private static int mask = 255;

  public static boolean isDevMode() {
    String mode = System.getProperty("tring.server.mode");
    return "dev".equals(mode);
  }

  public static String computePathForFileName(String fileName, int size) {
    int hashcode = fileName.hashCode() + size;
    StringBuilder sb = new StringBuilder();
    Stream.of(hashcode & mask, 
        (hashcode >> 8) & mask, 
        (hashcode >> 16) & mask,
        (hashcode >> 24) & mask).
    forEach(dir -> {
          sb.append(File.separator);
          sb.append(String.format("%02x", dir));
        });
    sb.append(File.separator);
    return sb.toString();
  }
  
  public static String uuidToBase64(UUID uuid) {
    ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
    bb.putLong(uuid.getMostSignificantBits());
    bb.putLong(uuid.getLeastSignificantBits());
    return Base64.encodeBase64URLSafeString(bb.array());
  }
  
  public static boolean isModerator(int role) {
	  return role != HandleDAO.HandleType.NONMEMBER.ordinal();
  }
  
  public static int parseString(String _page, int page) {
	if(_page != null) {
		  try {
			  page = Integer.valueOf(_page);
		  }
		  catch(NumberFormatException e) {
			  log.error("Invalid Page: " + _page);
		  }
	  }
	return page;
  }

  public static boolean isOwner(int role) {
    return HandleType.OWNER.ordinal() == role;
  }

  public static boolean isAdmin(int role) {
    return HandleType.ADMIN.ordinal() == role;
  }
}
