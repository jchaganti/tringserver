package com.manifolde.tring.server.utils;

import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.shareddata.AsyncMap;
import io.vertx.rxjava.core.shareddata.LocalMap;
import rx.Observable;

/**
 * @author jchaganti
 *
 */
public class ClusteredObjectStore {

  private final Vertx vertx;
  private final String mapName;

  private volatile Observable<AsyncMap<String, Object>> clusterWideMapObservable;
  

  public ClusteredObjectStore(Vertx vertx, String mapName) {
    this.vertx = vertx;
    this.mapName = mapName;
  }

  public Observable<Object> get(String id) {
    if(Utils.isDevMode()) {
      LocalMap<Object, Object> localMap = vertx.sharedData().getLocalMap(mapName);
      return Observable.just(localMap.get(id));
    }
    else {
      clusterWideMapObservable = vertx.sharedData().<String, Object>getClusterWideMapObservable(mapName);
      return clusterWideMapObservable.flatMap(asyncMap -> asyncMap.getObservable(id));
    }
  }


  public Observable<Object>  delete(String id) {
    if(Utils.isDevMode()) {
      LocalMap<Object, Object> localMap = vertx.sharedData().getLocalMap(mapName);
      return Observable.just(localMap.remove(id));
    }
    else {
      clusterWideMapObservable = vertx.sharedData().<String, Object>getClusterWideMapObservable(mapName);
      return clusterWideMapObservable.flatMap(asyncMap -> asyncMap.removeObservable(id));
    }
    
  }

  public Observable<Void> put(String id, Object object) {
    if(Utils.isDevMode()) {
      LocalMap<Object, Object> localMap = vertx.sharedData().getLocalMap(mapName);
      localMap.put(id, object);
      return Observable.just((Void)null);
    }
    else {
      clusterWideMapObservable = vertx.sharedData().<String, Object>getClusterWideMapObservable(mapName);
      return clusterWideMapObservable.flatMap(asyncMap -> asyncMap.putObservable(id, object));
    }
    
  }

  public Observable<Void> clear() {
    if(Utils.isDevMode()) {
      LocalMap<Object, Object> localMap = vertx.sharedData().getLocalMap(mapName);
      localMap.clear();
      return Observable.just((Void)null);
    }
    else {
      clusterWideMapObservable = vertx.sharedData().<String, Object>getClusterWideMapObservable(mapName);
      return clusterWideMapObservable.flatMap(asyncMap -> asyncMap.clearObservable());
    }
    
  }

  public Observable<Integer> size() {
    if(Utils.isDevMode()) {
      LocalMap<Object, Object> localMap = vertx.sharedData().getLocalMap(mapName);
      return Observable.just(localMap.size());
    }
    else {
      clusterWideMapObservable = vertx.sharedData().<String, Object>getClusterWideMapObservable(mapName);
      return clusterWideMapObservable.flatMap(asyncMap -> asyncMap.sizeObservable());
    }
    
  }

  public void close() {}
  
  
}
