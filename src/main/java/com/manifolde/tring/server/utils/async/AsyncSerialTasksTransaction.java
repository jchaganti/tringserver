package com.manifolde.tring.server.utils.async;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import io.vertx.core.AsyncResult;
import io.vertx.rxjava.core.Future;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Async Serial task Transaction emulation manager
 * 
 * @author ssashita
 *
 */
public class AsyncSerialTasksTransaction {
  private List<AsyncSerialTask> tasks = new ArrayList<>();
  private List<List<Object>> args = Arrays.asList();
  private List<List<Object>> undoArgs = Arrays.asList();
  private List<AsyncSerialTask> undoTasks = new ArrayList<>();

  /**
   * Key-value dictionary to store stuff in a step for use by later steps
   */
  private Map<String, Object> keyValue = new HashMap<>();

  public void setValue(String k, Object value) {
    keyValue.put(k, value);
  }

  public Object getValue(String k) {
    return keyValue.get(k);
  }

  /**
   * Constructor
   * 
   * @param tasks
   * @param args
   * @param undoTasks
   * @param undoArgs
   */
  public AsyncSerialTasksTransaction(List<AsyncSerialTask> tasks, List<List<Object>> args,
      List<AsyncSerialTask> undoTasks, List<List<Object>> undoArgs) {
    super();
    this.tasks = tasks;
    this.args = args;
    this.undoArgs = undoArgs;
    this.undoTasks = undoTasks;
  }

  /**
   * Constructor
   * 
   * @param tasks
   * @param args
   */
  public AsyncSerialTasksTransaction(List<AsyncSerialTask> tasks, List<List<Object>> args) {
    super();
    this.tasks = tasks;
    this.args = args;
  }

  /**
   * Constructor
   */
  public AsyncSerialTasksTransaction() {}

  /**
   * Number of steps of the transaction that completed successfully
   */
  private int numSuccesses = -1;
  protected boolean completed;
  /**
   * This field stores the action to execute after the transaction failed
   */
  protected Action1<Boolean> onFailureHandler = r -> {
  };
  /**
   * This field stores the action to execute after the transaction succeeded
   */
  protected Action0 onSuccessHandler = () -> {
  };

  /**
   * This is executed after every step completes successfully or unsuccessfully It performs the next
   * step of the transaction. When all steps are complete or when any step fails, it calls
   * transactionComplete passing the latest step result as argument
   * 
   * @param result
   */
  private void onTaskCompleteHandler(AsyncResult<Void> result) {
    // perform the next task
    if (null == result || result.succeeded()) {
      ++numSuccesses;
      if (numSuccesses < tasks.size()) {
        int argslen = args.size();
        List<Object> arguments = argslen > 0 ? args.get(numSuccesses) : Arrays.asList();
        Future<Void> future = Future.future();
        future.setHandler(res -> {
          onTaskCompleteHandler(res);
        });
        Object[] args1 = new Object[arguments.size()];
        for (int i = 0; i < arguments.size(); ++i) {
          args1[i] = arguments.get(i);
        }
        tasks.get(numSuccesses).perform(this, future, args1);
      } else {
        transactionComplete(result);
      }
    } else {
      transactionComplete(result);
    }
  }

  /**
   * This method massages the rollback task array to get a list of valid rollback tasks and
   * arguments
   * 
   * @return
   */
  protected Object[] getValidUndoTasksAndArgs() {
    List<AsyncSerialTask> serialTasks = new ArrayList<AsyncSerialTask>();
    List<List<Object>> serialTaskArgs = new ArrayList<List<Object>>();
    int lenTasks = undoTasks.size() >= numSuccesses ? numSuccesses : undoTasks.size();
    int lenArgs = undoArgs.size() >= numSuccesses ? numSuccesses : undoArgs.size();

    // Java 8 streams lacks the zip function, hence RxJava Observable
    Observable.zip(Observable.from(Lists.reverse(undoTasks.subList(0, lenTasks))),
        Observable.from(Lists.reverse(undoArgs.subList(0, lenArgs))), (t, a) -> {
          return new Object[] {t, a};
        }).filter((at) -> at[0] != null).subscribe(at -> {
          serialTasks.add((AsyncSerialTask) at[0]);
          serialTaskArgs
              .add((at[1] != null ? (List<Object>) at[1] : Arrays.asList(new Object[] {})));
        });
    return new Object[] {serialTasks, serialTaskArgs};
  }

  /**
   * This method is called when all steps of the transaction complete successfully or any step fails
   * The last step result is passed as argument. If the last step failed then the rollback tasks are
   * executed by constructing a rollback transaction. Finally the onFailureHandler is called. If all
   * steps were a success, then the onSuccessHandler is called.
   * 
   * @param result
   */
  protected void transactionComplete(AsyncResult<Void> result) {
    completed = true;
    if (null != result && result.failed()) {
      List<AsyncSerialTask> serialTasks;
      List<List<Object>> serialTaskArgs;
      Object[] returnValues = getValidUndoTasksAndArgs();
      serialTasks = (List<AsyncSerialTask>) returnValues[0];
      serialTaskArgs = (List<List<Object>>) returnValues[1];
      if (serialTasks.size() > 0) {
        AsyncSerialTasksTransaction rollBackTransaction =
            new AsyncSerialTasksTransaction(serialTasks, serialTaskArgs);
        rollBackTransaction.onFailure(onFailureHandler);
        rollBackTransaction.onSuccess(() -> {
          onFailureHandler.call(true);
        });
        rollBackTransaction.execute();
      } else {
        if (null != onFailureHandler) {
          onFailureHandler.call(false);
        }
      }

    } else {
      if (null != onSuccessHandler) {
        onSuccessHandler.call();
      }
    }
  }

  /**
   * Execute the steps of the transaction
   */
  public void execute() {
    onTaskCompleteHandler(null);
  }

  /**
   * Returns true if the transaction completed successfully
   * 
   * @return
   */
  public boolean succeeded() {
    if (!completed)
      throw new RuntimeException("Transaction not yet complete");
    return numSuccesses == tasks.size();
  }

  public boolean completed() {
    return completed;
  }

  /**
   * Method to let the onSuccessHandler be set.
   * 
   * @param h
   * @return
   */
  public AsyncSerialTasksTransaction onSuccess(Action0 h) {
    onSuccessHandler = h;
    return this;
  }

  /**
   * Method to let the onFailureHandler be set.
   * 
   * @param h
   * @return
   */
  public AsyncSerialTasksTransaction onFailure(Action1<Boolean> h) {
    onFailureHandler = h;
    return this;
  }

  /**
   * Method to add a task step to the Transaction
   * 
   * @param t
   * @param args
   * @return
   */
  public AsyncSerialTasksTransaction task(AsyncSerialTask t, List<Object> args) {
    if (null != t) {
      this.tasks.add(t);
      if (null != args)
        this.args.add(args);
    }
    return this;
  }

  /**
   * Method to add a rollback step to the Transaction
   * 
   * @param t
   * @param args
   * @return
   */
  public AsyncSerialTasksTransaction rollbackTask(AsyncSerialTask t, List<Object> args) {
    if (null != t) {
      this.undoTasks.add(t);
      if (null != args)
        this.undoArgs.add(args);
    }
    return this;
  }

  /**
   * Method to add a task step to the Transaction
   * 
   * @param t
   * @return
   */
  public AsyncSerialTasksTransaction task(AsyncSerialTask t) {
    return task(t, null);
  }

  /**
   * Method to add a rollback step to the Transaction
   * 
   * @param t
   * @return
   */
  public AsyncSerialTasksTransaction rollbackTask(AsyncSerialTask t) {
    return rollbackTask(t, null);
  }
}
