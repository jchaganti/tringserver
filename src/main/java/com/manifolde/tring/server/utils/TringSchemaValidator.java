package com.manifolde.tring.server.utils;

import static com.google.common.collect.Sets.difference;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

@SuppressWarnings("serial")
public class TringSchemaValidator {

  private static Map<String, Set<String>> validJsonKeys = new HashMap<>();
  private static final Logger log = LoggerFactory.getLogger(TringSchemaValidator.class);

  static {
    validJsonKeys.put("device-id", new HashSet<String>() {
      {
        add("mobileNo");
      }
    });
    validJsonKeys.put("device-verify", new HashSet<String>() {
      {
        add("did");
        add("pin");
      }
    });
  }

  public static boolean isValidJson(String key, JsonObject json) {
    log.debug("Key = " + key + " and input json = " + json.toString());
    Set<String> diff = difference(validJsonKeys.get(key), json.fieldNames());
    return diff.size() == 0;
  }

}
