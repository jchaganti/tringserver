package com.manifolde.tring.server.utils.async;

import io.vertx.core.AsyncResult;

public class AsyncSuccess<T> implements AsyncResult<T> {
  private T value;
  
  public AsyncSuccess(T obj) {
    // TODO Auto-generated constructor stub
    value =obj;
  }
  
  @Override
  public T result() {
    // TODO Auto-generated method stub
    return value;
  }

  @Override
  public Throwable cause() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean succeeded() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean failed() {
    // TODO Auto-generated method stub
    return false;
  }

}


