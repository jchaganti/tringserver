package com.manifolde.tring.server.utils;


import com.manifolde.tring.server.handlers.TringGroupHandler;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.shareddata.Counter;
import rx.Observable;

/**
 * This Class provides a run() method that will run action code synchronized on a cluster-wide lock
 * identified by a lockName, and ensure that this code is run exactly once irrespective of how many
 * verticles (or threads) are running this action code
 * 
 * @author ssashita
 *
 */
public class ExecuteOnceThreadSafe {
  private static final Logger log = LoggerFactory.getLogger(TringGroupHandler.class);

  private Vertx vertx;

  public ExecuteOnceThreadSafe(Vertx vertx) {
    this.vertx = vertx;
  }

  public void run(String countName, Handler<Future<Void> > action, Future<Void> future) {
    Observable<Counter> sharedCount = vertx.sharedData().getCounterObservable(countName);
    sharedCount.subscribe(c -> {
      c.compareAndSetObservable(0L,1L).subscribe(wasSet->{
        if (log.isDebugEnabled()) {
          log.debug(Thread.currentThread().getName() + ": wasSet is " + wasSet + " for count name " + countName);
        }
        if (wasSet) {
          action.handle(future);
        } else {
          if (log.isDebugEnabled()) {
            log.debug("This Once-only task synchronized on " + countName
                + " has already been performed by some other verticle instance");
          }
          future.complete();
        }
      });
    });
  }
}
