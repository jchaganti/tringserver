package com.passel.solr;


import com.manifolde.tring.server.constants.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amitvalse
 */
public class SolrSearchTest {

    List<String> strings = Arrays.asList("Mumbai match", "another mumbai Match will be played", "one of the best #mumbai match", "mumbai Indians match", "mumbai cricket match club", "mumbai football association match", "mumbaikar", "aamchi mumbai", "Atharva college mumbai", "mumbai police", "abc mumbai xyz", "match of mumbai", "match mumbai", "last match won by mumbai indians");
    Logger logger = LoggerFactory.getLogger(SolrSearchTest.class);

    @Ignore
    @Test
    public void solrAdd() {
        for (int i = 0; i < strings.size(); ++i) {
            addGroup(strings.get(i), UUID.randomUUID().toString());
        }
    }

    @Ignore
    @Test
    public void solrSearc() {
        searchGroup("mumbai match");
    }

    public void addGroup(String groupName, String groupId) {
        try {
            HttpSolrServer server = new HttpSolrServer(Constants.SERVER_ADDRESS);
            SolrInputDocument doc = new SolrInputDocument();
            doc.addField("groupId", groupId);
            doc.addField("groupName", groupName);
            server.add(doc);
            server.commit();
        } catch (SolrServerException | IOException ex) {
            logger.error("", ex);
        }
    }

    public void searchGroup(String searchParam) {

        List<String> groupIdList = new ArrayList<>();
        try {
            System.out.println("searchParam===="+searchParam);
            System.out.println("solr server add=" + Constants.SERVER_ADDRESS);
            HttpSolrServer solrServer = new HttpSolrServer(Constants.SERVER_ADDRESS);
            String finalKeywords = "";
            String[] keywords = searchParam.split(" ");
            for (String keyword : keywords) {
                finalKeywords += "+" + keyword;
            }
            finalKeywords = finalKeywords.substring(1);

            QueryResponse response;
            SolrQuery query = new SolrQuery();
            query.setQuery("groupName:" + finalKeywords);
            query.set("defType", "edismax");
            query.setFields("groupId", "groupName");
            response = solrServer.query(query);
            logger.info("response==============");
            SolrDocumentList results = response.getResults();
            for (int i = 0; i < results.size(); ++i) {
                groupIdList.add(response.getResults().get(i).getFieldValue("groupId").toString());
                logger.info(response.getResults().get(i).getFieldValue("groupName").toString());
                System.out.println(response.getResults().get(i).getFieldValue("groupName").toString());
            }
            logger.info(""+groupIdList);
        } catch (SolrServerException ex) {
            logger.error("", ex);
        }
    }
}