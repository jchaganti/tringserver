package com.manifolde.tring.server.utils.async;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import rx.Observable;


public class PerformAsyncTasksSeriallyTest {
  List<List<Object>> argsList0, argsList1, argsList2;
  List<AsyncSerialTask> tasks0, tasks1, tasks2, tasks3;

  @Before
  public void setup() {
    argsList0 = Arrays.asList(Arrays.asList(2, "sachin"));
    argsList1 = Arrays.asList(Arrays.asList(2, "sachin"), Arrays.asList(),
        Arrays.asList("three numbers", Arrays.asList(2, 3, 4)));
    argsList2 = Arrays.asList(Arrays.asList(200, "undo-sachin"), Arrays.asList(),
        Arrays.asList("undo-three numbers", Arrays.asList(2, 3, 4)));
    tasks0 = Arrays.asList((t, f, args) -> {
      int i = (Integer) args[0];
      String name = (String) args[1];
      System.out.println("Executed first task, args are " + i + " and " + name);
      f.complete();
    });
    tasks1 = Arrays.asList((t, f, args) -> {
      int i = (Integer) args[0];
      String name = (String) args[1];
      System.out.println("Executed first task, args are " + i + " and " + name);
      f.complete();
    } , (t, f, a) -> {
      System.out.println("Executed second task, no args");
      f.complete();
    } , (t, f, args) -> {
      String description = (String) args[0];
      List<Integer> intArray = (List<Integer>) args[1];
      System.out.println("Description is " + description);
      System.out.println("");
      Observable.from(intArray).subscribe(num -> {
        System.out.println(num);
      });
      f.complete();
    });
    tasks2 = Arrays.asList((t, f, args) -> {
      int i = (Integer) args[0];
      String name = (String) args[1];
      System.out.println("Executed first task, args are " + i + " and " + name);
      f.complete();
    } , (t, f, a) -> {
      System.out.println("Executed second task, no args");
      f.complete();
    } , (t, f, args) -> {
      String description = (String) args[0];
      List<Integer> intArray = (List<Integer>) args[1];
      System.out.println("Description is " + description);
      System.out.println("");
      Observable.from(intArray).subscribe(num -> {
        System.out.println(num);
      });
      f.fail("Something went wrong with this step");
    });
    tasks3 = Arrays.asList((t, f, args) -> {
      int i = (Integer) args[0];
      String name = (String) args[1];
      System.out.println("Executed first rollback task, args are " + i + " and " + name);
      f.complete();
    } , (t, f, a) -> {
      System.out.println("Failed to Execute second rollback task, no args");
      f.fail("Failed a rollback task");
    });
  }

  @Test
  public void testTransactionTask() {

    System.out.println("\ntestTransactionTask: ");

    AsyncSerialTasksTransaction trans = new AsyncSerialTasksTransaction(tasks0, argsList0);
    trans.execute();
    Assert.assertTrue(trans.succeeded());
  }

  @Test
  public void testTransactionWithManyStepsAndSuccessHandling() {
    System.out.println("\ntestTransactionWithManyStepsAndSuccessHandling: ");

    AsyncSerialTasksTransaction trans = new AsyncSerialTasksTransaction(tasks1, argsList1);
    trans.onSuccess(() -> {
      System.out.println("Transaction Succeeded !");
      Assert.assertTrue(trans.succeeded());
    });
    trans.execute();
  }

  @Test
  public void testTransactionWithManyStepsAndAFailedStep() {
    System.out.println("\ntestTransactionTaskWithManyStepsAndAFailedStep: ");
    AsyncSerialTasksTransaction trans = new AsyncSerialTasksTransaction(tasks2, argsList1);
    trans.execute();
    Assert.assertFalse(trans.succeeded());
  }

  @Test
  public void testTransactionWithManyStepsAndAFailedStepAndUndo() {
    System.out.println("\ntestTransactionTaskWithManyStepsAndAFailedStepAndUndo: ");
    AsyncSerialTasksTransaction trans =
        new AsyncSerialTasksTransaction(tasks2, argsList1, tasks2, argsList2);
    trans.onSuccess(() -> {
      System.out.println("Transaction Succeeded !");
      Assert.assertFalse(trans.succeeded());
    });
    trans.onFailure(succeeded -> {
      System.out.println("Transaction Failed !");
      if (succeeded) {
        System.out.println("But Rollback succeeded");
      } else {
        System.out.println("FATAL ERROR: Even Rollback failed");
      }
      Assert.assertFalse(trans.succeeded());
      Assert.assertTrue(succeeded);
    });
    trans.execute();
    Assert.assertFalse(trans.succeeded());
  }

  @Test
  public void testTransactionWithManyStepsAndAFailedStepAndFailingUndo() {
    System.out
        .println("\ntestTransactionWithManyStepsAndAFailedStepAndUndoAndTransactionHandlers: ");
    AsyncSerialTasksTransaction trans =
        new AsyncSerialTasksTransaction(tasks2, argsList1, tasks3, argsList2);
    trans.onSuccess(() -> {
      System.out.println("Transaction Succeeded !");
      Assert.assertFalse(trans.succeeded());
    }).onFailure(succeeded -> {
      System.out.println("Transaction Failed !");
      if (succeeded) {
        System.out.println("But Rollback succeeded");
      } else {
        System.out.println("FATAL ERROR: Even Rollback failed");
      }
      Assert.assertFalse(trans.succeeded());
      Assert.assertFalse(succeeded);
    }).execute();
    Assert.assertFalse(trans.succeeded());
  }

  @Test
  public void anotheTestTransactionWithManyStepsAndSuccessHandling() {
    System.out.println("\nanotherTestTransactionWithManyStepsAndSuccessHandling: ");

    AsyncSerialTasksTransaction trans = new AsyncSerialTasksTransaction();
    trans.task(tasks1.get(1));
    trans.onSuccess(() -> {
      System.out.println("Transaction Succeeded !");
      Assert.assertTrue(trans.succeeded());
    });
    trans.execute();
  }
}
