package com.manifolde.tring.integ.handle;

import static com.google.common.collect.Sets.difference;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.integ.TringBaseIntegrationTest;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO;
import com.manifolde.tring.server.handlers.TringGroupHandler;
import com.manifolde.tring.server.verticles.TringWebApiServer;

public class TringSubGroupTest extends TringBaseIntegrationTest {
  private static final String GROUP = "indiaN.mUsic_";
  private static final String OWNER_HANDLE_PREFIX = "Owner_";
  private static final String MEMBER_HANDLE_PREFIX = "Member_";
  private static final String SUBGROUP_NAME_PREFIX = "Subgroup_";
  private static final int NO_OF_MEMBERS = 10;
  private static long MOBILE_NO_START = 9860098600L;
  private static final String MOBILE_NO_PREFIX = "+91";
  private static String GROUP_NAME;
  private static String GID;
  private static String HID;
  private static String OWNER_HANDLE_NAME;
  private static List<Integer> ADMIN_COUNTERS = new ArrayList<Integer>() {
    {
      add(1);
      add(4);
      add(7);
    }
  };

  private static int[][][] createSubGroupData = {
      // AdminModerator, Moderator, Member
      {{1}, {2}, {3}}, 
      {{4}, {5, 6}, {2, 8, 10}}, 
      {{1, 7}, {10}, {8, 9}},
      {{}, {2, 5, 8}, {3, 6, 9}},
      {{}, {}, {3, 6}}};

  private static int[][][] createSubGroupResult = {
      // Moderators, Members
      {{1, 2}, {3}},
      {{4, 5, 6}, {2, 8, 10}},
      {{1, 7, 10}, {8, 9}}, 
      {{2, 5, 8}, {3, 6, 9}}};

  private static int[][][] createSubGroupUpdateResult = {
      // Moderators, Members, NonMembers
      {{1, 2}, {3}, {4, 5, 6, 7, 8, 9, 10}},
      {{4, 5, 6}, {2, 8, 10}, {1, 3, 7, 9}},
      {{1, 7, 10}, {8, 9}, {2, 3, 4, 5, 6}}, 
      {{2, 5, 8}, {3, 6, 9}, {1, 4, 7, 10}}};

  private static int[][][] editSubGroupData = {
      // AdminModerator, Moderator, Member, AdminAsNonMember, NonMember
      {{7}, {9}, {2, 6}, {1}, {3}}, 
      {{}, {2, 10}, {5, 8}, {4}, {6}},
      {{7}, {8, 5}, {3, 6}, {1}, {9}},
      {{1, 4}, {3, 6}, {10}, {}, {2, 5, 8}}};

  private static int[][][] editSubGroupResult = {
      // Moderators, Members
      {{7, 9}, {2, 6}}, 
      {{2, 10}, {5, 8}},
      {{5, 7, 10, 8}, {3, 6}},
      {{1, 3, 4, 6}, {9, 10}}};

  private static int[][][] editSubGroupUpdateResult = {
      // Moderators, Members, NonMembers
      {{7, 9}, {2, 6}, {1, 3, 4, 5, 8, 10}},
      {{2, 10}, {5, 8}, {1, 3, 4, 6, 7, 9}},
      {{5, 7, 10, 8}, {3, 6}, {1, 2, 4, 9}},
      {{1, 3, 4, 6}, {9, 10}, {2, 5, 7, 8}}};
  
  private static List<String> MEMBER_MOBILE_NOS = new ArrayList<>();
  private static List<String> MEMBER_HIDS = new ArrayList<>();
  private static List<String> MEMBER_HANDLE_NAMES = new ArrayList<>();
  private static List<String> SUB_GROUP_NAMES = new ArrayList<>();
  private static List<String> SUB_GROUP_IDS = new ArrayList<>();

  @Before
  public void setUp() {
    super.setUp();
    // Create Owner
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    GROUP_NAME = GROUP + now.getMillis();
    groupData.put(GroupDAO.GROUP_NAME, GROUP_NAME);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    OWNER_HANDLE_NAME = OWNER_HANDLE_PREFIX + now;
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE_NAME);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    GID = from(result).getString(TringWebApiServer.GID);
    HID = from(result).getString(HandleDAO.HID);

    // Create Members
    for (int i = 1; i <= NO_OF_MEMBERS; i++) {
      String mobileNo = MOBILE_NO_PREFIX + String.valueOf(MOBILE_NO_START++);
      MEMBER_MOBILE_NOS.add(mobileNo);
      createMember(i, mobileNo);
    }

    // Add as Admins
    req = getRequestWithHeader();
    for (int ADMIN_COUNTER : ADMIN_COUNTERS) {
      addAsAdmin(req, ADMIN_COUNTER);
    }
  }

  private void addAsAdmin(RequestSpecification req, int ADMIN_COUNTER) {
    String hid = MEMBER_HIDS.get(ADMIN_COUNTER - 1);
    Map<String, Object> memberData = new HashMap<>();
    memberData.put(TringWebApiServer.HANDLE_NAME, OWNER_HANDLE_NAME);
    memberData.put(HandleDAO.GID, GID);
    memberData.put("action", "add");
    memberData.put("adminHid", hid);
    Response res = req.contentType("application/json").body(memberData).when().post("/user/admin");
    String result = res.getBody().asString();
    boolean success = from(result).getBoolean("success");
    Assert.assertTrue(success);
  }

  private void createMember(int counter, String mobileNo) {
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, mobileNo);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    String handleName = MEMBER_HANDLE_PREFIX + counter + "_" + DateTime.now().getMillis();
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    MEMBER_HIDS.add(hid);
    MEMBER_HANDLE_NAMES.add(handleName);
  }

  public TringSubGroupTest() {}

  @Test
  public void testHandleSubGroup() {
    String action = "CREATE";
    for (int i = 0; i < 4; i++) {
      testHandleSubGroup(i, action, true);
    }
    for (int i = 0; i < 4; i++) {
      testSubGroup(i, createSubGroupResult, null);
    }
    for (int i = 0; i < 4; i++) {
      testSubGroup(i, createSubGroupResult, createSubGroupUpdateResult);
    }
    testHandleSubGroup(4, action, false);
    getGroupMemberships(-1, createSubGroupResult);
    action = "EDIT";
    for (int i = 0; i < 4; i++) {
      testHandleSubGroup(i, action, true);
    }
    getGroupMemberships(-1, editSubGroupResult);
    for (int i = 0; i < 4; i++) {
      testSubGroup(i, editSubGroupResult, null);
    }
    
    for (int i = 0; i < 4; i++) {
      testSubGroup(i, editSubGroupResult, editSubGroupUpdateResult);
    }
    
    testAllGroups();

  }
  
  @Test
  public void testNonMembers() {
    for(String mobileNo: MEMBER_MOBILE_NOS) {
      testDeviceVerification(mobileNo, 1729, (res) -> {
        assertTrue(res);
      });
    }
    
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object>  json = new HashMap<>();
    Set<String> expectedNonMembers = new HashSet<>();
    for (int i = 1; i <= NO_OF_MEMBERS; i++) {
      String mobileNo = MOBILE_NO_PREFIX + String.valueOf(MOBILE_NO_START++);
      expectedNonMembers.add(mobileNo);
      MEMBER_MOBILE_NOS.add(mobileNo);
    }
    json.put("mobileNos", MEMBER_MOBILE_NOS);
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/contacts" );
    assertTrue(res.statusCode() == 200);
    req = getRequestWithHeader();
    String url = "/user/nonmembers/" + GID + "/" + MEMBER_HANDLE_NAMES.get(ADMIN_COUNTERS.get(0) - 1);    
    res = req.contentType("application/json").when().get(url);
    String result = res.getBody().asString();
    System.out.println("The result: " + result);
    boolean success = from(result).getBoolean("success");
    assertTrue(success);
    List<String> nonMembers = from(result).getList("nonMembers");
    Set<String> actualNonMembers =  new HashSet<> (nonMembers);
    int setDiff = Sets.difference(expectedNonMembers, actualNonMembers).size();
    assertTrue(setDiff == 0);
    setDiff = Sets.difference(actualNonMembers, expectedNonMembers).size();
    assertTrue(setDiff == 0);
  }
  
  private void testAllGroups() {
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, MEMBER_MOBILE_NOS.get(ADMIN_COUNTERS.get(0) - 1));
    RequestSpecification req = getRequestWithHeader(json);
    Response res = req.contentType("application/json").when().get("/user/allgroups");
    String result = res.getBody().asString();
    System.out.println("All Groups: " + result);
//    List<Object> parentGroups = from(result).getList("parentGroups");
//    System.out.println("parentGroups.size():  " + parentGroups.size());
//    assertTrue(parentGroups.size() > 0);
//    List<Object> subGroups = from(result).getList("subGroups");
//    System.out.println("subGroups.size():  " + subGroups.size());
//    assertTrue(subGroups.size() > 0);
  }

  private void testSubGroup(int i, int[][][] subGroupResult, int[][][] subGroupNonMembersResult) {
    String subGroupId = SUB_GROUP_IDS.get(i);
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, MEMBER_MOBILE_NOS.get(ADMIN_COUNTERS.get(0) - 1));
    RequestSpecification req = getRequestWithHeader(json);

    String url = "/user/members/" + GID + "/" + MEMBER_HANDLE_NAMES.get(ADMIN_COUNTERS.get(0) - 1)
        + "/" + subGroupId + "?page=1&pageSize=20";
    if(subGroupNonMembersResult != null) {
      url += "&includeNonMembers=true";
    }
    Response res = req.contentType("application/json").when().get(url);
    String result = res.getBody().asString();

    Set<String> expectedAdminAsModerators = new HashSet<>();
    Set<String> expectedMembersAsModerators = new HashSet<>();
    Set<String> expectedAdminAsNonMembers = null;
    Set<String> expectedMembers = new HashSet<>();
    Set<String> expectedNonMembers = null;
    int[] moderators = subGroupResult[i][0];
    populateSubGroupModerators(moderators, expectedAdminAsModerators, expectedMembersAsModerators);
    if(subGroupNonMembersResult != null) {
      expectedAdminAsNonMembers = new HashSet<>();
      expectedAdminAsNonMembers.add(HID);
      expectedNonMembers = new HashSet<>();
      populateNonMembers(subGroupNonMembersResult[i][2], expectedAdminAsNonMembers, expectedNonMembers);
    }
    populateSubGroup(subGroupResult[i][1], expectedMembers);

    Set<String> actualAdminAsModerators = new HashSet<>();
    Set<String> actualMembersAsModerators = new HashSet<>();
    Set<String> actualMembers = new HashSet<>();
    Set<String> actualAdminAsNonMembers = new HashSet<>();
    Set<String> actualNonMembers = new HashSet<>();
    String key = "adminsAsModerator";
    populateActualSubGroupData(result, actualAdminAsModerators, key);
    int setDiff = Sets.difference(expectedAdminAsModerators, actualAdminAsModerators).size();
    assertTrue(setDiff == 0);
    setDiff = Sets.difference(actualAdminAsModerators, expectedAdminAsModerators).size();
    assertTrue(setDiff == 0);

    key = "adminsAsNonMembers";
    List<Object> adminsAsNonMembers = from(result).getList(key);
    if(subGroupNonMembersResult == null) {
      assertTrue(adminsAsNonMembers == null);
    }
    else {
      populateActualSubGroupData(result, actualAdminAsNonMembers, key);
      System.out.println("actualAdminAsNonMembers: " + actualAdminAsNonMembers);
      System.out.println("expectedAdminAsNonMembers: " + expectedAdminAsNonMembers);
      setDiff = Sets.difference(expectedAdminAsNonMembers, actualAdminAsNonMembers).size();
      assertTrue(setDiff == 0);
      setDiff = Sets.difference(actualAdminAsNonMembers, expectedAdminAsNonMembers).size();
      assertTrue(setDiff == 0);
    }
    

    key = "membersAsModerator";
    populateActualSubGroupData(result, actualMembersAsModerators, key);
    setDiff = Sets.difference(expectedMembersAsModerators, actualMembersAsModerators).size();
    assertTrue(setDiff == 0);
    setDiff = Sets.difference(actualMembersAsModerators, expectedMembersAsModerators).size();
    assertTrue(setDiff == 0);

    key = "members";
    populateActualSubGroupData(result, actualMembers, key);
    setDiff = Sets.difference(expectedMembers, actualMembers).size();
    assertTrue(setDiff == 0);
    setDiff = Sets.difference(actualMembers, expectedMembers).size();
    assertTrue(setDiff == 0);

    key = "nonMembers";
    List<Object> nonMembers = from(result).getList(key);
    if(subGroupNonMembersResult == null) {
      assertTrue(nonMembers == null);
    }
    else {
      populateActualSubGroupData(result, actualNonMembers, key);
      setDiff = Sets.difference(expectedNonMembers, actualNonMembers).size();
      assertTrue(setDiff == 0);
      setDiff = Sets.difference(actualNonMembers, expectedNonMembers).size();
      assertTrue(setDiff == 0);
    }
  }

  
  private void populateActualSubGroupData(String result, Set<String> actualAdminAsModerators,
      String key) {
    List<Object> list = from(result).getList(key);
    for (Object o : list) {
      Map<String, Object> hids = (Map<String, Object>) o;
      Set<Entry<String, Object>> entrySet = hids.entrySet();
      for (Entry<String, Object> e : entrySet) {
        actualAdminAsModerators.add(e.getKey());
      }
    }
  }

  private void testHandleSubGroup(int groupNo, String action, boolean result) {
    List<String> asModerators = new ArrayList<String>();
    List<String> adminAsModerators = new ArrayList<String>();
    List<String> asMembers = new ArrayList<String>();
    int[][][] subGroupData = action.equals("CREATE") ? createSubGroupData : editSubGroupData;
    setupCreateSubGroup(subGroupData[groupNo], adminAsModerators, asModerators, asMembers);
    boolean success = handleSubGroup(groupNo, asModerators, adminAsModerators, asMembers, action);
    if (result) {
      Assert.assertTrue(success);
    } else {
      Assert.assertFalse(success);
    }
  }

  private boolean handleSubGroup(int groupNo, List<String> asModerators,
      List<String> adminAsModerators, List<String> asMembers, String action) {
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, MEMBER_MOBILE_NOS.get(ADMIN_COUNTERS.get(0) - 1));
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    String subGroupName = null;
    if (action.equals("CREATE")) {
      subGroupName = SUBGROUP_NAME_PREFIX + groupNo + "_" + DateTime.now().getMillis();
      SUB_GROUP_NAMES.add(subGroupName);
    } else {
      handleEditSubGroup(groupNo, memberData);
      subGroupName = SUB_GROUP_NAMES.get(groupNo);
    }
    setMemberData(asModerators, adminAsModerators, asMembers, action, memberData, subGroupName);
    Response res =
        req.contentType("application/json").body(memberData).when().post("/user/members");
    String result = res.getBody().asString();
    System.out.println("Result of handleSubGroup: " + result);
    if (action.equals("CREATE")) {
      SUB_GROUP_IDS.add(from(result).getString(HandleDAO.SUBGROUP_GID));
    }
    return from(result).getBoolean("success");
  }

  private void setMemberData(List<String> asModerators, List<String> adminAsModerators,
      List<String> asMembers, String action, Map<String, Object> memberData, String subGroupName) {
    memberData.put(TringWebApiServer.HANDLE_NAME,
        MEMBER_HANDLE_NAMES.get(ADMIN_COUNTERS.get(0) - 1));
    memberData.put(HandleDAO.GID, GID);
    memberData.put(GroupDAO.GROUP_NAME, subGroupName);
    memberData.put(HandleDAO.AS_MODERATOR, asModerators);
    memberData.put(HandleDAO.ADMIN_AS_MODERATOR, adminAsModerators);
    memberData.put(HandleDAO.AS_MEMBER, asMembers);
    memberData.put("action", action);
  }

  private void handleEditSubGroup(int groupNo, Map<String, Object> memberData) {
    memberData.put(HandleDAO.SUBGROUP_GID, SUB_GROUP_IDS.get(groupNo));
    List<String> adminAsNonMembers = new ArrayList<String>();
    List<String> nonMembers = new ArrayList<String>();
    populateList(editSubGroupData[groupNo][3], adminAsNonMembers);
    populateList(editSubGroupData[groupNo][4], nonMembers);
    memberData.put(HandleDAO.ADMIN_AS_NONMEMBER, adminAsNonMembers);
    memberData.put(HandleDAO.AS_NON_MEMBER, nonMembers);
  }

  private void setupCreateSubGroup(int[][] inputData, List<String> adminAsModerators,
      List<String> asModerators, List<String> asMembers) {
    populateList(inputData[0], adminAsModerators);
    populateList(inputData[1], asModerators);
    populateList(inputData[2], asMembers);
  }

  private void getGroupMemberships(int groupNo, int[][][] subGroupResult) {
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, MEMBER_MOBILE_NOS.get(ADMIN_COUNTERS.get(0) - 1));
    RequestSpecification req = getRequestWithHeader(json);

    String url = "/user/members/" + GID + "/" + MEMBER_HANDLE_NAMES.get(ADMIN_COUNTERS.get(0) - 1)
        + "?page=1&pageSize=20";
    if (groupNo >= 0) {
      url += "&" + HandleDAO.SUBGROUP_GID + "=" + SUB_GROUP_IDS.get(groupNo);
    }
    Response res = req.contentType("application/json").when().get(url);
    String result = res.getBody().asString();
    System.out.println("getGroupMemberships result: " + result);

    Map<String, Set<String>> subGroupToMembers = new HashMap<>();
    Map<String, Set<String>> subGroupModerators = new HashMap<>();
    populateResultData(result, "MEMBER", subGroupToMembers, subGroupModerators);
    populateResultData(result, "ADMIN", subGroupToMembers, subGroupModerators);
    populateResultData(result, "OWNER", subGroupToMembers, subGroupModerators);

    Map<String, Set<String>> expectedSubGroupToMembers = new HashMap<>();
    Map<String, Set<String>> expectedSubGroupModerators = new HashMap<>();
    populateExpectedData(subGroupResult, expectedSubGroupToMembers, expectedSubGroupModerators);

    System.out.println("The Group No: " + groupNo);
    System.out.println("Actual subGroupToMembers: " + subGroupToMembers);
    System.out.println("Expected Result of subGroupToMembers: " + expectedSubGroupToMembers);
    System.out.println("Actual Result of subGroupModerators: " + subGroupModerators);
    System.out.println("Expected Result of subGroupModerators: " + expectedSubGroupModerators);
    assertSubGroups(subGroupToMembers, expectedSubGroupToMembers);
    assertSubGroups(subGroupModerators, expectedSubGroupModerators);

  }

  private void assertSubGroups(Map<String, Set<String>> subGroupToMembers,
      Map<String, Set<String>> expectedSubGroupToMembers) {
    for (Entry<String, Set<String>> subGroupData : expectedSubGroupToMembers.entrySet()) {
      String sgid = subGroupData.getKey();
      Set<String> expectedMembers = subGroupData.getValue();
      Set<String> actualMembers = subGroupToMembers.get(sgid);
      int setDiff = Sets.difference(expectedMembers, actualMembers).size();
      assertTrue(setDiff == 0);
      SetView<String> difference = Sets.difference(actualMembers, expectedMembers);
      System.out.println(difference);
      setDiff = difference.size();
      
      assertTrue(setDiff == 0);
    }
  }

  private void populateExpectedData(int[][][] subGroupResult,
      Map<String, Set<String>> expectedSubGroupToMembers,
      Map<String, Set<String>> expectedSubGroupModerators) {
    for (int i = 0; i < subGroupResult.length; i++) {
      populateExpectedData(i, 0, subGroupResult, expectedSubGroupModerators);
      populateExpectedData(i, 1, subGroupResult, expectedSubGroupToMembers);
    }
  }

  private void populateExpectedData(int subGroupNo, int type, int[][][] subGroupResult,
      Map<String, Set<String>> expectedSubGroupModerators) {
    int[][] subGroupData = subGroupResult[subGroupNo];
    int[] moderatorsData = subGroupData[type];
    Set<String> subGroupModerators = expectedSubGroupModerators.get(SUB_GROUP_IDS.get(subGroupNo));
    if (subGroupModerators == null) {
      subGroupModerators = new HashSet<>();
    }
    for (int i : moderatorsData) {
      subGroupModerators.add(MEMBER_HIDS.get(i - 1));
    }
    expectedSubGroupModerators.put(SUB_GROUP_IDS.get(subGroupNo), subGroupModerators);
  }

  private void populateResultData(String result, String type,
      Map<String, Set<String>> subGroupToMembers, Map<String, Set<String>> subGroupModerators) {
    List<Object> members = from(result).getList(type);
    for (Object i : members) {
      Map<String, Object> map = (Map<String, Object>) i;
      for (Entry<String, Object> e : map.entrySet()) {
        String hid = e.getKey();
        Map<String, Object> data = (Map<String, Object>) e.getValue();
        _populateSgidData(subGroupModerators, "asModerator", hid, data);
        _populateSgidData(subGroupToMembers, "asMember", hid, data);
      }
    }
  }

  private void _populateSgidData(Map<String, Set<String>> subGroupData, String type, String hid,
      Map<String, Object> data) {
    List<Object> subGoupMemberGids = (List<Object>) data.get(type);
    for (Object o1 : subGoupMemberGids) {
      Map<String, Object> sgidData = (Map<String, Object>) o1;
      for (Entry<String, Object> e1 : sgidData.entrySet()) {
        String sgid = e1.getKey();
        Set<String> hids = subGroupData.get(sgid);
        if (hids == null) {
          hids = new HashSet<>();
          subGroupData.put(sgid, hids);
        }
        hids.add(hid);
      }
    }
  }

  private void populateList(int[] is, List<String> members) {
    for (int i : is) {
      members.add(MEMBER_HIDS.get(i - 1));
    }
  }

  private Set<String> getMemberSet(int[] is, Set<String> members) {
    for (int i : is) {
      members.add(MEMBER_HIDS.get(i - 1));
    }
    return members;
  }

  private void populateSubGroupModerators(int[] is, Set<String> adminAsModerators,
      Set<String> membersAsModerators) {
    for (int i : is) {
      if (ADMIN_COUNTERS.contains(i)) {
        adminAsModerators.add(MEMBER_HIDS.get(i - 1));
      } else {
        membersAsModerators.add(MEMBER_HIDS.get(i - 1));
      }
    }
  }
  
  private void populateNonMembers(int[] is, Set<String> adminAsNonMembers,
      Set<String> nonMembers) {
    for (int i : is) {
      if (ADMIN_COUNTERS.contains(i)) {
        adminAsNonMembers.add(MEMBER_HIDS.get(i - 1));
      } else {
        nonMembers.add(MEMBER_HIDS.get(i - 1));
      }
    }
  }

  private void populateSubGroup(int[] is, Set<String> members) {
    for (int i : is) {
      members.add(MEMBER_HIDS.get(i - 1));
    }
  }
}
