package com.manifolde.tring.integ.handle;

import static com.jayway.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.integ.TringBaseIntegrationTest;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.dao.HandleDAO.HandleStatus;
import com.manifolde.tring.server.dao.UserDAO;
import com.manifolde.tring.server.dao.UserNotesDAO;
import com.manifolde.tring.server.handlers.TringGroupHandler;
import com.manifolde.tring.server.handlers.TringHandleHandler;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.json.JsonObject;

public class TringHandleHandlerTest extends TringBaseIntegrationTest {
  private static final String GROUP1 = "iNdian.mUsic";
  private static final String OWNER_HANDLE1 = "Baiju_Bawra";
  private static final String MEMBER_HANDLE = "TaNsen";
  private static final String [] NOTES = {
      "First set of notes",
      "Second set of notes"
  };
  private static  String GROUP_NAME;
  private static String GID;
  private static String OWNER_HANDLE_NAME;

  @Before
  public void setUp() {
    super.setUp();
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    GROUP_NAME = GROUP1 + now;
    GROUP_NAME=GROUP_NAME.replace(":", "").replace("+", "").replace("-","");
    groupData.put(GroupDAO.GROUP_NAME, GROUP_NAME);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("carton","marshmallows")));
    groupData.put(GroupDAO.MEMBERSHIP_NEEDS_APPROVAL, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    OWNER_HANDLE_NAME = OWNER_HANDLE1 + now;
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE_NAME);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    System.out.println("from(result) is " + from(result).prettify());
    Assert.assertEquals(true, from(result).getBoolean("success"));
    GID = from(result).getString(TringWebApiServer.GID);
  }

  public TringHandleHandlerTest() {}

  @Test
  public void testGetP2PTopicForHandleNameAndGroupId() {
	  Map<String, Object>  json = new HashMap<>();
	    json.put(MOBILE_NO, _9850820341);
	    RequestSpecification req = getRequestWithHeader(json);
	    Map<String, Object> memberData = new HashMap<>();
	    DateTime now = DateTime.now();
	    String handleName = MEMBER_HANDLE + now.getMillis();
	    
	    // This set checks availability of the group member. Now it is available
	    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
	    req = getRequestWithHeader(json);
	    Response res = req.contentType("application/json").when().get(url);
	    System.out.println(res.getBody());
	    String result = res.getBody().asString();
	    Assert.assertEquals(true, from(result).getBoolean("success"));
	    
	    // This creates the group member and confirms by checking it handle id.
	    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
	    res = req.contentType("application/json").body(memberData).when()
	        .post("/user/group/" + GID + "/member");
	    System.out.println(res.getBody());
	    result = res.getBody().asString();
	    String hid = from(result).getString("hid");
	    Assert.assertEquals(true, from(result).getBoolean("success"));
	    Assert.assertNotNull(hid);
	    req = getRequestWithHeader(json);
	    res = req.contentType("application/json").when()
	        .get("/group/" + GID + "/member/" + handleName);
	    result = res.getBody().asString();
	    String p2pTopic = from(result).getString("p2p");
	    Assert.assertNotNull(p2pTopic);
  } 
  
  @Test
  public void testRatingAndReviews(){
  //Use an alternate user (details go in json object)
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This creates the group temp member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GROUP_NAME + "/tempmember");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    //Now test acceptance of membership
    String url= "/user/group/"+GID+"/member/"+handleName+"/"+TringWebApiServer.ACCEPT;
    //revert to original user who created Group
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Get the ratings and reviews - 0 ratings
    url = "/group/"+GID+"/rating-reviews";
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(0, from(result).getInt(TringWebApiServer.RATING_COUNT));
    
    //User puts in a rating and review
    url = "/user/group/"+GID+"/rating-review";
    req = getRequestWithHeader(json);
    memberData.clear();
    String review = "This is a truely fantastic network. I just love it !";
    memberData.put(TringWebApiServer.RATING,4);
    memberData.put(TringWebApiServer.REVIEW,review);
    res = req.contentType("application/json")
        .body(memberData).when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    /*Assert.assertEquals(1, from(result).getInt(TringWebApiServer.RATING_COUNT));
    Assert.assertEquals(4.0, from(result).getInt(TringWebApiServer.MEAN_RATING),0.00001);
    Assert.assertTrue(from(result).getString(TringWebApiServer.REVIEWS).contains(review));*/
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Check the review again
    url = "/group/"+GID+"/rating-reviews";
    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(1, from(result).getInt(TringWebApiServer.RATING_COUNT));
    Assert.assertEquals(4.0, from(result).getDouble(TringWebApiServer.MEAN_RATING),0.00001);
     List<Map<String,Object>>lst = from(result).getList(TringWebApiServer.REVIEWS);
     
    Assert.assertTrue(lst.get(0).get(TringWebApiServer.REVIEW).equals(review));
    Assert.assertTrue(lst.get(0).get(TringWebApiServer.RATING).equals(4));
    Assert.assertTrue(lst.get(0).get(TringWebApiServer.HANDLE_NAME).equals(handleName.toLowerCase()));

  }
  
  @Test
  public void testCreateGroupTempMember(){
    //Use an alternate user (details go in json object)
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This set checks availability of the group member. Now it is available
    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    // This creates the group temp member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GROUP_NAME + "/tempmember");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);

    // This set will check availability of the group member. Now it is not available
    url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
    
    //Do the same check, this time with group name in place of gid
    url = "/group-name/" + GROUP_NAME + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success")); 
    
    //Now test acceptance of membership
    url= "/user/group/"+GID+"/member/"+handleName+"/"+TringWebApiServer.ACCEPT;
    //revert to original user who created Group
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Repeat reject (i.e. a reject after an accept) should not be accepted 
    //(also an accept after a reject is not)
    url= "/user/group/"+GID+"/member/"+handleName+"/"+TringWebApiServer.REJECT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertNotNull(from(result).getJsonObject("error"));
    //Assert.assertEquals(true, from(result).getBoolean("success"));

    // This creates the group temp member and confirms by checking its handle id.
    now = DateTime.now();
    String otherHandleName = MEMBER_HANDLE + now.getMillis();
    
    memberData.put(TringWebApiServer.HANDLE_NAME, otherHandleName);
    now=DateTime.now();
    //json.clear();
    req = getRequestWithHeader();
    String groupName = GROUP1+now.getMillis();
    Map<String, Object> groupData =  new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, groupName);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("easy join")));
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    String NEW_OWNER_HANDLE_NAME = OWNER_HANDLE1 + now.getMillis();
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, NEW_OWNER_HANDLE_NAME);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    result = res.getBody().asString();
    String NEW_GID = from(result).getString(TringWebApiServer.GID);

    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + groupName + "/tempmember");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    //membership cannot be accepted by non-Owners or non-Admins
    url= "/user/group/"+GID+"/member/"+handleName+"/"+TringWebApiServer.ACCEPT;
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertNotNull(from(result).getJsonObject("error"));
    
    //this reject should succeed, since it is done by group Owner
    url= "/user/group/"+NEW_GID+"/member/"+otherHandleName+"/"+TringWebApiServer.REJECT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Trying to create a handle for user (which has been just REJECTed ) again gives error 409
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + groupName + "/tempmember");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    int errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals(409, errorCode);
    Assert.assertEquals(HandleStatus.REJECTED.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(otherHandleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
    
    //create Group member (invited) should return error 409
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + NEW_GID + "/member");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals(409, errorCode);
    Assert.assertEquals(HandleStatus.REJECTED.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(otherHandleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
  }
  
  @Test
  public void testCreateGroupMember() {
	Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This set checks availability of the group member. Now it is available
    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    // This creates the group member and confirms by checking its handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);

    // This set will check availability of the group member. Now it is not available
    url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
    
    //Trying to create the handle again for this user gives error 409
    //But this is not an error case, the existing handle name and status 
    //should also be returned
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    int errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals((int)TringHandleHandler.CONFLICT_ERROR, errorCode);
    Assert.assertEquals(HandleStatus.ACTIVE.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(handleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
    
    //Now the owner tries to create another handle for the same group. Again 409
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals((int)TringHandleHandler.CONFLICT_ERROR, errorCode);
    Assert.assertEquals(HandleStatus.ACTIVE.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(OWNER_HANDLE_NAME.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
  }
  
 
  @Test
  public void testSettingReputationForMember() {
	Map<String, Object>  json = new HashMap<>();
	json.put(MOBILE_NO, _9850820341);
	RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    Map<String, Object> reputationData = new HashMap<>();
    reputationData.put("moderatorHandleName", OWNER_HANDLE_NAME);
    reputationData.put("reputationKey", UserDAO.ABUSES);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);
    
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    reputationData.put("reputationKey", UserDAO.WARNINGS);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);
    reputationData.put("moderatorMessage", "A warning message from moderator!!");
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    reputationData.put("reputationKey", UserDAO.EVICTIONS);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);
    reputationData.put("moderatorMessage", "An eviction message from moderator!!");
    res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }
  
  private void createGroupMember(String handleName) {
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();

    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
  }

  private void createReputationForMember(String handleName) {
    Map<String, Object> reputationData = new HashMap<>();
    reputationData.put("moderatorHandleName", OWNER_HANDLE_NAME);
    reputationData.put("reputationKey", UserDAO.ABUSES);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);

    RequestSpecification req = getRequestWithHeader();
    Response res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    reputationData.put("reputationKey", UserDAO.WARNINGS);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);
    reputationData.put("moderatorMessage", "A warning message from moderator!!");
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    reputationData.put("reputationKey", UserDAO.EVICTIONS);
    reputationData.put(TringWebApiServer.HANDLE_NAME, handleName);
    reputationData.put(TringWebApiServer.GID, GID);
    reputationData.put("reputationValue", 1);
    reputationData.put("moderatorMessage", "An eviction message from moderator!!");
    res = req.contentType("application/json").body(reputationData).when()
        .post("/user/reputation");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }

  @Test
  public void testGetUserReputation() {
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();

    createGroupMember(handleName);

    RequestSpecification req = getRequestWithHeader();
    Response res = req.contentType("application/json")
        .get("/user/reputation/group/"+GID+"/handle/"+handleName);
    String result = res.getBody().asString();
    Integer warnings0 = from(result).getInt("warnings");
    Integer evictions0 = from(result).getInt("evictions");
    Integer abuses0 = from(result).getInt("abuses");
    createReputationForMember(handleName);

    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .get("/user/reputation/group/"+GID+"/handle/"+handleName);
    result = res.getBody().asString();
    Integer warnings1 = from(result).getInt("warnings");
    Integer evictions1 = from(result).getInt("evictions");
    Integer abuses1 = from(result).getInt("abuses");

    int warningsDelta = warnings1-warnings0;
    int abusesDelta = abuses1-abuses0;
    int evictionsDelta = evictions1-evictions0;
    Assert.assertEquals(1, warningsDelta);
    Assert.assertEquals(1, abusesDelta);
    Assert.assertEquals(1, evictionsDelta);
    //Now member wants his own report
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    req = getRequestWithHeader(json);
    res = req.contentType("application/json")
        .get("/user/reputation");
    result = res.getBody().asString();
    Integer warnings2 = from(result).getInt("warnings");
    Integer evictions2 = from(result).getInt("evictions");
    Integer abuses2 = from(result).getInt("abuses");
    Assert.assertEquals(warnings1, warnings2);
    Assert.assertEquals(evictions1, evictions2);
    Assert.assertEquals(abuses1, abuses2);
  }

  @Test
  public void testUserNotes() {
	Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    Map<String, Object> notesData = new HashMap<>();
    notesData.put(UserNotesDAO.NOTES, NOTES[0]);
    notesData.put(TringWebApiServer.HANDLE_NAME, handleName);
    notesData.put(TringWebApiServer.GID, GID);
    res = req.contentType("application/json").body(notesData).when()
        .post("/user/notes");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    String firstNotesId = from(result).getString("notesId");
    Assert.assertNotNull(firstNotesId);
    
    notesData.put(UserNotesDAO.NOTES, NOTES[1]);
    res = req.contentType("application/json").body(notesData).when()
        .post("/user/notes");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    String notesId = from(result).getString("notesId");
    Assert.assertNotNull(notesId);
    
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when()
        .get("/user/notes/" + handleName + "/" + GID);
    result = res.getBody().asString();
    List<Object> objects= from(result).getList("userNotesInfo");
    System.out.println("Notes size: " + objects.size());
    assertTrue(objects.size() == NOTES.length);
    
    notesData = new HashMap<>();
    notesData.put("notesId", notesId);
    notesData.put(UserNotesDAO.NOTES, "Updated " + NOTES[1]);
    res = req.contentType("application/json").body(notesData).when()
        .post("/user/notes/update");
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when()
        .get("/user/notes/" + handleName + "/" + GID);
    result = res.getBody().asString();
    objects= from(result).getList("userNotesInfo");
    assertTrue(objects.size() == NOTES.length);
    System.out.println("The userNotesInfo: " + objects);
    Map<String, Object> secondNoteObject = (Map<String, Object>) objects.get(1);
    String secondNote = (String) secondNoteObject.get(UserNotesDAO.NOTES);
    Assert.assertEquals(secondNote, "Updated " + NOTES[1]);
    
    req = getRequestWithHeader(json);
    
    res = req.contentType("application/json").when()
        .delete("/user/notes/" + firstNotesId);
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }
  
  /* Test to check the adding same user twice in the group with different handle name */
  @Test
  public void testTryToAddExisingGroupMemberWithAnotherHandle() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now;   
      
    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);

    // Try Adding Same User With differnet Handle
    handleName = MEMBER_HANDLE + now; 
    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
  }
  
  /* Test to check the adding second user in the group with existing handle name */
  @Test
  public void testTryToAddSecondGroupMemberWithExistingHandle() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now;   
      
    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);

    // Try Adding another User With same Handle
    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9822245688);
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
  }
  
  /* Functional Test to check the adding second user in the group with unique handle name */
  @Test
  public void testTryToAddSecondGroupMemberWithUniqueHandle() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now;   
      
    // This creates the group member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);

    // Try Adding another User With unique Handle
    now = DateTime.now();
    handleName = MEMBER_HANDLE + now;
    String url = "/group/" + GID + "/member/" + handleName + "/" + TringWebApiServer.IS_AVAILABLE;
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9822245688);
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }

  @Test
  public void testLeaveGroup(){
    //Use an alternate user (details go in json object)
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);
    RequestSpecification req = getRequestWithHeader(json);
    Map<String, Object> memberData = new HashMap<>();
    DateTime now = DateTime.now();
    String handleName = MEMBER_HANDLE + now.getMillis();
    
    // This creates the group temp member and confirms by checking it handle id.
    memberData.put(TringWebApiServer.HANDLE_NAME, handleName);
    Response res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GROUP_NAME + "/tempmember");
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    //Now test acceptance of membership
    String url= "/user/group/"+GID+"/member/"+handleName+"/"+TringWebApiServer.ACCEPT;
    //revert to original user who created Group
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Leave the group succeeds
     url= "/user/group/"+GID+"/leave";
     req = getRequestWithHeader(json);
     res = req.contentType("application/json").when().post(url);
     System.out.println(res.getBody());
     result = res.getBody().asString();
     Assert.assertEquals(true, from(result).getBoolean("success"));
     
   //Prevent the Owner from leaving
     req = getRequestWithHeader();
     res = req.contentType("application/json").when().post(url);
     System.out.println(res.getBody());
     result = res.getBody().asString();
     Assert.assertNotNull(from(result).getJsonObject("error"));
     
     //Getgroups should give the status of the left group as LEFT
     url= "/user/groups";
     req = getRequestWithHeader(json);
     res = req.contentType("application/json").when().get(url);
     System.out.println(res.getBody());
     result = res.getBody().asString();
     List listGroups = (List) from(result).get();
     boolean found = false;
     for(Object obj : listGroups) {
       HashMap jobj = (HashMap)obj;
       if (((String)jobj.get(TringWebApiServer.HANDLE_NAME)).equals(handleName.toLowerCase())) {
         found=true;
         Assert.assertEquals((long)HandleStatus.LEFT.ordinal(), (long)(Integer)jobj.get(TringWebApiServer.MEMBERSHIP_STATUS));
       }
     }
     Assert.assertTrue(found);
     
     //Rejoin using Discover groups
     memberData.put(TringWebApiServer.HANDLE_NAME, "different_"+handleName);
      res = req.contentType("application/json").body(memberData).when()
         .post("/user/group/" + GROUP_NAME + "/tempmember");
     System.out.println(res.getBody());
      result = res.getBody().asString();
     int errorCode = (int)from(result).getMap("error").get("code");
     Assert.assertEquals(409, errorCode);
     Assert.assertEquals(HandleStatus.PENDING.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
     Assert.assertEquals(handleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
     
     //Rejoin by Discover groups again.Old handle should be retained
     memberData.put(TringWebApiServer.HANDLE_NAME, "very_different_"+handleName);
     res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GROUP_NAME + "/tempmember");
    System.out.println(res.getBody());
     result = res.getBody().asString();
    errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals(409, errorCode);
    Assert.assertEquals(HandleStatus.PENDING.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(handleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
    
     //Rejoining by invitation converts to active user directly
     memberData.put(TringWebApiServer.HANDLE_NAME, "different_"+handleName);
     res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GID + "/member");
    System.out.println(res.getBody());
     result = res.getBody().asString();
    errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals(409, errorCode);
    Assert.assertEquals(HandleStatus.ACTIVE.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(handleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
     
    //Leave again
    url= "/user/group/"+GID+"/leave";
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Rejoining by invitation converts to active user directly
    memberData.put(TringWebApiServer.HANDLE_NAME, "different_"+handleName);
    res = req.contentType("application/json").body(memberData).when()
       .post("/user/group/" + GID + "/member");
   System.out.println(res.getBody());
    result = res.getBody().asString();
   errorCode = (int)from(result).getMap("error").get("code");
   Assert.assertEquals(409, errorCode);
   Assert.assertEquals(HandleStatus.ACTIVE.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
   Assert.assertEquals(handleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
    
   //After all this membership count for the group as found from solr should be 2
   try {
     Thread.sleep(20000); //sleep needed so that solr can soft-commit - which is after 15 secs as set now
   } catch (InterruptedException e) {
     e.printStackTrace();
   }
   url = "/search-groups-by-keyword/";
   req = getRequestWithHeader(json);
   res = req.contentType("application/json").body(new JsonObject().put("keyword", GROUP_NAME))
       .when().post(url);
   System.out.println(res.getBody());
   result = res.getBody().asString();
   List<Map<String,Object>>lst = from(result).get();
   int count = (int) lst.get(0).get("membercount");
   Assert.assertEquals(count, 2);
  }
  
  @Test
  public void testRequireMembershipApproval() {
    // This creates the group temp member and confirms by checking its handle id.
    DateTime now = DateTime.now();
    String otherHandleName = MEMBER_HANDLE + now.getMillis();
    Map<String, Object> memberData = new HashMap<>();
    Map<String, Object> json = new HashMap<>();
    json.put(MOBILE_NO, _9850820341);

    memberData.put(TringWebApiServer.HANDLE_NAME, otherHandleName);
    now=DateTime.now();
    //json.clear();
    RequestSpecification req = getRequestWithHeader();
    String groupName = GROUP1+now.getMillis();
    Map<String, Object> groupData =  new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, groupName);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("easy join")));
    groupData.put(GroupDAO.MEMBERSHIP_NEEDS_APPROVAL, false);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    String NEW_OWNER_HANDLE_NAME = OWNER_HANDLE1 + now.getMillis();
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, NEW_OWNER_HANDLE_NAME);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    String result = res.getBody().asString();
    String NEW_GID = from(result).getString(TringWebApiServer.GID);

    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + groupName + "/tempmember");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    String hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    //accept membership has no effect
    String url = "/user/group/"+NEW_GID+"/member/"+otherHandleName+"/"+TringWebApiServer.ACCEPT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertNotNull(from(result).getJsonObject("error"));
    
    //Leave the group succeeds
    url= "/user/group/"+NEW_GID+"/leave";
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
 
    //Rejoin using Discover groups
    memberData.put(TringWebApiServer.HANDLE_NAME, "different_"+otherHandleName);
     res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + groupName + "/tempmember");
    System.out.println(res.getBody());
     result = res.getBody().asString();
    int errorCode = (int)from(result).getMap("error").get("code");
    Assert.assertEquals(409, errorCode);
    Assert.assertEquals(HandleStatus.ACTIVE.ordinal(), from(result).getInt(TringWebApiServer.MEMBERSHIP_STATUS));
    Assert.assertEquals(otherHandleName.toLowerCase(), from(result).getString(TringWebApiServer.HANDLE_NAME));
    
    //this reject should succeed, since it is done by group Owner
    url= "/user/group/"+NEW_GID+"/member/"+otherHandleName+"/"+TringWebApiServer.REJECT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
  //After all this membership count for the group as found from solr should be 2
    try {
      Thread.sleep(20000); //sleep needed so that solr can soft-commit - which is after 15 secs as set now
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    url = "/search-groups-by-keyword/";
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(new JsonObject().put("keyword", groupName))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    List<Map<String,Object>>lst = from(result).get();
    int count = (int) lst.get(0).get("membercount");
    Assert.assertEquals(count, 1);
    
    //Now join the group created in setup, which requires approval
    memberData.put(TringWebApiServer.HANDLE_NAME, otherHandleName);
    req = getRequestWithHeader(json);
    res = req.contentType("application/json").body(memberData).when()
        .post("/user/group/" + GROUP_NAME + "/tempmember");
    System.out.println(res.getBody());
    result = res.getBody().asString();
     hid = from(result).getString("hid");
    Assert.assertEquals(true, from(result).getBoolean("success"));
    Assert.assertNotNull(hid);
    
    //accept membership works
    url = "/user/group/"+GID+"/member/"+otherHandleName+"/"+TringWebApiServer.ACCEPT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertNotNull(from(result).getJsonObject("success"));
    
    //this reject should fail since it was accepted earlier and this is group requires approval
    url= "/user/group/"+GID+"/member/"+otherHandleName+"/"+TringWebApiServer.REJECT;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertNotNull(from(result).getJsonObject("error")); 
  }
}
