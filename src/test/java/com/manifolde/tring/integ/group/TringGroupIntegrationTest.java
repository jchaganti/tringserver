package com.manifolde.tring.integ.group;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.commonjs.module.provider.UrlConnectionExpiryCalculator;

import com.google.common.base.Strings;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.integ.TringBaseIntegrationTest;
import com.manifolde.tring.server.dao.GroupDAO;
import com.manifolde.tring.server.handlers.TringGroupHandler;
import com.manifolde.tring.server.verticles.TringWebApiServer;

import io.vertx.core.json.JsonObject;
import rx.Observable;

import static com.jayway.restassured.path.json.JsonPath.from;

public class TringGroupIntegrationTest extends TringBaseIntegrationTest {
  private static final String GROUP1 = "iNdian.mUsic";
  private static final String OWNER_HANDLE1 = "Baiju_Bawra";
  private static final String MEMBER_HANDLE = "taNseN";

  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void testGroupCreate() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    
    //is Available should give true
    String url = "/group/" + GROUP1 + now + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader();
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Create group
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("box","chocolates")));
    groupData.put(GroupDAO.COUNTRY, "India");
    groupData.put(GroupDAO.STATE,"Maharashtra");
    groupData.put(GroupDAO.CITY,"Mumbai");
    
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
     res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //is Available should give false
    url = "/group/" + GROUP1 + now + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader();
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
  }
  
  @Test
  public void testCreateGroups() {
    try
    {
      
        FileInputStream file = new FileInputStream(new File(ClassLoader.getSystemResource("Group_categories.xlsx").toURI()));
        //Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        boolean ignoreRow1 = false;
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            //For each row, iterate through all the columns
            if(!ignoreRow1) {
              ignoreRow1 = true;
              continue;
            }
            String name = null, desc = null, rules = null, country = null, state = null, city = null, did = null, handle = null;
            List<String> categories = null;
            
            for (int i = 0; i < 10; i++) {
              Cell cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
              switch (i) 
              {
                  case 0:
                      name = cell.getStringCellValue();
                      break;
                  case 1:
                      String _categories = cell.getStringCellValue();
                      if(!Strings.isNullOrEmpty(_categories)) {
                        categories = Arrays.asList(_categories.split(","));
                      }
                      break;
                  case 2:
                      desc = cell.getStringCellValue();
                      break;
                  case 3:
                      rules = cell.getStringCellValue();
                      break;
                  case 4:
                      country = cell.getStringCellValue();
                      break;
                  case 5:
                      state = cell.getStringCellValue();
                      break;
                  case 6:
                      city = cell.getStringCellValue();
                      break;
                  case 7:
                      break;
                  case 8:
                      did = cell.getStringCellValue();
                      System.out.println("Did parsed: " + did);
                      break;
                  case 9:
                      handle = cell.getStringCellValue();
                      break;
                  default:
                      break;
              } 
            }
            createGroup( name, categories, country, state, city,  rules,  desc,  handle,  did.trim());            
        }
        file.close();
    } 
    catch (Exception e) 
    {
        e.printStackTrace();
    }
  }
  
  private void createGroup(String name, List<String> categories, String country, String state, String city,  String rules, String desc, String ownerHandleName, String did) {
    RequestSpecification req = getRequestWithHeader(did);
    Map<String, Object> json = getGroupJson(name, categories, country, state, city, rules, desc, ownerHandleName);
    
    System.out.println(json);
    System.out.println("******************");
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    String result = res.getBody().asString();
    //Assert.assertEquals(true, from(result).getBoolean("success"));
    System.out.println("Result: " + from(result).getBoolean("success"));
  }
  
  private Map<String, Object> getGroupJson(String name, List<String> categories, String country, String state, String city,  String rules, String desc, String ownerHandleName) {
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
  //Create group
    groupData.put(GroupDAO.GROUP_NAME, name);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    if(categories != null) {
      groupData.put(GroupDAO.CATEGORIES, categories);
    }
    
    if(!Strings.isNullOrEmpty(country)) {
      groupData.put(GroupDAO.GROUP_COUNTRY, country);
    }
   
    if(!Strings.isNullOrEmpty(state)) {
      groupData.put(GroupDAO.GROUP_STATE, state);
    }
    
    if(!Strings.isNullOrEmpty(city)) {
      groupData.put(GroupDAO.GROUP_CITY, city);
    }
    
    if(!Strings.isNullOrEmpty(desc)) {
      groupData.put(GroupDAO.GROUP_DESCRIPTION, desc);
    }
    
    if(!Strings.isNullOrEmpty(rules)) {
      groupData.put(GroupDAO.GROUP_RULES, rules);
    }
    
    groupData.put("autoLoaded", true);
    
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, ownerHandleName);
    return json;
  }
  

  @SuppressWarnings("unchecked")
  @Test
  public void testGetGroups() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    String groupName1=GROUP1 + now;
    String ownerName1 = OWNER_HANDLE1 + now;
    groupData.put(GroupDAO.GROUP_NAME, groupName1);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, ownerName1);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    req = getRequestWithHeader();
    json.clear();
    groupData.clear();
    now = DateTime.now();
    String groupName2=GROUP1 + now;
    String ownerName2 = OWNER_HANDLE1 + now;
    groupData.put(GroupDAO.GROUP_NAME, groupName2);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, ownerName2);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    req = getRequestWithHeader();
    json.clear();
    groupData.clear();
    now = DateTime.now();
    res = req.contentType("application/json").when().get("/user/groups");
    String result = res.getBody().asString();
    List listGroups = (List) from(result).get();
    System.out.println("Size of the groups membership list is " + listGroups.size());
    System.out.println(res.getBody());
    JsonObject foundGroup1Handle1=new JsonObject();
    JsonObject foundGroup2Handle2=new JsonObject();

    Observable.from(listGroups).map(grpInfo->{
      return (new JsonObject().put("name", (String)((Map)grpInfo).get("name"))
          .put("handleName", (String)((Map)grpInfo).get("handleName")));
    }).subscribe(grpInf->{
      JsonObject grpInfo = (JsonObject)grpInf;
      if (grpInfo.getString("name").equals(groupName1) &&
          grpInfo.getString("handleName").equals(ownerName1)) {
         foundGroup1Handle1.put("value",Boolean.TRUE);
      }else if (grpInfo.getString("name").equals(groupName2) &&
          grpInfo.getString("handleName").equals(ownerName2)) {
        foundGroup2Handle2.put("value",Boolean.TRUE);
      }
    }, err->
    Assert.fail(),
    ()->{
      Assert.assertEquals(Boolean.TRUE,foundGroup1Handle1.getBoolean("value"));
      Assert.assertEquals(Boolean.TRUE,foundGroup2Handle2.getBoolean("value"));
    });
  }

  @Test
  public void testCheckForDuplicateGroupWithSameHandle() {
    // Use this for group creation twice.
    DateTime now = DateTime.now();

    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    req = getRequestWithHeader();
    json = new HashMap<>();
    groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
  }
  
  // Functional test
  @Test
  public void testCheckForTwoGroupWithSameHandle() {
	  // Use this for group creation twice.
	DateTime now = DateTime.now();
	  
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true,from(result).getBoolean("success"));
    
    req = getRequestWithHeader();
    json = new HashMap<>();
    groupData = new HashMap<>();
    DateTime now1 = DateTime.now();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now1);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true,from(result).getBoolean("success"));
  }
  
  @Test
  public void testCheckForDuplicateGroupWithDifferntHandle() {
    // Use this for group creation twice.
    DateTime now = DateTime.now();

    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    req = getRequestWithHeader();
    json = new HashMap<>();
    groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);


    // This will give another unique handle name.
    now = DateTime.now();
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(false, from(result).getBoolean("success"));
  }

  /*
   * Prashant : I think this test should pass, If it is not possible then proper message should be
   * given to the second user while creating group. The probability of this occurring is very less
   */
  @Test
  public void testCheckForGroupWithSameNameForTwoDifferentUsers() {
    // Use this for group creation twice.
    DateTime now = DateTime.now();

    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    Map<String, Object> jsonAnotherUsr = new HashMap<>();
    jsonAnotherUsr.put(MOBILE_NO, _9822245688);

    req = getRequestWithHeader(jsonAnotherUsr);
    json = new HashMap<>();
    groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);

    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }

  /* Prashant: My feeling is that this test must be supported in tring" */
  @Test
  public void testCheckForGroupWithSameNameForTwoDifferentUsersDifferentHandle() {
    // Use this for group creation twice.
    DateTime now = DateTime.now();

    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    Map<String, Object> jsonAnotherUsr = new HashMap<>();
    jsonAnotherUsr.put(MOBILE_NO, _9822245688);

    req = getRequestWithHeader(jsonAnotherUsr);
    json = new HashMap<>();
    groupData = new HashMap<>();
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);


    // This will give another unique handle name.
    now = DateTime.now();
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
  }
  
  @Test
  public void testGetGroupInfo() {
    DateTime now = DateTime.now();

    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    Object groupName=GROUP1.replace(" ", "") + now.getMillis();
    groupData.put(GroupDAO.GROUP_NAME, groupName);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, true);
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    Response res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    res = getRequestWithHeader().contentType("application/json").when().get("/group/"+groupName+"/info");
    result = res.getBody().asString();
    System.out.println(result);
    Assert.assertNotNull(from(result).getString("_id"));

    res = getRequestWithHeader().contentType("application/json").when().get("/user/group/"+groupName+"/info");
    result = res.getBody().asString();
    System.out.println(result);
    Assert.assertNotNull(from(result).getString("_id"));
  }
  @Test
  public void testGroupSearch() throws UnsupportedEncodingException {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    
    //is Available should give true
    String url = "/group/" + GROUP1 + now + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader();
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Create group
    groupData.put(GroupDAO.GROUP_NAME, GROUP1 + now);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("box","chocolates")));
    groupData.put(GroupDAO.COUNTRY, "India");
    groupData.put(GroupDAO.STATE,"Maharashtra");
    groupData.put(GroupDAO.CITY,"Mumbai");
    
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
     res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
       
    //Create another group
    String secondGroupName=GROUP1 + DateTime.now();
    groupData.put(GroupDAO.GROUP_NAME, secondGroupName);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("very dark","chocolates")));
    groupData.put(GroupDAO.STATE,"Karnataka");
    groupData.put(GroupDAO.CITY,"Chitrapur, Shirali");
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    groupData.put(GroupDAO.GROUP_DESCRIPTION, "This group is a very important group");
    groupData.put(GroupDAO.GROUP_RULES, "Rules of the game are ...");
    
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
 
    //Now search for the group in solr (local solr server should be running on port 8983)
    url = "/get-categories/0/10";
    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    
    url = "/search-groups-by-keyword/";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("keyword", "Chitrapur, Shirali"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    //Assert.assertEquals(false, from(result).getBoolean("success"));
    
    url = "/paged-search-groups-by-keyword/*/5";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject()
        .put("keyword", "Chitrapur, Shirali")
        .put("rows", "5"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url="/browse-groups-by-location";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("country","India").put("city", "Chitrapur, Shirali"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url="/browse-groups-by-category";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("category","dark"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url="/filter-groups-by-category";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("keyword","chocolates").put("category","dark"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();   
    
    url="/browse-groups-by-location";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("country","India").put("state", "Maharashtra"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url="/filter-groups-by-location";
    req = getRequestWithHeader();
    res = req.contentType("application/json").body(new JsonObject().put("keyword","chocolates").put("country","India").put("state", "Maharashtra"))
        .when().post(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url="/browse-groups-random/10";
    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    JsonPath obj = from(result);
    String newOffset = obj.getString("offset");
    
    url="/browse-groups-next-circular/"+newOffset+"/10";
    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
    url = "/get-featured-groups";
    req = getRequestWithHeader();
    res = req.contentType("application/json")
        .when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    
  }
  

  @Test
  public void testGroupUpdate(){
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object> json = new HashMap<>();
    Map<String, Object> groupData = new HashMap<>();
    DateTime now = DateTime.now();
    String GROUP_NAME = GROUP1 + now;
    GROUP_NAME=GROUP_NAME.replace(":", "").replace("+", "").replace("-","");

    //is Available should give true
    String url = "/group/" + GROUP_NAME + "/" + TringWebApiServer.IS_AVAILABLE;
    req = getRequestWithHeader();
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //Create group
    groupData.put(GroupDAO.GROUP_NAME, GROUP_NAME);
    groupData.put(GroupDAO.GROUP_O2M, false);
    groupData.put(GroupDAO.CLOSED_GROUP, false);
    groupData.put(GroupDAO.CATEGORIES, new ArrayList<String>(Arrays.asList("box","chocolates")));
    groupData.put(GroupDAO.COUNTRY, "India");
    groupData.put(GroupDAO.STATE,"Maharashtra");
    groupData.put(GroupDAO.CITY,"Mumbai");
    groupData.put(GroupDAO.GROUP_DESCRIPTION,"Some Description");
    groupData.put(GroupDAO.GROUP_RULES,"Some Rules");
    
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    json.put(TringGroupHandler.OWNER_HANDLE_NAME, OWNER_HANDLE1 + now);
    res = req.contentType("application/json").body(json).when().post("/user/group");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    //update group
    req = getRequestWithHeader();
    groupData.put(GroupDAO.ID, from(result).getString("gid"));
    groupData.put(GroupDAO.GROUP_DESCRIPTION,"Other Description");
    groupData.put(GroupDAO.GROUP_RULES,"Other Rules");
    groupData.put(GroupDAO.THUMBNAIL_DATA,"thumbnaildata");
    groupData.remove(GroupDAO.CATEGORIES);
    groupData.put(TringWebApiServer.CONTENT_ID,"1234");
    groupData.remove(GroupDAO.GROUP_O2M);
    groupData.remove(GroupDAO.CLOSED_GROUP);
    
    json.put(TringGroupHandler.GROUP_DATA, groupData);
    res = req.contentType("application/json").body(json).when().post("/user/group-update");
    System.out.println(res.getBody());

    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));
    
    
  }
  @Test
  public void testCustomerFeedback() throws UnsupportedEncodingException {
  //  /contact-tring/:feedback
    RequestSpecification req = getRequestWithHeader();
 
    //Send Client Feedback
    String url = "/user/contact-tring/" + URLEncoder.encode("This is my frank feedback. I love this app","UTF-8");
    Response res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    String result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

    url = "/user/contact-tring/" + URLEncoder.encode("This is other feedback.....I love this app even more","UTF-8");
    res = req.contentType("application/json").when().get(url);
    System.out.println(res.getBody());
    result = res.getBody().asString();
    Assert.assertEquals(true, from(result).getBoolean("success"));

  }
}
