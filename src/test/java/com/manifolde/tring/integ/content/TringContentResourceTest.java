package com.manifolde.tring.integ.content;

import static com.jayway.restassured.path.json.JsonPath.from;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Strings;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.integ.TringBaseIntegrationTest;
import org.apache.commons.io.IOUtils;
import static org.hamcrest.Matchers.equalTo;
import  com.google.common.hash.*;
import com.google.common.io.Files;


public class TringContentResourceTest extends TringBaseIntegrationTest {

  @Before
  public void setUp() {
      super.setUp();
  }
  
  @Test
  public void testContent() {
	String fileToBeTested = "IMG_25082015_182622.png";
    RequestSpecification req = getRequestWithHeader();

    Response res;
    try {
      res = req.
          multiPart(new File(ClassLoader.getSystemResource(fileToBeTested).toURI())).
      when().
          post("/content");
      assertTrue(res.statusCode() == 200);
      String contentId = from(res.getBody().asString()).getString("contentId");
      assertFalse(Strings.isNullOrEmpty(contentId));
      String contentType = from(res.getBody().asString()).getString("contentType");
      System.out.println("The content type: " + contentType);
      assertFalse(Strings.isNullOrEmpty(contentType));
      //Download
      req = getRequestWithHeader();
      res = req.contentType("*/*").when()
          .get("/content/" + contentId);
      assertTrue(res.statusCode() == 200);
      byte[] fileContents = res.asByteArray();
      assertNotNull(fileContents);
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testDeleteContent() {
    String fileToBeTested = "IMG_25082015_182622.png";
    RequestSpecification req = getRequestWithHeader();

    Response res;
    try {
      res = req.
          multiPart(new File(ClassLoader.getSystemResource(fileToBeTested).toURI())).
      when().
          post("/content");
      assertTrue(res.statusCode() == 200);
      String contentId = from(res.getBody().asString()).getString("contentId");
      assertFalse(Strings.isNullOrEmpty(contentId));
      req = getRequestWithHeader();
      res = req.contentType("*/*").when()
          .delete("/content/id/" + contentId);
      boolean success = from(res.getBody().asString()).getBoolean("success");
      assertTrue(success);
      
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }
  //@Test
  public void testContentMatch() throws Exception{
	String fileToBeTested = "IMG_25082015_182622.png";
	int expectedSize = IOUtils.toByteArray(ClassLoader.getSystemResource(fileToBeTested).openStream()).length;
    RequestSpecification req = getRequestWithHeader();

    Response res;
    try {
      res = req.
          multiPart(new File(ClassLoader.getSystemResource(fileToBeTested).toURI())).
      when().
          post("/content");
      assertTrue(res.statusCode() == 200);
      String contentId = from(res.getBody().asString()).getString("contentId");
      assertFalse(Strings.isNullOrEmpty(contentId));
      
      req = getRequestWithHeader();
      final java.io.InputStream inputStream =  req.contentType("*/*").when().
    		  get("/content/"+contentId).
    		  asInputStream();
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      IOUtils.copy(inputStream, byteArrayOutputStream);
      IOUtils.closeQuietly(byteArrayOutputStream);
      IOUtils.closeQuietly(inputStream);

      assertThat(byteArrayOutputStream.size(), equalTo(expectedSize));
      
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }
  
  @Test
  public void testTwoFileUploadAndDownload() throws Exception
  {
	  String fileToBeTested1 = "readme.txt";
	  String fileToBeTested2 = "Testdata.txt";
	  int expectedSizeFile1 = IOUtils.toByteArray(ClassLoader.getSystemResource(fileToBeTested1).openStream()).length;
	  //int expectedSizeFile2 = IOUtils.toByteArray(ClassLoader.getSystemResource(fileToBeTested2).openStream()).length;
		  
	  // Uploading First File
	  FileData fileData1 = UploadFile(fileToBeTested1);	  
	  assertFalse(Strings.isNullOrEmpty(fileData1.contentId));
	  
	  // Uploading Second File
	  //FileData fileData2 = UploadFile(fileToBeTested2);	 
	  //assertFalse(Strings.isNullOrEmpty(fileData2.contentId));
	  
	  // Downloading and Checking First File	
	  assertThat(DownloadFile(fileData1.contentId), equalTo(expectedSizeFile1));
	  
	  // Downloading and Checking Second File
	  //assertThat(DownloadFile(fileData2.contentId), equalTo(expectedSizeFile2));
  }
  
  class FileData
  {
	  public int fileSize;
	  public String contentId;
	  FileData()
	  {
		  
	  }
  }
  
  private FileData UploadFile(String fileName)
  {
	  try 
	  {
		  Response res;
		  int fileSize;
		
		  fileSize = IOUtils.toByteArray(ClassLoader.getSystemResource(fileName).openStream()).length;	
		  RequestSpecification req = getRequestWithHeader();
		 
		  res = req.
			multiPart(new File(ClassLoader.getSystemResource(fileName).toURI())).
			when().
			post("/content");	
		  assertTrue(res.statusCode() == 200);
		  String contentId = from(res.getBody().asString()).getString("contentId");
		  assertFalse(Strings.isNullOrEmpty(contentId));
		  FileData fData = new FileData();
		  fData.fileSize = fileSize;
		  fData.contentId = contentId;
		  return fData;
	  }
	  catch (IOException e) 
	  {		
		  e.printStackTrace();
		  return null;
	  }
	  catch (URISyntaxException e) 
	  {		
		  e.printStackTrace();
		  return null;
	  }  
	  
  }
  
  private int DownloadFile(String contentID)
  {
	  RequestSpecification req = getRequestWithHeader();
	  Response res = req.contentType("*/*").when().
          get("/content/"+contentID);
	  String contentDisposition = res.getHeader("Content-Disposition");
	  System.out.println("contentDisposition: " + contentDisposition);
      final java.io.InputStream inputStream =  res.asInputStream();
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      try 
      {
		IOUtils.copy(inputStream, byteArrayOutputStream);
      } 
      catch (IOException e) 
      {		
    	  e.printStackTrace();
    	  return -1;
      }
      IOUtils.closeQuietly(byteArrayOutputStream);
      IOUtils.closeQuietly(inputStream);

      return byteArrayOutputStream.size();
  }
}
