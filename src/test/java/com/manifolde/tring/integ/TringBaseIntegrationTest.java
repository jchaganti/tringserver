package com.manifolde.tring.integ;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.server.utils.TringConstants;

public class TringBaseIntegrationTest {

  protected static final String _9890128848 = "+919890128848";
  protected static final String _9822245688 = "+919822245688";
  protected static final String _9850820341 = "+919850820341";
  protected static final String MOBILE_NO = "mobileNo";
  
  protected void setUp() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = 9000;
    RestAssured.basePath = "/tring/api";
  }
  
  protected RequestSpecification getRequestWithHeader(Map<String, Object> json) {
    Response res = given().contentType( "application/json" )
               .body( json )
               .when().post( "/device" );
    checkDevice(res);
    
    RequestSpecification req = given().header("Authorization", from(res.getBody().asString()).getString("did"));
    return req;
  }
  
  protected RequestSpecification getRequestWithHeader(String did) {
    System.out.println("Did: " + did);
    return given().header("Authorization", did);
  }
  
  protected RequestSpecification tryToGetRequestWithHeaderFromFakedid(Map<String, Object> json) {
        Response res = given().contentType( "application/json" )
            .body( json )
            .when().post( "/device" );
        checkDevice(res);
	    RequestSpecification req = given().header("Authorization", "q8EMcOkkQdutuqRG3yjHSQ");
	    return req;
  }

  protected RequestSpecification getRequestWithHeader() {
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, _9890128848);
    return getRequestWithHeader(json);
  }
  
  private void checkDevice(Response res) {
    assertTrue(res.statusCode() == 200);
    String id = from(res.getBody().asString()).getString("did");
    
    assertTrue(id != null && id.length() > 0);
    int pin = from(res.getBody().asString()).getInt("pin");
    assertTrue(pin == TringConstants.DEV_MODE_PIN);    
  }
  
  protected void testDeviceVerification(String mobileNo, int pin, Checker checker) {
    Map<String, Object>  json = new HashMap<>();
    json.put(MOBILE_NO, mobileNo);
    RequestSpecification req = getRequestWithHeader(json);
    json = new HashMap<>();
    json.put("pin", pin);
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/device/verify" );
    assertTrue(res.statusCode() == 200);
    boolean success = from(res.getBody().asString()).getBoolean("success");
    checker.check(success);
  }

  @FunctionalInterface
  protected static  interface Checker{
    void check(boolean res);
  }
}
