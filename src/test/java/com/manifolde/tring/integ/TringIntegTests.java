package com.manifolde.tring.integ;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.manifolde.tring.integ.content.TringContentResourceTest;
import com.manifolde.tring.integ.group.TringGroupIntegrationTest;
import com.manifolde.tring.integ.handle.TringHandleHandlerTest;
import com.manifolde.tring.integ.user.TringUserResourceTest;

@RunWith(Suite.class)
@SuiteClasses( {
    TringUserResourceTest.class,
    TringContentResourceTest.class,
    TringGroupIntegrationTest.class,
    TringHandleHandlerTest.class
})
public class TringIntegTests {

}
