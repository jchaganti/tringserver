package com.manifolde.tring.integ.user;

import static com.google.common.collect.Sets.difference;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Strings;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.manifolde.tring.integ.TringBaseIntegrationTest;
import com.manifolde.tring.server.dao.UserDAO;

public class TringUserResourceTest extends TringBaseIntegrationTest {

  
  private static Set<String> inMobileNos = new HashSet<String>() {{
    add(_9822245688);
    add(_9850820341);
  }};
  
  @Before
  public void setUp() {
      super.setUp();
  }
  
  @Test
  public void testDeviceVerificationSuccess() {
    testDeviceVerification(_9822245688, 1729, (res) -> {
      assertTrue(res);
    });
  }
  
  @Test
  public void testDeviceVerificationFail() {
    testDeviceVerification(_9822245688, 1730, (res) -> {
      assertFalse(res);
    });
  }
  
  @Test
  public void testContacts() {
    testDeviceVerification(_9822245688, 1729, (res) -> {
      assertTrue(res);
    });
    testDeviceVerification(_9850820341, 1729, (res) -> {
      assertTrue(res);
    });
    
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object>  json = new HashMap<>();
    String [] mobileNos = {_9822245688, "dummyMobileNo", _9850820341};
    json.put("mobileNos", Arrays.asList(mobileNos));
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/contacts" );
    assertTrue(res.statusCode() == 200);
    List<Object> objects= from(res.getBody().asString()).getList("usersInfo");
    assertTrue(objects.size() == 2);
    Set<String> outMobileNos = new HashSet<>();
    objects.forEach(obj -> {
      Map<String, String> userInfo = (Map<String, String>)obj;
      outMobileNos.add(userInfo.get("mobileNo"));
    });
      
    Set<String> diff = difference(outMobileNos, inMobileNos);
    assertTrue(diff.size() == 0);
  }

  @Test
  public void testProfileInfos() {
    testDeviceVerification(_9822245688, 1729, (res) -> {
      assertTrue(res);
    });
    testDeviceVerification(_9850820341, 1729, (res) -> {
      assertTrue(res);
    });
    
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object>  json = new HashMap<>();
    String [] mobileNos = {_9822245688, "dummyMobileNo", _9850820341};
    json.put("mobileNos", Arrays.asList(mobileNos));
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/profiles" );
    assertTrue(res.statusCode() == 200);
    List<Object> objects= from(res.getBody().asString()).getList("profilesInfo");
    assertTrue(objects.size() == 2);
    Set<String> outMobileNos = new HashSet<>();
    objects.forEach(obj -> {
      Map<String, String> userInfo = (Map<String, String>)obj;
      outMobileNos.add(userInfo.get("mobileNo"));
    });
      
    Set<String> diff = difference(outMobileNos, inMobileNos);
    assertTrue(diff.size() == 0);
  }
  
  @Test
  public void testProfileUpdate() {
    testContacts();
    RequestSpecification req;
    Map<String, Object> jsonAnotherUsr = new HashMap<>();
    jsonAnotherUsr.put(MOBILE_NO, _9822245688);
    req = getRequestWithHeader(jsonAnotherUsr);
    Map<String, Object>  json = new HashMap<>();
    String [] mobileNos = {_9890128848, "dummyMobileNo", _9850820341};
    json.put("mobileNos", Arrays.asList(mobileNos));
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/contacts" );
    assertTrue(res.statusCode() == 200);
    
    req = getRequestWithHeader();
    String contentId =  "abcDEF123ghi";
    assertFalse(Strings.isNullOrEmpty(contentId));
    String thumbnailData = "R0lGODlhOQA4AIAAAAAAAP///ywAAAAAOQA4AAAC/owNiQnnDx1rU8WoqL0c1tod2RSW0teUI2W2S8DAmyyCqaui8buAOK7b0T40062Fgp16u9Tx8czJWCSL5hqVABHM5LTKgmaX4tjNJ1Yqxxf0F+vmjrgub/c9qzXpuePTR6Rno5Z2EvXntHJIRFVI4gEJJoQHB6UWxIH1JkIZSJf0SDjVJRSkc2m6x0lq1pnKetamCGjaSWqYl6oJeirq6asraNPLlvkJhjv72zF2Zqdrp8JHSAxtFis7WYZWC+jxmt3KOoS83btKPj0nF910/nKtjh4WqF0uHJbNmXjqXFjzBUM+J/i0rbGkZdwnPQWtyCkyjRojbKviHAslSuIgqXQX88DyQ4YSQo6P4CwD2XAen0HBSPp5ti+ZsjgvU1ZcaW1dxVbEdjaEKYuhJmo8XHn0OM+KN4kpZ2BcRG8js13vVvrcZOQWRkv2nBmMyC/eVHvJvl25dGfs0WsGn/LYuk0g21wAR2V0WjXd0G91EeXE5jfLhsEyoUVEhZRw0r+JIIY1G4JxlR62DGWSvAytG6Q/MbucxPkj5sZpDxeWjGof3MuoQY2CUwAAOw==";
    String name = "Paper Boy";
    String customMessage = "Hi there, paper boy here!!";
    json = new HashMap<>();
    json.put(UserDAO.CONTENT_ID, contentId);
    json.put(UserDAO.CUSTOM_MESSAGE, customMessage);
    json.put(UserDAO.THUMBNAIL_DATA, thumbnailData);
    json.put(UserDAO.NAME, name);
    Response res1 = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/profile" );
    boolean success = from(res1.getBody().asString()).getBoolean("success");
    assertTrue(success);
  }
  
  
  @Test
  public void testLastSeen() {
    RequestSpecification req = getRequestWithHeader();
    Map<String, Object>  json = new HashMap<>();
    json.put("mobileNo", _9822245688);
    Response res = req.contentType( "application/json" )
        .body( json )
        .when().post( "/user/lastseen" );
    assertTrue(res.statusCode() == 200);
    Object _userJson = from(res.getBody().asString()).get("user.lastSeen");
    Map<String, String>  userJson = (Map<String, String>)_userJson;
    String lastSeen = userJson.get("$date");
    assertTrue(lastSeen != null);
    DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
    DateTime dt = fmt.parseDateTime(lastSeen); 
    assertTrue(dt != null);
  }
  
  @Test
  public void testDeviceVerificationForWrongPinData() {
	    Map<String, Object>  json = new HashMap<>();
	    Map<String, Object>  jsonForWrongNumber = new HashMap<>();
	    json.put(MOBILE_NO, _9822245688);
	    RequestSpecification req = getRequestWithHeader(json);
	    jsonForWrongNumber.put("somejunkdata", "againjunkdata");	   
	    jsonForWrongNumber.put("pin", 1729);
	    jsonForWrongNumber.put(MOBILE_NO, _9850820341);	  
	    Response res = req.contentType( "application/json" )
	        .body( jsonForWrongNumber )
	        .when().post( "/device/verify" );
	    assertTrue(res.statusCode() == 401); 	   
	  }
  
  @Test
  public void testDeviceVerificationForFakeDid()
  {	  
	  Map<String, Object>  json = new HashMap<>();
	    Map<String, Object>  jsonForWrongNumber = new HashMap<>();
	    json.put(MOBILE_NO, _9822245688);
	    RequestSpecification req = tryToGetRequestWithHeaderFromFakedid(json);	    	   
	    jsonForWrongNumber.put("pin", 1729);	    	  
	    Response res = req.contentType( "application/json" )
	        .body( jsonForWrongNumber )
	        .when().post( "/device/verify" );
	    assertTrue(res.statusCode() == 401);	    	
  }
  
  /* Too small number */
  @Test
  public void testDeviceVerificationForJunkPhoneNumber1() {
	    Map<String, Object>  json = new HashMap<>();
	    Map<String, Object>  jsonForWrongNumber = new HashMap<>();
	    json.put(MOBILE_NO, "123");
	    RequestSpecification req = getRequestWithHeader(json);	    	   
	    jsonForWrongNumber.put("pin", 1729);	    	  
	    Response res = req.contentType( "application/json" )
	        .body( jsonForWrongNumber )
	        .when().post( "/device/verify" );
	    assertTrue(res.statusCode() == 401); 
	    /*
	    boolean success = from(res.getBody().asString()).getBoolean("success");
	    assertFalse(success);
	    */
	  }
  /* It should accept only number */
  @Test
  public void testDeviceVerificationForJunkPhoneNumber2() {
	    Map<String, Object>  json = new HashMap<>();
	    Map<String, Object>  jsonForWrongNumber = new HashMap<>();
	    json.put(MOBILE_NO, "!@#$%^&&*&*");
	    RequestSpecification req = getRequestWithHeader(json);	    	   
	    jsonForWrongNumber.put("pin", 1729);	    	  
	    Response res = req.contentType( "application/json" )
	        .body( jsonForWrongNumber )
	        .when().post( "/device/verify" );
	    assertTrue(res.statusCode() == 401); 	     	   
	  }
  
  /* Prashant: I need to confirm this, How are we going to store country code information */
  @Test
  public void testDeviceVerificationForJunkPhoneNumber3() {
	    Map<String, Object>  json = new HashMap<>();
	    Map<String, Object>  jsonForWrongNumber = new HashMap<>();
	    json.put(MOBILE_NO, "919850820341");
	    RequestSpecification req = getRequestWithHeader(json);	    	   
	    jsonForWrongNumber.put("pin", 1729);	    	  
	    Response res = req.contentType( "application/json" )
	        .body( jsonForWrongNumber )
	        .when().post( "/device/verify" );
	    assertTrue(res.statusCode() == 401); 
	    /*
	    boolean success = from(res.getBody().asString()).getBoolean("success");
	    assertFalse(success);
	    */
	  }
  
  
}
